use canonical_lr::CanonicalLR;
use codespan_reporting::{
    diagnostic::{Diagnostic, Label},
    term::{
        self,
        termcolor::{ColorChoice, StandardStream},
    },
};
use high_level::{ExpressionKind, Files as _, FilesManager, GrammarFiles, GrammarMap, TokenType};
use lexer::Lexer;
use lr_parser::{
    compute::{ComputeLR1States as _, TransitionTable},
    parser::TokenKind,
    syntax_tree::SyntaxElement,
    text_ref::{TextRc, TextReference as _},
    LR1Parser,
};
use std::{path::PathBuf, time::Instant};

pub fn main(grammar_path: PathBuf, file_path: PathBuf) {
    let start_time = Instant::now();
    let mut timer = Instant::now();

    let mut grammar_files = GrammarFiles::new();
    let mut files_manager = FilesManager::new();
    let root_file_id = files_manager.add_root(file_path).unwrap();
    let root_file = TextRc::new_full(files_manager.source(root_file_id).unwrap());

    eprintln!("Reading grammar file...");
    let grammar_map =
        match GrammarMap::from_root_path(&mut grammar_files, grammar_path, CanonicalLR::new()) {
            Ok((map, warnings)) => {
                for warning in warnings {
                    warning
                        .display(&grammar_files)
                        .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                        .unwrap();
                }
                map
            }
            Err(err) => {
                err.display(&grammar_files)
                    .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                    .unwrap();
                panic!()
            }
        };

    eprintln!("{} s", (Instant::now() - timer).as_secs_f32());
    timer = Instant::now();

    eprintln!("Building tokenizer...");
    let tokenizer = make_tokenizer(&grammar_files, root_file.as_ref(), &grammar_map).map(
        |(token_opt, span)| {
            (
                match token_opt {
                    Some(token) => {
                        if grammar_map.get_token(token).unwrap().skip() {
                            TokenKind::Skip(token)
                        } else {
                            TokenKind::Normal(token)
                        }
                    }
                    None => TokenKind::Error,
                },
                span,
            )
        },
    );

    eprintln!("{} s", (Instant::now() - timer).as_secs_f32());
    timer = Instant::now();

    eprintln!("Building parser tables...");
    let starting_states = grammar_map.nodes().filter_map(|node| {
        if node.is_silent() {
            None
        } else {
            Some(node.id())
        }
    });
    let tables = match CanonicalLR::new()
        .compute_states_with_starts(starting_states, grammar_map.grammar())
    {
        Ok(tables) => tables,
        Err(error) => {
            let mut message = String::new();
            crate::helpers::conflicts::ComputeErrorDisplay::new(
                |buffer, t| {
                    let token = grammar_map.get_token(t).unwrap();
                    if let ExpressionKind::Raw(raw) = token.expression() {
                        buffer.push('\'');
                        buffer.push_str(raw.as_str());
                        buffer.push('\'');
                    } else {
                        buffer.push_str(token.name());
                    }
                },
                |buffer, n| {
                    let node = grammar_map.get_node(n).unwrap();
                    buffer.push_str(node.representation())
                },
                |buffer, rule_idx| {
                    let node = grammar_map.get_node(rule_idx.lhs).unwrap();
                    buffer.push_str(node.representation());
                    if let Some(node_variant) = node.variant(rule_idx.index) {
                        buffer.push_str("::");
                        buffer.push_str(node_variant.name.as_str());
                    }
                },
                grammar_map.grammar(),
            )
            .print_compute_error(&mut message, grammar_map.grammar(), error);

            panic!("{}", message)
        }
    };

    eprintln!("{} s", (Instant::now() - timer).as_secs_f32(),);
    timer = Instant::now();

    eprintln!("Built parser tables ({} states)", tables.states().len());

    let mut parser = LR1Parser::new(
        &tables,
        tables.starting_state(grammar_map.starting_node()).unwrap(),
        tokenizer,
        |node| {
            grammar_map
                .get_node(node)
                .map(|node| node.is_silent())
                .unwrap()
        },
        |token| grammar_map.get_token(token).unwrap().remapped(),
    );

    eprintln!("{} s", (Instant::now() - timer).as_secs_f32(),);
    timer = Instant::now();

    eprintln!("Built parser");

    let (syntax_tree, errors) = SyntaxElement::from_parser(
        root_file.clone(),
        &mut parser,
        grammar_map.grammar(),
        grammar_map.starting_node(),
    );

    eprintln!(
        "{} s, total = {}",
        (Instant::now() - timer).as_secs_f32(),
        (Instant::now() - start_time).as_secs_f32()
    );
    eprintln!("Built syntax tree");

    println!("===========================");
    println!("======= Syntax tree =======");
    println!("===========================");
    println!(
        "{}",
        syntax_tree
            .display(
                &|f, rule_idx| {
                    let node = rule_idx.lhs;
                    let index = rule_idx.index;
                    let node = grammar_map.get_node(node).unwrap();
                    f.write_str({
                        node.name()
                            .unwrap_or_else(|| node.location().element.get_text())
                    })?;
                    if let Some(variant) = node.variant(index) {
                        f.write_str("::")?;
                        f.write_str(&variant.name)
                    } else {
                        Ok(())
                    }
                },
                &|f, token| {
                    let token = grammar_map.get_token(*token).unwrap();
                    f.write_str(match token.expression() {
                        ExpressionKind::Raw(raw) => raw.as_str(),
                        _ => token.name(),
                    })
                }
            )
            .print_skip_tokens(false)
    );
    if !errors.is_empty() {
        use lr_parser::{grammar::Lookahead, syntax_tree::SyntaxError};
        for error in errors {
            let diagnostic = match error {
                SyntaxError::IncompleteNode {
                    node,
                    text_ref,
                    expected,
                    token: _,
                } => {
                    let mut expected_str = String::from("expected ");
                    for (idx, t) in expected.iter().enumerate() {
                        let t = match t {
                            Lookahead::Eof => "EOF",
                            Lookahead::Token(t) => grammar_map.get_token(*t).unwrap().name(),
                        };
                        if idx != 0 {
                            expected_str.push_str(", ");
                        }
                        expected_str.push_str(t)
                    }
                    Diagnostic::error()
                        .with_message(format!("Syntax error: incomplete node {}", {
                            let node = grammar_map.get_node(node).unwrap();
                            node.name()
                                .unwrap_or_else(|| node.location().element.get_text())
                        }))
                        .with_labels(vec![
                            Label::primary(root_file_id, text_ref.get_span()),
                            Label::secondary(root_file_id, text_ref.get_span())
                                .with_message(expected_str),
                        ])
                }
                SyntaxError::Unexpected { token, expected } => {
                    let file_content = files_manager.source(root_file_id).unwrap();
                    let (token, span) = match token {
                        Lookahead::Eof => ("EOF", file_content.len()..file_content.len()),
                        Lookahead::Token((t, text_ref)) => (
                            grammar_map.get_token(t).unwrap().name(),
                            text_ref.get_span(),
                        ),
                    };

                    let mut expected_str = String::from("expected ");
                    for (idx, t) in expected.iter().enumerate() {
                        let t = match t {
                            Lookahead::Eof => "EOF",
                            Lookahead::Token(t) => grammar_map.get_token(*t).unwrap().name(),
                        };
                        if idx != 0 {
                            expected_str.push_str(", ");
                        }
                        expected_str.push_str(t)
                    }
                    Diagnostic::error()
                        .with_message(format!("Syntax error: unexpected token {}", token))
                        .with_labels(vec![
                            Label::primary(root_file_id, span.clone()),
                            Label::secondary(root_file_id, span).with_message(expected_str),
                        ])
                }
            };
            let term_config = term::Config::default();
            let mut stderr = StandardStream::stderr(ColorChoice::Always);
            term::emit(&mut stderr, &term_config, &files_manager, &diagnostic).unwrap();
        }
        panic!();
    }
    println!("===========================");
    println!("====== Abstract tree ======");
    println!("===========================");

    println!("TODO");
}

fn make_tokenizer<'src>(
    grammar_files: &'src GrammarFiles,
    root_file: &'src str,
    grammar_map: &GrammarMap,
) -> Lexer<'src, TokenType> {
    use lexer::{Callback, LexerBuilder};

    fn raw_string_literal(
        lexer: &Lexer<TokenType>,
        token: TokenType,
        end: usize,
    ) -> Result<(TokenType, usize), usize> {
        let slice = lexer.slice();
        if !lexer.slice().starts_with('r') {
            return Ok((token, end));
        }
        let hash_len = end.saturating_sub(2);
        let pattern = String::from("\"") + &"#".repeat(hash_len);
        if let Some(new_end) = slice[end..].find(&pattern) {
            Ok((token, new_end + end + pattern.len()))
        } else {
            Err(slice.len())
        }
    }

    fn multiline_comment(
        lexer: &Lexer<TokenType>,
        token: TokenType,
        end: usize,
    ) -> Result<(TokenType, usize), usize> {
        let slice = lexer.slice();
        if slice.starts_with("//") {
            return Ok((token, end));
        }
        let mut level = 0;
        let mut chars_indices = slice.char_indices().peekable();
        while let Some((_, c)) = chars_indices.next() {
            match c {
                '/' => {
                    if let Some((_, '*')) = chars_indices.peek() {
                        chars_indices.next();
                        level += 1;
                    }
                }
                '*' => {
                    if let Some((pos, '/')) = chars_indices.peek().copied() {
                        chars_indices.next();
                        level -= 1;
                        if level == 0 {
                            return Ok((token, pos + 1));
                        }
                    }
                }
                _ => {}
            }
        }
        Err(slice.len())
    }

    let char_or_lifetime = {
        grammar_map
            .token_by_name("LIFETIME")
            .and_then(|lifetime_token| {
                let lifetime_token = lifetime_token.id();
                grammar_map.token_by_name("CHAR").map(move |char_token| {
                    (lifetime_token, char_token.id())
                })
            }).map(|(lifetime_token, char_token)| move |lexer: &Lexer<TokenType>, _: TokenType, end: usize| -> Result<(TokenType, usize), usize> {
                let slice = &lexer.slice()[end..];
                let mut chars_indices = slice.char_indices().peekable();
                let mut is_lifetime = if lexer.slice().starts_with('b') {
                    false
                } else {
                    matches!(chars_indices.peek(), Some((_, c)) if c.is_alphabetic() || *c == '_')
                };
                let mut backslash = false;
                for (pos, c) in chars_indices {
                    match c {
                        '\\' => {
                            backslash = !backslash;
                            is_lifetime = false
                        }
                        '\'' => {
                            if !backslash {
                                return Ok((char_token, end + pos + 1));
                            } else {
                                backslash = false
                            }
                        }
                        c if !c.is_alphanumeric() && c != '_' => {
                            if is_lifetime {
                                return Ok((lifetime_token, end + pos));
                            }
                            backslash = false
                        }
                        _ => backslash = false,
                    }
                }
                Err(slice.len())
            })
    };

    let mut tokens: Vec<(TokenType, String, _)> = grammar_map
        .tokens()
        .tokens
        .iter()
        .filter_map(| token| {
            let expression = match token.expression() {
                ExpressionKind::Raw(raw) => {
                    let mut s = String::new();
                    for c in raw.chars() {
                        match c {
                            '+' | '*' | '?' | '(' | ')' | '[' | ']' | '{' | '}' | '|' | '^'
                            | '.' => {
                                s.push('\\');
                            }
                            _ => {}
                        }
                        s.push(c)
                    }
                    s
                }
                ExpressionKind::Regex(regex) => regex.to_string(),
                ExpressionKind::External => return None,
            };
            let callback: Option<Callback<TokenType>> = if let Some(callback) = token.callback() {
                match callback {
                    "raw_string_literal" => Some(Box::new(raw_string_literal)),
                    "multiline_comment" => Some(Box::new(multiline_comment)),
                    "char_or_lifetime" => Some(Box::new(char_or_lifetime.expect("the 'char_or_lifetime' callback needs both the 'CHAR' and 'LIFETIME' tokens"))),
                    _ => {
                        panic!("unknown callback: {}", callback)
                    }
                }
            } else {
                None
            };
            Some((token.id(), expression, callback))
        })
        .collect::<Vec<_>>();

    let mut builder = LexerBuilder::new();
    for (id, token, callback) in &mut tokens {
        builder.add_token(token, *id, None, callback.take());
    }

    let term_config = term::Config::default();
    let mut stderr = StandardStream::stderr(ColorChoice::Always);

    match builder.build(root_file) {
        Ok(lexer) => lexer,
        Err(err) => {
            use lexer::Error;
            let (token, msg) = match err {
                Error::Regex(token, err) => (token, format!("{}", err)),
                Error::RegexSyntax(token, err) => (token, format!("{}", err)),
                Error::RegexAutomata(token, err) => (token, format!("{}", err)),
                Error::MatchesEmpty(token) => {
                    (token, "the token matches an empty expression".to_string())
                }
                Error::Conflict(token1, token2) => {
                    let token1 = grammar_map.get_token(token1 as _).unwrap();
                    let token2 = grammar_map.get_token(token2 as _).unwrap();

                    let diagnostic = Diagnostic::error()
                        .with_message(format!(
                            "The tokens {} and {} conflicts with each other",
                            token1.name(),
                            token2.name(),
                        ))
                        .with_labels(vec![
                            Label::primary(token1.location().file, token1.location().span())
                                .with_message("first token"),
                            Label::primary(token2.location().file, token2.location().span())
                                .with_message("second token"),
                        ])
                        .with_notes(vec![String::from(
                            "Consider setting a priority with the `#[priority(...)]` attribute",
                        )]);

                    term::emit(&mut stderr, &term_config, grammar_files, &diagnostic).unwrap();
                    panic!();
                }
            };
            let token = grammar_map.get_token(token as _).unwrap();
            let diagnostic =
                Diagnostic::error()
                    .with_message(msg)
                    .with_labels(vec![Label::primary(
                        token.location().file,
                        token.location().span(),
                    )]);

            term::emit(&mut stderr, &term_config, grammar_files, &diagnostic).unwrap();
            panic!()
        }
    }
}
