use crate::helpers::print_rule_rhs;
use canonical_lr::{CanonicalLR, LR1Table};
use lr_parser::{
    compute::{ComputeLR1States, LR1Item, LR1Action, TransitionTable as _},
    grammar::{Grammar, Lookahead, RuntimeGrammar},
    make_grammar,
};
use std::{
    fmt::{self, Display},
    hash::Hash,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum N {
    Start,
    Sum,
    Prod,
    Value,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[allow(non_camel_case_types)]
pub enum T {
    plus,
    times,
    int,
    id,
}

impl Display for N {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

impl Display for T {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, formatter)
    }
}

pub fn main() {
    use N::*;
    use T::*;

    let grammar: RuntimeGrammar<T, N> = make_grammar!(
        Start -> { N(Sum) }
        Sum   -> { N(Sum) plus N(Prod) }
               | { N(Prod) }
        Prod  -> { N(Prod) times N(Value) }
               | { N(Value) }
        Value -> { id }
               | { int }
    );

    let table = CanonicalLR::new().compute_states(&grammar).unwrap();
    print_table(&table, &grammar);
    println!();
    println!("========================================");
    println!();

    let grammar: RuntimeGrammar<T, N> = make_grammar!(
        Start -> { N(Sum) }
        Sum   -> { N(Sum) plus N(Value) } | { N(Value) }
        Value -> { int }
    );

    let table = CanonicalLR::new().compute_states(&grammar).unwrap();
    print_table(&table, &grammar);
}

fn print_table<T, N, G>(table: &LR1Table<T, N>, grammar: &G)
where
    T: Copy + Eq + Hash + Display,
    N: Copy + Eq + Hash + Display,
    G: Grammar<Token = T, Node = N>,
{
    let tokens = grammar.tokens().into_iter().collect::<Vec<_>>();
    let nodes = grammar.nodes().into_iter().collect::<Vec<_>>();
    let node_len = {
        let mut max_len = 0;
        for n in nodes.iter().map(|x| x.to_string().len()) {
            if n > max_len {
                max_len = n;
            }
        }
        max_len
    };
    let token_len = {
        let mut max_len = 0;
        for n in tokens.iter().map(|x| x.to_string().len()) {
            if n > max_len {
                max_len = n;
            }
        }
        max_len
    };

    let states = {
        let mut states = Vec::new();
        let max_state = table.states().len();
        for state in 0..max_state {
            states.push(table.state_items(state))
        }
        states
    };

    for (index, state) in states.iter().enumerate() {
        print!("{:4} : ", index);
        print_state(state, grammar);
        println!();
    }

    println!();

    print!("      ");
    for t in &tokens {
        print!("│ {:1$} ", t.to_string(), token_len);
    }
    print!("│ {:1$} ", "EOF", node_len + 3);
    let mut nodes_iter = nodes.iter();
    if let Some(n) = nodes_iter.next() {
        print!("║ {:1$} ", n.to_string(), node_len)
    }
    for n in nodes_iter {
        print!("│ {:1$} ", n.to_string(), node_len)
    }
    println!();

    for state in 0..states.len() {
        print!("{:5} :", state);

        for t in tokens
            .iter()
            .map(|t| Lookahead::Token(*t))
            .chain(std::iter::once(Lookahead::Eof))
        {
            let token_len = if t.is_eof() { node_len + 3 } else { token_len };
            if let Some(action) = table.action(state, t) {
                match action {
                    LR1Action::Shift { state } => print!(" s{:<1$} ", state, token_len - 1),

                    LR1Action::Reduce { .. } => print!(" r{:<1$}", " ", token_len),
                    LR1Action::Accept(start) => {
                        let space_length = node_len - start.to_string().len() + 1;
                        print!(" A({}){:<2$}", start, " ", space_length);
                    }
                }
            } else {
                print!(" -{:<1$}", " ", token_len);
            }
            if !t.is_eof() {
                print!("│")
            }
        }

        let mut nodes_iter = nodes.iter();
        if let Some(n) = nodes_iter.next() {
            if let Some(state) = table.goto(state, *n) {
                print!("║ {:<1$} ", state, node_len);
            } else {
                print!("║ {:<1$} ", " ", node_len);
            }
        }
        for n in nodes_iter {
            if let Some(state) = table.goto(state, *n) {
                print!("│ {:<1$} ", state, node_len);
            } else {
                print!("│ {:<1$} ", " ", node_len);
            }
        }

        println!();
    }
}

/// Print a `LR1State`.
///
/// We have to use a function because `LR1State` is a type redefinition of a
/// `HashSet`.
fn print_state<T, N, G>(state: &[LR1Item<T, N>], grammar: &G)
where
    T: Copy + Eq + Hash + Display,
    N: Copy + Eq + Hash + Display,
    G: Grammar<Token = T, Node = N>,
{
    print!("{{ ");
    let mut state_iter = state.iter().copied();

    if let Some(indexed_rule) = state_iter.next() {
        print_indexed_rule(indexed_rule, grammar);
    }

    for indexed_rule in state_iter {
        print!(", ");
        print_indexed_rule(indexed_rule, grammar);
    }
    print!(" }}")
}

fn print_indexed_rule<T, N, G>(indexed_rule: LR1Item<T, N>, grammar: &G)
where
    T: Copy + Eq + Hash + Display,
    N: Copy + Eq + Hash + Display,
    G: Grammar<Token = T, Node = N>,
{
    print!("[{} ->", indexed_rule.rule_idx.lhs);
    print_rule_rhs(
        grammar.get_rhs(indexed_rule.rule_idx).unwrap_or_default(),
        indexed_rule.index,
    );
    print!(", {}]", indexed_rule.lookahead);
}
