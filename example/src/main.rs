use std::path::PathBuf;

mod complex_with_silents;
mod helpers;
mod high_level;
mod nox_grammar;
mod parser_build;
mod print_conflict;
mod print_table;
mod rust_grammar;

fn main() {
    let mut args = std::env::args();
    args.next();
    let example_name = args
        .next()
        .expect("you must specify the name of the example");
    match example_name.as_str() {
        "complex_with_silents" => complex_with_silents::main(),
        "nox_grammar" => nox_grammar::main(),
        "print_conflict" => print_conflict::main(),
        "print_table" => print_table::main(),
        "rust_grammar" => rust_grammar::main(),
        "high_level" => {
            let grammar = args
                .next()
                .expect("you must specify the path of the grammar file");
            let file = args
                .next()
                .expect("you must specify the path of the file to analyze");
            high_level::main(PathBuf::from(grammar), PathBuf::from(file))
        }
        "parser_build" => {
            let grammar = args
                .next()
                .expect("you must specify the path of the grammar file");
            let output = args
                .next()
                .expect("you must specify the path of the output file");
            parser_build::main(PathBuf::from(grammar), PathBuf::from(output))
        }
        _ => panic!("unknown example: {}", example_name),
    }
}
