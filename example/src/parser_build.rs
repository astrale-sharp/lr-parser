use std::path::PathBuf;

pub fn main(grammar_file: PathBuf, output_file: PathBuf) {
    let result = parser_build::generate_file(grammar_file, "lr_parser", "logos").unwrap();
    std::fs::write(&output_file, result).unwrap();
    eprintln!("File generated, running rustfmt...");
    parser_build::run_rustfmt(&output_file);
    eprintln!("Finished.");
}
