# Investigate

- ffi ?
- Good syntax errors: see [this article](https://research.swtch.com/yyerror)
- DSL via parser switch ? That'd be _very_ nice ! (asm !!!)

  One step in this direction is using the `#[context(...)]` stuff to switch between lexer states...

- External lexer !!!!
- Introduce a new attribute on _fields_ (that may replace `#[silent]` ?), that ask for the content of said field to be pulled up on the node ?
- Shrink LR states: https://link.springer.com/article/10.1007/BF00290336
  This is **very** important: the current Rust grammar generates > 10_000 states !
- !!! Change the `lookahead` thing in `conflict`: can't we check (for shifts at least) the _node_ we are parsing ???
- Attribute to make `(...)?` block switch the _whole_ rule
- Right now most stuff in the `GrammarMap` (including `MappingError` !) use
  `SyntaxIdx`'s to refer to the syntax tree. But it would be nice to have a
  reference to the _AST_ instead. So, should the AST use `Rc`/`Arc` to model nodes ?
  ```rust
  pub struct Ast<T> {
      syntax_index: SyntaxIdx,
      item: Arc<T>
  }
  ```
- That might even be nicer to do something similar for the _CST_: then AST building
  would not have to do so many checks... I think
  [`rowan`](https://github.com/rust-analyzer/rowan) can be a good inspiration here ?
- Report unused `conflict` blocks.
- Whoops, AST building can fail unexpectedly:
  ```groovy
  node Example { RepeatPlus '+' }
  node RepeatPlus { '+'* }
  conflict { prefer: reduce(RepeatPlus) over: shift('+') }
  ```
  Can lead to AST building failures...
  Ideas to resolve this:
  - Build the CST and AST _simultaneously_ ?
  - Keep meta-information in the CST (maybe not very good: what is there are parsing failures ? And this is not optimal for performances)
  - Ditch the AST, make a "typed" CST ('ala' RA, with wrappers). Then how would we keep the info about where a list ends ?
  - Never ditch silent nodes. This would be quite bad (esp. for performance !) :p
  - Use the `conflict` information
- Replace Set<Token> with a BitSet.

# References

- https://www.javatpoint.com/clr-1-parsing
