//! Data and functions used during the canonical LR algorithm

use crate::{types::State, ComputeErrorKind, Conflict};
use fnv::{FnvHashMap, FnvHashSet};
use lr_parser::{
    compute::LR1Item,
    grammar::{Grammar, Lookahead, RuleIdx, RulesNodeIter, Symbol},
};
use std::hash::Hash;

/// Data used by the algorithm
pub(crate) struct CanonicalLRData<'grammar, G>
where
    G: Grammar,
{
    /// Reference to the grammar.
    pub(crate) grammar: &'grammar G,
    /// Internal field used during [`crate::CanonicalLR::compute_states`].
    pub(crate) first_set_map: FnvHashMap<G::Node, (Vec<G::Token>, bool)>,
}

impl<'grammar, G, T, N> CanonicalLRData<'grammar, G>
where
    T: Copy + Eq + Ord + Hash,
    N: Copy + Eq + Ord + Hash,
    G: Grammar<Token = T, Node = N>,
{
    /// Initialize the `first_set_map`
    pub(crate) fn new(grammar: &'grammar G) -> Self {
        Self {
            grammar,
            first_set_map: Self::first_set_map(grammar),
        }
    }

    /// Get the FIRST set of this rule's tail.
    ///
    /// If the rule is `[A → α⋅Bβ, x]`, returns `(FIRST(βx), β →* ε)`.
    pub(crate) fn first_set_rule(&self, rule: &LR1Item<T, N>) -> (FnvHashSet<T>, bool) {
        let mut result = FnvHashSet::default();
        let mut matches_epsilon = true;
        if let Some(symbols) = self
            .grammar
            .get_rhs(rule.rule_idx)
            .and_then(|rhs| rhs.get(rule.index as usize + 1..))
        {
            for symbol in symbols {
                match symbol {
                    Symbol::Token(t) => {
                        matches_epsilon = false;
                        result.insert(*t);
                        break;
                    }
                    Symbol::Node(n) => {
                        let (first, rule_matches_epsilon) = &self.first_set_map[n];
                        for t in first {
                            result.insert(*t);
                        }
                        if !rule_matches_epsilon {
                            matches_epsilon = false;
                            break;
                        }
                    }
                }
            }
        }
        (result, matches_epsilon)
    }

    /// Compute the closure of the given state.
    pub(crate) fn closure(&self, state: &mut State<T, N>) {
        // All additional rules have index `0` : moreover, if
        // `[B → ⋅γ, y] ∈ SC` and `B → δ` is a rule, then we necessarily
        // `[B → ⋅δ, y] ∈ SC` as well.
        //
        // This means we only need to represent the left hand of the rule and
        // the lookahead.
        let mut processed: FnvHashSet<(N, Lookahead<T>)> = FnvHashSet::default();
        let mut to_process: Vec<(N, Lookahead<T>)> = Vec::with_capacity(4);
        for rule in &state.items {
            if let Some(Symbol::Node(n)) = rule.current_symbol(self.grammar) {
                let (lookaheads, matches_epsilon) = self.first_set_rule(rule);
                for lookahead in lookaheads {
                    if processed.insert((n, Lookahead::Token(lookahead))) {
                        to_process.push((n, Lookahead::Token(lookahead)));
                    }
                }
                if matches_epsilon && processed.insert((n, rule.lookahead)) {
                    to_process.push((n, rule.lookahead));
                }
            }
        }
        while let Some((n, lookahead)) = to_process.pop() {
            for (new_rule_idx, rhs) in RulesNodeIter::new(self.grammar, n) {
                let new_rule = LR1Item {
                    rule_idx: new_rule_idx,
                    index: 0,
                    lookahead,
                };
                state.items.push(new_rule);
                if let Some(Symbol::Node(new_n)) = rhs.first() {
                    let (lookaheads, matches_epsilon) = self.first_set_rule(&new_rule);
                    for lookahead in lookaheads {
                        if processed.insert((*new_n, Lookahead::Token(lookahead))) {
                            to_process.push((*new_n, Lookahead::Token(lookahead)));
                        }
                    }
                    if matches_epsilon && processed.insert((*new_n, new_rule.lookahead)) {
                        to_process.push((*new_n, new_rule.lookahead));
                    }
                }
            }
        }
    }

    /// Check the conflicts of the newly created state, eventually resolving them.
    pub(crate) fn check_conflicts(
        &self,
        from_state: usize,
        transitions: &mut FnvHashMap<Symbol<T, N>, State<T, N>>,
        reduces: &mut FnvHashMap<Lookahead<T>, Vec<RuleIdx<N>>>,
    ) -> Result<(), ComputeErrorKind<T, N>> {
        /// Detect if the given graph has a cycle.
        ///
        /// Returns Ok(()), or Err(vertices) with the vertices making up the cycle.
        fn has_cycle(graph: &FnvHashMap<u32, Vec<u32>>, nodes_nb: u32) -> Result<(), Vec<u32>> {
            /// Bit vector
            type BitVec = bitvec::vec::BitVec<bitvec::order::Lsb0, u8>;
            /// Inner recursive function.
            fn inner(
                graph: &FnvHashMap<u32, Vec<u32>>,
                visited: &mut BitVec,
                rec_stack: &mut BitVec,
                node: u32,
            ) -> Result<(), (Vec<u32>, bool)> {
                if let Some(mut b) = visited.get_mut(node as usize) {
                    *b = true;
                }
                if let Some(mut b) = rec_stack.get_mut(node as usize) {
                    *b = true;
                }
                for neighbor in graph.get(&node).map(|n| n.as_slice()).unwrap_or_default() {
                    if !visited
                        .get(*neighbor as usize)
                        .as_deref()
                        .copied()
                        .unwrap_or(false)
                    {
                        if let Err((mut vertices, continue_pushing)) =
                            inner(graph, visited, rec_stack, *neighbor)
                        {
                            let continue_pushing = if continue_pushing {
                                if let Some(first) = vertices.first().copied() {
                                    if node == first {
                                        false
                                    } else {
                                        vertices.push(node);
                                        true
                                    }
                                } else {
                                    false
                                }
                            } else {
                                false
                            };
                            return Err((vertices, continue_pushing));
                        }
                    } else if rec_stack
                        .get(*neighbor as usize)
                        .as_deref()
                        .copied()
                        .unwrap_or(false)
                    {
                        return Err((vec![node], true));
                    }
                }
                if let Some(mut b) = rec_stack.get_mut(node as usize) {
                    *b = false;
                }
                Ok(())
            }

            let mut visited = BitVec::with_capacity(nodes_nb as usize);
            let mut rec_stack = BitVec::with_capacity(nodes_nb as usize);
            for _ in 0..nodes_nb {
                visited.push(false);
                rec_stack.push(false);
            }
            for node in graph.keys().copied() {
                #[allow(clippy::collapsible_if)]
                if !visited
                    .get(node as usize)
                    .as_deref()
                    .copied()
                    .unwrap_or(false)
                {
                    inner(graph, &mut visited, &mut rec_stack, node).map_err(|(v, _)| v)?;
                }
            }
            Ok(())
        }

        // true if all reduces should be removed,
        // false if the shift should be removed.
        let mut shift_reduce_solutions: Vec<(T, bool)> = Vec::new();
        // What reduces to keep
        let mut reduce_reduce_solutions: Vec<(Lookahead<T>, usize)> = Vec::new();

        for (lookahead, rules) in &*reduces {
            // The third element is
            // - Some(true) if shift if preferred over any reduces
            // - Some(false) if all reduces all preferred
            // - None else (this would be an error !).
            let mut maybe_shift = match lookahead {
                Lookahead::Eof => None,
                Lookahead::Token(t) => transitions
                    .get(&Symbol::Token(*t))
                    .map(|state| (&state.items, *t, ConflictSolution::None)),
            };
            let mut reduce_reduce_solution: FnvHashMap<u32, Vec<u32>> = FnvHashMap::default();
            let mut reduce_to_solution = FnvHashMap::default();
            for (index, rule) in rules.iter().enumerate() {
                // reduce/reduce
                for (index2, rule2) in rules[..index].iter().enumerate() {
                    let new_solution =
                        match self.find_reduce_reduce_solution(*rule, *rule2, *lookahead) {
                            ConflictSolution::None => {
                                return Err(ComputeErrorKind::Conflict(Conflict::ReduceReduce {
                                    lookahead: *lookahead,
                                    reduce1: *rule,
                                    reduce2: *rule2,
                                }))
                            }
                            ConflictSolution::Conflicting(sol1, sol2) => {
                                return Err(ComputeErrorKind::CyclicConflictResolution {
                                    state: from_state,
                                    solutions: vec![sol1, sol2],
                                })
                            }
                            ConflictSolution::First(sol) => {
                                reduce_to_solution.insert(index as u32, sol);
                                (index as u32, index2 as u32)
                            }
                            ConflictSolution::Second(sol) => {
                                reduce_to_solution.insert(index2 as u32, sol);
                                (index2 as u32, index as u32)
                            }
                        };
                    reduce_reduce_solution
                        .entry(new_solution.0)
                        .or_default()
                        .push(new_solution.1);
                }
                // shift/reduce
                if let Some((shift_rules, token, shift_reduces_agree)) = &mut maybe_shift {
                    match self.find_shift_reduce_solution(*token, *rule, shift_rules) {
                        ConflictSolution::None => {
                            return Err(ComputeErrorKind::Conflict(Conflict::ShiftReduce {
                                letter: *token,
                                state: from_state,
                                reduce: *rule,
                            }))
                        }
                        ConflictSolution::Conflicting(sol1, sol2) => {
                            return Err(ComputeErrorKind::CyclicConflictResolution {
                                state: from_state,
                                solutions: vec![sol1, sol2],
                            })
                        }
                        ConflictSolution::First(sol) => {
                            if let ConflictSolution::Second(sol2) = shift_reduces_agree {
                                return Err(ComputeErrorKind::CyclicConflictResolution {
                                    state: from_state,
                                    solutions: vec![sol, *sol2],
                                });
                            }
                            *shift_reduces_agree = ConflictSolution::First(sol);
                        }
                        ConflictSolution::Second(sol) => {
                            if let ConflictSolution::First(sol2) = shift_reduces_agree {
                                return Err(ComputeErrorKind::CyclicConflictResolution {
                                    state: from_state,
                                    solutions: vec![sol, *sol2],
                                });
                            }
                            *shift_reduces_agree = ConflictSolution::Second(sol)
                        }
                    }
                }
            }
            if let Err(vertices) = has_cycle(&reduce_reduce_solution, rules.len() as _) {
                let solutions = vertices
                    .into_iter()
                    .filter_map(|reduce| reduce_to_solution.get(&reduce).copied())
                    .collect();
                return Err(ComputeErrorKind::CyclicConflictResolution {
                    state: from_state,
                    solutions,
                });
            }
            let all_reduce_removed = if let Some((_, token, shift_reduces_agree)) = maybe_shift {
                let all_reduce_removed = matches!(shift_reduces_agree, ConflictSolution::First(_));
                shift_reduce_solutions.push((token, all_reduce_removed));
                all_reduce_removed
            } else {
                false
            };
            if !all_reduce_removed {
                let mut to_remove = FnvHashSet::default();
                for (_, edges) in reduce_reduce_solution {
                    for idx in edges {
                        to_remove.insert(idx);
                    }
                }
                let mut to_keep = to_remove.len();
                for idx in 0..to_remove.len() {
                    if !to_remove.contains(&(idx as u32)) {
                        to_keep = idx;
                        break;
                    }
                }
                reduce_reduce_solutions.push((*lookahead, to_keep));
            }
        }
        for (token, prefer_shift) in shift_reduce_solutions {
            if prefer_shift {
                reduces.remove(&Lookahead::Token(token));
            } else {
                transitions.remove(&Symbol::Token(token));
            }
        }
        for (lookahead, to_keep) in reduce_reduce_solutions {
            if let Some(indices) = reduces.get_mut(&lookahead) {
                if let Some(to_keep) = indices.get(to_keep).copied() {
                    *indices = vec![to_keep];
                }
            }
        }
        Ok(())
    }

    /// Returns a [`ConflictSolution`] indicating what kind of solutions were found.
    fn find_reduce_reduce_solution(
        &self,
        reduce1: RuleIdx<N>,
        reduce2: RuleIdx<N>,
        lookahead: Lookahead<T>,
    ) -> ConflictSolution {
        let mut result = ConflictSolution::None;
        for (solution_index, solution) in self.grammar.conflict_solutions().iter().enumerate() {
            if let Some(t) = solution.lookahead {
                if Lookahead::Token(t) != lookahead {
                    continue;
                }
            }
            let new_result = if solution.prefer.reduce_matches(reduce1)
                && solution.over.reduce_matches(reduce2)
            {
                ConflictSolution::First(solution_index)
            } else if solution.over.reduce_matches(reduce1)
                && solution.prefer.reduce_matches(reduce2)
            {
                ConflictSolution::Second(solution_index)
            } else {
                continue;
            };
            result = match (result, new_result) {
                (ConflictSolution::None, _)
                | (ConflictSolution::First(_), ConflictSolution::First(_))
                | (ConflictSolution::Second(_), ConflictSolution::Second(_)) => new_result,
                (ConflictSolution::First(idx1), ConflictSolution::Second(idx2))
                | (ConflictSolution::Second(idx1), ConflictSolution::First(idx2)) => {
                    return ConflictSolution::Conflicting(idx1, idx2)
                }
                // unreachable
                _ => return ConflictSolution::None,
            };
        }

        result
    }

    /// Returns a [`ConflictSolution`] indicating what kind of solutions were found.
    fn find_shift_reduce_solution(
        &self,
        shift: T,
        reduce: RuleIdx<N>,
        shift_rules: &[LR1Item<T, N>],
    ) -> ConflictSolution {
        let mut result = ConflictSolution::None;
        for (solution_index, solution) in self.grammar.conflict_solutions().iter().enumerate() {
            // solution constraints
            if let Some(t) = solution.lookahead {
                let mut found = false;
                for rule in shift_rules {
                    if Lookahead::Token(t) != rule.lookahead {
                        found = true;
                        break;
                    }
                }
                if !found {
                    continue;
                }
            }
            let new_result = if solution.prefer.shift_matches(shift)
                && solution.over.reduce_matches(reduce)
            {
                ConflictSolution::First(solution_index)
            } else if solution.over.shift_matches(shift) && solution.prefer.reduce_matches(reduce) {
                ConflictSolution::Second(solution_index)
            } else {
                continue;
            };
            result = match (result, new_result) {
                (ConflictSolution::None, _)
                | (ConflictSolution::First(_), ConflictSolution::First(_))
                | (ConflictSolution::Second(_), ConflictSolution::Second(_)) => new_result,
                (ConflictSolution::First(idx1), ConflictSolution::Second(idx2))
                | (ConflictSolution::Second(idx1), ConflictSolution::First(idx2)) => {
                    return ConflictSolution::Conflicting(idx1, idx2)
                }
                // unreachable
                _ => return ConflictSolution::None,
            };
        }

        result
    }
}

/// What solutions were found ?
/// Used in [`CanonicalLRData::find_reduce_reduce_solution`] and
/// [`CanonicalLRData::find_shift_reduce_solution`].
#[derive(Clone, Copy, Debug, PartialEq)]
enum ConflictSolution {
    /// No solution was found
    None,
    /// Two opposite solutions were found
    Conflicting(usize, usize),
    /// A solution preferring the first alternative was found
    First(usize),
    /// A solution preferring the second alternative was found
    Second(usize),
}
