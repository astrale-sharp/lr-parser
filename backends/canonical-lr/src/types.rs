//! Various types used while implementing the canonical LR(1) algorithm.

use fnv::FnvHashMap;
use lr_parser::{
    compute::LR1Item,
    grammar::{Grammar, Lookahead, RuleIdx, Symbol},
};
use std::{borrow::BorrowMut, hash::Hash};

#[derive(Clone, Debug)]
/// State in the parsing machine.
pub(crate) struct State<T, N> {
    /// items of this state.
    ///
    /// Contains first the items where [`index`](LR1Item::index) != 0, ordered,
    /// then the other items in any order.
    pub(crate) items: Vec<LR1Item<T, N>>,
    /// Number of items where `index != 0`.
    pub(crate) nb_non_closed_items: usize,
    /// Transitions from this state.
    pub(crate) transitions: FnvHashMap<Symbol<T, N>, usize>,
    /// Reduce actions from this state.
    pub(crate) reduce: FnvHashMap<Lookahead<T>, RuleIdx<N>>,
    /// Potential accept action from this state.
    pub(crate) accept: Option<N>,
}

impl<T, N> Default for State<T, N> {
    fn default() -> Self {
        Self {
            items: Vec::new(),
            nb_non_closed_items: 0,
            transitions: FnvHashMap::default(),
            reduce: FnvHashMap::default(),
            accept: None,
        }
    }
}

impl<T, N> State<T, N>
where
    T: Copy + Eq + Ord + Hash,
    N: Copy + Eq + Ord + Hash,
{
    /// See if `other` can be merged into `self` (meaning that save for
    /// `[A → ⋅α, x]`, their items are the same).
    ///
    /// Other must **not** have been closed over.
    pub(crate) fn compatible(&self, other: &Self) -> bool {
        if self.nb_non_closed_items != other.nb_non_closed_items {
            return false;
        }
        let mut self_iter = self.items.iter();
        let other_iter = other.items.iter();
        for (self_item, other_item) in self_iter.borrow_mut().zip(other_iter) {
            if self_item != other_item {
                return false;
            }
        }
        if let Some(item) = self_iter.next() {
            item.index == 0
        } else {
            true
        }
    }

    /// Compute the transitions + reduces from this state.
    ///
    /// `self` must have been closed over, and the resulting transitions + reduces
    /// may contain conflicts.
    #[allow(clippy::type_complexity)]
    pub(crate) fn compute_transitions_and_reduce(
        &self,
        grammar: &impl Grammar<Token = T, Node = N>,
    ) -> (
        FnvHashMap<Symbol<T, N>, State<T, N>>,
        FnvHashMap<Lookahead<T>, Vec<RuleIdx<N>>>,
    ) {
        let mut transitions: FnvHashMap<Symbol<T, N>, State<T, N>> = FnvHashMap::default();
        let mut reduces: FnvHashMap<Lookahead<T>, Vec<RuleIdx<N>>> = FnvHashMap::default();
        for mut item in self.items.iter().copied() {
            if let Some(symbol) = item.current_symbol(grammar) {
                item.index += 1;
                transitions.entry(symbol).or_default().items.push(item);
            } else {
                reduces
                    .entry(item.lookahead)
                    .or_default()
                    .push(item.rule_idx);
            }
        }
        for state in transitions.values_mut() {
            state.nb_non_closed_items = state.items.len();
            state.items.sort_unstable_by(|item1, item2| {
                use std::cmp::Ordering;
                match item1.index.cmp(&item2.index) {
                    Ordering::Less => Ordering::Greater,
                    Ordering::Equal => item1.cmp(item2),
                    Ordering::Greater => Ordering::Less,
                }
            });
        }
        (transitions, reduces)
    }

    /// Use to produce the states for error reporting.
    #[allow(clippy::wrong_self_convention)]
    pub(crate) fn into_lr1states_vec(states: Vec<Self>) -> Vec<Vec<LR1Item<T, N>>> {
        states.into_iter().map(|state| state.items).collect()
    }
}

/// Conflict in the grammar between two conflicting actions.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Conflict<T, N> {
    /// Shift/reduce conflict
    ShiftReduce {
        /// Letter on which the conflict originated
        letter: T,
        /// Reference to the state we were into.
        state: usize,
        /// Reference to the reduce rule in the grammar.
        reduce: RuleIdx<N>,
    },
    /// Reduce/reduce conflict
    ReduceReduce {
        /// Letter on which the conflict originated
        lookahead: Lookahead<T>,
        /// Reference to the first reduce rule in the grammar.
        reduce1: RuleIdx<N>,
        /// Reference to the second reduce rule in the grammar.
        reduce2: RuleIdx<N>,
    },
}

impl<T, N> Conflict<T, N> {
    /// Get the lookahead token on which the conflict occurred
    #[inline]
    pub fn lookahead(&self) -> Lookahead<T>
    where
        T: Copy,
    {
        match self {
            Conflict::ShiftReduce { letter, .. } => Lookahead::Token(*letter),
            Conflict::ReduceReduce { lookahead, .. } => *lookahead,
        }
    }
}

/// Error emitted during [`compute_states`].
///
/// [`compute_states`]: crate::CanonicalLR::compute_states
#[derive(Clone, Debug)]
pub struct ComputeError<T, N> {
    /// Partially computed states.
    pub(crate) states: Vec<Vec<LR1Item<T, N>>>,
    /// Kind of error.
    pub kind: ComputeErrorKind<T, N>,
}

impl<T, N> ComputeError<T, N> {
    /// Get a state.
    pub fn state(&self, state: usize) -> Option<&Vec<LR1Item<T, N>>> {
        self.states.get(state)
    }

    /// Get all shifting items from `state` on `letter`.
    ///
    /// If `state` is not a valid state, returns `None`.
    pub fn shifts<'a>(
        &'a self,
        grammar: &'a (impl Grammar<Token = T, Node = N> + 'a),
        state: usize,
        letter: T,
    ) -> Option<impl Iterator<Item = LR1Item<T, N>> + 'a>
    where
        T: Copy + PartialEq,
        N: Copy,
    {
        self.states.get(state).map(|state| {
            state.iter().filter_map(move |item| {
                if matches!(item.current_symbol(grammar), Some(Symbol::Token(t)) if t == letter) {
                    let mut item = *item;
                    item.index += 1;
                    Some(item)
                } else {
                    None
                }
            })
        })
    }
}

/// Kind of error emitted during [`compute_states`].
///
/// [`compute_states`]: crate::CanonicalLR::compute_states
#[derive(Clone, Debug, PartialEq)]
pub enum ComputeErrorKind<T, N> {
    /// The maximum number of states has been reached during computation.
    ///
    /// See [`set_max_states`](crate::CanonicalLR::set_max_states) to change
    /// the maximum number of states.
    ///
    /// Be aware that if you run into this error, this is usually the sign that
    /// your grammar is too complex.
    TooManyStates(u32),
    /// A conflict was found in the grammar.
    Conflict(Conflict<T, N>),
    /// The specified conflict solutions form a cycle, and thus contradict with
    /// each other.
    CyclicConflictResolution {
        /// Index of the state in which solutions form a cycle
        state: usize,
        /// Conflict solutions are specified by their index.
        solutions: Vec<usize>,
    },
}

impl<T, N> From<Conflict<T, N>> for ComputeErrorKind<T, N> {
    fn from(conflict: Conflict<T, N>) -> Self {
        Self::Conflict(conflict)
    }
}
