//! LR1Table for [`CanonicalLR`](super::CanonicalLR).
//!
//! This implements [`TransitionTable`].

use fnv::FnvHashMap;
use lr_parser::{
    compute::{LR1Action, LR1Item, TransitionTable},
    grammar::Lookahead,
    parser::{RemappedToken, TokenKind},
    LR1Parser,
};
use std::hash::Hash;

/// State of the parser.
///
/// This is a set of LR(1) items.
pub(super) type LR1State<T, N> = Vec<LR1Item<T, N>>;
/// [Action table](LR1Table::actions) of a state of the parser.
pub(super) type LR1ActionTable<T, N> = FnvHashMap<Lookahead<T>, LR1Action<N, usize>>;

/// LR(1) parsing table.
///
/// This is the result of the [`canonical LR(1)`](super::CanonicalLR)
/// implementation.
#[derive(Clone, Debug)]
pub struct LR1Table<T, N> {
    /// Actions tables, at the index corresponding to the parser state.
    pub(super) actions: Vec<(LR1State<T, N>, LR1ActionTable<T, N>)>,
    /// Table from (state, node) to state.
    pub(super) goto_table: FnvHashMap<(usize, N), usize>,
    /// What is the starting state for a given node ?
    pub(super) starting_states: FnvHashMap<N, usize>,
}

impl<T: 'static, N> LR1Table<T, N> {
    /// Returns [`true`] if `word` is valid for the grammar.
    #[must_use]
    pub fn accept<'remapped>(
        &self,
        word: &[T],
        start: N,
        kind: impl Fn(T) -> TokenKind<T>,
        remap: impl Fn(T) -> RemappedToken<'remapped, T>,
    ) -> bool
    where
        N: Eq + Hash + Copy,
        T: Eq + Hash + Copy,
    {
        let starting_state = *self
            .starting_states
            .get(&start)
            .expect("This is not a valid starting state");
        let parser = LR1Parser::new(
            self,
            starting_state,
            word.iter().map(|t| (kind(*t), 0..0)),
            |_| false,
            remap,
        );

        for action in parser {
            if action.is_err() {
                return false;
            }
        }
        true
    }

    /// Returns this table's goto table.
    pub const fn goto_table(&self) -> &FnvHashMap<(usize, N), usize> {
        &self.goto_table
    }
}

impl<T, N> TransitionTable for LR1Table<T, N>
where
    N: Copy + Eq + Hash,
    T: Copy + Eq + Hash,
{
    type State = usize;
    type Token = T;
    type Node = N;
    type StatesIter = std::ops::Range<Self::State>;
    type Lookaheads = <Vec<Lookahead<Self::Token>> as IntoIterator>::IntoIter;

    fn starting_state(&self, node: Self::Node) -> Option<Self::State> {
        self.starting_states.get(&node).copied()
    }

    fn states(&self) -> Self::StatesIter {
        0..self.actions.len()
    }

    fn state_items(&self, state: Self::State) -> &[LR1Item<Self::Token, Self::Node>] {
        self.actions[state].0.as_slice()
    }

    fn available_lookaheads(&self, state: Self::State) -> Self::Lookaheads {
        match self.actions.get(state) {
            Some((_, action_table)) => action_table.keys().copied().collect::<Vec<_>>().into_iter(),
            None => Vec::new().into_iter(),
        }
    }

    fn action(
        &self,
        state: Self::State,
        lookahead: Lookahead<Self::Token>,
    ) -> Option<LR1Action<Self::Node, usize>> {
        self.actions.get(state)?.1.get(&lookahead).copied()
    }

    fn goto(&self, state: Self::State, node: Self::Node) -> Option<Self::State> {
        self.goto_table.get(&(state, node)).copied()
    }
}
