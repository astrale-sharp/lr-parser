//! A bunch of nodes/tokens to use in tests.

pub(crate) mod expressions {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub(crate) enum N {
        Start,
        Sum,
        Prod,
        Value,
        _Silent,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[allow(non_camel_case_types)]
    pub(crate) enum T {
        plus,
        times,
        int,
        id,
    }
}

pub(crate) mod test_grammar {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub(crate) enum N {
        S,
        A,
        B,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    #[allow(non_camel_case_types)]
    pub(crate) enum T {
        a,
        b,
        c,
        d,
        e,
    }
}
