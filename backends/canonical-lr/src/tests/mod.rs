mod grammars;

use self::grammars::*;
use crate::{CanonicalLR, ComputeErrorKind, Conflict};
use lr_parser::{
    compute::ComputeLR1States,
    grammar::{ConflictSolution, ConflictingAction, RuntimeGrammar, Symbol},
    make_grammar,
    parser::{RemappedToken, TokenKind},
};

/// This works ! So we have a full LR parser
#[test]
fn non_lalr_grammar() {
    use test_grammar::N::*;
    use test_grammar::T::*;

    let grammar = make_grammar!(
        S -> { a N(A) c }
            | { a N(B) d }
            | { b N(B) c }
            | { b N(A) d }
        A -> { e }
        B -> { e }
    );

    let table = CanonicalLR::new().compute_states(&grammar).unwrap();

    assert!(table.accept(&[a, e, c], S, TokenKind::Normal, |_| RemappedToken::None));
    assert!(table.accept(&[b, e, c], S, TokenKind::Normal, |_| RemappedToken::None));
    assert!(!table.accept(&[a, e, b], S, TokenKind::Normal, |_| RemappedToken::None));
    assert!(!table.accept(&[a, e, c, e], S, TokenKind::Normal, |_| RemappedToken::None));
}

#[test]
fn ambiguous_expressions() {
    use expressions::N::*;
    use expressions::T::*;

    let grammar = make_grammar!(
        Start -> { N(Value) }
        Value -> { N(Value) plus N(Value) }
               | { int }
    );

    // This is is ambiguous : `int + int + int` can be parsed as
    // `int + (int + int)`, or
    // `(int + int) + int`
    CanonicalLR::new().compute_states(&grammar).unwrap_err();
}

/// A grammar that is not LR(k) causes our computation of states to fail on
/// a conflict. Here this is reduce/reduce.
#[test]
fn non_lr_grammar() {
    use test_grammar::N::*;
    use test_grammar::T::*;

    let grammar = make_grammar!(
        S -> { N(A) a } | { N(B) b }
        A -> { c } | { d N(A) d }
        B -> { c } | { d N(B) d }
    );

    CanonicalLR::new().compute_states(&grammar).unwrap_err();
}

#[test]
fn accept_expressions() {
    use expressions::N::*;
    use expressions::T::*;

    let grammar = make_grammar!(
        Start -> { N(Sum) }
        Sum   -> { N(Sum) plus N(Prod) }
               | { N(Prod) }
        Prod  -> { N(Prod) times N(Value) }
               | { N(Value) }
        Value -> { id }
               | { int }
    );

    let table = CanonicalLR::new().compute_states(&grammar).unwrap();

    assert!(
        table.accept(&[int, plus, id], Start, TokenKind::Normal, |_| {
            RemappedToken::None
        })
    );
    assert!(table.accept(
        &[int, times, id, plus, int, times, int],
        Start,
        TokenKind::Normal,
        |_| RemappedToken::None
    ));
    assert!(
        !table.accept(&[int, plus, id, times], Start, TokenKind::Normal, |_| {
            RemappedToken::None
        })
    );
    assert!(!table.accept(
        &[int, plus, int, id, times, int],
        Start,
        TokenKind::Normal,
        |_| RemappedToken::None
    ));
}

#[test]
fn conflict_resolution() {
    use expressions::N::*;
    use expressions::T::*;

    let mut grammar = RuntimeGrammar::new();
    grammar.add_rule(Start, vec![Symbol::Node(Value)]).unwrap();
    let rule_plus = grammar
        .add_rule(
            Value,
            vec![
                Symbol::Node(Value),
                Symbol::Token(plus),
                Symbol::Node(Value),
            ],
        )
        .unwrap();
    let rule_times = grammar
        .add_rule(
            Value,
            vec![
                Symbol::Node(Value),
                Symbol::Token(times),
                Symbol::Node(Value),
            ],
        )
        .unwrap();
    grammar.add_rule(Value, vec![Symbol::Token(int)]).unwrap();

    // left-associativity of `+`
    grammar.add_conflict_resolution(ConflictSolution {
        prefer: ConflictingAction::Reduce(rule_plus),
        over: ConflictingAction::Shift(plus),
        lookahead: None,
    });
    // left-associativity of `*`
    grammar.add_conflict_resolution(ConflictSolution {
        prefer: ConflictingAction::Reduce(rule_times),
        over: ConflictingAction::Shift(times),
        lookahead: None,
    });
    // priority of `*` over `+`
    grammar.add_conflict_resolution(ConflictSolution {
        prefer: ConflictingAction::Shift(times),
        over: ConflictingAction::Reduce(rule_plus),
        lookahead: None,
    });
    // priority of `*` over `+`
    grammar.add_conflict_resolution(ConflictSolution {
        prefer: ConflictingAction::Reduce(rule_times),
        over: ConflictingAction::Shift(plus),
        lookahead: None,
    });

    CanonicalLR::new().compute_states(&grammar).unwrap();
}

#[test]
fn token_remapping() {
    use test_grammar::N::*;
    use test_grammar::T::*;

    let grammar = make_grammar!(
        S -> { N(A) } | { N(B) }
        A -> { a N(A) a } | { b } // a^n b a^n
        B -> { c d N(B) } | { c } // (c d)* c
    );
    let table = CanonicalLR::new().compute_states(&grammar).unwrap();

    // We have the remapping d -> aa
    let remapping = |t| match t {
        d => RemappedToken::Split(&[(a, 0), (a, 0)]),
        _ => RemappedToken::None,
    };
    assert!(table.accept(&[d, b, d], S, TokenKind::Normal, remapping));
    assert!(table.accept(&[a, a, a, a, b, d, d], S, TokenKind::Normal, remapping));
    assert!(!table.accept(&[a, a, a, a, b, a, d], S, TokenKind::Normal, remapping));
    assert!(table.accept(&[c, d, c, d, c], S, TokenKind::Normal, remapping));
    assert!(!table.accept(&[c, a, a, c, d, c], S, TokenKind::Normal, remapping));
}

#[test]
fn conflicts() {
    use test_grammar::N::*;
    use test_grammar::T::*;

    let mut grammar = make_grammar!(
        S -> { N(A) }
        A -> { N(B) a }
    );
    let reference_shift = (grammar.add_rule(S, vec![Symbol::Token(a)]).unwrap(), 1);
    let reference_reduce = grammar.add_rule(B, vec![]).unwrap();
    let err = CanonicalLR::new().compute_states(&grammar).unwrap_err();
    let is_the_error = match err.kind {
        ComputeErrorKind::Conflict(Conflict::ShiftReduce {
            letter,
            state,
            reduce,
        }) => {
            if letter != a || reduce != reference_reduce {
                false
            } else {
                let mut is_the_error = false;
                for rule in err.shifts(&grammar, state, letter).unwrap() {
                    if rule.index == reference_shift.1 && rule.rule_idx == reference_shift.0 {
                        is_the_error = true;
                        break;
                    }
                }
                is_the_error
            }
        }
        _ => false,
    };
    assert!(is_the_error, "{:#?}", err);
}
