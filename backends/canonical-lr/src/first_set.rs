//! Compute the FIRST sets.

use crate::data::CanonicalLRData;

use fnv::{FnvHashMap, FnvHashSet};
use lr_parser::grammar::{Grammar, RuleIdx, RulesIter, Symbol};
use std::hash::Hash;

impl<'grammar, T, N, G: 'grammar> CanonicalLRData<'grammar, G>
where
    T: Hash + Eq + Copy,
    N: Hash + Eq + Copy,
    G: Grammar<Token = T, Node = N>,
{
    /// Compute the set of nodes that matches ε.
    fn empty_nodes(grammar: &G) -> FnvHashSet<N> {
        let (potentially_empty, empty) = {
            let mut potentially_empty = Vec::new();
            let mut empty = FnvHashSet::default();
            for (RuleIdx { lhs, .. }, rhs) in RulesIter::new(grammar) {
                if rhs.is_empty() {
                    empty.insert(lhs);
                } else {
                    let mut nodes = Vec::new();
                    let mut only_nodes = true;
                    for symbol in rhs {
                        match symbol {
                            Symbol::Node(n) => nodes.push(*n),
                            Symbol::Token(_) => {
                                only_nodes = false;
                                break;
                            }
                        }
                    }
                    if only_nodes {
                        potentially_empty.push((lhs, nodes))
                    }
                }
            }

            (potentially_empty, empty)
        };

        let mut result = empty;
        let mut modified = true;
        while modified {
            modified = false;
            for (node, rule_rhs) in &potentially_empty {
                if rule_rhs.iter().all(|n| result.contains(n)) {
                    modified |= result.insert(*node);
                }
            }
        }
        result
    }

    /// Compute maps from `A` to the sets
    ///
    /// `{ B | A → αBβ, α →* ε }` and `{ x | A → αxβ, α →* ε }`.
    fn initial_graph(
        grammar: &G,
        empty_nodes: &FnvHashSet<N>,
    ) -> FnvHashMap<N, (FnvHashSet<T>, FnvHashSet<N>)> {
        let mut result: FnvHashMap<N, (FnvHashSet<T>, FnvHashSet<N>)> = FnvHashMap::default();
        for (RuleIdx { lhs, .. }, rhs) in RulesIter::new(grammar) {
            let (t_set, n_set) = result.entry(lhs).or_default();
            for symbol in rhs {
                match symbol {
                    Symbol::Token(t) => {
                        t_set.insert(*t);
                        break;
                    }
                    Symbol::Node(n) => {
                        n_set.insert(*n);
                        if !empty_nodes.contains(n) {
                            break;
                        }
                    }
                }
            }
        }
        result
    }

    /// Compute a map from a node `A` to (FIRST(A), `true` if `A →* {}`).
    pub(crate) fn first_set_map(grammar: &'grammar G) -> FnvHashMap<N, (Vec<T>, bool)> {
        let empty_nodes = Self::empty_nodes(grammar);
        let graph = Self::initial_graph(grammar, &empty_nodes);

        // compute the transitive closure of `graph`.
        //
        // This is a map from `A` to the set `{ B | A →* Bβ }`.
        let mut transitive_closure_graph = FnvHashMap::default();
        for (node, (_, node_set)) in &graph {
            let mut to_process = node_set.clone();
            let mut processed = FnvHashSet::default();
            while !to_process.is_empty() {
                let mut new_to_process = FnvHashSet::default();

                for node in to_process {
                    if let Some((_, nodes)) = graph.get(&node) {
                        for node in nodes {
                            new_to_process.insert(*node);
                        }
                    }
                    processed.insert(node);
                }

                to_process = new_to_process
                    .into_iter()
                    .filter(|node| !processed.contains(node))
                    .collect();
            }

            transitive_closure_graph.insert(*node, processed);
        }

        let mut result = FnvHashMap::default();

        for (node, node_set) in transitive_closure_graph {
            let mut first_set = if let Some((initial_first_set, _)) = graph.get(&node) {
                initial_first_set.clone()
            } else {
                FnvHashSet::default()
            };
            for n in node_set {
                if let Some((initial_first_set, _)) = graph.get(&n) {
                    first_set.extend(initial_first_set.clone());
                };
            }
            let empty = empty_nodes.contains(&node);
            result.insert(node, (first_set.into_iter().collect(), empty));
        }

        result
    }
}
