//! Implementation of the [canonical LR(1) algorithm](https://en.wikipedia.org/wiki/Canonical_LR_parser).

#![warn(clippy::missing_docs_in_private_items)]

mod data;
mod first_set;
mod table;
#[cfg(test)]
mod tests;
mod types;

pub use self::{
    table::LR1Table,
    types::{ComputeError, ComputeErrorKind, Conflict},
};

use self::types::State;
use data::CanonicalLRData;
use fnv::{FnvHashMap, FnvHashSet};
use lr_parser::{
    compute::{ComputeLR1States, LR1Action, LR1Item},
    grammar::{Grammar, Lookahead, RulesNodeIter, Symbol},
};
use std::{hash::Hash, iter::once};

/// Structure for computing the canonical LR(1) states.
///
/// The main method is [`compute_states`](Self::compute_states).
pub struct CanonicalLR {
    /// Maximum number of states generated.
    ///
    /// If this is exceeded, `compute_states` returns an error.
    max_states: u32,
}

impl CanonicalLR {
    /// Creates a new `CanonicalLR` structure.
    ///
    /// By default, `max_states` will be set to `10000`.
    pub fn new() -> Self {
        Self { max_states: 10_000 }
    }

    /// Set the maximum number of states computed during [`compute_states`].
    ///
    /// This is set to `10000` by default : be aware that raising this may
    /// dramatically increase the time and memory consumption of
    /// [`compute_states`].
    ///
    /// # Example
    ///
    /// ```
    /// # use canonical_lr::CanonicalLR;
    /// # #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
    /// enum Token { T }
    /// # #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
    /// enum Node { N }
    ///
    /// let mut compute = CanonicalLR::new();
    /// compute.set_max_states(1000);
    /// ```
    ///
    /// [`compute_states`]: Self::compute_states
    pub fn set_max_states(&mut self, max_states: u32) {
        self.max_states = max_states
    }
}

impl Default for CanonicalLR {
    fn default() -> Self {
        Self::new()
    }
}

/// Same as [`CanonicalLR`], but with a reference to the grammar for convenience.
struct CanonicalLRInternal<'grammar, T, N, G>
where
    G: Grammar<Token = T, Node = N>,
{
    /// Maximum number of states generated.
    ///
    /// If this is exceeded, `compute_states` returns an error.
    max_states: u32,
    /// Data used by the algorithm
    data: CanonicalLRData<'grammar, G>,
    /// Computed states
    states: Vec<State<G::Token, G::Node>>,
    /// Maps a symbol to the state that transitioned from this symbol.
    symbol_to_state: FnvHashMap<Symbol<G::Token, G::Node>, Vec<usize>>,
}

impl<G> ComputeLR1States<G> for CanonicalLR
where
    G::Token: Copy + Eq + Ord + Hash + std::fmt::Debug,
    G::Node: Copy + Eq + Ord + Hash + std::fmt::Debug,
    G: Grammar,
{
    type Result = LR1Table<G::Token, G::Node>;
    type ComputeError = ComputeError<G::Token, G::Node>;

    fn compute_states(self, grammar: &G) -> Result<Self::Result, Self::ComputeError> {
        let nodes: FnvHashSet<_> = grammar.nodes().collect();
        let internal = CanonicalLRInternal::new(grammar, self.max_states);
        internal.compute_states_internal(nodes.into_iter())
    }

    fn compute_states_with_start(
        self,
        start: G::Node,
        grammar: &G,
    ) -> Result<Self::Result, Self::ComputeError> {
        let internal = CanonicalLRInternal::new(grammar, self.max_states);
        internal.compute_states_internal(once(start))
    }

    fn compute_states_with_starts(
        self,
        starts: impl Iterator<Item = G::Node>,
        grammar: &G,
    ) -> Result<Self::Result, Self::ComputeError> {
        let internal = CanonicalLRInternal::new(grammar, self.max_states);
        internal.compute_states_internal(starts)
    }
}

impl<'grammar, T, N, G> CanonicalLRInternal<'grammar, T, N, G>
where
    T: Hash + Eq + Ord + Copy,
    N: Hash + Eq + Ord + Copy,
    G: Grammar<Token = T, Node = N>,
{
    /// Create a new [`CanonicalLRInternal`].
    fn new(grammar: &'grammar G, max_states: u32) -> Self {
        Self {
            max_states,
            data: CanonicalLRData::new(grammar),
            states: Vec::new(),
            symbol_to_state: FnvHashMap::default(),
        }
    }

    /// Internal implementation of the canonical LR algorithm.
    ///
    /// This is in a method for easier `self._` syntax :)
    fn compute_states_internal(
        mut self,
        starting_nodes: impl Iterator<Item = N>,
    ) -> Result<LR1Table<T, N>, ComputeError<T, N>> {
        let mut starting_states: FnvHashMap<N, usize> = FnvHashMap::default();
        for (index, start_node) in starting_nodes.enumerate() {
            starting_states.insert(start_node, 2 * index);
            let mut items = Vec::new();
            for (rule_idx, _) in RulesNodeIter::new(self.data.grammar, start_node) {
                items.push(LR1Item {
                    rule_idx,
                    index: 0,
                    lookahead: Lookahead::Eof,
                });
            }
            let mut new_items = Vec::new();
            for item in &items {
                if matches!(
                    item.current_symbol(self.data.grammar),
                    Some(Symbol::Node(n)) if n == start_node
                ) {
                    let mut new_item = *item;
                    new_item.index += 1;
                    new_items.push(new_item);
                }
            }
            let mut new_state = State {
                items: new_items,
                ..State::default()
            };
            let mut start_state = State {
                items,
                ..State::default()
            };
            self.data.closure(&mut start_state);
            self.data.closure(&mut new_state);
            // TODO: accept/reduce conflicts ?!!?
            // Right now, all those conflict resolve to accept.
            new_state.accept = Some(start_node);
            self.states.push(start_state);
            self.states.push(new_state);
        }

        let mut current_state_index = 0;
        while let Some(state) = self.states.get(current_state_index) {
            let (mut transitions, mut reduces) =
                state.compute_transitions_and_reduce(self.data.grammar);
            if let Err(kind) =
                self.data
                    .check_conflicts(current_state_index, &mut transitions, &mut reduces)
            {
                return Err(ComputeError {
                    states: State::into_lr1states_vec(self.states),
                    kind,
                });
            }
            let mut state_transitions = FnvHashMap::default();
            for (symbol, mut state) in transitions {
                let index = if let Some(index) = self.state_exists(symbol, &state) {
                    index
                } else {
                    let transition_to = self.states.len();
                    self.data.closure(&mut state);
                    self.states.push(state);
                    self.symbol_to_state
                        .entry(symbol)
                        .or_default()
                        .push(transition_to);
                    transition_to
                };
                state_transitions.insert(symbol, index);
            }
            let state = match self.states.get_mut(current_state_index) {
                Some(state) => state,
                // unreachable
                None => continue,
            };
            state.transitions = state_transitions;
            state.reduce = reduces
                .into_iter()
                .filter_map(|(lookahead, mut indices)| indices.pop().map(|idx| (lookahead, idx)))
                .collect();

            current_state_index += 1;
            if self.states.len() > self.max_states as usize {
                return Err(ComputeError {
                    states: State::into_lr1states_vec(self.states),
                    kind: ComputeErrorKind::TooManyStates(self.max_states),
                });
            }
        }

        for (start_node, start_state) in &starting_states {
            if let Some(state) = self.states.get_mut(*start_state) {
                state
                    .transitions
                    .insert(Symbol::Node(*start_node), *start_state + 1);
            }
        }

        // convert the states to the output format.
        // This might look like a lot of work, but is probably negligible compared to the previous computations :)
        let mut actions: Vec<(table::LR1State<T, N>, table::LR1ActionTable<T, N>)> = Vec::new();
        let mut goto_table: FnvHashMap<(usize, N), usize> = FnvHashMap::default();

        for (state_idx, state) in self.states.into_iter().enumerate() {
            let mut action_table = FnvHashMap::default();
            for (symbol, to_state) in state.transitions {
                match symbol {
                    Symbol::Token(t) => {
                        action_table
                            .insert(Lookahead::Token(t), LR1Action::Shift { state: to_state });
                    }
                    Symbol::Node(n) => {
                        goto_table.insert((state_idx, n), to_state);
                    }
                }
            }
            for (lookahead, rule_idx) in state.reduce {
                action_table.insert(
                    lookahead,
                    LR1Action::Reduce {
                        rhs_size: self
                            .data
                            .grammar
                            .get_rhs(rule_idx)
                            .unwrap_or_default()
                            .len() as u16,
                        rule_index: rule_idx,
                    },
                );
            }
            if let Some(node) = state.accept {
                action_table.insert(Lookahead::Eof, LR1Action::Accept(node));
            }
            actions.push((state.items, action_table))
        }

        Ok(LR1Table {
            actions,
            goto_table,
            starting_states,
        })
    }

    /// Check if `state` already exists in the list of states.
    ///
    /// Note that `state` transitioned on `symbol`, so the search is efficient.
    fn state_exists(&self, symbol: Symbol<T, N>, state: &State<T, N>) -> Option<usize> {
        if let Some(existing_states_indices) = self.symbol_to_state.get(&symbol) {
            for index in existing_states_indices {
                if let Some(state2) = self.states.get(*index) {
                    if state2.compatible(state) {
                        return Some(*index);
                    }
                }
            }
        }
        None
    }
}
