This provides a quick explanation of the theory behind LR parsing, as well as how it is implemented in this crate.

- [theory](./theory.md): This gives an overview of some of the theory behind LR parsing. This can be useful to contributors, but also if you are curious about this algorithm :)
- [practice](./practice.md): 