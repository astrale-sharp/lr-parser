# canonical-lr

This crate implements the [canonical LR(1) algorithm](https://en.wikipedia.org/wiki/Canonical_LR_parser).

This is meant to produce structures used by [lr-parser](https://gitlab.com/arnaudgolfouse/lr-parser).

An overview of what the 'canonical LR(1) algorithm' is and how it is implemented here can be found in the [docs](docs/index.md).
