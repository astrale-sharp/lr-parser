//! Strongly inspired by [regex-lexer](https://crates.io/crates/regex-lexer) and
//! [logos](https://crates.io/crates/logos).

/// Compute a priority associated with a regex (see <https://docs.rs/logos#token-disambiguation>)
mod priority;
/// Determine if two regexes can match the same text.
mod regex_overlap;

use regex::Regex;
use regex_automata::DFA;
use std::fmt::{self, Write};

type Span = std::ops::Range<usize>;

/// Error emitted by this library.
#[derive(Debug)]
pub enum Error<T> {
    /// Error while constructing a [`Regex`].
    Regex(T, regex::Error),
    /// Error while computing the priority using [`regex_syntax`]. This should
    /// probably not happen ? (TODO: check this)
    RegexSyntax(T, regex_syntax::Error),
    /// A Regex failed to compile using [`regex_automata`].
    RegexAutomata(T, regex_automata::Error),
    /// Conflict: two tokens *can* match the same string, and have the same
    /// priority.
    Conflict(T, T),
    /// A token can match an empty expression.
    MatchesEmpty(T),
}

/// Helper struct for [`LexerBuilder`]
struct BuilderToken<'regexes, T> {
    regex: &'regexes str,
    token: T,
    priority: Option<u32>,
    callback: Option<Callback<T>>,
}

impl<T: fmt::Debug> fmt::Debug for BuilderToken<'_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("BuilderToken")
            .field("regex", &self.regex)
            .field("token", &self.token)
            .field("priority", &self.priority)
            .field("callback", &self.callback.as_ref().map(|_| Underscore))
            .finish()
    }
}

/// Builder for the [`Lexer`]
#[derive(Debug)]
pub struct LexerBuilder<'regexes, T> {
    tokens: Vec<BuilderToken<'regexes, T>>,
}

impl<T: Copy> Default for LexerBuilder<'_, T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<'regexes, T: Copy> LexerBuilder<'regexes, T> {
    /// Create a new builder with no matches.
    pub fn new() -> Self {
        Self { tokens: Vec::new() }
    }

    /// Add a token match to the builder.
    ///
    /// # Priority
    ///
    /// If `priority` is `None`, the priority level will be automatically
    /// determined.
    ///
    /// The automatic priority assignment was directly inspired by
    /// [logos](https://docs.rs/logos/#token-disambiguation), look here if you want
    /// more informations.
    pub fn add_token(
        &mut self,
        regex: &'regexes str,
        token: T,
        priority: Option<u32>,
        callback: Option<Callback<T>>,
    ) {
        self.tokens.push(BuilderToken {
            regex,
            token,
            priority,
            callback,
        });
    }

    /// Build the lexer.
    ///
    /// # Errors
    ///
    /// This will return an error if:
    /// - One of the regexes was incorrect (for example, `"[incomplete"`)
    /// - Two regexes have overlapping matches, and have the same priority level.
    /// If this happen, you will need to disambiguate by setting a custom priority
    /// level in [`Self::add_token`].
    pub fn build(self, source: &str) -> Result<Lexer<T>, Error<T>> {
        let mut priorities_and_dfas = Vec::new();
        let mut tokens = Vec::new();
        for BuilderToken {
            regex,
            token,
            priority,
            callback,
        } in self.tokens
        {
            let line_begin_pattern = format!("^{}", regex);
            let priority = match priority {
                Some(p) => p,
                None => priority::compute_priority(&line_begin_pattern)
                    .map_err(|err| Error::RegexSyntax(token, err))?,
            };
            tokens.push(Token {
                token,
                regex: Regex::new(&line_begin_pattern).map_err(|err| Error::Regex(token, err))?,
                priority,
                callback,
            });

            priorities_and_dfas.push((
                priority,
                self::regex_overlap::get_dfa(regex)
                    .map_err(|err| Error::RegexAutomata(token, err))?,
            ));
        }

        if let Some((index1, index2)) = Self::check_conflicts(&priorities_and_dfas) {
            return match index2 {
                Some(index2) => Err(Error::Conflict(tokens[index1].token, tokens[index2].token)),
                None => Err(Error::MatchesEmpty(tokens[index1].token)),
            };
        }

        Ok(Lexer {
            source,
            location: 0,
            tokens,
        })
    }

    /// Check the list for conflict.
    ///
    /// A conflict arises if:
    /// - Two element have the same priority, and
    /// - Their regexes (as represented by `DFA`) overlap.
    fn check_conflicts(priorities_and_dfas: &[(u32, impl DFA)]) -> Option<(usize, Option<usize>)> {
        let empty_dfa = self::regex_overlap::get_dfa("").unwrap();
        for (index1, (priority1, dfa1)) in priorities_and_dfas.iter().enumerate() {
            if self::regex_overlap::dfa_overlap(dfa1, &empty_dfa) {
                return Some((index1, None));
            }
            for (index2, (priority2, dfa2)) in priorities_and_dfas[..index1].iter().enumerate() {
                if *priority1 == *priority2 && self::regex_overlap::dfa_overlap(dfa1, dfa2) {
                    return Some((index1, Some(index2)));
                }
            }
        }
        None
    }
}

/// Lexer callback.
///
/// Takes the lexer, and the end of the current token, and outputs the end of
/// the adjusted token.
///
/// Returns `Err` if there was an error.
pub type Callback<T> = Box<dyn Fn(&Lexer<T>, T, usize) -> Result<(T, usize), usize>>;

/// Small internal structure for [`Lexer`]. Basically a tuple with names.
struct Token<T> {
    token: T,
    regex: Regex,
    priority: u32,
    callback: Option<Callback<T>>,
}

struct Underscore;

impl fmt::Debug for Underscore {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_char('_')
    }
}

impl<T: fmt::Debug> fmt::Debug for Token<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Token")
            .field("token", &self.token)
            .field("regex", &self.regex)
            .field("priority", &self.priority)
            .field("callback", &self.callback.as_ref().map(|_| Underscore))
            .finish()
    }
}

/// Iterator over its source's tokens.
///
/// This returns the next token identifier (as specified in
/// [`LexerBuilder::add_token`]), and its range in the source text.
///
/// If no known token match, it will bump the next character and return
/// `Some((None, start, end))` (where `start..end` is the range of the bumped char).
#[derive(Debug)]
pub struct Lexer<'src, T> {
    source: &'src str,
    location: usize,
    tokens: Vec<Token<T>>,
}

impl<T> Lexer<'_, T> {
    /// Repurpose the lexer for another source text.
    pub fn set_source(self, source: &str) -> Lexer<T> {
        Lexer {
            source,
            location: 0,
            tokens: self.tokens,
        }
    }

    /// Currently parsed text.
    pub fn slice(&self) -> &str {
        &self.source[self.location..]
    }
}

impl<T: Copy> Iterator for Lexer<'_, T> {
    type Item = (Option<T>, Span);

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        if self.location == self.source.len() {
            return None;
        }
        let string = self.source.get(self.location..)?;
        if string.is_empty() {
            return None;
        }

        let mut current_match: Option<(T, usize, u32, &Option<Callback<T>>)> = None;
        for Token {
            token,
            regex,
            priority,
            callback,
        } in &self.tokens
        {
            if let Some(r_match) = regex.find(string) {
                let end = r_match.end();
                if end > 0 {
                    match &mut current_match {
                        Some(current_match) => {
                            if current_match.1 < end
                                || (current_match.1 == end && current_match.2 < *priority)
                            {
                                *current_match = (*token, end, *priority, callback)
                            }
                        }
                        None => current_match = Some((*token, end, *priority, callback)),
                    }
                }
            }
        }
        let (token, start, end) = match current_match {
            Some((token, end, _, callback)) => {
                if let Some(callback) = callback {
                    let (token, end) = match callback(self, token, end) {
                        Ok((token, new_end)) => (Some(token), new_end),
                        Err(new_end) => (None, new_end),
                    };
                    (token, self.location, self.location + end)
                } else {
                    (Some(token), self.location, self.location + end)
                }
            }
            None => {
                let next_char = string.chars().next().unwrap_or_default();
                let len_utf8 = next_char.len_utf8();
                (None, self.location, self.location + len_utf8)
            }
        };
        self.location = end;
        Some((token, start..end))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    static ITEMS: &[&str] = &[r"a", r"b", r"a+", r"ab", r"[\s]+"];
    static SOURCE: &str = "aaa b a cooab a";

    #[test]
    fn builder() {
        let mut builder = LexerBuilder::new();
        for (token_idx, token) in ITEMS.iter().enumerate() {
            let priority = if token_idx == 2 { Some(3) } else { None };
            builder.add_token(token, token_idx, priority, None)
        }
        let tokens = builder
            .build(SOURCE)
            .unwrap()
            .map(|(token, span)| (token, &SOURCE[span]))
            .collect::<Vec<_>>();
        assert_eq!(
            tokens,
            [
                (Some(2), "aaa"),
                (Some(4), " "),
                (Some(1), "b"),
                (Some(4), " "),
                (Some(2), "a"),
                (Some(4), " "),
                (None, "c"),
                (None, "o"),
                (None, "o"),
                (Some(3), "ab"),
                (Some(4), " "),
                (Some(2), "a"),
            ]
        );
    }
}
