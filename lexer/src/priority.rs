use regex_syntax::hir::{Hir, HirKind, RepetitionKind, RepetitionRange};

/// Compute a priority for the given pattern.
///
/// Should follow what [logos](https://crates.io/crates/logos) does.
pub fn compute_priority(pattern: &str) -> Result<u32, regex_syntax::Error> {
    let hir = regex_syntax::hir::translate::Translator::new().translate(
        pattern,
        &regex_syntax::ast::parse::Parser::new().parse(pattern)?,
    )?;

    Ok(hir_priority(&hir))
}

fn hir_priority(hir: &Hir) -> u32 {
    match hir.kind() {
        HirKind::Empty => 0,
        HirKind::Literal(_) => 2,
        HirKind::Class(_) => 1,
        HirKind::Anchor(_) => 0,
        HirKind::WordBoundary(_) => 0,
        HirKind::Repetition(repetition) => match &repetition.kind {
            RepetitionKind::ZeroOrOne | RepetitionKind::ZeroOrMore => 0,
            RepetitionKind::OneOrMore => hir_priority(&repetition.hir),
            RepetitionKind::Range(range) => {
                let priority = hir_priority(&repetition.hir);
                let n = match range {
                    RepetitionRange::Exactly(n)
                    | RepetitionRange::AtLeast(n)
                    | RepetitionRange::Bounded(n, _) => *n,
                };
                n * priority
            }
        },
        HirKind::Group(group) => hir_priority(&group.hir),
        HirKind::Concat(concat) => {
            let mut priority = 0;
            for hir in concat {
                priority += hir_priority(hir)
            }
            priority
        }
        HirKind::Alternation(alternation) => {
            let mut priority = u32::MAX;
            for hir in alternation {
                let hir_prior = hir_priority(hir);
                if hir_prior < priority {
                    priority = hir_prior;
                }
            }
            priority
        }
    }
}
