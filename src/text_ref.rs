//! Various ways to get a subspan of some source text.

use std::{cmp::Ordering, fmt, ops::Deref, rc::Rc, sync::Arc};

/// Type alias for a `Range<usize>` in source text.
pub type Span = std::ops::Range<usize>;

/// Types that implement that trait can be used as a way to retrieve some part of
/// text in a shared source.
pub trait TextReference: Clone + AsRef<str> {
    /// Create a new reference spanning `&self[span]`.
    ///
    /// This function might panic if `span` is invalid.
    fn new_subspan(&self, span: Span) -> Self;

    /// Get the span in the source text.
    fn get_span(&self) -> Span;

    /// Get the full source text.
    fn get_source(&self) -> &str;
}

/// Direct reference to a shared text.
///
/// This is the combination of a `&str` and a span.
///
/// This can be safely and efficiently used to retrieve a span of text without
/// overhead (since the validity of the reference is checked at creation time).
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct TextRef<'a> {
    text: &'a str,
    span: Span,
}

/// `Rc` reference to a shared text.
///
/// This is the combination of a `Rc<str>` and a span.
///
/// This can be safely and efficiently used to retrieve a span of text without
/// overhead (since the validity of the reference is checked at creation time).
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct TextRc {
    text: Rc<str>,
    span: Span,
}

/// `Arc` reference to a shared text.
///
/// This is the combination of a `Arc<str>` and a span.
///
/// This can be safely and efficiently used to retrieve a span of text without
/// overhead (since the validity of the reference is checked at creation time).
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct TextArc {
    text: Arc<str>,
    span: Span,
}

impl<'a> TextRef<'a> {
    /// Creates a new reference to the `text`.
    ///
    /// # Panics
    /// Panics if `span` is not valid for `text`.
    pub fn new(text: &'a str, span: Span) -> Self {
        if text.get(span.clone()).is_none() {
            panic!("The span {:?} was invalid for the given text", span)
        }
        Self { text, span }
    }

    /// Creates a `TextReference` spanning the entirety of the given text.
    pub fn new_full(text: &'a str) -> Self {
        let span = 0..text.len();
        Self { text, span }
    }
}

impl TextRc {
    /// Creates a new reference to the `text`.
    ///
    /// # Panics
    /// Panics if `span` is not valid for `text`.
    pub fn new(text: Rc<str>, span: Span) -> Self {
        if text.get(span.clone()).is_none() {
            panic!("The span {:?} was invalid for the given text", span)
        }
        Self { text, span }
    }

    /// Creates a `TextReference` spanning the entirety of the given text.
    pub fn new_full(text: Rc<str>) -> Self {
        let span = 0..text.len();
        Self { text, span }
    }
}

impl TextArc {
    /// Creates a new reference to the `text`.
    ///
    /// # Panics
    /// Panics if `span` is not valid for `text`.
    pub fn new(text: Arc<str>, span: Span) -> Self {
        if text.get(span.clone()).is_none() {
            panic!("The span {:?} was invalid for the given text", span)
        }
        Self { text, span }
    }

    /// Creates a `TextReference` spanning the entirety of the given text.
    pub fn new_full(text: Arc<str>) -> Self {
        let span = 0..text.len();
        Self { text, span }
    }

    /// Get the full reference to the underlying text.
    pub fn get_full_text(&self) -> &Arc<str> {
        &self.text
    }
}

impl fmt::Debug for TextRef<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl fmt::Display for TextRef<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl PartialOrd for TextRef<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for TextRef<'_> {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.text.cmp(other.text) {
            Ordering::Equal => match self.span.start.cmp(&other.span.start) {
                Ordering::Equal => self.span.end.cmp(&other.span.end),
                ordering => ordering,
            },
            ordering => ordering,
        }
    }
}

impl AsRef<str> for TextRef<'_> {
    fn as_ref(&self) -> &str {
        unsafe { self.text.get_unchecked(self.span.clone()) }
    }
}

impl Deref for TextRef<'_> {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl TextReference for TextRef<'_> {
    fn new_subspan(&self, mut span: Span) -> Self {
        span.start += self.span.start;
        span.end += self.span.start;
        Self::new(self.text, span)
    }

    fn get_span(&self) -> Span {
        self.span.clone()
    }

    fn get_source(&self) -> &str {
        self.text
    }
}

impl fmt::Debug for TextRc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl fmt::Display for TextRc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl PartialOrd for TextRc {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for TextRc {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.text.cmp(&other.text) {
            Ordering::Equal => match self.span.start.cmp(&other.span.start) {
                Ordering::Equal => self.span.end.cmp(&other.span.end),
                ordering => ordering,
            },
            ordering => ordering,
        }
    }
}

impl Deref for TextRc {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl AsRef<str> for TextRc {
    fn as_ref(&self) -> &str {
        unsafe { self.text.get_unchecked(self.span.clone()) }
    }
}

impl TextReference for TextRc {
    fn new_subspan(&self, mut span: Span) -> Self {
        span.start += self.span.start;
        span.end += self.span.start;
        Self::new(self.text.clone(), span)
    }

    fn get_span(&self) -> Span {
        self.span.clone()
    }

    fn get_source(&self) -> &str {
        &self.text
    }
}

impl fmt::Debug for TextArc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl fmt::Display for TextArc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl PartialOrd for TextArc {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for TextArc {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.text.cmp(&other.text) {
            Ordering::Equal => match self.span.start.cmp(&other.span.start) {
                Ordering::Equal => self.span.end.cmp(&other.span.end),
                ordering => ordering,
            },
            ordering => ordering,
        }
    }
}

impl Deref for TextArc {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl AsRef<str> for TextArc {
    fn as_ref(&self) -> &str {
        unsafe { self.text.get_unchecked(self.span.clone()) }
    }
}

impl TextReference for TextArc {
    fn new_subspan(&self, mut span: Span) -> Self {
        span.start += self.span.start;
        span.end += self.span.start;
        Self::new(self.text.clone(), span)
    }

    fn get_span(&self) -> Span {
        self.span.clone()
    }

    fn get_source(&self) -> &str {
        &self.text
    }
}
