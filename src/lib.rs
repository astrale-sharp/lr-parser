//! Implementation of a LR(1) parser.

pub mod compute;
pub mod grammar;
pub mod parser;
pub mod syntax_tree;
pub mod text_ref;

pub use self::parser::LR1Parser;
