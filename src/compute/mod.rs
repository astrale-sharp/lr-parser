//! Computing the [`TransitionTable`].

use crate::grammar::{Grammar, Lookahead, RuleIdx, Symbol};
use std::fmt::Debug;

/// Actions associated with a pair (state, token) in the action parsing table.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum LR1Action<N, State> {
    /// Advance the lexer, and go to `state`.
    Shift {
        /// `state` in which to go next.
        state: State,
    },
    /// Pop `rhs_size`, and push `rule_index.lhs`.
    Reduce {
        /// Number of letters consumed by the rule.
        // TODO: this field might not be necessary, as it is implied by `rule_index` !
        rhs_size: u16,
        /// Index of the applied rule in the grammar.
        ///
        /// This allows one to:
        /// - Get the newly created node: `rule_index.lhs`.
        /// - find the right-hand side of the rule if they have the grammar :
        /// `grammar.get_rule(rule_index)`.
        rule_index: RuleIdx<N>,
    },
    /// Accept the currently parsed text as a node.
    Accept(N),
}

/// A (rule, index, lookahead) triplet.
///
/// The index is a position in the rule's right-hand side.
///
/// The exact rule can be retrieved using [`Grammar::get_rhs`].
///
/// # Usage
/// This type should make up the [`State`](TransitionTable::State) of a transition
/// table.
///
/// It is exposed in order for an external crate to implement a
/// [recovery strategy](crate::parser::recover_strategy) in case of a parser error.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LR1Item<T, N> {
    /// Left-hand side of the rule and index of the right-hand side in the grammar.
    pub rule_idx: RuleIdx<N>,
    /// Position in the rule's right-hand side
    pub index: u16,
    /// Lookahead token
    pub lookahead: Lookahead<T>,
}

impl<T, N> LR1Item<T, N> {
    /// Get the symbol at the dot.
    pub fn current_symbol(
        &self,
        grammar: &impl Grammar<Token = T, Node = N>,
    ) -> Option<Symbol<T, N>>
    where
        T: Copy,
        N: Copy,
    {
        grammar
            .get_rhs(self.rule_idx)?
            .get(self.index as usize)
            .copied()
    }
}

/// Trait implemented by a type that can compute a transition table for the
/// [`Parser`](crate::LR1Parser).
///
/// This usually means using an algorithm like **LR**, **LALR**...
///
/// The method used will determine which node has a
/// [`starting_state`](TransitionTable::starting_state).
// TODO: Maybe this trait should be removed, since it might always be used in concrete situations.
pub trait ComputeLR1States<G>
where
    G: Grammar,
{
    /// Result of the computation.
    type Result: TransitionTable<Token = G::Token, Node = G::Node>;
    /// Error while computing the states.
    ///
    /// This is usually caused by conflicts in the grammar.
    type ComputeError: std::fmt::Debug;

    /// Compute the transition table.
    ///
    /// This should return a table that can start from any symbol.
    fn compute_states(self, grammar: &G) -> Result<Self::Result, Self::ComputeError>;

    /// Compute the transition table.
    ///
    /// This should return a table that can start from the given symbol.
    fn compute_states_with_start(
        self,
        start: G::Node,
        grammar: &G,
    ) -> Result<Self::Result, Self::ComputeError>;

    /// Compute the transition table.
    ///
    /// This should return a table that can start from any of the given symbols.
    fn compute_states_with_starts(
        self,
        starts: impl Iterator<Item = G::Node>,
        grammar: &G,
    ) -> Result<Self::Result, Self::ComputeError>;
}

/// Implements the transition table for the [`Parser`](crate::LR1Parser).
///
/// This should be computed by a structure implementing [`ComputeLR1States`].
///
/// # Note
/// These must encode valid LR states transition. The parser might
/// unexpectedly panic if this is not the case.
// FIXME: what does the above sentence mean, exactly ?
pub trait TransitionTable {
    /// State of the parser. This should be an integer type, or a plain enum.
    type State: Copy;
    /// Token type
    type Token;
    /// Node type
    type Node;
    /// Iterator over all possible states
    type StatesIter: Iterator<Item = Self::State>;
    /// Iterator over the valid lookaheads
    type Lookaheads: Iterator<Item = Lookahead<Self::Token>>;

    /// Get the starting state for `node` if it exists.
    ///
    /// The methods of [`ComputeLR1States`] can be used to ensure that the table
    /// has a starting state for `node`.
    fn starting_state(&self, node: Self::Node) -> Option<Self::State>;

    /// Returns an iterator over all possible states.
    fn states(&self) -> Self::StatesIter;

    /// Returns the items corresponding to `state` in the grammar.
    fn state_items(&self, state: Self::State) -> &[LR1Item<Self::Token, Self::Node>];

    /// Get the valid lookahead tokens for `state`
    fn available_lookaheads(&self, state: Self::State) -> Self::Lookaheads;

    /// Get the action in state `state` when the next token is `lookahead`.
    ///
    /// # Notes
    /// - It should be impossible to get the [`Shift`](LR1Action::Shift) action when
    /// `lookahead` is [`EOF`](Lookahead::Eof). Failing to upheld this might result
    /// in panics while parsing.
    /// - Conversely, only [`EOF`](Lookahead::Eof) may result in a
    /// [`Accept`](LR1Action::Accept) action.
    // TODO: Does this mean the types should reflect this ?
    // e.g. with a `action_eof` function ?
    fn action(
        &self,
        state: Self::State,
        lookahead: Lookahead<Self::Token>,
    ) -> Option<LR1Action<Self::Node, Self::State>>;

    /// Return the 'goto' state corresponding to `state` and `node`.
    fn goto(&self, state: Self::State, node: Self::Node) -> Option<Self::State>;
}
