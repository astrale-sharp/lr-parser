//! Parser types

pub mod recover_strategy;
mod structures;

pub use self::structures::{
    IncorrectToken, ParseAction, ParseError, ParseResult, ReduceItem, RemappedToken, TokenKind,
};

use crate::{
    compute::{LR1Action, TransitionTable},
    grammar::{Grammar, Lookahead, RuleIdx},
    text_ref::Span,
};
use std::iter::Peekable;

/// Element of the parser's stack.
#[derive(Clone, Copy, Debug)]
struct StackElement<State> {
    /// State in the parsing tables.
    ///
    /// We need to store this to be able to apply the 'goto' actions.
    state: State,
    /// contains a rule size if the state was pushed after parsing a
    /// [silent](LR1Parser#silent-nodes) node.
    silent_size: Option<usize>,
}

/// LR(1) parser.
///
/// # Tokenizer
///
/// The tokenizer is an [`Iterator`] : it returns the [next token](TokenKind), as
/// well as its [span](Span).
///
/// # Parser
///
/// The parser is an [`Iterator`] that returns [`ParseAction`] objects (or
/// [errors](ParseError)) and a [`Span`]. This is up to the user to e.g. construct
/// the abstract tree.
///
/// # Workarounds
///
/// A LR(1) parser will get you far, but there are some small things that can be
/// inconvenient or even impossible to do the usual way. The `LR1Parser` structure
/// can be [created](LR1Parser::new) with some additional processing functions to
/// work around this:
///
/// ## Silent nodes
///
/// This is useful to create nodes that are only used 'internally' to get around the
/// limitations of the parser: For example, here `Items`'s only purpose is to repeat
/// `x`
/// ```
/// // Using the canonical LR algorithm to compute states.
/// use canonical_lr::CanonicalLR;
/// use lr_parser::{
///     compute::{ComputeLR1States as _, TransitionTable as _},
///     make_grammar,
///     parser::{LR1Parser, ParseAction, RemappedToken, TokenKind},
///     text_ref::Span,
/// };
///
/// #[allow(non_camel_case_types)]
/// #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
/// enum Token { x }
/// #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
/// enum Node {
///     Start,
///     Items,
/// }
///
/// use Node::*; use Token::*;
/// // This grammar is x*
/// let mut grammar = make_grammar!(
///     Start -> { N(Items) }
///     Items -> { x N(Items) } | {}
/// );
/// let parse_table = CanonicalLR::new()
///     .compute_states(&grammar)
///     .unwrap();
/// let starting_state = parse_table.starting_state(Node::Start).unwrap();
/// let parser = LR1Parser::new(
///     &parse_table,
///     starting_state,
///     [x, x, x, x].iter().copied().map(|t| (TokenKind::Normal(t), Span::default())),
///     |node| node == Node::Items,
///     |_| RemappedToken::None
/// );
/// // without silent nodes
/// let mut result = String::new();
/// // with silent nodes
/// let mut loud_result = String::new();
/// for item in parser {
///     match item.unwrap() {
///         ParseAction::Shift { .. } => {
///             result.push_str("shift ");
///             loud_result.push_str("shift ")
///         },
///         ParseAction::Reduce { silent_size, size, rule_index } => {
///             result.push_str(&format!("reduce({:?}, {}) ", rule_index.lhs, size));
///             loud_result.push_str(&format!("reduce({:?}, {}) ", rule_index.lhs, silent_size))
///         }
///         ParseAction::SilentReduce { node, size } => {
///             loud_result.push_str(&format!("reduce({:?}, {}) ", node, size));
///         }
///         _ => {}
///     }
/// }
/// // directly reduce to `Start`, instead of having to go through multiple `Items` reductions.
/// assert_eq!(result, "shift shift shift shift reduce(Start, 4) ");
/// assert_eq!(
///     loud_result,
///     "shift shift shift shift reduce(Items, 0) reduce(Items, 2) reduce(Items, 2) reduce(Items, 2) reduce(Items, 2) reduce(Start, 1) "
/// );
/// ```
///
/// ## Token remapping
///
/// This is used to transform a token while parsing. This was designed with 2 use
/// case in mind:
/// 1. **Contextual keywords**
///     
///     Some languages accept a keyword in some positions, but otherwise treat it
/// as a normal identifier.
///
///     For example, there is a proposition to treat `default` as a contextual
/// keyword in Rust. A parser for Rust could then implement the function:
///     ```
///     # use lr_parser::parser::RemappedToken;
///     enum Token {
///         /// the `default` keyword
///         Default,
///         /// Any identifier, including `default`
///         Identifier
///     }
///
///     fn remap(token: Token) -> RemappedToken<'static, Token> {
///         if matches!(token, Token::Default) {
///             RemappedToken::Transform(Token::Identifier)
///         } else {
///             RemappedToken::None
///         }
///     }
///     ```
///
/// 2. **Remapping**
///
///     Some tokens might overlap with each other in unexpected ways. For example,
/// in Rust:
///     ```
///     type V = Vec<Vec<()>>;
///     //                 ^^ here
///     ```
///     Most tokenizers will interpret the highlighted part as one `>>`, where we
/// want two `>`s.
///     
///     So we can specify the following remapping function:
///     ```
///     # use lr_parser::parser::RemappedToken;
///     enum Token {
///         /// `>>`
///         RShift,
///         /// `>`
///         More
///     }
///
///     fn remap(token: Token) -> RemappedToken<'static, Token> {
///         if matches!(token, Token::RShift) {
///             RemappedToken::Split(&[(Token::More, 1), (Token::More, 1)])
///         } else {
///             RemappedToken::None
///         }
///     }
///     ```
///     This means that if the parser expects some `>` but no `>>`, it will split
/// up a `RShift` into two `More` of size 1.
///
/// Note that tokens are **NOT** recursively remapped: if `>>>` remap to `[>>, >]`
/// and `>>` remap to `[>, >]`, then `>>>` does **not** remap to `[>, >, >]`.
///
/// It should be emphasized that both of these are truly hacks, and should not be
/// used when they can be avoided.
///
/// Specifically, this might result in some strange errors, since the parser will
/// pretty eagerly remap tokens.
///
/// # Error recovery
///
/// When the parser encounter an [unexpected token], its internal state is not
/// modified. To avoid an infinite loop, you should use the [`apply_strategy`]
/// method, with a strategy picked from the [`recover_strategy`] module.
///
/// [unexpected token]: ParseError::IncorrectToken
/// [`apply_strategy`]: LR1Parser::apply_strategy
pub struct LR1Parser<'table, Tokenizer, Tables, Silent, Remap>
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
{
    /// LR parsing table.
    tables: &'table Tables,
    /// Stack of states : the current state is on top.
    stack: Vec<StackElement<Tables::State>>,
    /// Tokenizer iterator.
    tokenizer: Peekable<Tokenizer>,
    /// Span of the last parsed token.
    current_span: Span,
    /// Used to mark a rule as *silent*.
    silent: Silent,
    /// Remap tokens
    token_remapping: Remap,
    /// Stored remapped tokens and their spans.
    ///
    /// They are in reverse order for efficient popping :)
    remapped_tokens: Vec<(Tables::Token, Span)>,
}

impl<'remap, Tokenizer, Tables, Silent, Remap> Iterator
    for LR1Parser<'_, Tokenizer, Tables, Silent, Remap>
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: Copy + 'remap,
    Tables::Node: Copy,
{
    type Item = ParseResult<Tables>;

    fn next(&mut self) -> Option<Self::Item> {
        let current_state = self.get_current_state()?;

        let (mut lookahead, mut lookahead_span) = match self.peek_token() {
            Some((kind, span)) => match kind {
                TokenKind::Normal(t) => (Lookahead::Token(t), span),
                TokenKind::Skip(t) => {
                    self.next_token();
                    return Some(Ok(ParseAction::Skip { kind: t, span }));
                }
                TokenKind::Error => {
                    return Some(Err(ParseError::Tokenizer(span)));
                }
            },
            None => (Lookahead::Eof, self.current_span.end..self.current_span.end),
        };
        let remapped = !self.remapped_tokens.is_empty();

        let mut action = self.tables.action(current_state, lookahead);
        // Token remapping
        // Lots of binary checks...
        if action.is_none() && !remapped {
            if let Lookahead::Token(t) = lookahead {
                match (self.token_remapping)(t) {
                    RemappedToken::None => {}
                    RemappedToken::Transform(other_t) => {
                        lookahead = Lookahead::Token(other_t);
                        action = self.tables.action(current_state, lookahead);
                    }
                    RemappedToken::Split(remapping) => {
                        let mut pos = lookahead_span.start;
                        let mut tokens: Vec<_> = remapping
                            .iter()
                            .map(|(t, size)| {
                                let start_pos = pos;
                                pos += size;
                                (*t, start_pos..pos)
                            })
                            .collect();
                        tokens.reverse();

                        if let Some((new_token, new_span)) = tokens.last().cloned() {
                            if self
                                .tables
                                .action(current_state, Lookahead::Token(new_token))
                                .is_some()
                            {
                                // replace the next token with the remapped ones.
                                self.remapped_tokens = tokens;
                                self.tokenizer.next();
                                lookahead_span = new_span;
                                lookahead = Lookahead::Token(new_token);
                                action = self.tables.action(current_state, lookahead);
                            }
                        }
                    }
                }
            }
        }

        match action {
            Some(LR1Action::Shift { state }) => {
                self.next_token();
                match lookahead {
                    Lookahead::Eof => unreachable!(
                        "`Shift` should never be a valid action on the `EOF` lookahead"
                    ),
                    Lookahead::Token(t) => {
                        self.stack.push(StackElement {
                            state,
                            silent_size: None,
                        });
                        Some(Ok(ParseAction::Shift {
                            kind: t,
                            span: lookahead_span,
                        }))
                    }
                }
            }
            Some(LR1Action::Reduce {
                rhs_size,
                rule_index,
            }) => Some(Ok(self.reduce(rhs_size, rule_index).into())),
            Some(LR1Action::Accept(_)) => None,
            None => Some(Err(ParseError::IncorrectToken(IncorrectToken {
                token: match self.peek_token() {
                    Some((TokenKind::Normal(t), span)) | Some((TokenKind::Skip(t), span)) => {
                        Lookahead::Token((t, span))
                    }
                    // this can't be `TokenKind::Error` (look attentively at the code above, and remember that token remapping yield only `Normal` tokens)
                    _ => Lookahead::Eof,
                },
                state: current_state,
            }))),
        }
    }
}

impl<'remap, Tokenizer, Tables, Silent, Remap> LR1Parser<'_, Tokenizer, Tables, Silent, Remap>
where
    Tokenizer: Iterator<Item = (TokenKind<Tables::Token>, Span)>,
    Tables: TransitionTable,
    Silent: FnMut(Tables::Node) -> bool,
    Remap: Fn(Tables::Token) -> RemappedToken<'remap, Tables::Token>,
    Tables::Token: Copy + 'remap,
    Tables::Node: Copy,
{
    /// Creates a new `LR1Parser`.
    ///
    /// # Arguments
    /// - `tables`: The LR(1) parsing table.
    /// - `tokenizer`: An iterator over tokens and their associated
    /// informations (span + [kind](TokenKind)).
    /// - `silent_nodes`: A function that takes a node, and returns `true` if
    /// the node is silent.
    ///
    ///     This allows a higher-level parser to ignore some nodes : such a parser
    /// can only consider the [`size`](ParseAction::Reduce::size) field of
    /// [`Reduce`](ParseAction::Reduce) actions, and ignore
    /// [`SilentReduce`](ParseAction::SilentReduce) actions.
    ///
    ///     See [Silent nodes](LR1Parser#silent-nodes).
    /// - `token_remapping`: Allow for a special parser action called
    /// **token remapping**.
    ///
    ///     See [Token remapping](LR1Parser#token-remapping).
    pub fn new(
        tables: &Tables,
        starting_state: Tables::State,
        tokenizer: Tokenizer,
        silent_nodes: Silent,
        token_remapping: Remap,
    ) -> LR1Parser<'_, Tokenizer, Tables, Silent, Remap> {
        LR1Parser {
            tables,
            stack: vec![StackElement {
                state: starting_state,
                silent_size: None,
            }],
            tokenizer: tokenizer.peekable(),
            current_span: Span::default(),
            silent: silent_nodes,
            token_remapping,
            remapped_tokens: Vec::new(),
        }
    }

    /// Put the parser back in its initial state.
    ///
    /// # Panics
    /// Panics if the tables cannot start from `starting_node`.
    ///
    /// # Note
    /// To start from the node of your choice, make sure a starting state for the
    /// node has been computed, by using one of
    /// [`ComputeLR1States`](crate::compute::ComputeLR1States)'s methods.
    pub fn reset(&mut self, starting_node: Tables::Node, tokenizer: Tokenizer)
    where
        Tables::Node: std::fmt::Debug,
    {
        let starting_state = match self.tables.starting_state(starting_node) {
            Some(state) => state,
            None => panic!("{:?} is not a valid starting node !", starting_node),
        };

        self.stack = vec![StackElement {
            state: starting_state,
            silent_size: None,
        }];
        self.tokenizer = tokenizer.peekable();
        self.remapped_tokens = Vec::new();
    }

    /// Apply the given recover strategy to the parser.
    pub fn apply_strategy<S, G>(
        &mut self,
        error: &IncorrectToken<Tables::Token, Tables::State>,
        grammar: &G,
    ) -> S::Result
    where
        G: Grammar<Token = Tables::Token, Node = Tables::Node>,
        S: recover_strategy::Strategy<'remap, Tokenizer, Tables, Silent, Remap>,
    {
        S::apply(self, error, grammar)
    }

    /// - if `stack` is not empty, returns the current state.
    /// - if `stack` if empty and there is no more tokens, returns `None`.
    #[inline]
    fn get_current_state(&mut self) -> Option<Tables::State> {
        match self.stack.last().cloned() {
            Some(StackElement { state, .. }) => Some(state),
            None => {
                if self.peek_token().is_some() {
                    // This should not be reached, as the last action can only be
                    // popped on `Accept`, which requires a `EOF` lookahead.
                    unreachable!("The parser finished, but there are still tokens")
                } else {
                    None
                }
            }
        }
    }

    /// Peek the next token, either from the remapped tokens or the tokenizer.
    fn peek_token(&mut self) -> Option<(TokenKind<Tables::Token>, Span)> {
        if let Some((token, span)) = self.remapped_tokens.last().cloned() {
            return Some((TokenKind::Normal(token), span));
        }

        self.tokenizer.peek().cloned()
    }

    /// Advance to the next token.
    pub fn next_token(&mut self) -> Option<(TokenKind<Tables::Token>, Span)> {
        if let Some((token, span)) = self.remapped_tokens.pop() {
            self.current_span = span.clone();
            return Some((TokenKind::Normal(token), span));
        }

        if let Some((token, span)) = self.tokenizer.next() {
            self.current_span = span.clone();
            Some((token, span))
        } else {
            None
        }
    }

    /// Get the span of the last parsed token
    pub fn current_span(&self) -> Span {
        self.current_span.clone()
    }

    /// Get the span of the *next* token.
    ///
    /// Note that it takes `&mut self` to be able to internally peek the iterator.
    pub fn next_span(&mut self) -> Option<Span> {
        self.peek_token().map(|(_, span)| span)
    }

    /// Get the expected tokens for the state in which `error` was emitted.
    pub fn expected_tokens(
        &self,
        error: &IncorrectToken<Tables::Token, Tables::State>,
    ) -> Tables::Lookaheads {
        self.tables.available_lookaheads(error.state)
    }

    /// Apply a reduce action : this pops `rhs_size` states, and compute the
    /// `size`/`silent_size`.
    ///
    /// # Panics
    /// - Will panic if there are less than `rhs_size + 1` items in the stack.
    /// - _Might_ panic (maybe later on !) if the `rhs_size` last items on the
    /// stack cannot be interpreted as the rule in `rule_index`. Note that it is ok
    /// if the `rhs_size` last items only match the start of the rule.
    fn reduce(
        &mut self,
        rhs_size: u16,
        rule_index: RuleIdx<Tables::Node>,
    ) -> ReduceItem<Tables::Node> {
        let nodes = self
            .stack
            .split_off(self.stack.len().saturating_sub(rhs_size as usize)); // yolo, will panic later anyway if `rhs_size` is too big

        let mut silent_rules_count = 0;
        let mut silent_rules_size = 0;
        for StackElement { silent_size, .. } in nodes {
            if let Some(size) = silent_size {
                silent_rules_count += 1;
                silent_rules_size += size as usize;
            }
        }

        let state = if let Some(StackElement { state, .. }) = self.stack.last() {
            *state
        } else {
            unreachable!("Empty stack");
        };
        let is_silent = (self.silent)(rule_index.lhs);
        let silent_size = rhs_size;
        // should not overflow, c'mon
        let size = silent_size as usize + silent_rules_size - silent_rules_count;
        if let Some(next_state) = self.tables.goto(state, rule_index.lhs) {
            self.stack.push(StackElement {
                state: next_state,
                silent_size: if is_silent { Some(size) } else { None },
            });

            if is_silent {
                ReduceItem::SilentReduce {
                    node: rule_index.lhs,
                    size: silent_size,
                }
            } else {
                ReduceItem::Reduce {
                    rule_index,
                    size,
                    silent_size,
                }
            }
        } else {
            unreachable!("No 'goto' action")
        }
    }
}
