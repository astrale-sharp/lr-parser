use crate::{
    compute::TransitionTable,
    grammar::{Lookahead, RuleIdx},
    text_ref::Span,
};
use std::fmt::Debug;

/// Kind of token emitted by a tokenizer.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(u8)]
pub enum TokenKind<T> {
    /// The token is normal and may be used by the parser.
    Normal(T),
    /// The token should be skipped by the parser.
    ///
    /// This usually denote whitespace or comments.
    Skip(T),
    /// Error token.
    ///
    /// Emitted when the tokenizer failed to get the next token (because it didn't
    /// recognized it).
    Error,
}

/// Structure that indicates how a given token might be remapped.
///
/// For more information see [Token remapping](super::LR1Parser#token-remapping)
#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(u8)]
pub enum RemappedToken<'a, T> {
    /// No remapping possible
    None,
    /// The token can be transformed into another token.
    ///
    /// This is useful for e.g. contextual keywords.
    Transform(T),
    /// The token can be split into smaller tokens.
    ///
    /// The classic example for this is `Vec<Vec<T>>`, where the last `>>` must be
    /// split into two `>` (of size 1 each).
    Split(&'a [(T, usize)]),
}

impl<T> Default for RemappedToken<'_, T> {
    fn default() -> Self {
        Self::None
    }
}

/// Returned by the parser.
pub type ParseResult<Tables> = Result<
    ParseAction<<Tables as TransitionTable>::Token, <Tables as TransitionTable>::Node>,
    ParseError<<Tables as TransitionTable>::Token, <Tables as TransitionTable>::State>,
>;

/// Result of a reduction in the parser.
///
/// Can be converted into [`ParseAction`].
pub enum ReduceItem<N> {
    /// A new `node` can be created, using the n lasted parsed symbols
    /// (token or node).
    ///
    /// This node is marked as [silent](crate::parser::LR1Parser#silent-nodes).
    SilentReduce { node: N, size: u16 },
    /// A new node can be created, using the n lasted parsed symbols
    /// (token or node).
    Reduce {
        /// Index of the applied rule
        ///
        /// The right-hand side of the rule can then be retrieved in the
        /// [`Grammar`] by using [`get_rhs`].
        ///
        /// [`Grammar`]: crate::grammar::Grammar
        /// [`get_rhs`]: crate::grammar::Grammar::get_rhs
        rule_index: RuleIdx<N>,
        /// Number of children for the new node.
        size: usize,
        /// Number of children for the new node when accounting for
        /// [silent](crate::parser::LR1Parser#silent-nodes) nodes.
        silent_size: u16,
    },
}

impl<N> ReduceItem<N> {
    /// Node being reduced
    pub fn node(&self) -> N
    where
        N: Copy,
    {
        match self {
            ReduceItem::SilentReduce { node, .. } => *node,
            ReduceItem::Reduce { rule_index, .. } => rule_index.lhs,
        }
    }
}

impl<T, N> From<ReduceItem<N>> for ParseAction<T, N> {
    fn from(reduce_item: ReduceItem<N>) -> Self {
        match reduce_item {
            ReduceItem::SilentReduce { node, size } => Self::SilentReduce { node, size },
            ReduceItem::Reduce {
                rule_index,
                size,
                silent_size,
            } => Self::Reduce {
                size,
                silent_size,
                rule_index,
            },
        }
    }
}

/// Parser action.
///
/// This is returned by the [`next`] method on the [`Parser`] on the happy path: it
/// describe what action can be taken next, for e.g. building a syntax tree.
///
/// [`Parser`]: super::LR1Parser
/// [`next`]: super::LR1Parser::next
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum ParseAction<T, N> {
    /// A new token was parsed.
    Shift { kind: T, span: Span },
    /// A new token was parsed, and was [`skipped`](TokenKind::Skip).
    Skip { kind: T, span: Span },
    /// A new **node** can be created, using the n lasted parsed symbols
    /// (token or node).
    ///
    /// ## Silent nodes
    ///
    /// You should either :
    /// - Ignore [`SilentReduce`](Self::SilentReduce) actions, and use the `size`
    /// field.
    /// - Acknowledge [`SilentReduce`](Self::SilentReduce) actions, and use the
    /// `silent_size` field.
    Reduce {
        /// Number of children for the new node.
        size: usize,
        /// Number of children for the new node when accounting for
        /// [silent](crate::parser::LR1Parser#silent-nodes) nodes.
        silent_size: u16,
        /// Index of the applied rule
        ///
        /// The right-hand side of the rule can then be retrieved in the
        /// [`Grammar`] by using [`get_rhs`].
        ///
        /// [`Grammar`]: crate::grammar::Grammar
        /// [`get_rhs`]: crate::grammar::Grammar::get_rhs
        rule_index: RuleIdx<N>,
    },
    /// A new `node` can be created, using the n lasted parsed symbols
    /// (token or node).
    ///
    /// This node is marked as [silent](crate::parser::LR1Parser#silent-nodes).
    SilentReduce {
        /// New node.
        node: N,
        /// Number of children for the new node.
        size: u16,
    },
}

/// Error encountered while parsing.
///
/// This error can be returned by the [`next`] method of the [`Parser`].
///
/// [`Parser`]: super::LR1Parser
/// [`next`]: super::LR1Parser::next
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum ParseError<T, State> {
    /// The tokenizer returned an error.
    Tokenizer(Span),
    /// Encountered an unexpected token.
    ///
    /// If it is not [`EOF`](Lookahead::Eof), its associated span is returned as
    /// well.
    ///
    /// ## Error Recovery
    ///
    /// If this error is returned, the state of the parser was not modified. \
    /// You need to call [`LR1Parser::apply_strategy`] to recover from the
    /// error and make progress.
    ///
    /// [`LR1Parser::apply_strategy`]: super::LR1Parser::apply_strategy
    IncorrectToken(IncorrectToken<T, State>),
}

/// Encountered an unexpected token.
///
/// If it is not [`EOF`](Lookahead::Eof), its associated span is returned as
/// well. The token is always [`Normal`](TokenKind::Normal).
///
/// # Error Recovery
///
/// If this error is returned, the state of the parser was not modified. \
/// You need to call [`LR1Parser::apply_strategy`] to recover from the
/// error and make progress.
///
/// [`LR1Parser::apply_strategy`]: super::LR1Parser::apply_strategy
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct IncorrectToken<T, State> {
    /// Encountered token.
    pub token: Lookahead<(T, Span)>,
    /// State the parser was in when it encountered the token.
    ///
    /// This can be used in conjunction with the
    /// [`TransitionTable::state_items`](crate::compute::TransitionTable::state_items)
    /// method to get a list of expected tokens.
    pub state: State,
}
