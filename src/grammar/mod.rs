//! Creation of a grammar

mod r#macro;

use fnv::{FnvHashMap, FnvHashSet};
use std::{fmt, hash::Hash};

/// Symbol that can appear in a grammar, either a node or a token.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Symbol<T, N> {
    /// Token
    Token(T),
    /// Node
    Node(N),
}

impl<T: fmt::Display, N: fmt::Display> fmt::Display for Symbol<T, N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Symbol::Token(token) => fmt::Display::fmt(token, f),
            Symbol::Node(node) => fmt::Display::fmt(node, f),
        }
    }
}

/// Lookahead symbol.
///
/// If can be either a normal `T`, or `EOF`.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Lookahead<T> {
    /// End Of File
    Eof,
    /// Normal token
    Token(T),
}

impl<T: PartialEq> PartialEq<T> for Lookahead<T> {
    fn eq(&self, other: &T) -> bool {
        match self {
            Self::Eof => false,
            Self::Token(t) => t == other,
        }
    }
}

impl<T: fmt::Display> fmt::Display for Lookahead<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Lookahead::Eof => f.write_str("EOF"),
            Lookahead::Token(token) => fmt::Display::fmt(token, f),
        }
    }
}

impl<T> Lookahead<T> {
    /// Return `true` if `self` is the `EOF` symbol.
    pub const fn is_eof(&self) -> bool {
        matches!(self, Self::Eof)
    }

    /// Maps a `Lookahead<T>` to `Lookahead<U>` by applying a function to a
    /// contained value.
    pub fn map<U, M>(self, map_fn: M) -> Lookahead<U>
    where
        M: FnOnce(T) -> U,
    {
        match self {
            Lookahead::Eof => Lookahead::Eof,
            Lookahead::Token(t) => Lookahead::Token(map_fn(t)),
        }
    }

    /// Converts from `&Lookahead<T>` to `Lookahead<&T>`.
    pub const fn as_ref(&self) -> Lookahead<&T> {
        match self {
            Lookahead::Eof => Lookahead::Eof,
            Lookahead::Token(t) => Lookahead::Token(t),
        }
    }
}

impl<T> From<Option<T>> for Lookahead<T> {
    #[inline]
    fn from(opt: Option<T>) -> Self {
        match opt {
            None => Self::Eof,
            Some(t) => Self::Token(t),
        }
    }
}

impl<T> From<Lookahead<T>> for Option<T> {
    #[inline]
    fn from(lookahead: Lookahead<T>) -> Self {
        match lookahead {
            Lookahead::Eof => None,
            Lookahead::Token(t) => Some(t),
        }
    }
}

/// Reference to a rule in the [`Grammar`].
///
/// You can retrieve the right-hand side using [`Grammar::get_rhs`].
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RuleIdx<N> {
    /// Left-hand side of the rule.
    pub lhs: N,
    /// Index of the right-hand side of the rule in `Grammar::rules_indices`.
    pub index: u16,
}

/// One possible conflicting action.
///
/// Part of a [`ConflictSolution`].
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ConflictingAction<T, N> {
    /// Shift action.
    Shift(T),
    /// Any possible shift action.
    ShiftAny,
    /// Reduce action.
    Reduce(RuleIdx<N>),
    /// Any reduce action with `N` as `lhs`.
    ReduceNode(N),
}

impl<T, N> ConflictingAction<T, N> {
    /// Returns `true` if `reduce` matches the action.
    pub fn reduce_matches(&self, reduce: RuleIdx<N>) -> bool
    where
        N: PartialEq,
    {
        match self {
            ConflictingAction::Reduce(idx) => *idx == reduce,
            ConflictingAction::ReduceNode(node) => *node == reduce.lhs,
            _ => false,
        }
    }

    /// Returns `true` if `shift` matches the action.
    pub fn shift_matches(&self, shift: T) -> bool
    where
        T: PartialEq,
    {
        match self {
            ConflictingAction::Shift(token) => *token == shift,
            ConflictingAction::ShiftAny => true,
            _ => false,
        }
    }
}

/// A pair of actions, specifying which one should be preferred in case of conflict.
///
/// This should be inserted in a [`Grammar`] to be later used by the structures
/// tasked with [generating the LR(1) tables](crate::compute).
#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct ConflictSolution<T, N> {
    /// Preferred action.
    pub prefer: ConflictingAction<T, N>,
    /// Thrown away in case of conflict in favor of `prefer`.
    pub over: ConflictingAction<T, N>,
    /// Optional lookahead for `prefer` and `over`:
    /// - If the conflict is `shift/reduce`, ask that one of the shift's rules has
    /// this as lookahead.
    /// - If the conflict is `reduce/reduce`, ask that both reduce actions have
    /// this as lookahead.
    pub lookahead: Option<T>,
}

/// Collection of grammar rules buildable at runtime.
#[derive(Debug)]
pub struct RuntimeGrammar<T, N> {
    /// All rules, stored contiguously.
    rules: Vec<Symbol<T, N>>,
    /// References into `rules`.
    rules_indices: FnvHashMap<N, Vec<(usize, usize)>>,
    /// Contains a list of ways to resolve a conflict.
    conflicts_solver: Vec<ConflictSolution<T, N>>,
}

impl<T, N> Default for RuntimeGrammar<T, N> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T, N> RuntimeGrammar<T, N> {
    /// Creates a new grammar that contains no rule.
    pub fn new() -> Self {
        Self {
            rules: Vec::new(),
            rules_indices: FnvHashMap::default(),
            conflicts_solver: Vec::new(),
        }
    }
}

impl<T, N> RuntimeGrammar<T, N>
where
    N: Copy + Eq + Hash,
{
    /// Add a rule to the grammar, and returns a `RuleIdx` than can be used to
    /// retrieve it.
    ///
    /// Returns `None` if `rhs.len() > u16::MAX`, or if there is already `u16::MAX`
    /// rules associated with `lhs`.
    #[must_use]
    pub fn add_rule(&mut self, lhs: N, rhs: Vec<Symbol<T, N>>) -> Option<RuleIdx<N>> {
        let indices = self.rules_indices.entry(lhs).or_default();
        if (rhs.len() > u16::MAX as usize) || indices.len() == u16::MAX as usize {
            return None;
        }
        let start = self.rules.len();
        self.rules.extend(rhs);
        let end = self.rules.len();
        let index = indices.len() as u16;
        indices.push((start, end));
        Some(RuleIdx { lhs, index })
    }

    /// Manually add a solution to a conflict encountered by the parser.
    pub fn add_conflict_resolution(&mut self, solution: ConflictSolution<T, N>) {
        self.conflicts_solver.push(solution)
    }

    /// Returns the map of rules indices.
    ///
    /// This is used to implement the construction of the parser in
    /// `parser-proc-macro`.
    pub fn get_all_rules_indices(&self) -> &FnvHashMap<N, Vec<(usize, usize)>> {
        &self.rules_indices
    }
}

impl<T, N> Grammar for RuntimeGrammar<T, N>
where
    T: Copy + Eq + Hash,
    N: Copy + Eq + Hash,
{
    type Token = T;
    type Node = N;

    type TokensIter = <FnvHashSet<T> as IntoIterator>::IntoIter;
    type NodesIter = <FnvHashSet<N> as IntoIterator>::IntoIter;

    fn nodes(&self) -> Self::NodesIter {
        let mut result = FnvHashSet::default();
        for symbol in &self.rules {
            match symbol {
                Symbol::Token(_) => {}
                Symbol::Node(node) => {
                    result.insert(*node);
                }
            }
        }
        for node in self.rules_indices.keys() {
            result.insert(*node);
        }
        result.into_iter()
    }

    fn tokens(&self) -> Self::TokensIter {
        let mut result = FnvHashSet::default();
        for symbol in &self.rules {
            match symbol {
                Symbol::Token(t) => {
                    result.insert(*t);
                }
                Symbol::Node(_) => {}
            }
        }
        result.into_iter()
    }

    fn rules(&self) -> &[Symbol<Self::Token, Self::Node>] {
        &self.rules
    }

    fn rules_indices(&self, node: Self::Node) -> Option<&[(usize, usize)]> {
        self.rules_indices.get(&node).map(|v| v.as_slice())
    }

    fn conflict_solutions(&self) -> &[ConflictSolution<Self::Token, Self::Node>] {
        &self.conflicts_solver
    }
}

/// Implemented by a grammar.
///
/// A grammar is a collection of rules: that is, each `Node` is associated with a
/// number of slices `[Symbol<Token, Node>]`, which can be retrieved using
/// [`rules`](Self::rules) and [`rules_indices`](Self::rules_indices).
///
/// # Notes
/// - The main reason for this to be a trait is to make it possible to work with
/// both runtime or compile-time generated grammars.
/// - It is possible to easily iterate over its rules using [`RulesIter`] and
/// [`RulesNodeIter`].
pub trait Grammar {
    /// Token type of the grammar
    type Token;
    /// Node type of the grammar
    type Node: Copy;
    /// Iterator over the tokens stored in the grammar.
    type TokensIter: Iterator<Item = Self::Token>;
    /// Iterator over the nodes stored in the grammar.
    type NodesIter: Iterator<Item = Self::Node>;

    /// Returns all the tokens that appear in the grammar.
    ///
    /// The elements of the returned iterator are unique.
    fn tokens(&self) -> Self::TokensIter;

    /// Returns all the nodes that appear in the grammar.
    ///
    /// The elements of the returned iterator are unique.
    fn nodes(&self) -> Self::NodesIter;

    /// Returns the slice of all rule's symbols.
    ///
    // TODO: Not the most flexible, but it works for us so :)
    fn rules(&self) -> &[Symbol<Self::Token, Self::Node>];

    /// Returns the indices in [`rules`](Self::rules) corresponding with the rules
    /// for `node`.
    fn rules_indices(&self, node: Self::Node) -> Option<&[(usize, usize)]>;

    /// Get the right-hand side of the rule referred to by `rule_index`.
    ///
    /// The default implementation combines [`Self::rules`] with
    /// [`Self::rules_indices`].
    fn get_rhs(
        &self,
        rule_index: RuleIdx<Self::Node>,
    ) -> Option<&[Symbol<Self::Token, Self::Node>]> {
        self.rules_indices(rule_index.lhs).and_then(|rules| {
            match rules.get(rule_index.index as usize) {
                Some((start, end)) => self.rules().get(*start..*end),
                None => None,
            }
        })
    }

    /// Get the stored conflicts solutions.
    fn conflict_solutions(&self) -> &[ConflictSolution<Self::Token, Self::Node>];
}

/// Iterator over the rules of a [`Grammar`].
pub struct RulesIter<'a, G: Grammar> {
    grammar: &'a G,
    current_node: Option<RulesNodeIter<'a, G>>,
    nodes_iter: G::NodesIter,
}

impl<'a, G> RulesIter<'a, G>
where
    G: Grammar,
{
    /// Get an iterator over the rules of the grammar.
    pub fn new(grammar: &'a G) -> Self {
        let mut nodes_iter = grammar.nodes();
        let current_node = nodes_iter
            .next()
            .map(|node| RulesNodeIter::new(grammar, node));
        RulesIter {
            grammar,
            current_node,
            nodes_iter,
        }
    }
}

impl<'a, G> Iterator for RulesIter<'a, G>
where
    G: Grammar,
{
    #[allow(clippy::type_complexity)]
    type Item = (RuleIdx<G::Node>, &'a [Symbol<G::Token, G::Node>]);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.current_node.as_mut()?.next() {
                Some((idx, symbols)) => return Some((idx, symbols)),
                None => {
                    let next_node = self.nodes_iter.next()?;
                    self.current_node = Some(RulesNodeIter {
                        rules: self.grammar.rules(),
                        indices: self.grammar.rules_indices(next_node).unwrap_or_default(),
                        index: 0,
                        node: next_node,
                    });
                }
            }
        }
    }
}

/// Iterator over the rules of a [`Grammar`] associated with a specific node.
pub struct RulesNodeIter<'a, G: Grammar> {
    /// Reference to the rules of the grammar.
    rules: &'a [Symbol<G::Token, G::Node>],
    /// Indices in [`rules`](Self::rules) of the rules corresponding to [`node`](Self::node).
    indices: &'a [(usize, usize)],
    /// Index in [`indices`](Self::indices) of the currently read rule.
    index: u16,
    /// Node which rules we are iterating over.
    node: G::Node,
}

impl<'a, G: Grammar> RulesNodeIter<'a, G> {
    /// Get an iterator over the rules associated with `node`.
    pub fn new(grammar: &'a G, node: G::Node) -> Self {
        Self {
            rules: grammar.rules(),
            indices: grammar.rules_indices(node).unwrap_or_default(),
            index: 0,
            node,
        }
    }
}

impl<'a, G: Grammar> Iterator for RulesNodeIter<'a, G> {
    #[allow(clippy::type_complexity)]
    type Item = (RuleIdx<G::Node>, &'a [Symbol<G::Token, G::Node>]);

    fn next(&mut self) -> Option<Self::Item> {
        let (start, end) = self.indices.get(self.index as usize).copied()?;
        let rule_idx = RuleIdx {
            lhs: self.node,
            index: self.index,
        };
        self.index += 1;
        Some((rule_idx, self.rules.get(start..end)?))
    }
}
