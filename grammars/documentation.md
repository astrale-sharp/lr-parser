# Tokens

These describe the tokens of the grammar, and must be part of a `tokens` block.

They can either be simple string (in single quotes), or a regex (in a `regex(...)` block, and with double quotes).

To refer to a token elsewhere in the file, you can use its name, or its string representation if it is a simple string.

## Example

In

```
tokens {
    MY_TOKEN: 'hello',
    MY_REGEX: regex("[a-z]*")
}
```

`MY_TOKEN` and `'hello'` both refer to `MY_TOKEN`, while `MY_REGEX` refer to `MY_REGEX`.

## Attributes

- `#[skip]`: indicate that a token can be inserted anywhere between
  other tokens (for example, whitespace).
- `#[into(function)]`: indicate that when building the typed AST, `function`
  will be used to convert the string for the token into an object.
  Note that tokens that are _not_ marked with `#[convert(...)]` will not
  appear in the type AST.
- `#[callback(function)]`: once the tokenizer eats the marked token, `function`
  will be called. It can e.g. bump the tokenizer forward.
