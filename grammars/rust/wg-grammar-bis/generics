node Generics { _: '<', params: (GenericParam ',')*_, _: '>' }

node GenericParam { attrs: OuterAttr*, kind: GenericParamKind }

node GenericParamKind {
    | Lifetime => name: LIFETIME, bounds: (':' (LifetimeBound '+')*_)?
    | Type =>
        name: IDENT,
        bounds: (':' (TypeBound '+')*_)?,
        default: ('=' Type)?
}

node ForAllBinder { _: 'for', generics: Generics }

node WhereClause { _: 'where', bounds: (WhereBound ',')*_ }

node WhereBound {
    | Lifetime => lt: LIFETIME, _: ':', bounds: (LifetimeBound '+')*_
    | Type => binder: ForAllBinder?, ty: Type, _: ':', bounds: (TypeBound '+')*_
    // unstable(#20041):
    | TypeEq => binder: ForAllBinder?, left: Type, _: '=', right:Type
    // unstable(#20041):
    | TypeEqEq => binder: ForAllBinder?, left: Type, _: '==', right:Type
}

node LifetimeBound { outlives: LIFETIME }

node TypeBound {
    | Outlives => 0: LIFETIME
    | Trait => 0: TypeTraitBound
    | TraitParen => _: '(', bound:TypeTraitBound, _: ')'
}

node TypeTraitBound { unbound: '?'?, binder: ForAllBinder?, path: Path }

// Generic args of a path segment.
node GenericArgs {
    | AngleBracket =>
        _: '<',
        args_and_bindings: AngleBracketGenericArgsAndBindings?,
        _: '>'
    | Paren =>
        _: '(',
        inputs: (Type ',')*_,
        _: ')',
        output: ('->' Type)?
}

node AngleBracketGenericArgsAndBindings {
    | Args => 0: (GenericArg ',')+_
    | Bindings => 0: (TypeBinding ',')+_
    | ArgsAndBindings =>
        args: (GenericArg ',')_+,
        _: ',',
        bindings: (TypeBinding ',')+_
}

node GenericArg {
    | Lifetime => 0: LIFETIME
    | Type => 0: Type
}

node TypeBinding { name: IDENT, _: '=', ty: Type }