use crate::Result;
use high_level::ExpressionKind;
use lr_parser::parser::RemappedToken;
use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use std::fmt;

/// Holds the textual representation of the tokens, as `TokenStream`s.
pub(crate) struct Tokens {
    /// `struct Context { ... }`
    pub context: String,
    /// `mod verbatim { ... }`
    pub verbatim: TokenStream,
    /// `pub enum TokenWithError { ... }`
    ///
    /// `pub enum Token { ... }`
    pub tokens: TokenStream,
    /// `pub struct Tokenizer { ... }`
    pub tokenizer: TokenStream,
    /// `impl Token { fn remap() { ... } pub fn skip() { ... } }`
    ///
    /// `impl Display for Token { ... }`
    pub tokens_impl: TokenStream,
    /// `impl Iterator for Tokenizer { ... }`
    ///
    /// `impl Tokenizer { fn new() { ... } }`
    pub tokenizer_impl: TokenStream,
}

impl Tokens {
    pub(crate) fn new(info: &super::Info) -> Result<Self> {
        let logos_crate_name = &info.logos_crate_name;
        let lr_parser_crate_name = &info.lr_parser_crate_name;
        let error_variant = format_ident!(
            "{}ERROR",
            "_".repeat(info.tokens_max_leading_underscores + 1)
        );

        let context = {
            let mut context = String::from("#[derive(Default)]\nstruct Context {");
            for (field_name, field_type) in &info.grammar_map.tokens().context {
                crate::check_ident(field_name)?;
                crate::check_ident(field_type)?;
                context.push_str(&format!("    {}: {},\n", field_name, field_type));
            }
            context.push('}');
            context
        };

        let verbatim = Self::verbatim_block(info)?;

        let tokens = {
            let tokens_variants = info.tokens.values().map(|(token, name)| {
                let documentation = if let Some(doc) = &token.documentation {
                    quote! { #[doc = #doc] }
                } else {
                    quote! {}
                };
                quote! {
                    #documentation
                    #name
                }
            });
            let tokens_with_error_variants = info
                .tokens
                .values()
                .filter_map(|(token, name)| {
                    let logos_attribute = match token.expression() {
                        ExpressionKind::Raw(raw) => quote! { #[token(#raw)] },
                        ExpressionKind::Regex(regex) => quote! { #[regex(#regex)] },
                        ExpressionKind::External => return None,
                    };
                    Some(quote! {
                        #logos_attribute
                        #name
                    })
                })
                .chain(std::iter::once(quote! { #[error] #error_variant }));
            let token_doc = if let Some(doc) = &info.grammar_map.tokens().documentation {
                quote! { #[doc = #doc] }
            } else {
                quote! {}
            };
            quote! {
            #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, ::#logos_crate_name::Logos)]
            #[allow(clippy::upper_case_acronyms, non_camel_case_types)]
            enum TokenWithError {
                #( #tokens_with_error_variants, )*
            }
            #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
            #[allow(clippy::upper_case_acronyms, non_camel_case_types)]
            #token_doc
            pub enum Token {
                #( #tokens_variants, )*
            }
            }
        };

        let tokens_impl = {
            let remap_function = {
                let match_arms = info.tokens.values().filter_map(|(token, name)| {
                    let remapped = match token.remapped() {
                        RemappedToken::None => return None,
                        RemappedToken::Transform(transform) => {
                            let transform = &info.tokens[&transform].1;
                            quote! { RemappedToken::Transform(Token::#transform) }
                        }
                        RemappedToken::Split(split) => {
                            let split = split.iter().map(|(t, length)| {
                                let name = &info.tokens[t].1;
                                quote! { (Token::#name, #length) }
                            });
                            quote! { RemappedToken::Split(&[
                                #( #split ),*
                            ]) }
                        }
                    };
                    Some(quote! { Token::#name => #remapped })
                });
                let remap_doc = format!(
                    "Token remapping: this is the result of using the `#[remap(...)]` attribute.\n\nSee the [token remapping](::{}::LR1Parser#token-remapping) section of `lr_parser`.",
                    lr_parser_crate_name
                );
                quote! {
                #[doc = #remap_doc]
                pub fn remap(self) -> ::#lr_parser_crate_name::parser::RemappedToken<'static, Self> {
                    use ::#lr_parser_crate_name::parser::RemappedToken;
                    match self {
                        #( #match_arms, )*
                        _ => RemappedToken::None
                    }
                }
                }
            };
            let fn_kind_with_error = {
                let match_arms = info.tokens.values().map(|(token, name)| {
                    if token.skip() {
                        quote! { Self::#name => TokenKind::Skip(Token::#name) }
                    } else {
                        quote! { Self::#name => TokenKind::Normal(Token::#name) }
                    }
                });
                quote! {
                fn kind(self) -> ::#lr_parser_crate_name::parser::TokenKind<Token> {
                    use ::#lr_parser_crate_name::parser::TokenKind;
                    match self {
                        #(#match_arms,)*
                        Self::#error_variant => TokenKind::Error,
                    }
                }
                }
            };
            let fn_kind = {
                let match_arms = info.tokens.values().filter_map(|(token, name)| {
                    if token.skip() {
                        Some(quote! { Self::#name => true })
                    } else {
                        None
                    }
                });
                quote! {
                pub fn skip(self) -> bool {
                    match self {
                        #(#match_arms,)*
                        _ => false
                    }
                }
                }
            };
            let display_match_arms = info.tokens.values().map(|(token, name)| {
                let display = match token.expression() {
                    ExpressionKind::Raw(raw) => raw.as_str(),
                    _ => token.name(),
                };
                quote! { Token::#name => #display }
            });
            quote! {
            impl TokenWithError {
                #fn_kind_with_error
            }
            impl Token {
                #fn_kind
                #remap_function
            }

            impl std::fmt::Display for Token {
                fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                    f.write_str(match self { #( #display_match_arms, )* })
                }
            }
            }
        };

        let tokenizer = {
            let doc = format!("Wrapper around a [`logos`](::{logos}) lexer.

This is an iterator that returns ([`TokenKind<T>`](::{lr_parser}::parser::TokenKind), [`Span`](::{lr_parser}::Span)), where the `Span` is the span of the token in the source code.", logos = logos_crate_name, lr_parser = lr_parser_crate_name);
            quote! {
            #[doc = #doc]
            pub struct Tokenizer<'src> {
                inner: ::#logos_crate_name::Lexer<'src, TokenWithError>,
                context: Context,
            }
            }
        };

        let tokenizer_impl = {
            quote! {
            impl<'src> Iterator for Tokenizer<'src> {
                type Item = (::#lr_parser_crate_name::parser::TokenKind<Token>, std::ops::Range<usize>);

                fn next(&mut self) -> Option<<Self as Iterator>::Item> {
                    use ::#lr_parser_crate_name::parser::TokenKind;

                    let mut token_kind = self.inner.next()?.kind();
                    match token_kind {
                        TokenKind::Normal(token) | TokenKind::Skip(token)  => {
                            match token.callback(&mut self.context, self.inner.slice(), self.inner.remainder()) {
                                Ok((new_token, bump)) => {
                                    self.inner.bump(bump);
                                    token_kind = if new_token.skip() {
                                        TokenKind::Skip(new_token)
                                    } else {
                                        TokenKind::Normal(new_token)
                                    };
                                }
                                Err(bump) => {
                                    self.inner.bump(bump);
                                    token_kind = TokenKind::Error;
                                }
                            }
                        }
                        TokenKind::Error => {}
                    }
                    Some((token_kind, self.inner.span()))
                }
            }

            impl<'src> Tokenizer<'src> {
                pub fn new(source: &'src str) -> Self {
                    Self {
                        inner: <TokenWithError as ::#logos_crate_name::Logos>::lexer(source),
                        context: Context::default(),
                    }
                }
            }
            }
        };

        Ok(Tokens {
            context,
            verbatim,
            tokens,
            tokenizer,
            tokens_impl,
            tokenizer_impl,
        })
    }

    /// Creates the `mod verbatim { ... }`, and associated structures.
    fn verbatim_block(info: &super::Info) -> Result<TokenStream> {
        use std::str::FromStr;
        let verbatim_content =
            TokenStream::from_str(info.grammar_map.tokens().verbatim.as_deref().unwrap_or(""))
                .expect("The verbatim block is not well formed !");
        // (associated_token, callback, callback_type)
        let callback_functions = info
            .tokens
            .values()
            .filter_map(|(token, name)| {
                token
                    .callback()
                    .map(|callback| crate::make_ident(callback).map(|callback| (name, callback)))
            })
            .collect::<Result<Vec<_>>>()?;
        let impl_token = {
            let match_arms = callback_functions.iter().map(|(token, callback)| {
                quote! { Token::#token => {
                    #callback(context, self, current_span, remainder).map_err(|err| {
                        usize::from(super::verbatim_utils::UnitOrNumber::from(err))
                    })
                } }
            });
            quote! {
            impl Token {
                #[inline]
                pub(super) fn callback(self, context: &mut Context, current_span: &str, remainder: &str) -> Result<(Self, usize), usize> {
                    match self {
                        #(#match_arms),*
                        _ => Ok((self, 0))
                    }
                }
            }
            }
        };
        Ok(quote! {
        mod verbatim_utils {
            pub(super) enum UnitOrNumber {
                Unit,
                Number(usize)
            }
            impl From<usize> for UnitOrNumber {
                fn from(n: usize) -> Self {
                    Self::Number(n)
                }
            }
            impl From<()> for UnitOrNumber {
                fn from(_: ()) -> Self {
                    Self::Unit
                }
            }
            impl From<UnitOrNumber> for usize {
                fn from(unit_or_number: UnitOrNumber) -> Self {
                    match unit_or_number {
                        UnitOrNumber::Unit => 0,
                        UnitOrNumber::Number(n) => n,
                    }
                }
            }
        }
        mod verbatim {
            use super::{Token, Context};
            #impl_token
            #verbatim_content
        }
        })
    }
}

impl fmt::Display for Tokens {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}\n\n{}\n\n{}\n\n{}\n\n{}\n\n{}",
            self.context,
            self.verbatim,
            self.tokens,
            self.tokenizer,
            self.tokens_impl,
            self.tokenizer_impl,
        )
    }
}
