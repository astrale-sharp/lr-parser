use crate::{
    grammar_map::{
        ExpressionKind, FilesManager, GrammarFiles, GrammarMap, MappingError, NodeType, TokenType,
    },
    parser::{
        ast::{self, Ast},
        get_grammar, Node, Token, Tokenizer,
    },
};
use canonical_lr::CanonicalLR;
use codespan_reporting::{
    diagnostic::{Diagnostic, Label},
    files::{Files, SimpleFiles},
    term::{
        self,
        termcolor::{ColorChoice, StandardStream},
    },
};
use lexer::{Callback, LexerBuilder};
use lr_parser::{
    compute::{ComputeLR1States as _, TransitionTable},
    grammar::Lookahead,
    parser::{ParseError, RemappedToken, TokenKind},
    syntax_tree::SyntaxElement,
    text_ref::{TextRc, TextReference as _},
};
use std::{convert::TryFrom, path::PathBuf};

static GRAMMAR_PATH: &str = "../grammars/grammar";

type Grammar = lr_parser::grammar::RuntimeGrammar<Token, Node>;
type LR1Table = canonical_lr::LR1Table<Token, Node>;
type LR1Parser<'table, 'tokenizer> = lr_parser::LR1Parser<
    'table,
    Tokenizer<'tokenizer>,
    LR1Table,
    fn(Node) -> bool,
    fn(Token) -> RemappedToken<'static, Token>,
>;

/// Helper function for tests.
mod helpers {
    use super::*;
    use core::panic;
    use lr_parser::grammar::{RuleIdx, Symbol};

    pub(super) fn get_table(grammar: &Grammar) -> LR1Table {
        use canonical_lr::{ComputeErrorKind, Conflict};
        use lr_parser::grammar::Grammar as _;
        match CanonicalLR::new().compute_states(grammar) {
            Ok(table) => table,
            Err(error) => match &error.kind {
                ComputeErrorKind::CyclicConflictResolution { .. } => todo!(),
                ComputeErrorKind::TooManyStates(max) => {
                    panic!("too many states in the parser: max is {} !", max)
                }
                ComputeErrorKind::Conflict(conflict) => {
                    let mut output = String::from("conflict!\n");
                    output.push_str(&format!("on lookahead {}\n", conflict.lookahead()));
                    match conflict {
                        Conflict::ShiftReduce {
                            letter,
                            state,
                            reduce,
                        } => {
                            for rule in error.shifts(grammar, *state, *letter).unwrap() {
                                output.push_str(&format!(
                                    "SHIFT({}) {} -> {:?} (LOOKAHEAD = {})\n",
                                    rule.index,
                                    rule.rule_idx.lhs,
                                    grammar.get_rhs(rule.rule_idx),
                                    rule.lookahead
                                ));
                            }
                            output.push_str(&format!(
                                "REDUCE {} -> {:?}\n",
                                reduce.lhs,
                                grammar.get_rhs(*reduce)
                            ));
                        }
                        Conflict::ReduceReduce {
                            reduce1, reduce2, ..
                        } => {
                            output.push_str(&format!(
                                "REDUCE {} -> {:?}\n",
                                reduce1.lhs,
                                grammar.get_rhs(*reduce1)
                            ));
                            output.push_str(&format!(
                                "REDUCE {} -> {:?}\n",
                                reduce2.lhs,
                                grammar.get_rhs(*reduce2)
                            ));
                        }
                    }
                    panic!("{}", output)
                }
            },
        }
    }

    pub(super) fn get_parser<'table, 'src>(
        table: &'table LR1Table,
        source: &'src str,
    ) -> LR1Parser<'table, 'src> {
        let file_node_state = table.starting_state(Node::File).unwrap();
        LR1Parser::new(
            table,
            file_node_state,
            Tokenizer::new(source),
            Node::silent,
            Token::remap,
        )
    }

    pub(super) fn get_grammar_map(root_path: PathBuf) -> (GrammarMap, GrammarFiles) {
        let mut grammar_files = GrammarFiles::new();

        let canonical_lr = CanonicalLR::new();
        match GrammarMap::from_root_path(&mut grammar_files, root_path, canonical_lr) {
            Ok((map, warnings)) => {
                for warning in warnings {
                    warning
                        .display(&grammar_files)
                        .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                        .unwrap();
                }
                (map, grammar_files)
            }
            Err(err) => {
                err.display(&grammar_files)
                    .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                    .unwrap();
                panic!()
            }
        }
    }

    /// Build the lexer from the `GrammarMap` and a `source`.
    pub(super) fn get_lexer<'src>(
        map: &GrammarMap,
        source: &'src str,
    ) -> lexer::Lexer<'src, TokenType> {
        fn raw_string_literal(
            lexer: &lexer::Lexer<TokenType>,
            token: TokenType,
            end: usize,
        ) -> Result<(TokenType, usize), usize> {
            let slice = lexer.slice();
            if !lexer.slice().starts_with('r') {
                return Ok((token, end));
            }
            let hash_len = end.saturating_sub(2);
            let pattern = String::from("\"") + &"#".repeat(hash_len);
            if let Some(new_end) = slice[end..].find(&pattern) {
                Ok((token, new_end + end + pattern.len()))
            } else {
                Err(slice.len())
            }
        }

        fn multiline_comment(
            lexer: &lexer::Lexer<TokenType>,
            token: TokenType,
            _: usize,
        ) -> Result<(TokenType, usize), usize> {
            let slice = lexer.slice();
            let mut level = 0;
            let mut chars_indices = slice.char_indices().peekable();
            while let Some((_, c)) = chars_indices.next() {
                match c {
                    '/' => {
                        if let Some((_, '*')) = chars_indices.peek() {
                            chars_indices.next();
                            level += 1;
                        }
                    }
                    '*' => {
                        if let Some((pos, '/')) = chars_indices.peek().copied() {
                            chars_indices.next();
                            level -= 1;
                            if level == 0 {
                                return Ok((token, pos + 1));
                            }
                        }
                    }
                    _ => {}
                }
            }
            Err(slice.len())
        }

        let mut tokens: Vec<(TokenType, String, _)> = map
            .tokens()
            .tokens
            .iter()
            .filter_map(|token| {
                let expression = match token.expression() {
                    ExpressionKind::Raw(raw) => {
                        let mut s = String::new();
                        for c in raw.chars() {
                            match c {
                                '+' | '*' | '?' | '(' | ')' | '[' | ']' | '{' | '}' | '|' | '^'
                                | '.' => {
                                    s.push('\\');
                                }
                                _ => {}
                            }
                            s.push(c)
                        }
                        s
                    }
                    ExpressionKind::Regex(regex) => regex.to_string(),
                    ExpressionKind::External => return None,
                };
                let callback: Option<Callback<TokenType>> = if let Some(callback) = token.callback()
                {
                    match callback {
                        "raw_string_literal" => Some(Box::new(raw_string_literal)),
                        "multiline_comment" => Some(Box::new(multiline_comment)),
                        _ => {
                            panic!("unknown callback: {}", callback)
                        }
                    }
                } else {
                    None
                };
                Some((token.id(), expression, callback))
            })
            .collect::<Vec<_>>();

        let mut builder = LexerBuilder::new();
        for (id, token, callback) in &mut tokens {
            builder.add_token(token, *id, None, callback.take());
        }
        builder.build(source).unwrap()
    }

    pub(super) fn print_expand_compute_error(
        map: &GrammarMap,
        error: canonical_lr::ComputeError<TokenType, NodeType>,
    ) -> ! {
        use canonical_lr::ComputeErrorKind;
        match &error.kind {
            ComputeErrorKind::CyclicConflictResolution { .. } => todo!(),
            ComputeErrorKind::TooManyStates(n) => {
                panic!("computed {} states: that's too many !", n)
            }
            ComputeErrorKind::Conflict(conflict) => {
                use canonical_lr::Conflict;

                let mut message = String::new();
                match conflict {
                    Conflict::ShiftReduce {
                        letter,
                        state,
                        reduce,
                    } => {
                        print_shift(map, &mut message, &error, *letter, *state);
                        print_reduce(map, &mut message, *reduce, Lookahead::Token(*letter));
                    }
                    Conflict::ReduceReduce {
                        lookahead,
                        reduce1,
                        reduce2,
                    } => {
                        print_reduce(map, &mut message, *reduce1, *lookahead);
                        print_reduce(map, &mut message, *reduce2, *lookahead);
                    }
                }

                panic!("{}", message)
            }
        }
    }

    fn print_symbol(map: &GrammarMap, message: &mut String, symbol: Symbol<TokenType, NodeType>) {
        message.push_str(match symbol {
            Symbol::Token(token) => map.get_token(token).unwrap().name(),
            Symbol::Node(node) => {
                let node = map.get_node(node).unwrap();
                match node.name() {
                    Some(name) => name,
                    None => {
                        let node_location = node.location();
                        node_location.element.get_text()
                    }
                }
            }
        })
    }

    fn print_shift(
        map: &GrammarMap,
        message: &mut String,
        error: &canonical_lr::ComputeError<TokenType, NodeType>,
        letter: TokenType,
        state: usize,
    ) {
        use lr_parser::grammar::Grammar as _;

        for rule in error.shifts(map.grammar(), state, letter).unwrap() {
            message.push_str("\nShift: ");
            print_symbol(map, message, Symbol::Node(rule.rule_idx.lhs));
            message.push_str(" →");
            let rhs = map.grammar().get_rhs(rule.rule_idx).unwrap();
            for (idx, symbol) in rhs.iter().enumerate() {
                if rule.index as usize == idx {
                    message.push('⋅')
                } else {
                    message.push(' ')
                }
                print_symbol(map, message, *symbol);
            }
            message.push_str(" LOOKAHEAD = ");
            match rule.lookahead {
                Lookahead::Eof => message.push_str("EOF"),
                Lookahead::Token(token) => print_symbol(map, message, Symbol::Token(token)),
            }
        }
    }

    fn print_reduce(
        map: &GrammarMap,
        message: &mut String,
        rule_idx: RuleIdx<NodeType>,
        lookahead: Lookahead<TokenType>,
    ) {
        use lr_parser::grammar::Grammar as _;

        message.push('\n');
        let rhs = map.grammar().get_rhs(rule_idx).unwrap();
        message.push_str("Reduce: ");
        print_symbol(map, message, Symbol::Node(rule_idx.lhs));
        message.push_str(" →");
        for symbol in rhs {
            message.push(' ');
            print_symbol(map, message, *symbol);
        }
        message.push_str(" LOOKAHEAD = ");
        match lookahead {
            Lookahead::Eof => message.push_str("EOF"),
            Lookahead::Token(token) => print_symbol(map, message, Symbol::Token(token)),
        }
    }
}

/// Parse with the coded grammar
mod normal {
    use super::*;
    use codespan_reporting::files::Files;

    #[test]
    fn grammar_is_lr() {
        let grammar = get_grammar();
        let _ = helpers::get_table(&grammar);
    }

    #[test]
    fn grammar_file_is_compliant() {
        let source = std::fs::read_to_string(GRAMMAR_PATH).unwrap();
        let source = source.as_str();

        let grammar = get_grammar();
        let table = helpers::get_table(&grammar);
        let parser = helpers::get_parser(&table, source);

        let mut files = SimpleFiles::new();
        let file_id = files.add(GRAMMAR_PATH, source);
        let term_config = term::Config::default();
        let mut stderr = StandardStream::stderr(ColorChoice::Always);

        for event in parser {
            if let Err(err) = event {
                match err {
                    ParseError::Tokenizer(span) => {
                        let diagnostic = Diagnostic::error()
                            .with_message("Tokenizer error")
                            .with_labels(vec![Label::primary(file_id, span)]);
                        term::emit(&mut stderr, &term_config, &files, &diagnostic).unwrap();
                        panic!()
                    }
                    ParseError::IncorrectToken(incorrect_token) => {
                        let (_, span) = match incorrect_token.token {
                            Lookahead::Eof => (
                                Lookahead::Eof,
                                source.len().saturating_sub(1)..source.len().saturating_sub(1),
                            ),
                            Lookahead::Token((token, span)) => (Lookahead::Token(token), span),
                        };
                        let diagnostic = Diagnostic::error()
                            .with_message("Incorrect token")
                            .with_labels(vec![Label::primary(file_id, span)]);
                        term::emit(&mut stderr, &term_config, &files, &diagnostic).unwrap();
                        panic!()
                    }
                }
            }
        }
    }

    #[test]
    fn syntax_tree() {
        let source = TextRc::new_full(std::fs::read_to_string(GRAMMAR_PATH).unwrap().into());

        let grammar = get_grammar();
        let table = helpers::get_table(&grammar);
        let mut parser = helpers::get_parser(&table, source.as_ref());
        let (syntax_tree, errors) =
            SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::File);
        if !errors.is_empty() {
            let mut grammar_files = GrammarFiles::new();
            let file = grammar_files
                .files_manager
                .add_root(PathBuf::from(GRAMMAR_PATH))
                .unwrap();
            let mapping_error = MappingError::SyntaxErrors {
                included_from: None,
                file,
                errors,
            };
            mapping_error
                .display(&grammar_files)
                .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                .unwrap();
            panic!()
        }
        println!(
            "{}",
            syntax_tree.display(
                &|f, node| std::fmt::Display::fmt(&node.lhs, f),
                &|f, token| std::fmt::Display::fmt(token, f)
            )
        )
    }

    #[test]
    fn ast() {
        let mut grammar_files = GrammarFiles::new();
        let file = grammar_files
            .files_manager
            .add_root(PathBuf::from(GRAMMAR_PATH))
            .unwrap();
        let source = TextRc::new_full(grammar_files.source(file).unwrap());

        let grammar = get_grammar();
        eprintln!("get_grammar... ok");
        let table = helpers::get_table(&grammar);
        eprintln!("get_table... ok");
        let mut parser = helpers::get_parser(&table, source.as_ref());
        eprintln!("get_parser... ok");
        let (syntax_tree, errors) =
            SyntaxElement::from_parser(source.clone(), &mut parser, &grammar, Node::File);
        eprintln!("SyntaxElement... ok");
        if !errors.is_empty() {
            let mapping_error = MappingError::SyntaxErrors {
                included_from: None,
                file,
                errors,
            };
            mapping_error
                .display(&grammar_files)
                .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                .unwrap();
            panic!()
        }
        let file: Ast<ast::File> = match TryFrom::try_from(&syntax_tree) {
            Ok(file) => file,
            Err(error) => {
                let mapping_error = MappingError::AstError {
                    included_from: None,
                    file,
                    error,
                };
                mapping_error
                    .display(&grammar_files)
                    .write_on(&mut StandardStream::stderr(ColorChoice::Always))
                    .unwrap();
                panic!()
            }
        };
        eprintln!("Ast... ok");
        println!("{:#?}", file)
    }
}

// Parse with the grammar extracted from the grammar file
mod expand {
    use super::*;
    use crate::FileId;
    use lr_parser::syntax_tree::SyntaxError;

    fn print_syntax_error(
        grammar_map: &GrammarMap,
        file_id: FileId,
        files_manager: &FilesManager,
        syntax_error: SyntaxError<TokenType, NodeType, TextRc>,
    ) {
        let term_config = term::Config::default();
        let mut stderr = StandardStream::stderr(ColorChoice::Always);
        let node_str = |node: NodeType| {
            let node = grammar_map.get_node(node).unwrap();
            match node.name() {
                Some(name) => name,
                None => {
                    let node_location = node.location();
                    node_location.element.get_text()
                }
            }
        };
        let diagnostic = match syntax_error {
            SyntaxError::IncompleteNode { node, text_ref, .. } => Diagnostic::error()
                .with_message(format!("incomplete node: {}", node_str(node)))
                .with_labels(vec![
                    Label::primary(file_id, text_ref.get_span()).with_message("encountered token")
                ]),
            SyntaxError::Unexpected { token, expected } => {
                let (token, span) = match token {
                    Lookahead::Eof => {
                        let source = files_manager.source(file_id).unwrap();
                        let span = source.len()..source.len();
                        ("EOF", span)
                    }
                    Lookahead::Token((t, text_ref)) => (
                        grammar_map.get_token(t).unwrap().name(),
                        text_ref.get_span(),
                    ),
                };
                let mut expected_str = String::from("expected ");
                for token in expected {
                    expected_str.push_str(match token {
                        Lookahead::Eof => "EOF",
                        Lookahead::Token(t) => grammar_map.get_token(t).unwrap().name(),
                    });
                    expected_str.push_str(", ");
                }
                Diagnostic::error()
                    .with_message(format!("unexpected token {}", token))
                    .with_labels(vec![
                        Label::primary(file_id, span).with_message(expected_str)
                    ])
            }
        };
        term::emit(&mut stderr, &term_config, files_manager, &diagnostic).unwrap();
    }

    #[test]
    fn grammar_map() {
        let map = helpers::get_grammar_map(PathBuf::from(GRAMMAR_PATH));
        println!("{:#?}", map)
    }

    #[test]
    fn lexer() {
        let source = &std::fs::read_to_string(GRAMMAR_PATH).unwrap();

        let (map, _) = helpers::get_grammar_map(PathBuf::from(GRAMMAR_PATH));
        let lexer = helpers::get_lexer(&map, source);

        let mut files = SimpleFiles::new();
        let file_id = files.add(GRAMMAR_PATH, source);
        let term_config = term::Config::default();
        let mut stderr = StandardStream::stderr(ColorChoice::Always);

        for (token, span) in lexer {
            match token {
                Some(token) => {
                    let mapping_token = map.get_token(token).unwrap();
                    println!("match {:<16}: {:?}", mapping_token.name(), &source[span]);
                }
                None => {
                    let diagnostic = Diagnostic::error()
                        .with_message("unknown token")
                        .with_labels(vec![Label::primary(file_id, span)]);
                    term::emit(&mut stderr, &term_config, &files, &diagnostic).unwrap();
                    panic!()
                }
            }
        }
    }

    #[test]
    fn parser() {
        let source = std::fs::read_to_string(GRAMMAR_PATH).unwrap();
        let source = source.as_str();

        let (map, _) = helpers::get_grammar_map(PathBuf::from(GRAMMAR_PATH));
        let lexer = helpers::get_lexer(&map, source);

        let grammar = map.grammar();
        let starting_states = map.nodes().filter_map(|node| {
            if node.is_silent() {
                None
            } else {
                Some(node.id())
            }
        });
        let table = CanonicalLR::new()
            .compute_states_with_starts(starting_states, grammar)
            .unwrap();
        let starting_state = table.starting_state(map.starting_node()).unwrap();
        let parser = lr_parser::LR1Parser::new(
            &table,
            starting_state,
            lexer.map(|(token, span)| {
                let kind = match token {
                    None => TokenKind::Error,
                    Some(token) => {
                        let token = token as TokenType;
                        if map.get_token(token).unwrap().skip() {
                            TokenKind::Skip(token)
                        } else {
                            TokenKind::Normal(token)
                        }
                    }
                };
                (kind, span)
            }),
            |node| {
                map.get_node(node)
                    .map(|node| node.is_silent())
                    .unwrap_or(false)
            },
            |token| map.get_token(token).unwrap().remapped(),
        );

        let mut files = SimpleFiles::new();
        let file_id = files.add(GRAMMAR_PATH, source);
        let term_config = term::Config::default();
        let mut stderr = StandardStream::stderr(ColorChoice::Always);

        for event in parser {
            if let Err(err) = event {
                match err {
                    ParseError::Tokenizer(span) => {
                        let diagnostic = Diagnostic::error()
                            .with_message("Tokenizer error")
                            .with_labels(vec![Label::primary(file_id, span)]);
                        term::emit(&mut stderr, &term_config, &files, &diagnostic).unwrap();
                        panic!()
                    }
                    ParseError::IncorrectToken(incorrect_token) => {
                        let (_, span) = match incorrect_token.token {
                            Lookahead::Eof => (
                                Lookahead::Eof,
                                source.len().saturating_sub(1)..source.len().saturating_sub(1),
                            ),
                            Lookahead::Token((token, span)) => (Lookahead::Token(token), span),
                        };
                        let diagnostic = Diagnostic::error()
                            .with_message("Incorrect token")
                            .with_labels(vec![Label::primary(file_id, span)]);
                        term::emit(&mut stderr, &term_config, &files, &diagnostic).unwrap();
                        panic!()
                    }
                }
            }
        }
    }

    #[test]
    fn syntax_tree() {
        let source = TextRc::new_full(std::fs::read_to_string(GRAMMAR_PATH).unwrap().into());

        let (map, grammar_files) = helpers::get_grammar_map(PathBuf::from(GRAMMAR_PATH));
        let lexer = helpers::get_lexer(&map, source.as_ref());

        let grammar = map.grammar();
        let starting_states = map.nodes().filter_map(|node| {
            if node.is_silent() {
                None
            } else {
                Some(node.id())
            }
        });
        let table = match CanonicalLR::new().compute_states_with_starts(starting_states, grammar) {
            Ok(table) => table,
            Err(error) => helpers::print_expand_compute_error(&map, error),
        };
        let starting_node = map.starting_node();
        let starting_state = table.starting_state(starting_node).unwrap();
        let mut parser = lr_parser::LR1Parser::new(
            &table,
            starting_state,
            lexer.map(|(token, span)| {
                let kind = match token {
                    None => TokenKind::Error,
                    Some(token) => {
                        let token = token as TokenType;
                        if map.get_token(token).unwrap().skip() {
                            TokenKind::Skip(token)
                        } else {
                            TokenKind::Normal(token)
                        }
                    }
                };
                (kind, span)
            }),
            |node| {
                map.get_node(node)
                    .map(|node| node.is_silent())
                    .unwrap_or(false)
            },
            |token| map.get_token(token).unwrap().remapped(),
        );
        let (syntax_tree, errors) =
            SyntaxElement::from_parser(source.clone(), &mut parser, grammar, starting_node);
        if !errors.is_empty() {
            for error in errors {
                print_syntax_error(&map, 0, &grammar_files.files_manager, error);
            }
            panic!()
        }

        println!(
            "{}",
            syntax_tree.display(
                &|f, node| std::fmt::Display::fmt(&node.lhs, f),
                &|f, token| std::fmt::Display::fmt(token, f)
            )
        );
    }
}
