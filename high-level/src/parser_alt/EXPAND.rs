#[derive(Default)]
struct Context {}

mod verbatim_utils {
    pub(super) enum UnitOrNumber {
        Unit,
        Number(usize),
    }
    impl From<usize> for UnitOrNumber {
        fn from(n: usize) -> Self {
            Self::Number(n)
        }
    }
    impl From<()> for UnitOrNumber {
        fn from(_: ()) -> Self {
            Self::Unit
        }
    }
    impl From<UnitOrNumber> for usize {
        fn from(unit_or_number: UnitOrNumber) -> Self {
            match unit_or_number {
                UnitOrNumber::Unit => 0,
                UnitOrNumber::Number(n) => n,
            }
        }
    }
}
mod verbatim {
    use super::{Context, Token};
    impl Token {
        #[inline]
        pub(super) fn callback(
            self,
            context: &mut Context,
            current_span: &str,
            remainder: &str,
        ) -> Result<(Self, usize), usize> {
            match self {
                Token::MULTILINE_COMMENT => {
                    multiline_comment(context, self, current_span, remainder)
                        .map_err(|err| usize::from(super::verbatim_utils::UnitOrNumber::from(err)))
                }
                Token::STRING => raw_string_literal(context, self, current_span, remainder)
                    .map_err(|err| usize::from(super::verbatim_utils::UnitOrNumber::from(err))),
                _ => Ok((self, 0)),
            }
        }
    }
    fn multiline_comment(
        _: &mut Context,
        token_kind: Token,
        current: &str,
        rest: &str,
    ) -> Result<(Token, usize), usize> {
        let mut level = 0;
        let mut chars_indices = current.char_indices().chain(rest.char_indices()).peekable();
        while let Some((_, c)) = chars_indices.next() {
            match c {
                '/' => {
                    if let Some((_, '*')) = chars_indices.peek() {
                        chars_indices.next();
                        level += 1;
                    }
                }
                '*' => {
                    if let Some((pos, '/')) = chars_indices.peek().copied() {
                        chars_indices.next();
                        level -= 1;
                        if level == 0 {
                            return Ok((token_kind, pos + 1));
                        }
                    }
                }
                _ => {}
            }
        }
        Err(rest.len())
    }
    fn raw_string_literal(
        _: &mut Context,
        token_kind: Token,
        current: &str,
        rest: &str,
    ) -> Result<(Token, usize), usize> {
        if !current.starts_with('r') {
            return Ok((token_kind, 0));
        }
        let hash_len = current.len().saturating_sub(2);
        let pattern = String::from("\"") + &"#".repeat(hash_len);
        if let Some(end) = rest.find(&pattern) {
            Ok((token_kind, end + pattern.len()))
        } else {
            Err(rest.len())
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, :: logos :: Logos)]
#[allow(clippy::upper_case_acronyms, non_camel_case_types)]
enum TokenWithError {
    #[regex("//[^\\n]*")]
    COMMENT,
    #[regex("/\\*")]
    MULTILINE_COMMENT,
    #[regex("[\\s]+")]
    WHITESPACE,
    #[regex("///[^\\n]*")]
    DOC_COMMENT,
    #[token("include")]
    INCLUDE_KW,
    #[token("tokens")]
    TOKENS_KW,
    #[token("regex")]
    REGEX_KW,
    #[token("external")]
    EXTERNAL_KW,
    #[token("verbatim")]
    VERBATIM_KW,
    #[token("node")]
    NODE_KW,
    #[token("test")]
    TEST_KW,
    #[token("conflict")]
    CONFLICT_KW,
    #[token("prefer")]
    PREFER_KW,
    #[token("over")]
    OVER_KW,
    #[token("lookahead")]
    LOOKAHEAD_KW,
    #[token("shift")]
    SHIFT_KW,
    #[token("reduce")]
    REDUCE_KW,
    #[token(";")]
    SEMI_COLON,
    #[token(":")]
    COLON,
    #[token("::")]
    COLON_COLON,
    #[token(",")]
    COMMA,
    #[token("*")]
    STAR,
    #[token("*_")]
    STAR_UNDERSCORE,
    #[token("_*")]
    UNDERSCORE_STAR,
    #[token("+")]
    PLUS,
    #[token("+_")]
    PLUS_UNDERSCORE,
    #[token("_+")]
    UNDERSCORE_PLUS,
    #[token("?")]
    QUESTION,
    #[token("#")]
    SHARP,
    #[token("(")]
    PAR_LEFT,
    #[token(")")]
    PAR_RIGHT,
    #[token("[")]
    BRACKET_LEFT,
    #[token("]")]
    BRACKET_RIGHT,
    #[token("{")]
    BRACE_LEFT,
    #[token("}")]
    BRACE_RIGHT,
    #[token("|")]
    BAR,
    #[token("=")]
    EQUAL,
    #[token("=>")]
    ARROW,
    #[token("_")]
    UNDERSCORE,
    #[regex("(r#[a-zA-Z_][0-9a-zA-Z_]*|[a-zA-Z_][0-9a-zA-Z_]*)")]
    ID,
    #[regex("'(?:[^'\\\\]|\\\\.)*'")]
    TOKEN_STR,
    #[regex("(\"(?:[^\"\\\\]|\\\\.)*\"|r#*\")")]
    STRING,
    #[regex("[0-9][0-9_]*")]
    INT,
    #[error]
    _ERROR,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[allow(clippy::upper_case_acronyms, non_camel_case_types)]
#[doc = " Tokens of the grammar."]
pub enum Token {
    #[doc = " A simple comment.\n\n Comments starts with `//`, and continue until the end of the line.\n\n # Example\n ```text\n // A comment !\n ```"]
    COMMENT,
    #[doc = " A comment that can span multiple lines.\n\n You can nest multiple such comments within one another.\n\n # Example\n ```text\n /* A comment on\n    multiple lines */\n\n /* /* Inner comment */\n    Outer comment */\n ```"]
    MULTILINE_COMMENT,
    #[doc = " Whitespace.\n\n This matches any of `' '`, `'\\t'`, `'\\n'` or `'\\r'`."]
    WHITESPACE,
    #[doc = " A documentation comment.\n\n Such comment can appear before most items, such as the `tokens` block, an\n individual token, a node, or a field.\n\n They will be included as documentation in the generated structures.\n\n # Example\n ```text\n /// The tokens block's documentation.\n tokens {\n \t/// An individual token's documentation.\n\t\tTOKEN: regex(\"...\")\n }\n\n /// A node's documentation.\n node Node {\n\t\t/// A field's documentation.\n \tfield: TOKEN\n }\n ```"]
    DOC_COMMENT,
    #[doc = " The `include` keyword."]
    INCLUDE_KW,
    #[doc = " The `tokens` keyword."]
    TOKENS_KW,
    #[doc = " The `regex` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    REGEX_KW,
    #[doc = " The `external` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    EXTERNAL_KW,
    #[doc = " The `verbatim` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    VERBATIM_KW,
    #[doc = " The `node` keyword."]
    NODE_KW,
    #[doc = " The `test` keyword."]
    TEST_KW,
    #[doc = " The `conflict` keyword."]
    CONFLICT_KW,
    #[doc = " The `prefer` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    PREFER_KW,
    #[doc = " The `over` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    OVER_KW,
    #[doc = " The `lookahead` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    LOOKAHEAD_KW,
    #[doc = " The `shift` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    SHIFT_KW,
    #[doc = " The `reduce` keyword.\n\n This keyword is contextual, and can be interpreted as an identifier if \n needed."]
    REDUCE_KW,
    SEMI_COLON,
    COLON,
    COLON_COLON,
    COMMA,
    STAR,
    STAR_UNDERSCORE,
    UNDERSCORE_STAR,
    PLUS,
    PLUS_UNDERSCORE,
    UNDERSCORE_PLUS,
    QUESTION,
    SHARP,
    PAR_LEFT,
    PAR_RIGHT,
    BRACKET_LEFT,
    BRACKET_RIGHT,
    BRACE_LEFT,
    BRACE_RIGHT,
    BAR,
    EQUAL,
    ARROW,
    UNDERSCORE,
    #[doc = " Identifier\n\n Can begin with, but not be only `_`.\n\n # Raw identifiers\n\n You can use a keyword by prefixing it with `r#`:\n ```text\n node R {\n     r#node: /* ... */\n }\n ```"]
    ID,
    #[doc = " Single-quoted string : `'...'`"]
    TOKEN_STR,
    #[doc = " Double-quoted string : `\"...\"`\n\n You can also use raw string literals: `r##\"...\"##`"]
    STRING,
    #[doc = " An integer number.\n\n Integers starts with `0-9`, and then contain any of the `0-9` and `_` \n characters."]
    INT,
}

#[doc = "Wrapper around a [`logos`](::logos) lexer.\n\nThis is an iterator that returns ([`TokenKind<T>`](::lr_parser::parser::TokenKind), [`Span`](::lr_parser::Span)), where the `Span` is the span of the token in the source code."]
pub struct Tokenizer<'src> {
    inner: ::logos::Lexer<'src, TokenWithError>,
    context: Context,
}

impl TokenWithError {
    fn kind(self) -> ::lr_parser::parser::TokenKind<Token> {
        use lr_parser::parser::TokenKind;
        match self {
            Self::COMMENT => TokenKind::Skip(Token::COMMENT),
            Self::MULTILINE_COMMENT => TokenKind::Skip(Token::MULTILINE_COMMENT),
            Self::WHITESPACE => TokenKind::Skip(Token::WHITESPACE),
            Self::DOC_COMMENT => TokenKind::Normal(Token::DOC_COMMENT),
            Self::INCLUDE_KW => TokenKind::Normal(Token::INCLUDE_KW),
            Self::TOKENS_KW => TokenKind::Normal(Token::TOKENS_KW),
            Self::REGEX_KW => TokenKind::Normal(Token::REGEX_KW),
            Self::EXTERNAL_KW => TokenKind::Normal(Token::EXTERNAL_KW),
            Self::VERBATIM_KW => TokenKind::Normal(Token::VERBATIM_KW),
            Self::NODE_KW => TokenKind::Normal(Token::NODE_KW),
            Self::TEST_KW => TokenKind::Normal(Token::TEST_KW),
            Self::CONFLICT_KW => TokenKind::Normal(Token::CONFLICT_KW),
            Self::PREFER_KW => TokenKind::Normal(Token::PREFER_KW),
            Self::OVER_KW => TokenKind::Normal(Token::OVER_KW),
            Self::LOOKAHEAD_KW => TokenKind::Normal(Token::LOOKAHEAD_KW),
            Self::SHIFT_KW => TokenKind::Normal(Token::SHIFT_KW),
            Self::REDUCE_KW => TokenKind::Normal(Token::REDUCE_KW),
            Self::SEMI_COLON => TokenKind::Normal(Token::SEMI_COLON),
            Self::COLON => TokenKind::Normal(Token::COLON),
            Self::COLON_COLON => TokenKind::Normal(Token::COLON_COLON),
            Self::COMMA => TokenKind::Normal(Token::COMMA),
            Self::STAR => TokenKind::Normal(Token::STAR),
            Self::STAR_UNDERSCORE => TokenKind::Normal(Token::STAR_UNDERSCORE),
            Self::UNDERSCORE_STAR => TokenKind::Normal(Token::UNDERSCORE_STAR),
            Self::PLUS => TokenKind::Normal(Token::PLUS),
            Self::PLUS_UNDERSCORE => TokenKind::Normal(Token::PLUS_UNDERSCORE),
            Self::UNDERSCORE_PLUS => TokenKind::Normal(Token::UNDERSCORE_PLUS),
            Self::QUESTION => TokenKind::Normal(Token::QUESTION),
            Self::SHARP => TokenKind::Normal(Token::SHARP),
            Self::PAR_LEFT => TokenKind::Normal(Token::PAR_LEFT),
            Self::PAR_RIGHT => TokenKind::Normal(Token::PAR_RIGHT),
            Self::BRACKET_LEFT => TokenKind::Normal(Token::BRACKET_LEFT),
            Self::BRACKET_RIGHT => TokenKind::Normal(Token::BRACKET_RIGHT),
            Self::BRACE_LEFT => TokenKind::Normal(Token::BRACE_LEFT),
            Self::BRACE_RIGHT => TokenKind::Normal(Token::BRACE_RIGHT),
            Self::BAR => TokenKind::Normal(Token::BAR),
            Self::EQUAL => TokenKind::Normal(Token::EQUAL),
            Self::ARROW => TokenKind::Normal(Token::ARROW),
            Self::UNDERSCORE => TokenKind::Normal(Token::UNDERSCORE),
            Self::ID => TokenKind::Normal(Token::ID),
            Self::TOKEN_STR => TokenKind::Normal(Token::TOKEN_STR),
            Self::STRING => TokenKind::Normal(Token::STRING),
            Self::INT => TokenKind::Normal(Token::INT),
            Self::_ERROR => TokenKind::Error,
        }
    }
}
impl Token {
    pub fn skip(self) -> bool {
        match self {
            Self::COMMENT => true,
            Self::MULTILINE_COMMENT => true,
            Self::WHITESPACE => true,
            _ => false,
        }
    }
    #[doc = "Token remapping: this is the result of using the `#[remap(...)]` attribute.\n\nSee the [token remapping](::lr_parser::LR1Parser#token-remapping) section of `lr_parser`."]
    pub fn remap(self) -> ::lr_parser::parser::RemappedToken<'static, Self> {
        use lr_parser::parser::RemappedToken;
        match self {
            Token::REGEX_KW => RemappedToken::Transform(Token::ID),
            Token::EXTERNAL_KW => RemappedToken::Transform(Token::ID),
            Token::VERBATIM_KW => RemappedToken::Transform(Token::ID),
            Token::PREFER_KW => RemappedToken::Transform(Token::ID),
            Token::OVER_KW => RemappedToken::Transform(Token::ID),
            Token::LOOKAHEAD_KW => RemappedToken::Transform(Token::ID),
            Token::SHIFT_KW => RemappedToken::Transform(Token::ID),
            Token::REDUCE_KW => RemappedToken::Transform(Token::ID),
            _ => RemappedToken::None,
        }
    }
}
impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            Token::COMMENT => "COMMENT",
            Token::MULTILINE_COMMENT => "MULTILINE_COMMENT",
            Token::WHITESPACE => "WHITESPACE",
            Token::DOC_COMMENT => "DOC_COMMENT",
            Token::INCLUDE_KW => "include",
            Token::TOKENS_KW => "tokens",
            Token::REGEX_KW => "regex",
            Token::EXTERNAL_KW => "external",
            Token::VERBATIM_KW => "verbatim",
            Token::NODE_KW => "node",
            Token::TEST_KW => "test",
            Token::CONFLICT_KW => "conflict",
            Token::PREFER_KW => "prefer",
            Token::OVER_KW => "over",
            Token::LOOKAHEAD_KW => "lookahead",
            Token::SHIFT_KW => "shift",
            Token::REDUCE_KW => "reduce",
            Token::SEMI_COLON => ";",
            Token::COLON => ":",
            Token::COLON_COLON => "::",
            Token::COMMA => ",",
            Token::STAR => "*",
            Token::STAR_UNDERSCORE => "*_",
            Token::UNDERSCORE_STAR => "_*",
            Token::PLUS => "+",
            Token::PLUS_UNDERSCORE => "+_",
            Token::UNDERSCORE_PLUS => "_+",
            Token::QUESTION => "?",
            Token::SHARP => "#",
            Token::PAR_LEFT => "(",
            Token::PAR_RIGHT => ")",
            Token::BRACKET_LEFT => "[",
            Token::BRACKET_RIGHT => "]",
            Token::BRACE_LEFT => "{",
            Token::BRACE_RIGHT => "}",
            Token::BAR => "|",
            Token::EQUAL => "=",
            Token::ARROW => "=>",
            Token::UNDERSCORE => "_",
            Token::ID => "ID",
            Token::TOKEN_STR => "TOKEN_STR",
            Token::STRING => "STRING",
            Token::INT => "INT",
        })
    }
}

impl<'src> Iterator for Tokenizer<'src> {
    type Item = (
        ::lr_parser::parser::TokenKind<Token>,
        std::ops::Range<usize>,
    );
    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        use lr_parser::parser::TokenKind;
        let mut token_kind = self.inner.next()?.kind();
        match token_kind {
            TokenKind::Normal(token) | TokenKind::Skip(token) => {
                match token.callback(
                    &mut self.context,
                    self.inner.slice(),
                    self.inner.remainder(),
                ) {
                    Ok((new_token, bump)) => {
                        self.inner.bump(bump);
                        token_kind = if new_token.skip() {
                            TokenKind::Skip(new_token)
                        } else {
                            TokenKind::Normal(new_token)
                        };
                    }
                    Err(bump) => {
                        self.inner.bump(bump);
                        token_kind = TokenKind::Error;
                    }
                }
            }
            TokenKind::Error => {}
        }
        Some((token_kind, self.inner.span()))
    }
}
impl<'src> Tokenizer<'src> {
    pub fn new(source: &'src str) -> Self {
        Self {
            inner: <TokenWithError as ::logos::Logos>::lexer(source),
            context: Context::default(),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Node {
    #[doc = " Starting node.\n\n This represents the content of a single grammar file."]
    File,
    #[doc = " One of the items that can appear at the top-level of a `File`."]
    TopLevelItem,
    #[doc = " One of the tokens in a `tokens { ... }` block."]
    Token,
    #[doc = " How is the token defined.\n\n This can either be a single quoted string, a `regex` or an `external` token.\n\n # Example\n ```text\n tokens {\n\t\tSINGLE_QUOTED: 'some_token',\n\t\tREGEX: regex(\"[a]+regex?\"),\n\t\tEXTERNAL: external\n }\n ```"]
    TokenDef,
    #[doc = " Content of a `node` block.\n\n This is what is between the `{` and `}` tokens."]
    Block,
    #[doc = " One of the variants in a block with multiple variants."]
    Variant,
    #[doc = " A repetition of `Field`s."]
    Fields,
    #[doc = " A field in a `Block`.\n\n This can be labeled or not.\n\n # Example\n ```text\n labeled_field: '+'\n (Stmt ';')* // Anonymous field\n ```"]
    Field,
    #[doc = " Label of a `Field`.\n\n This is either an identifier or an integer."]
    Label,
    #[doc = " Second part of a `Field` (after the optional `Label`)."]
    Item,
    #[doc = " Part of an `Item` stripped of the eventual modifier."]
    Primary,
    Alternative,
    #[doc = " Modifier than can be applied to an `Item`."]
    Modifier,
    #[doc = " Either a doc comment, or a `NormalAttribute`."]
    Attribute,
    #[doc = " An attribute of the form `#[...]`"]
    NormalAttribute,
    #[doc = " One of the part that can appear inside a `NormalAttribute`."]
    AttributeItem,
    #[doc = " Arguments of a function-like attribute.\n\n # Example\n ```text\n #[function_like(hello, 1, 'world')]\n                 ^^^^^^^^^^^^^^^^^ these are the AttributeArgs\n ```"]
    AttributeArgs,
    #[doc = " Either an integer, or a single-quoted/double-quoted string."]
    Literal,
    #[doc = " Value for a `test` block to test against."]
    TestValue,
    TestField,
    TestItem,
    #[doc = " References a node, or a node's variant."]
    Rule,
    #[doc = " A `conflict { ... }` block."]
    Conflict,
    #[doc = " One of the actions that can appear in a `conflict { ... }` block."]
    ConflictAction,
    #[doc = " Either an identifier, or a single-quoted string.\n\n This is used to refer to a token."]
    IdOrTokenStr,
    #[doc(hidden)]
    _25,
    #[doc(hidden)]
    _26,
    #[doc(hidden)]
    _27,
    #[doc(hidden)]
    _28,
    #[doc(hidden)]
    _29,
    #[doc(hidden)]
    _30,
    #[doc(hidden)]
    _31,
    #[doc(hidden)]
    _32,
    #[doc(hidden)]
    _33,
    #[doc(hidden)]
    _34,
    #[doc(hidden)]
    _35,
    #[doc(hidden)]
    _36,
    #[doc(hidden)]
    _37,
    #[doc(hidden)]
    _38,
    #[doc(hidden)]
    _39,
}

impl Node {
    fn silent(self) -> bool {
        match self {
            Node::File => false,
            Node::TopLevelItem => false,
            Node::Token => false,
            Node::TokenDef => false,
            Node::Block => false,
            Node::Variant => false,
            Node::Fields => false,
            Node::Field => false,
            Node::Label => false,
            Node::Item => false,
            Node::Primary => false,
            Node::Alternative => false,
            Node::Modifier => false,
            Node::Attribute => false,
            Node::NormalAttribute => false,
            Node::AttributeItem => false,
            Node::AttributeArgs => false,
            Node::Literal => false,
            Node::TestValue => false,
            Node::TestField => false,
            Node::TestItem => false,
            Node::Rule => false,
            Node::Conflict => false,
            Node::ConflictAction => false,
            Node::IdOrTokenStr => false,
            Node::_25 => true,
            Node::_26 => true,
            Node::_27 => true,
            Node::_28 => true,
            Node::_29 => true,
            Node::_30 => true,
            Node::_31 => true,
            Node::_32 => true,
            Node::_33 => true,
            Node::_34 => true,
            Node::_35 => true,
            Node::_36 => true,
            Node::_37 => true,
            Node::_38 => true,
            Node::_39 => true,
        }
    }
}
impl std::fmt::Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(match self {
            Node::File => "File",
            Node::TopLevelItem => "TopLevelItem",
            Node::Token => "Token",
            Node::TokenDef => "TokenDef",
            Node::Block => "Block",
            Node::Variant => "Variant",
            Node::Fields => "Fields",
            Node::Field => "Field",
            Node::Label => "Label",
            Node::Item => "Item",
            Node::Primary => "Primary",
            Node::Alternative => "Alternative",
            Node::Modifier => "Modifier",
            Node::Attribute => "Attribute",
            Node::NormalAttribute => "NormalAttribute",
            Node::AttributeItem => "AttributeItem",
            Node::AttributeArgs => "AttributeArgs",
            Node::Literal => "Literal",
            Node::TestValue => "TestValue",
            Node::TestField => "TestField",
            Node::TestItem => "TestItem",
            Node::Rule => "Rule",
            Node::Conflict => "Conflict",
            Node::ConflictAction => "ConflictAction",
            Node::IdOrTokenStr => "IdOrTokenStr",
            Node::_25 => "TopLevelItem*",
            Node::_26 => "Attribute*",
            Node::_27 => "(Token ',')*_",
            Node::_28 => "('verbatim' STRING)?",
            Node::_29 => "Variant+",
            Node::_30 => "Field*",
            Node::_31 => "Modifier?",
            Node::_32 => "('|' Alternative)*",
            Node::_33 => "(Alternative '|')_*",
            Node::_34 => "Item+",
            Node::_35 => "(AttributeItem ',')+_",
            Node::_36 => "(AttributeItem ',')*_",
            Node::_37 => "TestField*",
            Node::_38 => "TestItem*",
            Node::_39 => "('lookahead' ':' IdOrTokenStr)?",
        })
    }
}

#[doc = "Structure that implements [`lr_parser::grammar::Grammar`](::lr_parser::grammar::Grammar)."]
pub struct Grammar;
mod grammar_rules {
    use super::{Node, Token};
    use lr_parser::grammar::Symbol;
    pub(super) static RULES: &[Symbol<Token, Node>] = &[
        Symbol::Node(Node::TopLevelItem),
        Symbol::Node(Node::_25),
        Symbol::Node(Node::_25),
        Symbol::Token(Token::INCLUDE_KW),
        Symbol::Token(Token::STRING),
        Symbol::Token(Token::SEMI_COLON),
        Symbol::Node(Node::Attribute),
        Symbol::Node(Node::_26),
        Symbol::Node(Node::Token),
        Symbol::Token(Token::COMMA),
        Symbol::Node(Node::_27),
        Symbol::Node(Node::Token),
        Symbol::Token(Token::VERBATIM_KW),
        Symbol::Token(Token::STRING),
        Symbol::Node(Node::_26),
        Symbol::Token(Token::TOKENS_KW),
        Symbol::Token(Token::BRACE_LEFT),
        Symbol::Node(Node::_27),
        Symbol::Node(Node::_28),
        Symbol::Token(Token::BRACE_RIGHT),
        Symbol::Node(Node::_26),
        Symbol::Token(Token::NODE_KW),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::BRACE_LEFT),
        Symbol::Node(Node::Block),
        Symbol::Token(Token::BRACE_RIGHT),
        Symbol::Node(Node::_26),
        Symbol::Token(Token::TEST_KW),
        Symbol::Token(Token::STRING),
        Symbol::Node(Node::TestValue),
        Symbol::Node(Node::Conflict),
        Symbol::Node(Node::_26),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::COLON),
        Symbol::Node(Node::TokenDef),
        Symbol::Token(Token::TOKEN_STR),
        Symbol::Token(Token::REGEX_KW),
        Symbol::Token(Token::PAR_LEFT),
        Symbol::Token(Token::STRING),
        Symbol::Token(Token::PAR_RIGHT),
        Symbol::Token(Token::EXTERNAL_KW),
        Symbol::Node(Node::Fields),
        Symbol::Node(Node::Variant),
        Symbol::Node(Node::Variant),
        Symbol::Node(Node::_29),
        Symbol::Node(Node::_29),
        Symbol::Token(Token::BAR),
        Symbol::Node(Node::_26),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::ARROW),
        Symbol::Node(Node::Fields),
        Symbol::Node(Node::Field),
        Symbol::Node(Node::_30),
        Symbol::Node(Node::_30),
        Symbol::Node(Node::_26),
        Symbol::Node(Node::Item),
        Symbol::Node(Node::_26),
        Symbol::Node(Node::Label),
        Symbol::Token(Token::COLON),
        Symbol::Node(Node::Item),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::INT),
        Symbol::Node(Node::Modifier),
        Symbol::Node(Node::Primary),
        Symbol::Node(Node::_31),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::TOKEN_STR),
        Symbol::Token(Token::BAR),
        Symbol::Node(Node::Alternative),
        Symbol::Node(Node::_32),
        Symbol::Node(Node::Alternative),
        Symbol::Node(Node::_32),
        Symbol::Token(Token::PAR_LEFT),
        Symbol::Node(Node::_33),
        Symbol::Token(Token::PAR_RIGHT),
        Symbol::Node(Node::Item),
        Symbol::Node(Node::Item),
        Symbol::Node(Node::_34),
        Symbol::Node(Node::_34),
        Symbol::Token(Token::STAR),
        Symbol::Token(Token::STAR_UNDERSCORE),
        Symbol::Token(Token::UNDERSCORE_STAR),
        Symbol::Token(Token::PLUS),
        Symbol::Token(Token::PLUS_UNDERSCORE),
        Symbol::Token(Token::UNDERSCORE_PLUS),
        Symbol::Token(Token::QUESTION),
        Symbol::Token(Token::DOC_COMMENT),
        Symbol::Node(Node::NormalAttribute),
        Symbol::Node(Node::AttributeItem),
        Symbol::Token(Token::COMMA),
        Symbol::Node(Node::AttributeItem),
        Symbol::Token(Token::COMMA),
        Symbol::Node(Node::_35),
        Symbol::Node(Node::AttributeItem),
        Symbol::Token(Token::SHARP),
        Symbol::Token(Token::BRACKET_LEFT),
        Symbol::Node(Node::_35),
        Symbol::Token(Token::BRACKET_RIGHT),
        Symbol::Token(Token::ID),
        Symbol::Node(Node::Literal),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::PAR_LEFT),
        Symbol::Node(Node::AttributeArgs),
        Symbol::Token(Token::PAR_RIGHT),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::EQUAL),
        Symbol::Node(Node::Literal),
        Symbol::Node(Node::AttributeItem),
        Symbol::Token(Token::COMMA),
        Symbol::Node(Node::_36),
        Symbol::Node(Node::AttributeItem),
        Symbol::Node(Node::_36),
        Symbol::Token(Token::INT),
        Symbol::Token(Token::STRING),
        Symbol::Token(Token::TOKEN_STR),
        Symbol::Node(Node::TestField),
        Symbol::Node(Node::_37),
        Symbol::Node(Node::Rule),
        Symbol::Token(Token::BRACE_LEFT),
        Symbol::Node(Node::_37),
        Symbol::Token(Token::BRACE_RIGHT),
        Symbol::Node(Node::_26),
        Symbol::Node(Node::Label),
        Symbol::Token(Token::COLON),
        Symbol::Node(Node::TestItem),
        Symbol::Token(Token::STRING),
        Symbol::Node(Node::TestValue),
        Symbol::Node(Node::TestItem),
        Symbol::Node(Node::_38),
        Symbol::Token(Token::BRACKET_LEFT),
        Symbol::Node(Node::_38),
        Symbol::Token(Token::BRACKET_RIGHT),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::COLON_COLON),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::LOOKAHEAD_KW),
        Symbol::Token(Token::COLON),
        Symbol::Node(Node::IdOrTokenStr),
        Symbol::Token(Token::CONFLICT_KW),
        Symbol::Token(Token::BRACE_LEFT),
        Symbol::Token(Token::PREFER_KW),
        Symbol::Token(Token::COLON),
        Symbol::Node(Node::ConflictAction),
        Symbol::Token(Token::OVER_KW),
        Symbol::Token(Token::COLON),
        Symbol::Node(Node::ConflictAction),
        Symbol::Node(Node::_39),
        Symbol::Token(Token::BRACE_RIGHT),
        Symbol::Token(Token::SHIFT_KW),
        Symbol::Token(Token::PAR_LEFT),
        Symbol::Token(Token::UNDERSCORE),
        Symbol::Token(Token::PAR_RIGHT),
        Symbol::Token(Token::SHIFT_KW),
        Symbol::Token(Token::PAR_LEFT),
        Symbol::Node(Node::IdOrTokenStr),
        Symbol::Token(Token::PAR_RIGHT),
        Symbol::Token(Token::REDUCE_KW),
        Symbol::Token(Token::PAR_LEFT),
        Symbol::Node(Node::Rule),
        Symbol::Token(Token::PAR_RIGHT),
        Symbol::Token(Token::ID),
        Symbol::Token(Token::TOKEN_STR),
    ];
}
impl ::lr_parser::grammar::Grammar for Grammar {
    type Token = Token;
    type Node = Node;
    type TokensIter = std::iter::Copied<std::slice::Iter<'static, Token>>;
    type NodesIter = std::iter::Copied<std::slice::Iter<'static, Node>>;
    fn tokens(&self) -> Self::TokensIter {
        [
            Token::COMMENT,
            Token::MULTILINE_COMMENT,
            Token::WHITESPACE,
            Token::DOC_COMMENT,
            Token::INCLUDE_KW,
            Token::TOKENS_KW,
            Token::REGEX_KW,
            Token::EXTERNAL_KW,
            Token::VERBATIM_KW,
            Token::NODE_KW,
            Token::TEST_KW,
            Token::CONFLICT_KW,
            Token::PREFER_KW,
            Token::OVER_KW,
            Token::LOOKAHEAD_KW,
            Token::SHIFT_KW,
            Token::REDUCE_KW,
            Token::SEMI_COLON,
            Token::COLON,
            Token::COLON_COLON,
            Token::COMMA,
            Token::STAR,
            Token::STAR_UNDERSCORE,
            Token::UNDERSCORE_STAR,
            Token::PLUS,
            Token::PLUS_UNDERSCORE,
            Token::UNDERSCORE_PLUS,
            Token::QUESTION,
            Token::SHARP,
            Token::PAR_LEFT,
            Token::PAR_RIGHT,
            Token::BRACKET_LEFT,
            Token::BRACKET_RIGHT,
            Token::BRACE_LEFT,
            Token::BRACE_RIGHT,
            Token::BAR,
            Token::EQUAL,
            Token::ARROW,
            Token::UNDERSCORE,
            Token::ID,
            Token::TOKEN_STR,
            Token::STRING,
            Token::INT,
        ]
        .iter()
        .copied()
    }
    fn nodes(&self) -> Self::NodesIter {
        [
            Node::File,
            Node::TopLevelItem,
            Node::Token,
            Node::TokenDef,
            Node::Block,
            Node::Variant,
            Node::Fields,
            Node::Field,
            Node::Label,
            Node::Item,
            Node::Primary,
            Node::Alternative,
            Node::Modifier,
            Node::Attribute,
            Node::NormalAttribute,
            Node::AttributeItem,
            Node::AttributeArgs,
            Node::Literal,
            Node::TestValue,
            Node::TestField,
            Node::TestItem,
            Node::Rule,
            Node::Conflict,
            Node::ConflictAction,
            Node::IdOrTokenStr,
            Node::_25,
            Node::_26,
            Node::_27,
            Node::_28,
            Node::_29,
            Node::_30,
            Node::_31,
            Node::_32,
            Node::_33,
            Node::_34,
            Node::_35,
            Node::_36,
            Node::_37,
            Node::_38,
            Node::_39,
        ]
        .iter()
        .copied()
    }
    fn rules(&self) -> &'static [::lr_parser::grammar::Symbol<Token, Node>] {
        grammar_rules::RULES
    }
    fn rules_indices(&self, node: Node) -> Option<&'static [(usize, usize)]> {
        match node {
            Node::File => Some(&[(2, 3)]),
            Node::TopLevelItem => Some(&[(3, 6), (14, 20), (20, 26), (26, 30), (30, 31)]),
            Node::Token => Some(&[(31, 35)]),
            Node::TokenDef => Some(&[(35, 36), (36, 40), (40, 41)]),
            Node::Block => Some(&[(41, 42), (45, 46)]),
            Node::Variant => Some(&[(46, 51)]),
            Node::Fields => Some(&[(53, 54)]),
            Node::Field => Some(&[(54, 56), (56, 60)]),
            Node::Label => Some(&[(60, 61), (61, 62)]),
            Node::Item => Some(&[(63, 65)]),
            Node::Primary => Some(&[(65, 66), (66, 67), (72, 75)]),
            Node::Alternative => Some(&[(78, 79)]),
            Node::Modifier => Some(&[
                (79, 80),
                (80, 81),
                (81, 82),
                (82, 83),
                (83, 84),
                (84, 85),
                (85, 86),
            ]),
            Node::Attribute => Some(&[(86, 87), (87, 88)]),
            Node::NormalAttribute => Some(&[(94, 98)]),
            Node::AttributeItem => Some(&[(98, 99), (99, 100), (100, 104), (104, 107)]),
            Node::AttributeArgs => Some(&[(111, 112)]),
            Node::Literal => Some(&[(112, 113), (113, 114), (114, 115)]),
            Node::TestValue => Some(&[(117, 121)]),
            Node::TestField => Some(&[(121, 125)]),
            Node::TestItem => Some(&[(125, 126), (126, 127), (129, 132)]),
            Node::Rule => Some(&[(132, 133), (133, 136)]),
            Node::Conflict => Some(&[(139, 149)]),
            Node::ConflictAction => Some(&[(149, 153), (153, 157), (157, 161)]),
            Node::IdOrTokenStr => Some(&[(161, 162), (162, 163)]),
            Node::_25 => Some(&[(0, 0), (0, 2)]),
            Node::_26 => Some(&[(6, 6), (6, 8)]),
            Node::_27 => Some(&[(8, 8), (8, 11), (11, 12)]),
            Node::_28 => Some(&[(12, 12), (12, 14)]),
            Node::_29 => Some(&[(42, 43), (43, 45)]),
            Node::_30 => Some(&[(51, 51), (51, 53)]),
            Node::_31 => Some(&[(62, 62), (62, 63)]),
            Node::_32 => Some(&[(67, 70), (70, 70)]),
            Node::_33 => Some(&[(70, 70), (70, 72)]),
            Node::_34 => Some(&[(75, 76), (76, 78)]),
            Node::_35 => Some(&[(88, 90), (90, 93), (93, 94)]),
            Node::_36 => Some(&[(107, 107), (107, 110), (110, 111)]),
            Node::_37 => Some(&[(115, 115), (115, 117)]),
            Node::_38 => Some(&[(127, 127), (127, 129)]),
            Node::_39 => Some(&[(136, 136), (136, 139)]),
            _ => None,
        }
    }
    fn conflict_solutions(&self) -> &'static [::lr_parser::grammar::ConflictSolution<Token, Node>] {
        use lr_parser::grammar::{ConflictSolution, ConflictingAction, RuleIdx};
        &[]
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ParserState {
    S0,
    S1,
    S2,
    S3,
    S4,
    S5,
    S6,
    S7,
    S8,
    S9,
    S10,
    S11,
    S12,
    S13,
    S14,
    S15,
    S16,
    S17,
    S18,
    S19,
    S20,
    S21,
    S22,
    S23,
    S24,
    S25,
    S26,
    S27,
    S28,
    S29,
    S30,
    S31,
    S32,
    S33,
    S34,
    S35,
    S36,
    S37,
    S38,
    S39,
    S40,
    S41,
    S42,
    S43,
    S44,
    S45,
    S46,
    S47,
    S48,
    S49,
    S50,
    S51,
    S52,
    S53,
    S54,
    S55,
    S56,
    S57,
    S58,
    S59,
    S60,
    S61,
    S62,
    S63,
    S64,
    S65,
    S66,
    S67,
    S68,
    S69,
    S70,
    S71,
    S72,
    S73,
    S74,
    S75,
    S76,
    S77,
    S78,
    S79,
    S80,
    S81,
    S82,
    S83,
    S84,
    S85,
    S86,
    S87,
    S88,
    S89,
    S90,
    S91,
    S92,
    S93,
    S94,
    S95,
    S96,
    S97,
    S98,
    S99,
    S100,
    S101,
    S102,
    S103,
    S104,
    S105,
    S106,
    S107,
    S108,
    S109,
    S110,
    S111,
    S112,
    S113,
    S114,
    S115,
    S116,
    S117,
    S118,
    S119,
    S120,
    S121,
    S122,
    S123,
    S124,
    S125,
    S126,
    S127,
    S128,
    S129,
    S130,
    S131,
    S132,
    S133,
    S134,
    S135,
    S136,
    S137,
    S138,
    S139,
    S140,
    S141,
    S142,
    S143,
    S144,
    S145,
    S146,
    S147,
    S148,
    S149,
    S150,
    S151,
    S152,
    S153,
    S154,
    S155,
    S156,
    S157,
    S158,
    S159,
    S160,
    S161,
    S162,
    S163,
    S164,
    S165,
    S166,
    S167,
    S168,
    S169,
    S170,
    S171,
    S172,
    S173,
    S174,
    S175,
    S176,
    S177,
    S178,
    S179,
    S180,
    S181,
    S182,
    S183,
    S184,
    S185,
    S186,
    S187,
    S188,
    S189,
    S190,
    S191,
    S192,
    S193,
    S194,
    S195,
    S196,
    S197,
    S198,
    S199,
    S200,
    S201,
    S202,
    S203,
    S204,
    S205,
    S206,
    S207,
    S208,
    S209,
    S210,
    S211,
    S212,
    S213,
    S214,
    S215,
    S216,
    S217,
    S218,
    S219,
    S220,
    S221,
    S222,
    S223,
    S224,
    S225,
    S226,
    S227,
    S228,
    S229,
    S230,
    S231,
    S232,
    S233,
    S234,
    S235,
    S236,
    S237,
    S238,
    S239,
    S240,
    S241,
    S242,
    S243,
    S244,
    S245,
    S246,
    S247,
    S248,
    S249,
    S250,
    S251,
    S252,
    S253,
    S254,
    S255,
    S256,
    S257,
    S258,
    S259,
    S260,
    S261,
    S262,
    S263,
    S264,
    S265,
    S266,
    S267,
    S268,
    S269,
    S270,
    S271,
    S272,
    S273,
    S274,
    S275,
    S276,
    S277,
    S278,
    S279,
    S280,
    S281,
    S282,
    S283,
    S284,
    S285,
    S286,
    S287,
    S288,
    S289,
    S290,
    S291,
    S292,
    S293,
    S294,
    S295,
    S296,
    S297,
    S298,
    S299,
    S300,
    S301,
    S302,
    S303,
    S304,
    S305,
    S306,
    S307,
    S308,
    S309,
    S310,
    S311,
    S312,
    S313,
    S314,
    S315,
    S316,
    S317,
    S318,
    S319,
    S320,
    S321,
    S322,
    S323,
    S324,
    S325,
    S326,
    S327,
    S328,
    S329,
    S330,
    S331,
    S332,
    S333,
    S334,
    S335,
    S336,
    S337,
    S338,
    S339,
    S340,
    S341,
    S342,
    S343,
    S344,
    S345,
    S346,
    S347,
    S348,
    S349,
    S350,
    S351,
    S352,
    S353,
    S354,
    S355,
    S356,
    S357,
    S358,
    S359,
    S360,
    S361,
    S362,
    S363,
    S364,
    S365,
    S366,
    S367,
    S368,
    S369,
    S370,
    S371,
    S372,
    S373,
    S374,
    S375,
    S376,
    S377,
    S378,
    S379,
    S380,
    S381,
    S382,
    S383,
    S384,
    S385,
    S386,
    S387,
    S388,
    S389,
    S390,
    S391,
    S392,
    S393,
    S394,
    S395,
    S396,
    S397,
    S398,
    S399,
    S400,
    S401,
    S402,
    S403,
    S404,
    S405,
    S406,
    S407,
    S408,
    S409,
    S410,
    S411,
    S412,
    S413,
    S414,
    S415,
    S416,
    S417,
    S418,
    S419,
    S420,
    S421,
    S422,
    S423,
    S424,
    S425,
    S426,
    S427,
    S428,
    S429,
    S430,
    S431,
    S432,
    S433,
    S434,
    S435,
    S436,
    S437,
    S438,
    S439,
    S440,
    S441,
    S442,
    S443,
    S444,
    S445,
    S446,
    S447,
    S448,
    S449,
    S450,
    S451,
    S452,
    S453,
    S454,
    S455,
    S456,
    S457,
    S458,
    S459,
    S460,
    S461,
    S462,
    S463,
    S464,
    S465,
    S466,
    S467,
    S468,
    S469,
    S470,
    S471,
    S472,
    S473,
    S474,
    S475,
    S476,
    S477,
    S478,
    S479,
    S480,
    S481,
    S482,
    S483,
    S484,
    S485,
    S486,
    S487,
    S488,
    S489,
    S490,
    S491,
    S492,
    S493,
    S494,
    S495,
    S496,
    S497,
    S498,
    S499,
    S500,
    S501,
    S502,
    S503,
}
#[doc = "Transition table for the [`Parser`].\n\nThis implements the [`lr_parser::compute::TransitionTable`](::lr_parser::compute::TransitionTable) trait."]
pub struct TransitionTable;
impl ::lr_parser::compute::TransitionTable for TransitionTable {
    type State = ParserState;
    type Token = Token;
    type Node = Node;
    type StatesIter = std::iter::Copied<std::slice::Iter<'static, ParserState>>;
    type Lookaheads =
        std::iter::Copied<std::slice::Iter<'static, ::lr_parser::grammar::Lookahead<Token>>>;
    fn starting_state(&self, node: Self::Node) -> Option<Self::State> {
        use ParserState::*;
        match node {
            Node::File => Some(S0),
            Node::TopLevelItem => Some(S252),
            Node::Token => Some(S286),
            Node::TokenDef => Some(S298),
            Node::Block => Some(S300),
            Node::Variant => Some(S359),
            Node::Fields => Some(S366),
            Node::Field => Some(S368),
            Node::Label => Some(S391),
            Node::Item => Some(S395),
            Node::Primary => Some(S397),
            Node::Alternative => Some(S404),
            Node::Modifier => Some(S424),
            Node::Attribute => Some(S426),
            Node::NormalAttribute => Some(S434),
            Node::AttributeItem => Some(S436),
            Node::AttributeArgs => Some(S448),
            Node::Literal => Some(S464),
            Node::TestValue => Some(S466),
            Node::TestField => Some(S468),
            Node::TestItem => Some(S479),
            Node::Rule => Some(S481),
            Node::Conflict => Some(S486),
            Node::ConflictAction => Some(S488),
            Node::IdOrTokenStr => Some(S500),
            _ => None,
        }
    }
    fn states(&self) -> Self::StatesIter {
        use ParserState::*;
        [
            S0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14, S15, S16, S17, S18,
            S19, S20, S21, S22, S23, S24, S25, S26, S27, S28, S29, S30, S31, S32, S33, S34, S35,
            S36, S37, S38, S39, S40, S41, S42, S43, S44, S45, S46, S47, S48, S49, S50, S51, S52,
            S53, S54, S55, S56, S57, S58, S59, S60, S61, S62, S63, S64, S65, S66, S67, S68, S69,
            S70, S71, S72, S73, S74, S75, S76, S77, S78, S79, S80, S81, S82, S83, S84, S85, S86,
            S87, S88, S89, S90, S91, S92, S93, S94, S95, S96, S97, S98, S99, S100, S101, S102,
            S103, S104, S105, S106, S107, S108, S109, S110, S111, S112, S113, S114, S115, S116,
            S117, S118, S119, S120, S121, S122, S123, S124, S125, S126, S127, S128, S129, S130,
            S131, S132, S133, S134, S135, S136, S137, S138, S139, S140, S141, S142, S143, S144,
            S145, S146, S147, S148, S149, S150, S151, S152, S153, S154, S155, S156, S157, S158,
            S159, S160, S161, S162, S163, S164, S165, S166, S167, S168, S169, S170, S171, S172,
            S173, S174, S175, S176, S177, S178, S179, S180, S181, S182, S183, S184, S185, S186,
            S187, S188, S189, S190, S191, S192, S193, S194, S195, S196, S197, S198, S199, S200,
            S201, S202, S203, S204, S205, S206, S207, S208, S209, S210, S211, S212, S213, S214,
            S215, S216, S217, S218, S219, S220, S221, S222, S223, S224, S225, S226, S227, S228,
            S229, S230, S231, S232, S233, S234, S235, S236, S237, S238, S239, S240, S241, S242,
            S243, S244, S245, S246, S247, S248, S249, S250, S251, S252, S253, S254, S255, S256,
            S257, S258, S259, S260, S261, S262, S263, S264, S265, S266, S267, S268, S269, S270,
            S271, S272, S273, S274, S275, S276, S277, S278, S279, S280, S281, S282, S283, S284,
            S285, S286, S287, S288, S289, S290, S291, S292, S293, S294, S295, S296, S297, S298,
            S299, S300, S301, S302, S303, S304, S305, S306, S307, S308, S309, S310, S311, S312,
            S313, S314, S315, S316, S317, S318, S319, S320, S321, S322, S323, S324, S325, S326,
            S327, S328, S329, S330, S331, S332, S333, S334, S335, S336, S337, S338, S339, S340,
            S341, S342, S343, S344, S345, S346, S347, S348, S349, S350, S351, S352, S353, S354,
            S355, S356, S357, S358, S359, S360, S361, S362, S363, S364, S365, S366, S367, S368,
            S369, S370, S371, S372, S373, S374, S375, S376, S377, S378, S379, S380, S381, S382,
            S383, S384, S385, S386, S387, S388, S389, S390, S391, S392, S393, S394, S395, S396,
            S397, S398, S399, S400, S401, S402, S403, S404, S405, S406, S407, S408, S409, S410,
            S411, S412, S413, S414, S415, S416, S417, S418, S419, S420, S421, S422, S423, S424,
            S425, S426, S427, S428, S429, S430, S431, S432, S433, S434, S435, S436, S437, S438,
            S439, S440, S441, S442, S443, S444, S445, S446, S447, S448, S449, S450, S451, S452,
            S453, S454, S455, S456, S457, S458, S459, S460, S461, S462, S463, S464, S465, S466,
            S467, S468, S469, S470, S471, S472, S473, S474, S475, S476, S477, S478, S479, S480,
            S481, S482, S483, S484, S485, S486, S487, S488, S489, S490, S491, S492, S493, S494,
            S495, S496, S497, S498, S499, S500, S501, S502, S503,
        ]
        .iter()
        .copied()
    }
    fn state_rules(&self, state: Self::State) -> &[lr_parser::compute::IndexedRule<Token, Node>] {
        use lr_parser::{
            compute::IndexedRule,
            grammar::{Lookahead, RuleIdx},
        };
        use Lookahead::{Eof, Token as LookT};
        use Node as N;
        use ParserState::*;
        use Token as T;
        #[allow(non_snake_case)]
        macro_rules! Norm {
            ($ lhs : expr , $ rule_idx_index : expr , $ index : expr , $ lookahead : expr) => {
                IndexedRule::Normal {
                    rule_idx: RuleIdx {
                        lhs: $lhs,
                        index: $rule_idx_index,
                    },
                    index: $index,
                    lookahead: $lookahead,
                }
            };
        }
        match state {
            S0 => &[IndexedRule::Start {
                node: N::File,
                index: 0,
            }],
            S1 => &[
                Norm!(N::TopLevelItem, 0, 1, Eof),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 0, 1, LookT(T::SHARP)),
            ],
            S2 => &[
                Norm!(N::TopLevelItem, 4, 1, Eof),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 4, 1, LookT(T::SHARP)),
            ],
            S3 => &[IndexedRule::Start {
                node: N::File,
                index: 1,
            }],
            S4 => &[Norm!(N::_25, 1, 1, Eof)],
            S5 => &[
                Norm!(N::Attribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 0, 1, LookT(T::TOKENS_KW)),
                Norm!(N::Attribute, 0, 1, LookT(T::NODE_KW)),
                Norm!(N::Attribute, 0, 1, LookT(T::TEST_KW)),
                Norm!(N::Attribute, 0, 1, LookT(T::SHARP)),
            ],
            S6 => &[
                Norm!(N::_26, 1, 1, LookT(T::TOKENS_KW)),
                Norm!(N::_26, 1, 1, LookT(T::NODE_KW)),
                Norm!(N::_26, 1, 1, LookT(T::TEST_KW)),
            ],
            S7 => &[
                Norm!(N::Attribute, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 1, 1, LookT(T::TOKENS_KW)),
                Norm!(N::Attribute, 1, 1, LookT(T::NODE_KW)),
                Norm!(N::Attribute, 1, 1, LookT(T::TEST_KW)),
                Norm!(N::Attribute, 1, 1, LookT(T::SHARP)),
            ],
            S8 => &[
                Norm!(N::NormalAttribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::TOKENS_KW)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::NODE_KW)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::TEST_KW)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::SHARP)),
            ],
            S9 => &[Norm!(N::File, 0, 1, Eof)],
            S10 => &[
                Norm!(N::TopLevelItem, 1, 1, Eof),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 1, 1, LookT(T::SHARP)),
                Norm!(N::TopLevelItem, 2, 1, Eof),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 2, 1, LookT(T::SHARP)),
                Norm!(N::TopLevelItem, 3, 1, Eof),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 3, 1, LookT(T::SHARP)),
            ],
            S11 => &[
                Norm!(N::Conflict, 0, 1, Eof),
                Norm!(N::Conflict, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 1, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 1, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 1, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 1, LookT(T::SHARP)),
            ],
            S12 => &[
                Norm!(N::TopLevelItem, 0, 2, Eof),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 0, 2, LookT(T::SHARP)),
            ],
            S13 => &[Norm!(N::_25, 1, 2, Eof)],
            S14 => &[
                Norm!(N::_26, 1, 2, LookT(T::TOKENS_KW)),
                Norm!(N::_26, 1, 2, LookT(T::NODE_KW)),
                Norm!(N::_26, 1, 2, LookT(T::TEST_KW)),
            ],
            S15 => &[
                Norm!(N::NormalAttribute, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::TOKENS_KW)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::NODE_KW)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::TEST_KW)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::SHARP)),
            ],
            S16 => &[
                Norm!(N::TopLevelItem, 1, 2, Eof),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 1, 2, LookT(T::SHARP)),
            ],
            S17 => &[
                Norm!(N::TopLevelItem, 2, 2, Eof),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 2, 2, LookT(T::SHARP)),
            ],
            S18 => &[
                Norm!(N::TopLevelItem, 3, 2, Eof),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 3, 2, LookT(T::SHARP)),
            ],
            S19 => &[
                Norm!(N::Conflict, 0, 2, Eof),
                Norm!(N::Conflict, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 2, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 2, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 2, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 2, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 2, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 2, LookT(T::SHARP)),
            ],
            S20 => &[
                Norm!(N::TopLevelItem, 0, 3, Eof),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 0, 3, LookT(T::SHARP)),
            ],
            S21 => &[
                Norm!(N::NormalAttribute, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::TOKENS_KW)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::NODE_KW)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::TEST_KW)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::SHARP)),
            ],
            S22 => &[
                Norm!(N::AttributeItem, 0, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 0, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::AttributeItem, 2, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::AttributeItem, 3, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 1, LookT(T::BRACKET_RIGHT)),
            ],
            S23 => &[
                Norm!(N::_35, 0, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::_35, 1, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::_35, 2, 1, LookT(T::BRACKET_RIGHT)),
            ],
            S24 => &[
                Norm!(N::Literal, 1, 1, LookT(T::COMMA)),
                Norm!(N::Literal, 1, 1, LookT(T::BRACKET_RIGHT)),
            ],
            S25 => &[
                Norm!(N::Literal, 2, 1, LookT(T::COMMA)),
                Norm!(N::Literal, 2, 1, LookT(T::BRACKET_RIGHT)),
            ],
            S26 => &[
                Norm!(N::AttributeItem, 1, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 1, 1, LookT(T::BRACKET_RIGHT)),
            ],
            S27 => &[
                Norm!(N::Literal, 0, 1, LookT(T::COMMA)),
                Norm!(N::Literal, 0, 1, LookT(T::BRACKET_RIGHT)),
            ],
            S28 => &[
                Norm!(N::TopLevelItem, 1, 3, Eof),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 1, 3, LookT(T::SHARP)),
            ],
            S29 => &[
                Norm!(N::TopLevelItem, 2, 3, Eof),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 2, 3, LookT(T::SHARP)),
            ],
            S30 => &[
                Norm!(N::TopLevelItem, 3, 3, Eof),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 3, 3, LookT(T::SHARP)),
            ],
            S31 => &[
                Norm!(N::Conflict, 0, 3, Eof),
                Norm!(N::Conflict, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 3, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 3, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 3, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 3, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 3, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 3, LookT(T::SHARP)),
            ],
            S32 => &[
                Norm!(N::NormalAttribute, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::TOKENS_KW)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::NODE_KW)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::TEST_KW)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::SHARP)),
            ],
            S33 => &[
                Norm!(N::AttributeItem, 2, 2, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 2, LookT(T::BRACKET_RIGHT)),
            ],
            S34 => &[
                Norm!(N::AttributeItem, 3, 2, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 2, LookT(T::BRACKET_RIGHT)),
            ],
            S35 => &[
                Norm!(N::_35, 0, 2, LookT(T::BRACKET_RIGHT)),
                Norm!(N::_35, 1, 2, LookT(T::BRACKET_RIGHT)),
            ],
            S36 => &[
                Norm!(N::TopLevelItem, 1, 4, Eof),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 1, 4, LookT(T::SHARP)),
            ],
            S37 => &[
                Norm!(N::NormalAttribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::ID)),
            ],
            S38 => &[
                Norm!(N::_27, 1, 1, LookT(T::VERBATIM_KW)),
                Norm!(N::_27, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::_27, 2, 1, LookT(T::VERBATIM_KW)),
                Norm!(N::_27, 2, 1, LookT(T::BRACE_RIGHT)),
            ],
            S39 => &[Norm!(N::_26, 1, 1, LookT(T::ID))],
            S40 => &[
                Norm!(N::Attribute, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 1, 1, LookT(T::SHARP)),
                Norm!(N::Attribute, 1, 1, LookT(T::ID)),
            ],
            S41 => &[
                Norm!(N::Attribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 0, 1, LookT(T::SHARP)),
                Norm!(N::Attribute, 0, 1, LookT(T::ID)),
            ],
            S42 => &[
                Norm!(N::Token, 0, 1, LookT(T::VERBATIM_KW)),
                Norm!(N::Token, 0, 1, LookT(T::COMMA)),
                Norm!(N::Token, 0, 1, LookT(T::BRACE_RIGHT)),
            ],
            S43 => &[
                Norm!(N::TopLevelItem, 2, 4, Eof),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 2, 4, LookT(T::SHARP)),
            ],
            S44 => &[
                Norm!(N::TestValue, 0, 1, Eof),
                Norm!(N::TestValue, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 1, LookT(T::INCLUDE_KW)),
                Norm!(N::TestValue, 0, 1, LookT(T::TOKENS_KW)),
                Norm!(N::TestValue, 0, 1, LookT(T::NODE_KW)),
                Norm!(N::TestValue, 0, 1, LookT(T::TEST_KW)),
                Norm!(N::TestValue, 0, 1, LookT(T::CONFLICT_KW)),
                Norm!(N::TestValue, 0, 1, LookT(T::SHARP)),
            ],
            S45 => &[
                Norm!(N::Rule, 0, 1, LookT(T::BRACE_LEFT)),
                Norm!(N::Rule, 1, 1, LookT(T::BRACE_LEFT)),
            ],
            S46 => &[
                Norm!(N::TopLevelItem, 3, 4, Eof),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 3, 4, LookT(T::SHARP)),
            ],
            S47 => &[
                Norm!(N::Conflict, 0, 4, Eof),
                Norm!(N::Conflict, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 4, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 4, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 4, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 4, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 4, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 4, LookT(T::SHARP)),
            ],
            S48 => &[Norm!(N::AttributeArgs, 0, 1, LookT(T::PAR_RIGHT))],
            S49 => &[
                Norm!(N::AttributeItem, 0, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::AttributeItem, 2, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::AttributeItem, 3, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 1, LookT(T::PAR_RIGHT)),
            ],
            S50 => &[
                Norm!(N::AttributeItem, 2, 3, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 3, LookT(T::BRACKET_RIGHT)),
            ],
            S51 => &[
                Norm!(N::AttributeItem, 1, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 1, 1, LookT(T::PAR_RIGHT)),
            ],
            S52 => &[
                Norm!(N::_36, 1, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::_36, 2, 1, LookT(T::PAR_RIGHT)),
            ],
            S53 => &[
                Norm!(N::Literal, 1, 1, LookT(T::COMMA)),
                Norm!(N::Literal, 1, 1, LookT(T::PAR_RIGHT)),
            ],
            S54 => &[
                Norm!(N::Literal, 2, 1, LookT(T::COMMA)),
                Norm!(N::Literal, 2, 1, LookT(T::PAR_RIGHT)),
            ],
            S55 => &[
                Norm!(N::Literal, 0, 1, LookT(T::COMMA)),
                Norm!(N::Literal, 0, 1, LookT(T::PAR_RIGHT)),
            ],
            S56 => &[
                Norm!(N::AttributeItem, 3, 3, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 3, LookT(T::BRACKET_RIGHT)),
            ],
            S57 => &[Norm!(N::_35, 1, 3, LookT(T::BRACKET_RIGHT))],
            S58 => &[
                Norm!(N::TopLevelItem, 1, 5, Eof),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 1, 5, LookT(T::SHARP)),
            ],
            S59 => &[Norm!(N::_28, 1, 1, LookT(T::BRACE_RIGHT))],
            S60 => &[
                Norm!(N::NormalAttribute, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::ID)),
            ],
            S61 => &[
                Norm!(N::_27, 1, 2, LookT(T::VERBATIM_KW)),
                Norm!(N::_27, 1, 2, LookT(T::BRACE_RIGHT)),
            ],
            S62 => &[Norm!(N::_26, 1, 2, LookT(T::ID))],
            S63 => &[
                Norm!(N::Token, 0, 2, LookT(T::VERBATIM_KW)),
                Norm!(N::Token, 0, 2, LookT(T::COMMA)),
                Norm!(N::Token, 0, 2, LookT(T::BRACE_RIGHT)),
            ],
            S64 => &[
                Norm!(N::TopLevelItem, 2, 5, Eof),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 2, 5, LookT(T::SHARP)),
            ],
            S65 => &[
                Norm!(N::_29, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::_29, 1, 1, LookT(T::BRACE_RIGHT)),
            ],
            S66 => &[Norm!(N::Block, 0, 1, LookT(T::BRACE_RIGHT))],
            S67 => &[Norm!(N::_30, 1, 1, LookT(T::BRACE_RIGHT))],
            S68 => &[
                Norm!(N::Variant, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Variant, 0, 1, LookT(T::BAR)),
            ],
            S69 => &[
                Norm!(N::Attribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 0, 1, LookT(T::SHARP)),
                Norm!(N::Attribute, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Attribute, 0, 1, LookT(T::ID)),
                Norm!(N::Attribute, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Attribute, 0, 1, LookT(T::INT)),
            ],
            S70 => &[
                Norm!(N::NormalAttribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::INT)),
            ],
            S71 => &[
                Norm!(N::Attribute, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 1, 1, LookT(T::SHARP)),
                Norm!(N::Attribute, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Attribute, 1, 1, LookT(T::ID)),
                Norm!(N::Attribute, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Attribute, 1, 1, LookT(T::INT)),
            ],
            S72 => &[Norm!(N::Fields, 0, 1, LookT(T::BRACE_RIGHT))],
            S73 => &[
                Norm!(N::_26, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_26, 1, 1, LookT(T::ID)),
                Norm!(N::_26, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::_26, 1, 1, LookT(T::INT)),
            ],
            S74 => &[Norm!(N::Block, 1, 1, LookT(T::BRACE_RIGHT))],
            S75 => &[
                Norm!(N::Field, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 1, LookT(T::SHARP)),
                Norm!(N::Field, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 0, 1, LookT(T::ID)),
                Norm!(N::Field, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 1, LookT(T::INT)),
                Norm!(N::Field, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 1, LookT(T::SHARP)),
                Norm!(N::Field, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 1, LookT(T::ID)),
                Norm!(N::Field, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 1, LookT(T::INT)),
            ],
            S76 => &[
                Norm!(N::TestValue, 0, 2, Eof),
                Norm!(N::TestValue, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 2, LookT(T::INCLUDE_KW)),
                Norm!(N::TestValue, 0, 2, LookT(T::TOKENS_KW)),
                Norm!(N::TestValue, 0, 2, LookT(T::NODE_KW)),
                Norm!(N::TestValue, 0, 2, LookT(T::TEST_KW)),
                Norm!(N::TestValue, 0, 2, LookT(T::CONFLICT_KW)),
                Norm!(N::TestValue, 0, 2, LookT(T::SHARP)),
            ],
            S77 => &[Norm!(N::Rule, 1, 2, LookT(T::BRACE_LEFT))],
            S78 => &[Norm!(N::ConflictAction, 2, 1, LookT(T::OVER_KW))],
            S79 => &[
                Norm!(N::ConflictAction, 0, 1, LookT(T::OVER_KW)),
                Norm!(N::ConflictAction, 1, 1, LookT(T::OVER_KW)),
            ],
            S80 => &[
                Norm!(N::Conflict, 0, 5, Eof),
                Norm!(N::Conflict, 0, 5, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 5, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 5, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 5, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 5, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 5, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 5, LookT(T::SHARP)),
            ],
            S81 => &[
                Norm!(N::AttributeItem, 2, 2, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 2, LookT(T::PAR_RIGHT)),
            ],
            S82 => &[
                Norm!(N::AttributeItem, 3, 2, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 2, LookT(T::PAR_RIGHT)),
            ],
            S83 => &[
                Norm!(N::AttributeItem, 2, 4, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 4, LookT(T::BRACKET_RIGHT)),
            ],
            S84 => &[Norm!(N::_36, 1, 2, LookT(T::PAR_RIGHT))],
            S85 => &[
                Norm!(N::TopLevelItem, 1, 6, Eof),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 1, 6, LookT(T::SHARP)),
            ],
            S86 => &[Norm!(N::_28, 1, 2, LookT(T::BRACE_RIGHT))],
            S87 => &[
                Norm!(N::NormalAttribute, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::ID)),
            ],
            S88 => &[
                Norm!(N::_27, 1, 3, LookT(T::VERBATIM_KW)),
                Norm!(N::_27, 1, 3, LookT(T::BRACE_RIGHT)),
            ],
            S89 => &[
                Norm!(N::Token, 0, 3, LookT(T::VERBATIM_KW)),
                Norm!(N::Token, 0, 3, LookT(T::COMMA)),
                Norm!(N::Token, 0, 3, LookT(T::BRACE_RIGHT)),
            ],
            S90 => &[
                Norm!(N::TopLevelItem, 2, 6, Eof),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::DOC_COMMENT)),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::INCLUDE_KW)),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::TOKENS_KW)),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::NODE_KW)),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::TEST_KW)),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::CONFLICT_KW)),
                Norm!(N::TopLevelItem, 2, 6, LookT(T::SHARP)),
            ],
            S91 => &[Norm!(N::_29, 1, 2, LookT(T::BRACE_RIGHT))],
            S92 => &[Norm!(N::_30, 1, 2, LookT(T::BRACE_RIGHT))],
            S93 => &[
                Norm!(N::Variant, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Variant, 0, 2, LookT(T::BAR)),
            ],
            S94 => &[
                Norm!(N::NormalAttribute, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::INT)),
            ],
            S95 => &[
                Norm!(N::_26, 1, 2, LookT(T::PAR_LEFT)),
                Norm!(N::_26, 1, 2, LookT(T::ID)),
                Norm!(N::_26, 1, 2, LookT(T::TOKEN_STR)),
                Norm!(N::_26, 1, 2, LookT(T::INT)),
            ],
            S96 => &[
                Norm!(N::Primary, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 2, 1, LookT(T::ID)),
                Norm!(N::Primary, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 1, LookT(T::INT)),
            ],
            S97 => &[
                Norm!(N::Item, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 1, LookT(T::SHARP)),
                Norm!(N::Item, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Item, 0, 1, LookT(T::ID)),
                Norm!(N::Item, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 1, LookT(T::INT)),
            ],
            S98 => &[
                Norm!(N::Label, 0, 1, LookT(T::COLON)),
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S99 => &[
                Norm!(N::Field, 1, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 2, LookT(T::SHARP)),
                Norm!(N::Field, 1, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 2, LookT(T::ID)),
                Norm!(N::Field, 1, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 2, LookT(T::INT)),
            ],
            S100 => &[
                Norm!(N::Field, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 2, LookT(T::SHARP)),
                Norm!(N::Field, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 0, 2, LookT(T::ID)),
                Norm!(N::Field, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 2, LookT(T::INT)),
            ],
            S101 => &[
                Norm!(N::Primary, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 1, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 1, 1, LookT(T::ID)),
                Norm!(N::Primary, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 1, 1, LookT(T::INT)),
            ],
            S102 => &[Norm!(N::Label, 1, 1, LookT(T::COLON))],
            S103 => &[
                Norm!(N::Attribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 0, 1, LookT(T::SHARP)),
                Norm!(N::Attribute, 0, 1, LookT(T::ID)),
                Norm!(N::Attribute, 0, 1, LookT(T::INT)),
            ],
            S104 => &[
                Norm!(N::TestValue, 0, 3, Eof),
                Norm!(N::TestValue, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 3, LookT(T::INCLUDE_KW)),
                Norm!(N::TestValue, 0, 3, LookT(T::TOKENS_KW)),
                Norm!(N::TestValue, 0, 3, LookT(T::NODE_KW)),
                Norm!(N::TestValue, 0, 3, LookT(T::TEST_KW)),
                Norm!(N::TestValue, 0, 3, LookT(T::CONFLICT_KW)),
                Norm!(N::TestValue, 0, 3, LookT(T::SHARP)),
            ],
            S105 => &[
                Norm!(N::_26, 1, 1, LookT(T::ID)),
                Norm!(N::_26, 1, 1, LookT(T::INT)),
            ],
            S106 => &[
                Norm!(N::NormalAttribute, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 1, LookT(T::INT)),
            ],
            S107 => &[
                Norm!(N::Attribute, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Attribute, 1, 1, LookT(T::SHARP)),
                Norm!(N::Attribute, 1, 1, LookT(T::ID)),
                Norm!(N::Attribute, 1, 1, LookT(T::INT)),
            ],
            S108 => &[
                Norm!(N::TestField, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TestField, 0, 1, LookT(T::SHARP)),
                Norm!(N::TestField, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestField, 0, 1, LookT(T::ID)),
                Norm!(N::TestField, 0, 1, LookT(T::INT)),
            ],
            S109 => &[Norm!(N::_37, 1, 1, LookT(T::BRACE_RIGHT))],
            S110 => &[Norm!(N::Rule, 1, 3, LookT(T::BRACE_LEFT))],
            S111 => &[Norm!(N::ConflictAction, 2, 2, LookT(T::OVER_KW))],
            S112 => &[
                Norm!(N::ConflictAction, 0, 2, LookT(T::OVER_KW)),
                Norm!(N::ConflictAction, 1, 2, LookT(T::OVER_KW)),
            ],
            S113 => &[
                Norm!(N::Conflict, 0, 6, Eof),
                Norm!(N::Conflict, 0, 6, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 6, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 6, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 6, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 6, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 6, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 6, LookT(T::SHARP)),
            ],
            S114 => &[
                Norm!(N::AttributeItem, 2, 3, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 3, LookT(T::PAR_RIGHT)),
            ],
            S115 => &[
                Norm!(N::AttributeItem, 3, 3, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 3, LookT(T::PAR_RIGHT)),
            ],
            S116 => &[Norm!(N::_36, 1, 3, LookT(T::PAR_RIGHT))],
            S117 => &[
                Norm!(N::NormalAttribute, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::ID)),
            ],
            S118 => &[
                Norm!(N::TokenDef, 2, 1, LookT(T::VERBATIM_KW)),
                Norm!(N::TokenDef, 2, 1, LookT(T::COMMA)),
                Norm!(N::TokenDef, 2, 1, LookT(T::BRACE_RIGHT)),
            ],
            S119 => &[
                Norm!(N::TokenDef, 1, 1, LookT(T::VERBATIM_KW)),
                Norm!(N::TokenDef, 1, 1, LookT(T::COMMA)),
                Norm!(N::TokenDef, 1, 1, LookT(T::BRACE_RIGHT)),
            ],
            S120 => &[
                Norm!(N::TokenDef, 0, 1, LookT(T::VERBATIM_KW)),
                Norm!(N::TokenDef, 0, 1, LookT(T::COMMA)),
                Norm!(N::TokenDef, 0, 1, LookT(T::BRACE_RIGHT)),
            ],
            S121 => &[
                Norm!(N::Token, 0, 4, LookT(T::VERBATIM_KW)),
                Norm!(N::Token, 0, 4, LookT(T::COMMA)),
                Norm!(N::Token, 0, 4, LookT(T::BRACE_RIGHT)),
            ],
            S122 => &[
                Norm!(N::Variant, 0, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::Variant, 0, 3, LookT(T::BAR)),
            ],
            S123 => &[
                Norm!(N::NormalAttribute, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::PAR_LEFT)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::TOKEN_STR)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::INT)),
            ],
            S124 => &[Norm!(N::_33, 1, 1, LookT(T::PAR_RIGHT))],
            S125 => &[
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Primary, 0, 1, LookT(T::BAR)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
            ],
            S126 => &[
                Norm!(N::Primary, 2, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 2, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 2, 2, LookT(T::ID)),
                Norm!(N::Primary, 2, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 2, LookT(T::INT)),
            ],
            S127 => &[
                Norm!(N::Alternative, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Alternative, 0, 1, LookT(T::BAR)),
            ],
            S128 => &[
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Primary, 2, 1, LookT(T::BAR)),
                Norm!(N::Primary, 2, 1, LookT(T::ID)),
                Norm!(N::Primary, 2, 1, LookT(T::TOKEN_STR)),
            ],
            S129 => &[
                Norm!(N::_34, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::_34, 0, 1, LookT(T::BAR)),
                Norm!(N::_34, 1, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::_34, 1, 1, LookT(T::BAR)),
            ],
            S130 => &[
                Norm!(N::Item, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Item, 0, 1, LookT(T::BAR)),
                Norm!(N::Item, 0, 1, LookT(T::ID)),
                Norm!(N::Item, 0, 1, LookT(T::TOKEN_STR)),
            ],
            S131 => &[
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Primary, 1, 1, LookT(T::BAR)),
                Norm!(N::Primary, 1, 1, LookT(T::ID)),
                Norm!(N::Primary, 1, 1, LookT(T::TOKEN_STR)),
            ],
            S132 => &[
                Norm!(N::Modifier, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 0, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 0, 1, LookT(T::ID)),
                Norm!(N::Modifier, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 0, 1, LookT(T::INT)),
            ],
            S133 => &[
                Norm!(N::Modifier, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 2, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 2, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 2, 1, LookT(T::ID)),
                Norm!(N::Modifier, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 2, 1, LookT(T::INT)),
            ],
            S134 => &[
                Norm!(N::Modifier, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 1, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 1, 1, LookT(T::ID)),
                Norm!(N::Modifier, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 1, 1, LookT(T::INT)),
            ],
            S135 => &[
                Norm!(N::_31, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::_31, 1, 1, LookT(T::SHARP)),
                Norm!(N::_31, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_31, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::_31, 1, 1, LookT(T::ID)),
                Norm!(N::_31, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::_31, 1, 1, LookT(T::INT)),
            ],
            S136 => &[
                Norm!(N::Item, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 2, LookT(T::SHARP)),
                Norm!(N::Item, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Item, 0, 2, LookT(T::ID)),
                Norm!(N::Item, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 2, LookT(T::INT)),
            ],
            S137 => &[
                Norm!(N::Modifier, 4, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 4, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 4, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 4, 1, LookT(T::ID)),
                Norm!(N::Modifier, 4, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 4, 1, LookT(T::INT)),
            ],
            S138 => &[
                Norm!(N::Modifier, 3, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 3, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 3, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 3, 1, LookT(T::ID)),
                Norm!(N::Modifier, 3, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 3, 1, LookT(T::INT)),
            ],
            S139 => &[
                Norm!(N::Modifier, 6, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 6, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 6, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 6, 1, LookT(T::ID)),
                Norm!(N::Modifier, 6, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 6, 1, LookT(T::INT)),
            ],
            S140 => &[
                Norm!(N::Modifier, 5, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 5, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 5, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 5, 1, LookT(T::ID)),
                Norm!(N::Modifier, 5, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 5, 1, LookT(T::INT)),
            ],
            S141 => &[
                Norm!(N::Field, 1, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 3, LookT(T::SHARP)),
                Norm!(N::Field, 1, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 3, LookT(T::ID)),
                Norm!(N::Field, 1, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 3, LookT(T::INT)),
            ],
            S142 => &[
                Norm!(N::TestValue, 0, 4, Eof),
                Norm!(N::TestValue, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 4, LookT(T::INCLUDE_KW)),
                Norm!(N::TestValue, 0, 4, LookT(T::TOKENS_KW)),
                Norm!(N::TestValue, 0, 4, LookT(T::NODE_KW)),
                Norm!(N::TestValue, 0, 4, LookT(T::TEST_KW)),
                Norm!(N::TestValue, 0, 4, LookT(T::CONFLICT_KW)),
                Norm!(N::TestValue, 0, 4, LookT(T::SHARP)),
            ],
            S143 => &[
                Norm!(N::_26, 1, 2, LookT(T::ID)),
                Norm!(N::_26, 1, 2, LookT(T::INT)),
            ],
            S144 => &[
                Norm!(N::NormalAttribute, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 2, LookT(T::INT)),
            ],
            S145 => &[
                Norm!(N::TestField, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TestField, 0, 2, LookT(T::SHARP)),
                Norm!(N::TestField, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestField, 0, 2, LookT(T::ID)),
                Norm!(N::TestField, 0, 2, LookT(T::INT)),
            ],
            S146 => &[Norm!(N::Label, 0, 1, LookT(T::COLON))],
            S147 => &[Norm!(N::_37, 1, 2, LookT(T::BRACE_RIGHT))],
            S148 => &[Norm!(N::ConflictAction, 2, 3, LookT(T::OVER_KW))],
            S149 => &[
                Norm!(N::Rule, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Rule, 1, 1, LookT(T::PAR_RIGHT)),
            ],
            S150 => &[Norm!(N::IdOrTokenStr, 0, 1, LookT(T::PAR_RIGHT))],
            S151 => &[Norm!(N::ConflictAction, 0, 3, LookT(T::OVER_KW))],
            S152 => &[Norm!(N::ConflictAction, 1, 3, LookT(T::OVER_KW))],
            S153 => &[Norm!(N::IdOrTokenStr, 1, 1, LookT(T::PAR_RIGHT))],
            S154 => &[
                Norm!(N::Conflict, 0, 7, Eof),
                Norm!(N::Conflict, 0, 7, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 7, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 7, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 7, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 7, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 7, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 7, LookT(T::SHARP)),
            ],
            S155 => &[
                Norm!(N::AttributeItem, 2, 4, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 4, LookT(T::PAR_RIGHT)),
            ],
            S156 => &[
                Norm!(N::TokenDef, 1, 2, LookT(T::VERBATIM_KW)),
                Norm!(N::TokenDef, 1, 2, LookT(T::COMMA)),
                Norm!(N::TokenDef, 1, 2, LookT(T::BRACE_RIGHT)),
            ],
            S157 => &[
                Norm!(N::Variant, 0, 4, LookT(T::BRACE_RIGHT)),
                Norm!(N::Variant, 0, 4, LookT(T::BAR)),
            ],
            S158 => &[
                Norm!(N::NormalAttribute, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::PAR_LEFT)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::TOKEN_STR)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::INT)),
            ],
            S159 => &[Norm!(N::_33, 1, 2, LookT(T::PAR_RIGHT))],
            S160 => &[Norm!(N::_32, 0, 1, LookT(T::PAR_RIGHT))],
            S161 => &[
                Norm!(N::Primary, 2, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 3, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 2, 3, LookT(T::ID)),
                Norm!(N::Primary, 2, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 3, LookT(T::INT)),
            ],
            S162 => &[
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_RIGHT)),
                Norm!(N::Primary, 2, 2, LookT(T::BAR)),
                Norm!(N::Primary, 2, 2, LookT(T::ID)),
                Norm!(N::Primary, 2, 2, LookT(T::TOKEN_STR)),
            ],
            S163 => &[
                Norm!(N::_34, 1, 2, LookT(T::PAR_RIGHT)),
                Norm!(N::_34, 1, 2, LookT(T::BAR)),
            ],
            S164 => &[
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 0, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 0, 1, LookT(T::ID)),
                Norm!(N::Modifier, 0, 1, LookT(T::TOKEN_STR)),
            ],
            S165 => &[
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 2, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 2, 1, LookT(T::ID)),
                Norm!(N::Modifier, 2, 1, LookT(T::TOKEN_STR)),
            ],
            S166 => &[
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 1, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 1, 1, LookT(T::ID)),
                Norm!(N::Modifier, 1, 1, LookT(T::TOKEN_STR)),
            ],
            S167 => &[
                Norm!(N::_31, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_31, 1, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::_31, 1, 1, LookT(T::BAR)),
                Norm!(N::_31, 1, 1, LookT(T::ID)),
                Norm!(N::_31, 1, 1, LookT(T::TOKEN_STR)),
            ],
            S168 => &[
                Norm!(N::Item, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 2, LookT(T::PAR_RIGHT)),
                Norm!(N::Item, 0, 2, LookT(T::BAR)),
                Norm!(N::Item, 0, 2, LookT(T::ID)),
                Norm!(N::Item, 0, 2, LookT(T::TOKEN_STR)),
            ],
            S169 => &[
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 4, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 4, 1, LookT(T::ID)),
                Norm!(N::Modifier, 4, 1, LookT(T::TOKEN_STR)),
            ],
            S170 => &[
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 3, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 3, 1, LookT(T::ID)),
                Norm!(N::Modifier, 3, 1, LookT(T::TOKEN_STR)),
            ],
            S171 => &[
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 6, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 6, 1, LookT(T::ID)),
                Norm!(N::Modifier, 6, 1, LookT(T::TOKEN_STR)),
            ],
            S172 => &[
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_RIGHT)),
                Norm!(N::Modifier, 5, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 5, 1, LookT(T::ID)),
                Norm!(N::Modifier, 5, 1, LookT(T::TOKEN_STR)),
            ],
            S173 => &[
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S174 => &[
                Norm!(N::Field, 1, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 4, LookT(T::SHARP)),
                Norm!(N::Field, 1, 4, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 4, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 4, LookT(T::ID)),
                Norm!(N::Field, 1, 4, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 4, LookT(T::INT)),
            ],
            S175 => &[
                Norm!(N::NormalAttribute, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 3, LookT(T::INT)),
            ],
            S176 => &[
                Norm!(N::TestField, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TestField, 0, 3, LookT(T::SHARP)),
                Norm!(N::TestField, 0, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestField, 0, 3, LookT(T::ID)),
                Norm!(N::TestField, 0, 3, LookT(T::INT)),
            ],
            S177 => &[Norm!(N::ConflictAction, 2, 4, LookT(T::OVER_KW))],
            S178 => &[Norm!(N::Rule, 1, 2, LookT(T::PAR_RIGHT))],
            S179 => &[Norm!(N::ConflictAction, 0, 4, LookT(T::OVER_KW))],
            S180 => &[Norm!(N::ConflictAction, 1, 4, LookT(T::OVER_KW))],
            S181 => &[
                Norm!(N::ConflictAction, 2, 1, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 2, 1, LookT(T::BRACE_RIGHT)),
            ],
            S182 => &[
                Norm!(N::ConflictAction, 0, 1, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::ConflictAction, 1, 1, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 1, 1, LookT(T::BRACE_RIGHT)),
            ],
            S183 => &[
                Norm!(N::Conflict, 0, 8, Eof),
                Norm!(N::Conflict, 0, 8, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 8, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 8, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 8, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 8, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 8, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 8, LookT(T::SHARP)),
            ],
            S184 => &[
                Norm!(N::TokenDef, 1, 3, LookT(T::VERBATIM_KW)),
                Norm!(N::TokenDef, 1, 3, LookT(T::COMMA)),
                Norm!(N::TokenDef, 1, 3, LookT(T::BRACE_RIGHT)),
            ],
            S185 => &[
                Norm!(N::Variant, 0, 5, LookT(T::BRACE_RIGHT)),
                Norm!(N::Variant, 0, 5, LookT(T::BAR)),
            ],
            S186 => &[
                Norm!(N::_30, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::_30, 1, 1, LookT(T::BAR)),
            ],
            S187 => &[
                Norm!(N::Fields, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Fields, 0, 1, LookT(T::BAR)),
            ],
            S188 => &[
                Norm!(N::Field, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 1, LookT(T::SHARP)),
                Norm!(N::Field, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 0, 1, LookT(T::BAR)),
                Norm!(N::Field, 0, 1, LookT(T::ID)),
                Norm!(N::Field, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 1, LookT(T::INT)),
                Norm!(N::Field, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 1, LookT(T::SHARP)),
                Norm!(N::Field, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 1, LookT(T::BAR)),
                Norm!(N::Field, 1, 1, LookT(T::ID)),
                Norm!(N::Field, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 1, LookT(T::INT)),
            ],
            S189 => &[Norm!(N::_32, 0, 2, LookT(T::PAR_RIGHT))],
            S190 => &[
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_RIGHT)),
                Norm!(N::Primary, 2, 3, LookT(T::BAR)),
                Norm!(N::Primary, 2, 3, LookT(T::ID)),
                Norm!(N::Primary, 2, 3, LookT(T::TOKEN_STR)),
            ],
            S191 => &[
                Norm!(N::NormalAttribute, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::SHARP)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::ID)),
                Norm!(N::NormalAttribute, 0, 4, LookT(T::INT)),
            ],
            S192 => &[
                Norm!(N::TestField, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::TestField, 0, 4, LookT(T::SHARP)),
                Norm!(N::TestField, 0, 4, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestField, 0, 4, LookT(T::ID)),
                Norm!(N::TestField, 0, 4, LookT(T::INT)),
            ],
            S193 => &[
                Norm!(N::TestValue, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 1, LookT(T::SHARP)),
                Norm!(N::TestValue, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestValue, 0, 1, LookT(T::ID)),
                Norm!(N::TestValue, 0, 1, LookT(T::INT)),
            ],
            S194 => &[
                Norm!(N::TestItem, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TestItem, 2, 1, LookT(T::SHARP)),
                Norm!(N::TestItem, 2, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestItem, 2, 1, LookT(T::ID)),
                Norm!(N::TestItem, 2, 1, LookT(T::INT)),
            ],
            S195 => &[
                Norm!(N::TestItem, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TestItem, 0, 1, LookT(T::SHARP)),
                Norm!(N::TestItem, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestItem, 0, 1, LookT(T::ID)),
                Norm!(N::TestItem, 0, 1, LookT(T::INT)),
            ],
            S196 => &[
                Norm!(N::TestItem, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::TestItem, 1, 1, LookT(T::SHARP)),
                Norm!(N::TestItem, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestItem, 1, 1, LookT(T::ID)),
                Norm!(N::TestItem, 1, 1, LookT(T::INT)),
            ],
            S197 => &[Norm!(N::Rule, 1, 3, LookT(T::PAR_RIGHT))],
            S198 => &[
                Norm!(N::ConflictAction, 2, 2, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 2, 2, LookT(T::BRACE_RIGHT)),
            ],
            S199 => &[
                Norm!(N::ConflictAction, 0, 2, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::ConflictAction, 1, 2, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 1, 2, LookT(T::BRACE_RIGHT)),
            ],
            S200 => &[Norm!(N::_39, 1, 1, LookT(T::BRACE_RIGHT))],
            S201 => &[
                Norm!(N::Conflict, 0, 9, Eof),
                Norm!(N::Conflict, 0, 9, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 9, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 9, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 9, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 9, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 9, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 9, LookT(T::SHARP)),
            ],
            S202 => &[
                Norm!(N::TokenDef, 1, 4, LookT(T::VERBATIM_KW)),
                Norm!(N::TokenDef, 1, 4, LookT(T::COMMA)),
                Norm!(N::TokenDef, 1, 4, LookT(T::BRACE_RIGHT)),
            ],
            S203 => &[
                Norm!(N::_30, 1, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::_30, 1, 2, LookT(T::BAR)),
            ],
            S204 => &[
                Norm!(N::Primary, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 2, 1, LookT(T::BAR)),
                Norm!(N::Primary, 2, 1, LookT(T::ID)),
                Norm!(N::Primary, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 1, LookT(T::INT)),
            ],
            S205 => &[
                Norm!(N::Label, 0, 1, LookT(T::COLON)),
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 0, 1, LookT(T::BAR)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S206 => &[
                Norm!(N::Field, 1, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 2, LookT(T::SHARP)),
                Norm!(N::Field, 1, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 2, LookT(T::BAR)),
                Norm!(N::Field, 1, 2, LookT(T::ID)),
                Norm!(N::Field, 1, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 2, LookT(T::INT)),
            ],
            S207 => &[
                Norm!(N::Field, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 2, LookT(T::SHARP)),
                Norm!(N::Field, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 0, 2, LookT(T::BAR)),
                Norm!(N::Field, 0, 2, LookT(T::ID)),
                Norm!(N::Field, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 2, LookT(T::INT)),
            ],
            S208 => &[
                Norm!(N::Primary, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 1, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 1, 1, LookT(T::BAR)),
                Norm!(N::Primary, 1, 1, LookT(T::ID)),
                Norm!(N::Primary, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 1, 1, LookT(T::INT)),
            ],
            S209 => &[
                Norm!(N::Item, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 1, LookT(T::SHARP)),
                Norm!(N::Item, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Item, 0, 1, LookT(T::BAR)),
                Norm!(N::Item, 0, 1, LookT(T::ID)),
                Norm!(N::Item, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 1, LookT(T::INT)),
            ],
            S210 => &[Norm!(N::_32, 0, 3, LookT(T::PAR_RIGHT))],
            S211 => &[
                Norm!(N::TestValue, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 2, LookT(T::SHARP)),
                Norm!(N::TestValue, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestValue, 0, 2, LookT(T::ID)),
                Norm!(N::TestValue, 0, 2, LookT(T::INT)),
            ],
            S212 => &[Norm!(N::_38, 1, 1, LookT(T::BRACKET_RIGHT))],
            S213 => &[
                Norm!(N::TestValue, 0, 1, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestValue, 0, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestValue, 0, 1, LookT(T::ID)),
                Norm!(N::TestValue, 0, 1, LookT(T::STRING)),
            ],
            S214 => &[
                Norm!(N::TestItem, 2, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::TestItem, 2, 2, LookT(T::SHARP)),
                Norm!(N::TestItem, 2, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestItem, 2, 2, LookT(T::ID)),
                Norm!(N::TestItem, 2, 2, LookT(T::INT)),
            ],
            S215 => &[
                Norm!(N::TestItem, 0, 1, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestItem, 0, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestItem, 0, 1, LookT(T::ID)),
                Norm!(N::TestItem, 0, 1, LookT(T::STRING)),
            ],
            S216 => &[
                Norm!(N::TestItem, 2, 1, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestItem, 2, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestItem, 2, 1, LookT(T::ID)),
                Norm!(N::TestItem, 2, 1, LookT(T::STRING)),
            ],
            S217 => &[
                Norm!(N::TestItem, 1, 1, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestItem, 1, 1, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestItem, 1, 1, LookT(T::ID)),
                Norm!(N::TestItem, 1, 1, LookT(T::STRING)),
            ],
            S218 => &[
                Norm!(N::ConflictAction, 2, 3, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 2, 3, LookT(T::BRACE_RIGHT)),
            ],
            S219 => &[
                Norm!(N::ConflictAction, 0, 3, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 0, 3, LookT(T::BRACE_RIGHT)),
            ],
            S220 => &[
                Norm!(N::ConflictAction, 1, 3, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 1, 3, LookT(T::BRACE_RIGHT)),
            ],
            S221 => &[Norm!(N::_39, 1, 2, LookT(T::BRACE_RIGHT))],
            S222 => &[
                Norm!(N::Conflict, 0, 10, Eof),
                Norm!(N::Conflict, 0, 10, LookT(T::DOC_COMMENT)),
                Norm!(N::Conflict, 0, 10, LookT(T::INCLUDE_KW)),
                Norm!(N::Conflict, 0, 10, LookT(T::TOKENS_KW)),
                Norm!(N::Conflict, 0, 10, LookT(T::NODE_KW)),
                Norm!(N::Conflict, 0, 10, LookT(T::TEST_KW)),
                Norm!(N::Conflict, 0, 10, LookT(T::CONFLICT_KW)),
                Norm!(N::Conflict, 0, 10, LookT(T::SHARP)),
            ],
            S223 => &[
                Norm!(N::Primary, 2, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 2, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 2, 2, LookT(T::BAR)),
                Norm!(N::Primary, 2, 2, LookT(T::ID)),
                Norm!(N::Primary, 2, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 2, LookT(T::INT)),
            ],
            S224 => &[
                Norm!(N::Field, 1, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 3, LookT(T::SHARP)),
                Norm!(N::Field, 1, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 3, LookT(T::BAR)),
                Norm!(N::Field, 1, 3, LookT(T::ID)),
                Norm!(N::Field, 1, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 3, LookT(T::INT)),
            ],
            S225 => &[
                Norm!(N::Modifier, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 0, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 0, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 0, 1, LookT(T::ID)),
                Norm!(N::Modifier, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 0, 1, LookT(T::INT)),
            ],
            S226 => &[
                Norm!(N::Modifier, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 2, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 2, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 2, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 2, 1, LookT(T::ID)),
                Norm!(N::Modifier, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 2, 1, LookT(T::INT)),
            ],
            S227 => &[
                Norm!(N::Modifier, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 1, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 1, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 1, 1, LookT(T::ID)),
                Norm!(N::Modifier, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 1, 1, LookT(T::INT)),
            ],
            S228 => &[
                Norm!(N::_31, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::_31, 1, 1, LookT(T::SHARP)),
                Norm!(N::_31, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_31, 1, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::_31, 1, 1, LookT(T::BAR)),
                Norm!(N::_31, 1, 1, LookT(T::ID)),
                Norm!(N::_31, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::_31, 1, 1, LookT(T::INT)),
            ],
            S229 => &[
                Norm!(N::Item, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 2, LookT(T::SHARP)),
                Norm!(N::Item, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 2, LookT(T::BRACE_RIGHT)),
                Norm!(N::Item, 0, 2, LookT(T::BAR)),
                Norm!(N::Item, 0, 2, LookT(T::ID)),
                Norm!(N::Item, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 2, LookT(T::INT)),
            ],
            S230 => &[
                Norm!(N::Modifier, 4, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 4, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 4, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 4, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 4, 1, LookT(T::ID)),
                Norm!(N::Modifier, 4, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 4, 1, LookT(T::INT)),
            ],
            S231 => &[
                Norm!(N::Modifier, 3, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 3, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 3, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 3, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 3, 1, LookT(T::ID)),
                Norm!(N::Modifier, 3, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 3, 1, LookT(T::INT)),
            ],
            S232 => &[
                Norm!(N::Modifier, 6, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 6, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 6, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 6, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 6, 1, LookT(T::ID)),
                Norm!(N::Modifier, 6, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 6, 1, LookT(T::INT)),
            ],
            S233 => &[
                Norm!(N::Modifier, 5, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 5, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 5, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Modifier, 5, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 5, 1, LookT(T::ID)),
                Norm!(N::Modifier, 5, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 5, 1, LookT(T::INT)),
            ],
            S234 => &[
                Norm!(N::TestValue, 0, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 3, LookT(T::SHARP)),
                Norm!(N::TestValue, 0, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestValue, 0, 3, LookT(T::ID)),
                Norm!(N::TestValue, 0, 3, LookT(T::INT)),
            ],
            S235 => &[Norm!(N::_38, 1, 2, LookT(T::BRACKET_RIGHT))],
            S236 => &[
                Norm!(N::TestValue, 0, 2, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestValue, 0, 2, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestValue, 0, 2, LookT(T::ID)),
                Norm!(N::TestValue, 0, 2, LookT(T::STRING)),
            ],
            S237 => &[
                Norm!(N::TestItem, 2, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::TestItem, 2, 3, LookT(T::SHARP)),
                Norm!(N::TestItem, 2, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestItem, 2, 3, LookT(T::ID)),
                Norm!(N::TestItem, 2, 3, LookT(T::INT)),
            ],
            S238 => &[
                Norm!(N::TestItem, 2, 2, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestItem, 2, 2, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestItem, 2, 2, LookT(T::ID)),
                Norm!(N::TestItem, 2, 2, LookT(T::STRING)),
            ],
            S239 => &[
                Norm!(N::ConflictAction, 2, 4, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 2, 4, LookT(T::BRACE_RIGHT)),
            ],
            S240 => &[
                Norm!(N::ConflictAction, 0, 4, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 0, 4, LookT(T::BRACE_RIGHT)),
            ],
            S241 => &[
                Norm!(N::ConflictAction, 1, 4, LookT(T::LOOKAHEAD_KW)),
                Norm!(N::ConflictAction, 1, 4, LookT(T::BRACE_RIGHT)),
            ],
            S242 => &[Norm!(N::_39, 1, 3, LookT(T::BRACE_RIGHT))],
            S243 => &[Norm!(N::IdOrTokenStr, 1, 1, LookT(T::BRACE_RIGHT))],
            S244 => &[Norm!(N::IdOrTokenStr, 0, 1, LookT(T::BRACE_RIGHT))],
            S245 => &[
                Norm!(N::Primary, 2, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 3, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 3, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 2, 3, LookT(T::BAR)),
                Norm!(N::Primary, 2, 3, LookT(T::ID)),
                Norm!(N::Primary, 2, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 3, LookT(T::INT)),
            ],
            S246 => &[
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::BRACE_RIGHT)),
                Norm!(N::Primary, 0, 1, LookT(T::BAR)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S247 => &[
                Norm!(N::Field, 1, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 4, LookT(T::SHARP)),
                Norm!(N::Field, 1, 4, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 4, LookT(T::BRACE_RIGHT)),
                Norm!(N::Field, 1, 4, LookT(T::BAR)),
                Norm!(N::Field, 1, 4, LookT(T::ID)),
                Norm!(N::Field, 1, 4, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 4, LookT(T::INT)),
            ],
            S248 => &[
                Norm!(N::TestValue, 0, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::TestValue, 0, 4, LookT(T::SHARP)),
                Norm!(N::TestValue, 0, 4, LookT(T::BRACE_RIGHT)),
                Norm!(N::TestValue, 0, 4, LookT(T::ID)),
                Norm!(N::TestValue, 0, 4, LookT(T::INT)),
            ],
            S249 => &[
                Norm!(N::TestValue, 0, 3, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestValue, 0, 3, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestValue, 0, 3, LookT(T::ID)),
                Norm!(N::TestValue, 0, 3, LookT(T::STRING)),
            ],
            S250 => &[
                Norm!(N::TestItem, 2, 3, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestItem, 2, 3, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestItem, 2, 3, LookT(T::ID)),
                Norm!(N::TestItem, 2, 3, LookT(T::STRING)),
            ],
            S251 => &[
                Norm!(N::TestValue, 0, 4, LookT(T::BRACKET_LEFT)),
                Norm!(N::TestValue, 0, 4, LookT(T::BRACKET_RIGHT)),
                Norm!(N::TestValue, 0, 4, LookT(T::ID)),
                Norm!(N::TestValue, 0, 4, LookT(T::STRING)),
            ],
            S252 => &[IndexedRule::Start {
                node: N::TopLevelItem,
                index: 0,
            }],
            S253 => &[Norm!(N::TopLevelItem, 0, 1, Eof)],
            S254 => &[Norm!(N::TopLevelItem, 4, 1, Eof)],
            S255 => &[IndexedRule::Start {
                node: N::TopLevelItem,
                index: 1,
            }],
            S256 => &[Norm!(N::Conflict, 0, 1, Eof)],
            S257 => &[
                Norm!(N::TopLevelItem, 1, 1, Eof),
                Norm!(N::TopLevelItem, 2, 1, Eof),
                Norm!(N::TopLevelItem, 3, 1, Eof),
            ],
            S258 => &[Norm!(N::TopLevelItem, 0, 2, Eof)],
            S259 => &[Norm!(N::Conflict, 0, 2, Eof)],
            S260 => &[Norm!(N::TopLevelItem, 1, 2, Eof)],
            S261 => &[Norm!(N::TopLevelItem, 2, 2, Eof)],
            S262 => &[Norm!(N::TopLevelItem, 3, 2, Eof)],
            S263 => &[Norm!(N::TopLevelItem, 0, 3, Eof)],
            S264 => &[Norm!(N::Conflict, 0, 3, Eof)],
            S265 => &[Norm!(N::TopLevelItem, 1, 3, Eof)],
            S266 => &[Norm!(N::TopLevelItem, 2, 3, Eof)],
            S267 => &[Norm!(N::TopLevelItem, 3, 3, Eof)],
            S268 => &[Norm!(N::Conflict, 0, 4, Eof)],
            S269 => &[Norm!(N::TopLevelItem, 1, 4, Eof)],
            S270 => &[Norm!(N::TopLevelItem, 2, 4, Eof)],
            S271 => &[Norm!(N::TestValue, 0, 1, Eof)],
            S272 => &[Norm!(N::TopLevelItem, 3, 4, Eof)],
            S273 => &[Norm!(N::Conflict, 0, 5, Eof)],
            S274 => &[Norm!(N::TopLevelItem, 1, 5, Eof)],
            S275 => &[Norm!(N::TopLevelItem, 2, 5, Eof)],
            S276 => &[Norm!(N::TestValue, 0, 2, Eof)],
            S277 => &[Norm!(N::Conflict, 0, 6, Eof)],
            S278 => &[Norm!(N::TopLevelItem, 1, 6, Eof)],
            S279 => &[Norm!(N::TopLevelItem, 2, 6, Eof)],
            S280 => &[Norm!(N::TestValue, 0, 3, Eof)],
            S281 => &[Norm!(N::Conflict, 0, 7, Eof)],
            S282 => &[Norm!(N::TestValue, 0, 4, Eof)],
            S283 => &[Norm!(N::Conflict, 0, 8, Eof)],
            S284 => &[Norm!(N::Conflict, 0, 9, Eof)],
            S285 => &[Norm!(N::Conflict, 0, 10, Eof)],
            S286 => &[IndexedRule::Start {
                node: N::Token,
                index: 0,
            }],
            S287 => &[IndexedRule::Start {
                node: N::Token,
                index: 1,
            }],
            S288 => &[Norm!(N::Token, 0, 1, Eof)],
            S289 => &[Norm!(N::Token, 0, 2, Eof)],
            S290 => &[Norm!(N::Token, 0, 3, Eof)],
            S291 => &[Norm!(N::TokenDef, 2, 1, Eof)],
            S292 => &[Norm!(N::TokenDef, 1, 1, Eof)],
            S293 => &[Norm!(N::TokenDef, 0, 1, Eof)],
            S294 => &[Norm!(N::Token, 0, 4, Eof)],
            S295 => &[Norm!(N::TokenDef, 1, 2, Eof)],
            S296 => &[Norm!(N::TokenDef, 1, 3, Eof)],
            S297 => &[Norm!(N::TokenDef, 1, 4, Eof)],
            S298 => &[IndexedRule::Start {
                node: N::TokenDef,
                index: 0,
            }],
            S299 => &[IndexedRule::Start {
                node: N::TokenDef,
                index: 1,
            }],
            S300 => &[IndexedRule::Start {
                node: N::Block,
                index: 0,
            }],
            S301 => &[IndexedRule::Start {
                node: N::Block,
                index: 1,
            }],
            S302 => &[Norm!(N::_29, 0, 1, Eof), Norm!(N::_29, 1, 1, Eof)],
            S303 => &[Norm!(N::Block, 0, 1, Eof)],
            S304 => &[Norm!(N::_30, 1, 1, Eof)],
            S305 => &[
                Norm!(N::Variant, 0, 1, Eof),
                Norm!(N::Variant, 0, 1, LookT(T::BAR)),
            ],
            S306 => &[Norm!(N::Block, 1, 1, Eof)],
            S307 => &[Norm!(N::Fields, 0, 1, Eof)],
            S308 => &[
                Norm!(N::Field, 0, 1, Eof),
                Norm!(N::Field, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 1, LookT(T::SHARP)),
                Norm!(N::Field, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 1, LookT(T::ID)),
                Norm!(N::Field, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 1, LookT(T::INT)),
                Norm!(N::Field, 1, 1, Eof),
                Norm!(N::Field, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 1, LookT(T::SHARP)),
                Norm!(N::Field, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 1, LookT(T::ID)),
                Norm!(N::Field, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 1, LookT(T::INT)),
            ],
            S309 => &[Norm!(N::_29, 1, 2, Eof)],
            S310 => &[Norm!(N::_30, 1, 2, Eof)],
            S311 => &[
                Norm!(N::Variant, 0, 2, Eof),
                Norm!(N::Variant, 0, 2, LookT(T::BAR)),
            ],
            S312 => &[
                Norm!(N::Primary, 2, 1, Eof),
                Norm!(N::Primary, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 1, LookT(T::ID)),
                Norm!(N::Primary, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 1, LookT(T::INT)),
            ],
            S313 => &[
                Norm!(N::Item, 0, 1, Eof),
                Norm!(N::Item, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 1, LookT(T::SHARP)),
                Norm!(N::Item, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 1, LookT(T::ID)),
                Norm!(N::Item, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 1, LookT(T::INT)),
            ],
            S314 => &[
                Norm!(N::Label, 0, 1, LookT(T::COLON)),
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S315 => &[
                Norm!(N::Field, 1, 2, Eof),
                Norm!(N::Field, 1, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 2, LookT(T::SHARP)),
                Norm!(N::Field, 1, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 2, LookT(T::ID)),
                Norm!(N::Field, 1, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 2, LookT(T::INT)),
            ],
            S316 => &[
                Norm!(N::Primary, 1, 1, Eof),
                Norm!(N::Primary, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 1, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 1, 1, LookT(T::ID)),
                Norm!(N::Primary, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 1, 1, LookT(T::INT)),
            ],
            S317 => &[
                Norm!(N::Field, 0, 2, Eof),
                Norm!(N::Field, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 2, LookT(T::SHARP)),
                Norm!(N::Field, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 2, LookT(T::ID)),
                Norm!(N::Field, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 2, LookT(T::INT)),
            ],
            S318 => &[
                Norm!(N::Variant, 0, 3, Eof),
                Norm!(N::Variant, 0, 3, LookT(T::BAR)),
            ],
            S319 => &[
                Norm!(N::Primary, 2, 2, Eof),
                Norm!(N::Primary, 2, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 2, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 2, LookT(T::ID)),
                Norm!(N::Primary, 2, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 2, LookT(T::INT)),
            ],
            S320 => &[
                Norm!(N::Modifier, 0, 1, Eof),
                Norm!(N::Modifier, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 0, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 0, 1, LookT(T::ID)),
                Norm!(N::Modifier, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 0, 1, LookT(T::INT)),
            ],
            S321 => &[
                Norm!(N::Modifier, 2, 1, Eof),
                Norm!(N::Modifier, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 2, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 2, 1, LookT(T::ID)),
                Norm!(N::Modifier, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 2, 1, LookT(T::INT)),
            ],
            S322 => &[
                Norm!(N::Modifier, 1, 1, Eof),
                Norm!(N::Modifier, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 1, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 1, 1, LookT(T::ID)),
                Norm!(N::Modifier, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 1, 1, LookT(T::INT)),
            ],
            S323 => &[
                Norm!(N::_31, 1, 1, Eof),
                Norm!(N::_31, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::_31, 1, 1, LookT(T::SHARP)),
                Norm!(N::_31, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_31, 1, 1, LookT(T::ID)),
                Norm!(N::_31, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::_31, 1, 1, LookT(T::INT)),
            ],
            S324 => &[
                Norm!(N::Item, 0, 2, Eof),
                Norm!(N::Item, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 2, LookT(T::SHARP)),
                Norm!(N::Item, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 2, LookT(T::ID)),
                Norm!(N::Item, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 2, LookT(T::INT)),
            ],
            S325 => &[
                Norm!(N::Modifier, 4, 1, Eof),
                Norm!(N::Modifier, 4, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 4, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 4, 1, LookT(T::ID)),
                Norm!(N::Modifier, 4, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 4, 1, LookT(T::INT)),
            ],
            S326 => &[
                Norm!(N::Modifier, 3, 1, Eof),
                Norm!(N::Modifier, 3, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 3, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 3, 1, LookT(T::ID)),
                Norm!(N::Modifier, 3, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 3, 1, LookT(T::INT)),
            ],
            S327 => &[
                Norm!(N::Modifier, 6, 1, Eof),
                Norm!(N::Modifier, 6, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 6, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 6, 1, LookT(T::ID)),
                Norm!(N::Modifier, 6, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 6, 1, LookT(T::INT)),
            ],
            S328 => &[
                Norm!(N::Modifier, 5, 1, Eof),
                Norm!(N::Modifier, 5, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 5, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 5, 1, LookT(T::ID)),
                Norm!(N::Modifier, 5, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 5, 1, LookT(T::INT)),
            ],
            S329 => &[
                Norm!(N::Field, 1, 3, Eof),
                Norm!(N::Field, 1, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 3, LookT(T::SHARP)),
                Norm!(N::Field, 1, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 3, LookT(T::ID)),
                Norm!(N::Field, 1, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 3, LookT(T::INT)),
            ],
            S330 => &[
                Norm!(N::Variant, 0, 4, Eof),
                Norm!(N::Variant, 0, 4, LookT(T::BAR)),
            ],
            S331 => &[
                Norm!(N::Primary, 2, 3, Eof),
                Norm!(N::Primary, 2, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 3, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 3, LookT(T::ID)),
                Norm!(N::Primary, 2, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 3, LookT(T::INT)),
            ],
            S332 => &[
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S333 => &[
                Norm!(N::Field, 1, 4, Eof),
                Norm!(N::Field, 1, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 4, LookT(T::SHARP)),
                Norm!(N::Field, 1, 4, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 4, LookT(T::ID)),
                Norm!(N::Field, 1, 4, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 4, LookT(T::INT)),
            ],
            S334 => &[
                Norm!(N::Variant, 0, 5, Eof),
                Norm!(N::Variant, 0, 5, LookT(T::BAR)),
            ],
            S335 => &[Norm!(N::_30, 1, 1, Eof), Norm!(N::_30, 1, 1, LookT(T::BAR))],
            S336 => &[
                Norm!(N::Fields, 0, 1, Eof),
                Norm!(N::Fields, 0, 1, LookT(T::BAR)),
            ],
            S337 => &[
                Norm!(N::Field, 0, 1, Eof),
                Norm!(N::Field, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 1, LookT(T::SHARP)),
                Norm!(N::Field, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 1, LookT(T::BAR)),
                Norm!(N::Field, 0, 1, LookT(T::ID)),
                Norm!(N::Field, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 1, LookT(T::INT)),
                Norm!(N::Field, 1, 1, Eof),
                Norm!(N::Field, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 1, LookT(T::SHARP)),
                Norm!(N::Field, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 1, LookT(T::BAR)),
                Norm!(N::Field, 1, 1, LookT(T::ID)),
                Norm!(N::Field, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 1, LookT(T::INT)),
            ],
            S338 => &[Norm!(N::_30, 1, 2, Eof), Norm!(N::_30, 1, 2, LookT(T::BAR))],
            S339 => &[
                Norm!(N::Primary, 2, 1, Eof),
                Norm!(N::Primary, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 1, LookT(T::BAR)),
                Norm!(N::Primary, 2, 1, LookT(T::ID)),
                Norm!(N::Primary, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 1, LookT(T::INT)),
            ],
            S340 => &[
                Norm!(N::Label, 0, 1, LookT(T::COLON)),
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::BAR)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S341 => &[
                Norm!(N::Field, 1, 2, Eof),
                Norm!(N::Field, 1, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 2, LookT(T::SHARP)),
                Norm!(N::Field, 1, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 2, LookT(T::BAR)),
                Norm!(N::Field, 1, 2, LookT(T::ID)),
                Norm!(N::Field, 1, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 2, LookT(T::INT)),
            ],
            S342 => &[
                Norm!(N::Primary, 1, 1, Eof),
                Norm!(N::Primary, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 1, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 1, 1, LookT(T::BAR)),
                Norm!(N::Primary, 1, 1, LookT(T::ID)),
                Norm!(N::Primary, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 1, 1, LookT(T::INT)),
            ],
            S343 => &[
                Norm!(N::Field, 0, 2, Eof),
                Norm!(N::Field, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 0, 2, LookT(T::SHARP)),
                Norm!(N::Field, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 0, 2, LookT(T::BAR)),
                Norm!(N::Field, 0, 2, LookT(T::ID)),
                Norm!(N::Field, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 0, 2, LookT(T::INT)),
            ],
            S344 => &[
                Norm!(N::Item, 0, 1, Eof),
                Norm!(N::Item, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 1, LookT(T::SHARP)),
                Norm!(N::Item, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 1, LookT(T::BAR)),
                Norm!(N::Item, 0, 1, LookT(T::ID)),
                Norm!(N::Item, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 1, LookT(T::INT)),
            ],
            S345 => &[
                Norm!(N::Primary, 2, 2, Eof),
                Norm!(N::Primary, 2, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 2, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 2, LookT(T::BAR)),
                Norm!(N::Primary, 2, 2, LookT(T::ID)),
                Norm!(N::Primary, 2, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 2, LookT(T::INT)),
            ],
            S346 => &[
                Norm!(N::Field, 1, 3, Eof),
                Norm!(N::Field, 1, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 3, LookT(T::SHARP)),
                Norm!(N::Field, 1, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 3, LookT(T::BAR)),
                Norm!(N::Field, 1, 3, LookT(T::ID)),
                Norm!(N::Field, 1, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 3, LookT(T::INT)),
            ],
            S347 => &[
                Norm!(N::Modifier, 0, 1, Eof),
                Norm!(N::Modifier, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 0, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 0, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 0, 1, LookT(T::ID)),
                Norm!(N::Modifier, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 0, 1, LookT(T::INT)),
            ],
            S348 => &[
                Norm!(N::Modifier, 2, 1, Eof),
                Norm!(N::Modifier, 2, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 2, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 2, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 2, 1, LookT(T::ID)),
                Norm!(N::Modifier, 2, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 2, 1, LookT(T::INT)),
            ],
            S349 => &[
                Norm!(N::Modifier, 1, 1, Eof),
                Norm!(N::Modifier, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 1, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 1, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 1, 1, LookT(T::ID)),
                Norm!(N::Modifier, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 1, 1, LookT(T::INT)),
            ],
            S350 => &[
                Norm!(N::_31, 1, 1, Eof),
                Norm!(N::_31, 1, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::_31, 1, 1, LookT(T::SHARP)),
                Norm!(N::_31, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_31, 1, 1, LookT(T::BAR)),
                Norm!(N::_31, 1, 1, LookT(T::ID)),
                Norm!(N::_31, 1, 1, LookT(T::TOKEN_STR)),
                Norm!(N::_31, 1, 1, LookT(T::INT)),
            ],
            S351 => &[
                Norm!(N::Item, 0, 2, Eof),
                Norm!(N::Item, 0, 2, LookT(T::DOC_COMMENT)),
                Norm!(N::Item, 0, 2, LookT(T::SHARP)),
                Norm!(N::Item, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 2, LookT(T::BAR)),
                Norm!(N::Item, 0, 2, LookT(T::ID)),
                Norm!(N::Item, 0, 2, LookT(T::TOKEN_STR)),
                Norm!(N::Item, 0, 2, LookT(T::INT)),
            ],
            S352 => &[
                Norm!(N::Modifier, 4, 1, Eof),
                Norm!(N::Modifier, 4, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 4, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 4, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 4, 1, LookT(T::ID)),
                Norm!(N::Modifier, 4, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 4, 1, LookT(T::INT)),
            ],
            S353 => &[
                Norm!(N::Modifier, 3, 1, Eof),
                Norm!(N::Modifier, 3, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 3, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 3, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 3, 1, LookT(T::ID)),
                Norm!(N::Modifier, 3, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 3, 1, LookT(T::INT)),
            ],
            S354 => &[
                Norm!(N::Modifier, 6, 1, Eof),
                Norm!(N::Modifier, 6, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 6, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 6, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 6, 1, LookT(T::ID)),
                Norm!(N::Modifier, 6, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 6, 1, LookT(T::INT)),
            ],
            S355 => &[
                Norm!(N::Modifier, 5, 1, Eof),
                Norm!(N::Modifier, 5, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Modifier, 5, 1, LookT(T::SHARP)),
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 5, 1, LookT(T::BAR)),
                Norm!(N::Modifier, 5, 1, LookT(T::ID)),
                Norm!(N::Modifier, 5, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Modifier, 5, 1, LookT(T::INT)),
            ],
            S356 => &[
                Norm!(N::Primary, 2, 3, Eof),
                Norm!(N::Primary, 2, 3, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 3, LookT(T::SHARP)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 3, LookT(T::BAR)),
                Norm!(N::Primary, 2, 3, LookT(T::ID)),
                Norm!(N::Primary, 2, 3, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 2, 3, LookT(T::INT)),
            ],
            S357 => &[
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::DOC_COMMENT)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::SHARP)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::BAR)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
                Norm!(N::Primary, 0, 1, LookT(T::INT)),
            ],
            S358 => &[
                Norm!(N::Field, 1, 4, Eof),
                Norm!(N::Field, 1, 4, LookT(T::DOC_COMMENT)),
                Norm!(N::Field, 1, 4, LookT(T::SHARP)),
                Norm!(N::Field, 1, 4, LookT(T::PAR_LEFT)),
                Norm!(N::Field, 1, 4, LookT(T::BAR)),
                Norm!(N::Field, 1, 4, LookT(T::ID)),
                Norm!(N::Field, 1, 4, LookT(T::TOKEN_STR)),
                Norm!(N::Field, 1, 4, LookT(T::INT)),
            ],
            S359 => &[IndexedRule::Start {
                node: N::Variant,
                index: 0,
            }],
            S360 => &[IndexedRule::Start {
                node: N::Variant,
                index: 1,
            }],
            S361 => &[Norm!(N::Variant, 0, 1, Eof)],
            S362 => &[Norm!(N::Variant, 0, 2, Eof)],
            S363 => &[Norm!(N::Variant, 0, 3, Eof)],
            S364 => &[Norm!(N::Variant, 0, 4, Eof)],
            S365 => &[Norm!(N::Variant, 0, 5, Eof)],
            S366 => &[IndexedRule::Start {
                node: N::Fields,
                index: 0,
            }],
            S367 => &[IndexedRule::Start {
                node: N::Fields,
                index: 1,
            }],
            S368 => &[IndexedRule::Start {
                node: N::Field,
                index: 0,
            }],
            S369 => &[IndexedRule::Start {
                node: N::Field,
                index: 1,
            }],
            S370 => &[Norm!(N::Field, 0, 1, Eof), Norm!(N::Field, 1, 1, Eof)],
            S371 => &[
                Norm!(N::Primary, 2, 1, Eof),
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
            ],
            S372 => &[Norm!(N::Field, 0, 2, Eof)],
            S373 => &[
                Norm!(N::Label, 0, 1, LookT(T::COLON)),
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
            ],
            S374 => &[Norm!(N::Field, 1, 2, Eof)],
            S375 => &[
                Norm!(N::Primary, 1, 1, Eof),
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
            ],
            S376 => &[Norm!(N::Item, 0, 1, Eof)],
            S377 => &[
                Norm!(N::Primary, 2, 2, Eof),
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
            ],
            S378 => &[Norm!(N::Field, 1, 3, Eof)],
            S379 => &[Norm!(N::Modifier, 0, 1, Eof)],
            S380 => &[Norm!(N::Modifier, 2, 1, Eof)],
            S381 => &[Norm!(N::Modifier, 1, 1, Eof)],
            S382 => &[Norm!(N::_31, 1, 1, Eof)],
            S383 => &[Norm!(N::Item, 0, 2, Eof)],
            S384 => &[Norm!(N::Modifier, 4, 1, Eof)],
            S385 => &[Norm!(N::Modifier, 3, 1, Eof)],
            S386 => &[Norm!(N::Modifier, 6, 1, Eof)],
            S387 => &[Norm!(N::Modifier, 5, 1, Eof)],
            S388 => &[
                Norm!(N::Primary, 2, 3, Eof),
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
            ],
            S389 => &[
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
            ],
            S390 => &[Norm!(N::Field, 1, 4, Eof)],
            S391 => &[IndexedRule::Start {
                node: N::Label,
                index: 0,
            }],
            S392 => &[IndexedRule::Start {
                node: N::Label,
                index: 1,
            }],
            S393 => &[Norm!(N::Label, 0, 1, Eof)],
            S394 => &[Norm!(N::Label, 1, 1, Eof)],
            S395 => &[IndexedRule::Start {
                node: N::Item,
                index: 0,
            }],
            S396 => &[IndexedRule::Start {
                node: N::Item,
                index: 1,
            }],
            S397 => &[IndexedRule::Start {
                node: N::Primary,
                index: 0,
            }],
            S398 => &[Norm!(N::Primary, 2, 1, Eof)],
            S399 => &[Norm!(N::Primary, 0, 1, Eof)],
            S400 => &[Norm!(N::Primary, 1, 1, Eof)],
            S401 => &[IndexedRule::Start {
                node: N::Primary,
                index: 1,
            }],
            S402 => &[Norm!(N::Primary, 2, 2, Eof)],
            S403 => &[Norm!(N::Primary, 2, 3, Eof)],
            S404 => &[IndexedRule::Start {
                node: N::Alternative,
                index: 0,
            }],
            S405 => &[
                Norm!(N::Primary, 2, 1, Eof),
                Norm!(N::Primary, 2, 1, LookT(T::STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 1, LookT(T::ID)),
                Norm!(N::Primary, 2, 1, LookT(T::TOKEN_STR)),
            ],
            S406 => &[IndexedRule::Start {
                node: N::Alternative,
                index: 1,
            }],
            S407 => &[
                Norm!(N::Primary, 0, 1, Eof),
                Norm!(N::Primary, 0, 1, LookT(T::STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 0, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 0, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 0, 1, LookT(T::ID)),
                Norm!(N::Primary, 0, 1, LookT(T::TOKEN_STR)),
            ],
            S408 => &[Norm!(N::Alternative, 0, 1, Eof)],
            S409 => &[
                Norm!(N::Primary, 1, 1, Eof),
                Norm!(N::Primary, 1, 1, LookT(T::STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 1, 1, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 1, 1, LookT(T::QUESTION)),
                Norm!(N::Primary, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 1, 1, LookT(T::ID)),
                Norm!(N::Primary, 1, 1, LookT(T::TOKEN_STR)),
            ],
            S410 => &[Norm!(N::_34, 0, 1, Eof), Norm!(N::_34, 1, 1, Eof)],
            S411 => &[
                Norm!(N::Item, 0, 1, Eof),
                Norm!(N::Item, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 1, LookT(T::ID)),
                Norm!(N::Item, 0, 1, LookT(T::TOKEN_STR)),
            ],
            S412 => &[
                Norm!(N::Primary, 2, 2, Eof),
                Norm!(N::Primary, 2, 2, LookT(T::STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 2, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 2, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 2, LookT(T::ID)),
                Norm!(N::Primary, 2, 2, LookT(T::TOKEN_STR)),
            ],
            S413 => &[Norm!(N::_34, 1, 2, Eof)],
            S414 => &[
                Norm!(N::Modifier, 0, 1, Eof),
                Norm!(N::Modifier, 0, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 0, 1, LookT(T::ID)),
                Norm!(N::Modifier, 0, 1, LookT(T::TOKEN_STR)),
            ],
            S415 => &[
                Norm!(N::Modifier, 2, 1, Eof),
                Norm!(N::Modifier, 2, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 2, 1, LookT(T::ID)),
                Norm!(N::Modifier, 2, 1, LookT(T::TOKEN_STR)),
            ],
            S416 => &[
                Norm!(N::Modifier, 1, 1, Eof),
                Norm!(N::Modifier, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 1, 1, LookT(T::ID)),
                Norm!(N::Modifier, 1, 1, LookT(T::TOKEN_STR)),
            ],
            S417 => &[
                Norm!(N::_31, 1, 1, Eof),
                Norm!(N::_31, 1, 1, LookT(T::PAR_LEFT)),
                Norm!(N::_31, 1, 1, LookT(T::ID)),
                Norm!(N::_31, 1, 1, LookT(T::TOKEN_STR)),
            ],
            S418 => &[
                Norm!(N::Item, 0, 2, Eof),
                Norm!(N::Item, 0, 2, LookT(T::PAR_LEFT)),
                Norm!(N::Item, 0, 2, LookT(T::ID)),
                Norm!(N::Item, 0, 2, LookT(T::TOKEN_STR)),
            ],
            S419 => &[
                Norm!(N::Modifier, 4, 1, Eof),
                Norm!(N::Modifier, 4, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 4, 1, LookT(T::ID)),
                Norm!(N::Modifier, 4, 1, LookT(T::TOKEN_STR)),
            ],
            S420 => &[
                Norm!(N::Modifier, 3, 1, Eof),
                Norm!(N::Modifier, 3, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 3, 1, LookT(T::ID)),
                Norm!(N::Modifier, 3, 1, LookT(T::TOKEN_STR)),
            ],
            S421 => &[
                Norm!(N::Modifier, 6, 1, Eof),
                Norm!(N::Modifier, 6, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 6, 1, LookT(T::ID)),
                Norm!(N::Modifier, 6, 1, LookT(T::TOKEN_STR)),
            ],
            S422 => &[
                Norm!(N::Modifier, 5, 1, Eof),
                Norm!(N::Modifier, 5, 1, LookT(T::PAR_LEFT)),
                Norm!(N::Modifier, 5, 1, LookT(T::ID)),
                Norm!(N::Modifier, 5, 1, LookT(T::TOKEN_STR)),
            ],
            S423 => &[
                Norm!(N::Primary, 2, 3, Eof),
                Norm!(N::Primary, 2, 3, LookT(T::STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::STAR_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_STAR)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::PLUS_UNDERSCORE)),
                Norm!(N::Primary, 2, 3, LookT(T::UNDERSCORE_PLUS)),
                Norm!(N::Primary, 2, 3, LookT(T::QUESTION)),
                Norm!(N::Primary, 2, 3, LookT(T::PAR_LEFT)),
                Norm!(N::Primary, 2, 3, LookT(T::ID)),
                Norm!(N::Primary, 2, 3, LookT(T::TOKEN_STR)),
            ],
            S424 => &[IndexedRule::Start {
                node: N::Modifier,
                index: 0,
            }],
            S425 => &[IndexedRule::Start {
                node: N::Modifier,
                index: 1,
            }],
            S426 => &[IndexedRule::Start {
                node: N::Attribute,
                index: 0,
            }],
            S427 => &[IndexedRule::Start {
                node: N::Attribute,
                index: 1,
            }],
            S428 => &[Norm!(N::Attribute, 1, 1, Eof)],
            S429 => &[Norm!(N::NormalAttribute, 0, 1, Eof)],
            S430 => &[Norm!(N::Attribute, 0, 1, Eof)],
            S431 => &[Norm!(N::NormalAttribute, 0, 2, Eof)],
            S432 => &[Norm!(N::NormalAttribute, 0, 3, Eof)],
            S433 => &[Norm!(N::NormalAttribute, 0, 4, Eof)],
            S434 => &[IndexedRule::Start {
                node: N::NormalAttribute,
                index: 0,
            }],
            S435 => &[IndexedRule::Start {
                node: N::NormalAttribute,
                index: 1,
            }],
            S436 => &[IndexedRule::Start {
                node: N::AttributeItem,
                index: 0,
            }],
            S437 => &[
                Norm!(N::AttributeItem, 0, 1, Eof),
                Norm!(N::AttributeItem, 2, 1, Eof),
                Norm!(N::AttributeItem, 3, 1, Eof),
            ],
            S438 => &[IndexedRule::Start {
                node: N::AttributeItem,
                index: 1,
            }],
            S439 => &[Norm!(N::Literal, 1, 1, Eof)],
            S440 => &[Norm!(N::Literal, 2, 1, Eof)],
            S441 => &[Norm!(N::AttributeItem, 1, 1, Eof)],
            S442 => &[Norm!(N::Literal, 0, 1, Eof)],
            S443 => &[Norm!(N::AttributeItem, 2, 2, Eof)],
            S444 => &[Norm!(N::AttributeItem, 3, 2, Eof)],
            S445 => &[Norm!(N::AttributeItem, 2, 3, Eof)],
            S446 => &[Norm!(N::AttributeItem, 3, 3, Eof)],
            S447 => &[Norm!(N::AttributeItem, 2, 4, Eof)],
            S448 => &[IndexedRule::Start {
                node: N::AttributeArgs,
                index: 0,
            }],
            S449 => &[Norm!(N::AttributeArgs, 0, 1, Eof)],
            S450 => &[
                Norm!(N::AttributeItem, 0, 1, Eof),
                Norm!(N::AttributeItem, 0, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 2, 1, Eof),
                Norm!(N::AttributeItem, 2, 1, LookT(T::COMMA)),
                Norm!(N::AttributeItem, 3, 1, Eof),
                Norm!(N::AttributeItem, 3, 1, LookT(T::COMMA)),
            ],
            S451 => &[IndexedRule::Start {
                node: N::AttributeArgs,
                index: 1,
            }],
            S452 => &[
                Norm!(N::AttributeItem, 1, 1, Eof),
                Norm!(N::AttributeItem, 1, 1, LookT(T::COMMA)),
            ],
            S453 => &[Norm!(N::_36, 1, 1, Eof), Norm!(N::_36, 2, 1, Eof)],
            S454 => &[
                Norm!(N::Literal, 1, 1, Eof),
                Norm!(N::Literal, 1, 1, LookT(T::COMMA)),
            ],
            S455 => &[
                Norm!(N::Literal, 2, 1, Eof),
                Norm!(N::Literal, 2, 1, LookT(T::COMMA)),
            ],
            S456 => &[
                Norm!(N::Literal, 0, 1, Eof),
                Norm!(N::Literal, 0, 1, LookT(T::COMMA)),
            ],
            S457 => &[
                Norm!(N::AttributeItem, 2, 2, Eof),
                Norm!(N::AttributeItem, 2, 2, LookT(T::COMMA)),
            ],
            S458 => &[
                Norm!(N::AttributeItem, 3, 2, Eof),
                Norm!(N::AttributeItem, 3, 2, LookT(T::COMMA)),
            ],
            S459 => &[Norm!(N::_36, 1, 2, Eof)],
            S460 => &[
                Norm!(N::AttributeItem, 2, 3, Eof),
                Norm!(N::AttributeItem, 2, 3, LookT(T::COMMA)),
            ],
            S461 => &[
                Norm!(N::AttributeItem, 3, 3, Eof),
                Norm!(N::AttributeItem, 3, 3, LookT(T::COMMA)),
            ],
            S462 => &[Norm!(N::_36, 1, 3, Eof)],
            S463 => &[
                Norm!(N::AttributeItem, 2, 4, Eof),
                Norm!(N::AttributeItem, 2, 4, LookT(T::COMMA)),
            ],
            S464 => &[IndexedRule::Start {
                node: N::Literal,
                index: 0,
            }],
            S465 => &[IndexedRule::Start {
                node: N::Literal,
                index: 1,
            }],
            S466 => &[IndexedRule::Start {
                node: N::TestValue,
                index: 0,
            }],
            S467 => &[IndexedRule::Start {
                node: N::TestValue,
                index: 1,
            }],
            S468 => &[IndexedRule::Start {
                node: N::TestField,
                index: 0,
            }],
            S469 => &[IndexedRule::Start {
                node: N::TestField,
                index: 1,
            }],
            S470 => &[Norm!(N::TestField, 0, 1, Eof)],
            S471 => &[Norm!(N::TestField, 0, 2, Eof)],
            S472 => &[Norm!(N::TestField, 0, 3, Eof)],
            S473 => &[Norm!(N::TestField, 0, 4, Eof)],
            S474 => &[Norm!(N::TestItem, 2, 1, Eof)],
            S475 => &[Norm!(N::TestItem, 0, 1, Eof)],
            S476 => &[Norm!(N::TestItem, 1, 1, Eof)],
            S477 => &[Norm!(N::TestItem, 2, 2, Eof)],
            S478 => &[Norm!(N::TestItem, 2, 3, Eof)],
            S479 => &[IndexedRule::Start {
                node: N::TestItem,
                index: 0,
            }],
            S480 => &[IndexedRule::Start {
                node: N::TestItem,
                index: 1,
            }],
            S481 => &[IndexedRule::Start {
                node: N::Rule,
                index: 0,
            }],
            S482 => &[IndexedRule::Start {
                node: N::Rule,
                index: 1,
            }],
            S483 => &[Norm!(N::Rule, 0, 1, Eof), Norm!(N::Rule, 1, 1, Eof)],
            S484 => &[Norm!(N::Rule, 1, 2, Eof)],
            S485 => &[Norm!(N::Rule, 1, 3, Eof)],
            S486 => &[IndexedRule::Start {
                node: N::Conflict,
                index: 0,
            }],
            S487 => &[IndexedRule::Start {
                node: N::Conflict,
                index: 1,
            }],
            S488 => &[IndexedRule::Start {
                node: N::ConflictAction,
                index: 0,
            }],
            S489 => &[Norm!(N::ConflictAction, 2, 1, Eof)],
            S490 => &[
                Norm!(N::ConflictAction, 0, 1, Eof),
                Norm!(N::ConflictAction, 1, 1, Eof),
            ],
            S491 => &[IndexedRule::Start {
                node: N::ConflictAction,
                index: 1,
            }],
            S492 => &[Norm!(N::ConflictAction, 2, 2, Eof)],
            S493 => &[
                Norm!(N::ConflictAction, 0, 2, Eof),
                Norm!(N::ConflictAction, 1, 2, Eof),
            ],
            S494 => &[Norm!(N::ConflictAction, 2, 3, Eof)],
            S495 => &[Norm!(N::ConflictAction, 0, 3, Eof)],
            S496 => &[Norm!(N::ConflictAction, 1, 3, Eof)],
            S497 => &[Norm!(N::ConflictAction, 2, 4, Eof)],
            S498 => &[Norm!(N::ConflictAction, 0, 4, Eof)],
            S499 => &[Norm!(N::ConflictAction, 1, 4, Eof)],
            S500 => &[IndexedRule::Start {
                node: N::IdOrTokenStr,
                index: 0,
            }],
            S501 => &[IndexedRule::Start {
                node: N::IdOrTokenStr,
                index: 1,
            }],
            S502 => &[Norm!(N::IdOrTokenStr, 1, 1, Eof)],
            S503 => &[Norm!(N::IdOrTokenStr, 0, 1, Eof)],
        }
    }
    fn available_lookaheads(&self, state: Self::State) -> Self::Lookaheads {
        use lr_parser::grammar::Lookahead;
        use Lookahead::{Eof, Token as LookT};
        use ParserState::*;
        use Token as T;
        match state {
            S0 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S1 => &[LookT(T::STRING)] as &[_],
            S2 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S3 => &[Eof] as &[_],
            S4 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S5 => &[
                LookT(T::SHARP),
                LookT(T::TOKENS_KW),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S6 => &[
                LookT(T::SHARP),
                LookT(T::TOKENS_KW),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S7 => &[
                LookT(T::SHARP),
                LookT(T::TOKENS_KW),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S8 => &[LookT(T::BRACKET_LEFT)] as &[_],
            S9 => &[Eof] as &[_],
            S10 => &[LookT(T::TOKENS_KW), LookT(T::NODE_KW), LookT(T::TEST_KW)] as &[_],
            S11 => &[LookT(T::BRACE_LEFT)] as &[_],
            S12 => &[LookT(T::SEMI_COLON)] as &[_],
            S13 => &[Eof] as &[_],
            S14 => &[LookT(T::NODE_KW), LookT(T::TEST_KW), LookT(T::TOKENS_KW)] as &[_],
            S15 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S16 => &[LookT(T::BRACE_LEFT)] as &[_],
            S17 => &[LookT(T::ID)] as &[_],
            S18 => &[LookT(T::STRING)] as &[_],
            S19 => &[LookT(T::PREFER_KW)] as &[_],
            S20 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S21 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S22 => &[
                LookT(T::COMMA),
                LookT(T::PAR_LEFT),
                LookT(T::EQUAL),
                LookT(T::BRACKET_RIGHT),
            ] as &[_],
            S23 => &[LookT(T::BRACKET_RIGHT), LookT(T::COMMA)] as &[_],
            S24 => &[LookT(T::COMMA), LookT(T::BRACKET_RIGHT)] as &[_],
            S25 => &[LookT(T::COMMA), LookT(T::BRACKET_RIGHT)] as &[_],
            S26 => &[LookT(T::BRACKET_RIGHT), LookT(T::COMMA)] as &[_],
            S27 => &[LookT(T::BRACKET_RIGHT), LookT(T::COMMA)] as &[_],
            S28 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::VERBATIM_KW),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S29 => &[LookT(T::BRACE_LEFT)] as &[_],
            S30 => &[LookT(T::ID)] as &[_],
            S31 => &[LookT(T::COLON)] as &[_],
            S32 => &[
                LookT(T::SHARP),
                LookT(T::TOKENS_KW),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S33 => &[
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S34 => &[LookT(T::TOKEN_STR), LookT(T::STRING), LookT(T::INT)] as &[_],
            S35 => &[
                LookT(T::ID),
                LookT(T::BRACKET_RIGHT),
                LookT(T::STRING),
                LookT(T::INT),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S36 => &[LookT(T::VERBATIM_KW), LookT(T::BRACE_RIGHT)] as &[_],
            S37 => &[LookT(T::BRACKET_LEFT)] as &[_],
            S38 => &[
                LookT(T::VERBATIM_KW),
                LookT(T::COMMA),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S39 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S40 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S41 => &[LookT(T::SHARP), LookT(T::DOC_COMMENT), LookT(T::ID)] as &[_],
            S42 => &[LookT(T::ID)] as &[_],
            S43 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::BAR),
                LookT(T::DOC_COMMENT),
                LookT(T::PAR_LEFT),
                LookT(T::SHARP),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S44 => &[LookT(T::BRACE_LEFT)] as &[_],
            S45 => &[LookT(T::BRACE_LEFT), LookT(T::COLON_COLON)] as &[_],
            S46 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S47 => &[LookT(T::REDUCE_KW), LookT(T::SHIFT_KW)] as &[_],
            S48 => &[LookT(T::PAR_RIGHT)] as &[_],
            S49 => &[
                LookT(T::COMMA),
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::EQUAL),
            ] as &[_],
            S50 => &[LookT(T::PAR_RIGHT)] as &[_],
            S51 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S52 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S53 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S54 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S55 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S56 => &[LookT(T::BRACKET_RIGHT), LookT(T::COMMA)] as &[_],
            S57 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S58 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S59 => &[LookT(T::STRING)] as &[_],
            S60 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S61 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::VERBATIM_KW),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S62 => &[LookT(T::ID)] as &[_],
            S63 => &[LookT(T::COLON)] as &[_],
            S64 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S65 => &[LookT(T::BRACE_RIGHT), LookT(T::BAR)] as &[_],
            S66 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S67 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S68 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S69 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S70 => &[LookT(T::BRACKET_LEFT)] as &[_],
            S71 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S72 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S73 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S74 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S75 => &[
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S76 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S77 => &[LookT(T::ID)] as &[_],
            S78 => &[LookT(T::PAR_LEFT)] as &[_],
            S79 => &[LookT(T::PAR_LEFT)] as &[_],
            S80 => &[LookT(T::OVER_KW)] as &[_],
            S81 => &[
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S82 => &[LookT(T::TOKEN_STR), LookT(T::STRING), LookT(T::INT)] as &[_],
            S83 => &[LookT(T::BRACKET_RIGHT), LookT(T::COMMA)] as &[_],
            S84 => &[
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S85 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S86 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S87 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S88 => &[LookT(T::VERBATIM_KW), LookT(T::BRACE_RIGHT)] as &[_],
            S89 => &[
                LookT(T::TOKEN_STR),
                LookT(T::REGEX_KW),
                LookT(T::EXTERNAL_KW),
            ] as &[_],
            S90 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S91 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S92 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S93 => &[LookT(T::ID)] as &[_],
            S94 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S95 => &[
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S96 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S97 => &[
                LookT(T::INT),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::PLUS),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S98 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BRACE_RIGHT),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::COLON),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S99 => &[LookT(T::COLON)] as &[_],
            S100 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::INT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S101 => &[
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::ID),
                LookT(T::QUESTION),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::PLUS_UNDERSCORE),
            ] as &[_],
            S102 => &[LookT(T::COLON)] as &[_],
            S103 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S104 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S105 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S106 => &[LookT(T::BRACKET_LEFT)] as &[_],
            S107 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S108 => &[LookT(T::INT), LookT(T::ID)] as &[_],
            S109 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S110 => &[LookT(T::BRACE_LEFT)] as &[_],
            S111 => &[LookT(T::ID)] as &[_],
            S112 => &[LookT(T::TOKEN_STR), LookT(T::UNDERSCORE), LookT(T::ID)] as &[_],
            S113 => &[LookT(T::COLON)] as &[_],
            S114 => &[LookT(T::PAR_RIGHT)] as &[_],
            S115 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S116 => &[LookT(T::PAR_RIGHT)] as &[_],
            S117 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S118 => &[
                LookT(T::COMMA),
                LookT(T::VERBATIM_KW),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S119 => &[LookT(T::PAR_LEFT)] as &[_],
            S120 => &[
                LookT(T::VERBATIM_KW),
                LookT(T::COMMA),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S121 => &[
                LookT(T::VERBATIM_KW),
                LookT(T::COMMA),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S122 => &[LookT(T::ARROW)] as &[_],
            S123 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S124 => &[LookT(T::PAR_RIGHT), LookT(T::BAR)] as &[_],
            S125 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S126 => &[LookT(T::PAR_RIGHT)] as &[_],
            S127 => &[LookT(T::PAR_RIGHT), LookT(T::BAR)] as &[_],
            S128 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S129 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S130 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::PLUS),
            ] as &[_],
            S131 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S132 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S133 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::INT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S134 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::INT),
            ] as &[_],
            S135 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::INT),
            ] as &[_],
            S136 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S137 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::INT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S138 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::INT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S139 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BRACE_RIGHT),
                LookT(T::INT),
            ] as &[_],
            S140 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S141 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S142 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S143 => &[LookT(T::INT), LookT(T::ID)] as &[_],
            S144 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S145 => &[LookT(T::COLON)] as &[_],
            S146 => &[LookT(T::COLON)] as &[_],
            S147 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S148 => &[LookT(T::PAR_RIGHT)] as &[_],
            S149 => &[LookT(T::PAR_RIGHT), LookT(T::COLON_COLON)] as &[_],
            S150 => &[LookT(T::PAR_RIGHT)] as &[_],
            S151 => &[LookT(T::PAR_RIGHT)] as &[_],
            S152 => &[LookT(T::PAR_RIGHT)] as &[_],
            S153 => &[LookT(T::PAR_RIGHT)] as &[_],
            S154 => &[LookT(T::REDUCE_KW), LookT(T::SHIFT_KW)] as &[_],
            S155 => &[LookT(T::COMMA), LookT(T::PAR_RIGHT)] as &[_],
            S156 => &[LookT(T::STRING)] as &[_],
            S157 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::SHARP),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S158 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S159 => &[LookT(T::PAR_RIGHT)] as &[_],
            S160 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S161 => &[
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::QUESTION),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::PLUS_UNDERSCORE),
            ] as &[_],
            S162 => &[LookT(T::PAR_RIGHT)] as &[_],
            S163 => &[LookT(T::PAR_RIGHT), LookT(T::BAR)] as &[_],
            S164 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S165 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S166 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S167 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S168 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S169 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S170 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S171 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S172 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::BAR),
            ] as &[_],
            S173 => &[
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::INT),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::QUESTION),
            ] as &[_],
            S174 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S175 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S176 => &[LookT(T::ID), LookT(T::STRING), LookT(T::BRACKET_LEFT)] as &[_],
            S177 => &[LookT(T::OVER_KW)] as &[_],
            S178 => &[LookT(T::ID)] as &[_],
            S179 => &[LookT(T::OVER_KW)] as &[_],
            S180 => &[LookT(T::OVER_KW)] as &[_],
            S181 => &[LookT(T::PAR_LEFT)] as &[_],
            S182 => &[LookT(T::PAR_LEFT)] as &[_],
            S183 => &[LookT(T::BRACE_RIGHT), LookT(T::LOOKAHEAD_KW)] as &[_],
            S184 => &[LookT(T::PAR_RIGHT)] as &[_],
            S185 => &[LookT(T::BRACE_RIGHT), LookT(T::BAR)] as &[_],
            S186 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S187 => &[LookT(T::BRACE_RIGHT), LookT(T::BAR)] as &[_],
            S188 => &[
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S189 => &[LookT(T::PAR_RIGHT), LookT(T::BAR)] as &[_],
            S190 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S191 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S192 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S193 => &[LookT(T::BRACE_LEFT)] as &[_],
            S194 => &[
                LookT(T::ID),
                LookT(T::BRACKET_RIGHT),
                LookT(T::STRING),
                LookT(T::BRACKET_LEFT),
            ] as &[_],
            S195 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S196 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S197 => &[LookT(T::PAR_RIGHT)] as &[_],
            S198 => &[LookT(T::ID)] as &[_],
            S199 => &[LookT(T::TOKEN_STR), LookT(T::UNDERSCORE), LookT(T::ID)] as &[_],
            S200 => &[LookT(T::COLON)] as &[_],
            S201 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S202 => &[
                LookT(T::VERBATIM_KW),
                LookT(T::COMMA),
                LookT(T::BRACE_RIGHT),
            ] as &[_],
            S203 => &[LookT(T::BRACE_RIGHT), LookT(T::BAR)] as &[_],
            S204 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S205 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BRACE_RIGHT),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::COLON),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S206 => &[LookT(T::COLON)] as &[_],
            S207 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S208 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BRACE_RIGHT),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S209 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BRACE_RIGHT),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S210 => &[LookT(T::PAR_RIGHT)] as &[_],
            S211 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S212 => &[
                LookT(T::BRACKET_LEFT),
                LookT(T::BRACKET_RIGHT),
                LookT(T::ID),
                LookT(T::STRING),
            ] as &[_],
            S213 => &[LookT(T::BRACE_LEFT)] as &[_],
            S214 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S215 => &[
                LookT(T::BRACKET_LEFT),
                LookT(T::BRACKET_RIGHT),
                LookT(T::STRING),
                LookT(T::ID),
            ] as &[_],
            S216 => &[
                LookT(T::BRACKET_LEFT),
                LookT(T::BRACKET_RIGHT),
                LookT(T::ID),
                LookT(T::STRING),
            ] as &[_],
            S217 => &[
                LookT(T::ID),
                LookT(T::BRACKET_RIGHT),
                LookT(T::STRING),
                LookT(T::BRACKET_LEFT),
            ] as &[_],
            S218 => &[LookT(T::PAR_RIGHT)] as &[_],
            S219 => &[LookT(T::PAR_RIGHT)] as &[_],
            S220 => &[LookT(T::PAR_RIGHT)] as &[_],
            S221 => &[LookT(T::TOKEN_STR), LookT(T::ID)] as &[_],
            S222 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S223 => &[LookT(T::PAR_RIGHT)] as &[_],
            S224 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S225 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S226 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S227 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S228 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S229 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S230 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S231 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S232 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S233 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S234 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S235 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S236 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S237 => &[
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::INT),
            ] as &[_],
            S238 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S239 => &[LookT(T::BRACE_RIGHT), LookT(T::LOOKAHEAD_KW)] as &[_],
            S240 => &[LookT(T::LOOKAHEAD_KW), LookT(T::BRACE_RIGHT)] as &[_],
            S241 => &[LookT(T::BRACE_RIGHT), LookT(T::LOOKAHEAD_KW)] as &[_],
            S242 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S243 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S244 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S245 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BRACE_RIGHT),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::BAR),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S246 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::BRACE_RIGHT),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S247 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S248 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S249 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S250 => &[
                LookT(T::BRACKET_LEFT),
                LookT(T::BRACKET_RIGHT),
                LookT(T::STRING),
                LookT(T::ID),
            ] as &[_],
            S251 => &[
                LookT(T::ID),
                LookT(T::BRACKET_RIGHT),
                LookT(T::STRING),
                LookT(T::BRACKET_LEFT),
            ] as &[_],
            S252 => &[
                LookT(T::INCLUDE_KW),
                LookT(T::TOKENS_KW),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::NODE_KW),
                LookT(T::TEST_KW),
                LookT(T::CONFLICT_KW),
            ] as &[_],
            S253 => &[LookT(T::STRING)] as &[_],
            S254 => &[Eof] as &[_],
            S255 => &[Eof] as &[_],
            S256 => &[LookT(T::BRACE_LEFT)] as &[_],
            S257 => &[LookT(T::TOKENS_KW), LookT(T::NODE_KW), LookT(T::TEST_KW)] as &[_],
            S258 => &[LookT(T::SEMI_COLON)] as &[_],
            S259 => &[LookT(T::PREFER_KW)] as &[_],
            S260 => &[LookT(T::BRACE_LEFT)] as &[_],
            S261 => &[LookT(T::ID)] as &[_],
            S262 => &[LookT(T::STRING)] as &[_],
            S263 => &[Eof] as &[_],
            S264 => &[LookT(T::COLON)] as &[_],
            S265 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::VERBATIM_KW),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S266 => &[LookT(T::BRACE_LEFT)] as &[_],
            S267 => &[LookT(T::ID)] as &[_],
            S268 => &[LookT(T::REDUCE_KW), LookT(T::SHIFT_KW)] as &[_],
            S269 => &[LookT(T::VERBATIM_KW), LookT(T::BRACE_RIGHT)] as &[_],
            S270 => &[
                LookT(T::ID),
                LookT(T::BRACE_RIGHT),
                LookT(T::DOC_COMMENT),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::SHARP),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S271 => &[LookT(T::BRACE_LEFT)] as &[_],
            S272 => &[Eof] as &[_],
            S273 => &[LookT(T::OVER_KW)] as &[_],
            S274 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S275 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S276 => &[
                LookT(T::SHARP),
                LookT(T::BRACE_RIGHT),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S277 => &[LookT(T::COLON)] as &[_],
            S278 => &[Eof] as &[_],
            S279 => &[Eof] as &[_],
            S280 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S281 => &[LookT(T::REDUCE_KW), LookT(T::SHIFT_KW)] as &[_],
            S282 => &[Eof] as &[_],
            S283 => &[LookT(T::BRACE_RIGHT), LookT(T::LOOKAHEAD_KW)] as &[_],
            S284 => &[LookT(T::BRACE_RIGHT)] as &[_],
            S285 => &[Eof] as &[_],
            S286 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S287 => &[Eof] as &[_],
            S288 => &[LookT(T::ID)] as &[_],
            S289 => &[LookT(T::COLON)] as &[_],
            S290 => &[
                LookT(T::TOKEN_STR),
                LookT(T::REGEX_KW),
                LookT(T::EXTERNAL_KW),
            ] as &[_],
            S291 => &[Eof] as &[_],
            S292 => &[LookT(T::PAR_LEFT)] as &[_],
            S293 => &[Eof] as &[_],
            S294 => &[Eof] as &[_],
            S295 => &[LookT(T::STRING)] as &[_],
            S296 => &[LookT(T::PAR_RIGHT)] as &[_],
            S297 => &[Eof] as &[_],
            S298 => &[
                LookT(T::TOKEN_STR),
                LookT(T::REGEX_KW),
                LookT(T::EXTERNAL_KW),
            ] as &[_],
            S299 => &[Eof] as &[_],
            S300 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::SHARP),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S301 => &[Eof] as &[_],
            S302 => &[Eof, LookT(T::BAR)] as &[_],
            S303 => &[Eof] as &[_],
            S304 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S305 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S306 => &[Eof] as &[_],
            S307 => &[Eof] as &[_],
            S308 => &[
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S309 => &[Eof] as &[_],
            S310 => &[Eof] as &[_],
            S311 => &[LookT(T::ID)] as &[_],
            S312 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S313 => &[
                LookT(T::INT),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PLUS),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S314 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::COLON),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S315 => &[LookT(T::COLON)] as &[_],
            S316 => &[
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::ID),
                Eof,
                LookT(T::QUESTION),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::PLUS_UNDERSCORE),
            ] as &[_],
            S317 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S318 => &[LookT(T::ARROW)] as &[_],
            S319 => &[LookT(T::PAR_RIGHT)] as &[_],
            S320 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S321 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S322 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S323 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S324 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S325 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S326 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S327 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S328 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S329 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S330 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::SHARP),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S331 => &[
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::QUESTION),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::PLUS_UNDERSCORE),
            ] as &[_],
            S332 => &[
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::QUESTION),
            ] as &[_],
            S333 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S334 => &[Eof, LookT(T::BAR)] as &[_],
            S335 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S336 => &[Eof, LookT(T::BAR)] as &[_],
            S337 => &[
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S338 => &[Eof, LookT(T::BAR)] as &[_],
            S339 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S340 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::COLON),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S341 => &[LookT(T::COLON)] as &[_],
            S342 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S343 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::BAR),
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S344 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S345 => &[LookT(T::PAR_RIGHT)] as &[_],
            S346 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S347 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S348 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S349 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S350 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S351 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S352 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S353 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S354 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S355 => &[
                LookT(T::ID),
                Eof,
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::BAR),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S356 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::DOC_COMMENT),
                LookT(T::SHARP),
                LookT(T::BAR),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S357 => &[
                LookT(T::STAR),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::INT),
                LookT(T::QUESTION),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::DOC_COMMENT),
                LookT(T::PLUS),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S358 => &[
                LookT(T::ID),
                Eof,
                LookT(T::BAR),
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::DOC_COMMENT),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S359 => &[LookT(T::BAR)] as &[_],
            S360 => &[Eof] as &[_],
            S361 => &[LookT(T::SHARP), LookT(T::ID), LookT(T::DOC_COMMENT)] as &[_],
            S362 => &[LookT(T::ID)] as &[_],
            S363 => &[LookT(T::ARROW)] as &[_],
            S364 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S365 => &[Eof] as &[_],
            S366 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S367 => &[Eof] as &[_],
            S368 => &[
                LookT(T::SHARP),
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S369 => &[Eof] as &[_],
            S370 => &[
                LookT(T::PAR_LEFT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::INT),
            ] as &[_],
            S371 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S372 => &[Eof] as &[_],
            S373 => &[
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::COLON),
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S374 => &[LookT(T::COLON)] as &[_],
            S375 => &[
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S376 => &[
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S377 => &[LookT(T::PAR_RIGHT)] as &[_],
            S378 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S379 => &[Eof] as &[_],
            S380 => &[Eof] as &[_],
            S381 => &[Eof] as &[_],
            S382 => &[Eof] as &[_],
            S383 => &[Eof] as &[_],
            S384 => &[Eof] as &[_],
            S385 => &[Eof] as &[_],
            S386 => &[Eof] as &[_],
            S387 => &[Eof] as &[_],
            S388 => &[
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S389 => &[
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S390 => &[Eof] as &[_],
            S391 => &[LookT(T::INT), LookT(T::ID)] as &[_],
            S392 => &[Eof] as &[_],
            S393 => &[Eof] as &[_],
            S394 => &[Eof] as &[_],
            S395 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S396 => &[Eof] as &[_],
            S397 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S398 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S399 => &[Eof] as &[_],
            S400 => &[Eof] as &[_],
            S401 => &[Eof] as &[_],
            S402 => &[LookT(T::PAR_RIGHT)] as &[_],
            S403 => &[Eof] as &[_],
            S404 => &[LookT(T::TOKEN_STR), LookT(T::PAR_LEFT), LookT(T::ID)] as &[_],
            S405 => &[
                LookT(T::PAR_LEFT),
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
            ] as &[_],
            S406 => &[Eof] as &[_],
            S407 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S408 => &[Eof] as &[_],
            S409 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::ID),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S410 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S411 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::PLUS),
            ] as &[_],
            S412 => &[LookT(T::PAR_RIGHT)] as &[_],
            S413 => &[Eof] as &[_],
            S414 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S415 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S416 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S417 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S418 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S419 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S420 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S421 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S422 => &[LookT(T::PAR_LEFT), LookT(T::ID), LookT(T::TOKEN_STR), Eof] as &[_],
            S423 => &[
                LookT(T::QUESTION),
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::ID),
                LookT(T::UNDERSCORE_STAR),
                Eof,
                LookT(T::PAR_LEFT),
                LookT(T::PLUS),
                LookT(T::TOKEN_STR),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
            ] as &[_],
            S424 => &[
                LookT(T::STAR),
                LookT(T::STAR_UNDERSCORE),
                LookT(T::UNDERSCORE_STAR),
                LookT(T::PLUS),
                LookT(T::PLUS_UNDERSCORE),
                LookT(T::UNDERSCORE_PLUS),
                LookT(T::QUESTION),
            ] as &[_],
            S425 => &[Eof] as &[_],
            S426 => &[LookT(T::SHARP), LookT(T::DOC_COMMENT)] as &[_],
            S427 => &[Eof] as &[_],
            S428 => &[Eof] as &[_],
            S429 => &[LookT(T::BRACKET_LEFT)] as &[_],
            S430 => &[Eof] as &[_],
            S431 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S432 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S433 => &[Eof] as &[_],
            S434 => &[LookT(T::SHARP)] as &[_],
            S435 => &[Eof] as &[_],
            S436 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S437 => &[LookT(T::EQUAL), Eof, LookT(T::PAR_LEFT)] as &[_],
            S438 => &[Eof] as &[_],
            S439 => &[Eof] as &[_],
            S440 => &[Eof] as &[_],
            S441 => &[Eof] as &[_],
            S442 => &[Eof] as &[_],
            S443 => &[
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S444 => &[LookT(T::TOKEN_STR), LookT(T::STRING), LookT(T::INT)] as &[_],
            S445 => &[LookT(T::PAR_RIGHT)] as &[_],
            S446 => &[Eof] as &[_],
            S447 => &[Eof] as &[_],
            S448 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S449 => &[Eof] as &[_],
            S450 => &[LookT(T::COMMA), LookT(T::PAR_LEFT), LookT(T::EQUAL), Eof] as &[_],
            S451 => &[Eof] as &[_],
            S452 => &[LookT(T::COMMA), Eof] as &[_],
            S453 => &[LookT(T::COMMA), Eof] as &[_],
            S454 => &[LookT(T::COMMA), Eof] as &[_],
            S455 => &[LookT(T::COMMA), Eof] as &[_],
            S456 => &[LookT(T::COMMA), Eof] as &[_],
            S457 => &[
                LookT(T::PAR_RIGHT),
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S458 => &[LookT(T::TOKEN_STR), LookT(T::STRING), LookT(T::INT)] as &[_],
            S459 => &[
                LookT(T::ID),
                LookT(T::TOKEN_STR),
                Eof,
                LookT(T::STRING),
                LookT(T::INT),
            ] as &[_],
            S460 => &[LookT(T::PAR_RIGHT)] as &[_],
            S461 => &[LookT(T::COMMA), Eof] as &[_],
            S462 => &[Eof] as &[_],
            S463 => &[LookT(T::COMMA), Eof] as &[_],
            S464 => &[LookT(T::TOKEN_STR), LookT(T::STRING), LookT(T::INT)] as &[_],
            S465 => &[Eof] as &[_],
            S466 => &[LookT(T::ID)] as &[_],
            S467 => &[Eof] as &[_],
            S468 => &[
                LookT(T::SHARP),
                LookT(T::ID),
                LookT(T::INT),
                LookT(T::DOC_COMMENT),
            ] as &[_],
            S469 => &[Eof] as &[_],
            S470 => &[LookT(T::INT), LookT(T::ID)] as &[_],
            S471 => &[LookT(T::COLON)] as &[_],
            S472 => &[LookT(T::ID), LookT(T::STRING), LookT(T::BRACKET_LEFT)] as &[_],
            S473 => &[Eof] as &[_],
            S474 => &[
                LookT(T::BRACKET_LEFT),
                LookT(T::BRACKET_RIGHT),
                LookT(T::ID),
                LookT(T::STRING),
            ] as &[_],
            S475 => &[Eof] as &[_],
            S476 => &[Eof] as &[_],
            S477 => &[LookT(T::BRACKET_RIGHT)] as &[_],
            S478 => &[Eof] as &[_],
            S479 => &[LookT(T::ID), LookT(T::STRING), LookT(T::BRACKET_LEFT)] as &[_],
            S480 => &[Eof] as &[_],
            S481 => &[LookT(T::ID)] as &[_],
            S482 => &[Eof] as &[_],
            S483 => &[Eof, LookT(T::COLON_COLON)] as &[_],
            S484 => &[LookT(T::ID)] as &[_],
            S485 => &[Eof] as &[_],
            S486 => &[LookT(T::CONFLICT_KW)] as &[_],
            S487 => &[Eof] as &[_],
            S488 => &[LookT(T::REDUCE_KW), LookT(T::SHIFT_KW)] as &[_],
            S489 => &[LookT(T::PAR_LEFT)] as &[_],
            S490 => &[LookT(T::PAR_LEFT)] as &[_],
            S491 => &[Eof] as &[_],
            S492 => &[LookT(T::ID)] as &[_],
            S493 => &[LookT(T::TOKEN_STR), LookT(T::UNDERSCORE), LookT(T::ID)] as &[_],
            S494 => &[LookT(T::PAR_RIGHT)] as &[_],
            S495 => &[LookT(T::PAR_RIGHT)] as &[_],
            S496 => &[LookT(T::PAR_RIGHT)] as &[_],
            S497 => &[Eof] as &[_],
            S498 => &[Eof] as &[_],
            S499 => &[Eof] as &[_],
            S500 => &[LookT(T::TOKEN_STR), LookT(T::ID)] as &[_],
            S501 => &[Eof] as &[_],
            S502 => &[Eof] as &[_],
            S503 => &[Eof] as &[_],
        }
        .iter()
        .copied()
    }
    fn action(
        &self,
        state: Self::State,
        lookahead: ::lr_parser::grammar::Lookahead<Token>,
    ) -> Option<::lr_parser::compute::LR1Action<Node, Self::State>> {
        use lr_parser::{
            compute::LR1Action,
            grammar::{Lookahead, RuleIdx},
        };
        use LR1Action::{Accept, Reduce, Shift};
        use Lookahead::{Eof, Token as LookT};
        use ParserState::*;
        match (state, lookahead) {
            (S0, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_25,
                    index: 0,
                },
            }),
            (S0, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S5 }),
            (S0, LookT(Token::INCLUDE_KW)) => Some(Shift { state: S1 }),
            (S0, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S0, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S0, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S0, LookT(Token::CONFLICT_KW)) => Some(Shift { state: S11 }),
            (S0, LookT(Token::SHARP)) => Some(Shift { state: S8 }),
            (S1, LookT(Token::STRING)) => Some(Shift { state: S12 }),
            (S2, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S2, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S3, Eof) => Some(Accept(Node::File)),
            (S4, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_25,
                    index: 0,
                },
            }),
            (S4, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S5 }),
            (S4, LookT(Token::INCLUDE_KW)) => Some(Shift { state: S1 }),
            (S4, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S4, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S4, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S4, LookT(Token::CONFLICT_KW)) => Some(Shift { state: S11 }),
            (S4, LookT(Token::SHARP)) => Some(Shift { state: S8 }),
            (S5, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S5, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S5, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S5, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S5, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S6, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S5 }),
            (S6, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S6, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S6, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S6, LookT(Token::SHARP)) => Some(Shift { state: S8 }),
            (S7, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S7, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S7, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S7, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S7, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S8, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S15 }),
            (S9, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::File,
                    index: 0,
                },
            }),
            (S10, LookT(Token::TOKENS_KW)) => Some(Shift { state: S16 }),
            (S10, LookT(Token::NODE_KW)) => Some(Shift { state: S17 }),
            (S10, LookT(Token::TEST_KW)) => Some(Shift { state: S18 }),
            (S11, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S19 }),
            (S12, LookT(Token::SEMI_COLON)) => Some(Shift { state: S20 }),
            (S13, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_25,
                    index: 1,
                },
            }),
            (S14, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S14, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S14, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S15, LookT(Token::ID)) => Some(Shift { state: S22 }),
            (S15, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S15, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S15, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S16, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S28 }),
            (S17, LookT(Token::ID)) => Some(Shift { state: S29 }),
            (S18, LookT(Token::STRING)) => Some(Shift { state: S30 }),
            (S19, LookT(Token::PREFER_KW)) => Some(Shift { state: S31 }),
            (S20, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S20, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S21, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S32 }),
            (S22, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S22, LookT(Token::PAR_LEFT)) => Some(Shift { state: S33 }),
            (S22, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S22, LookT(Token::EQUAL)) => Some(Shift { state: S34 }),
            (S23, LookT(Token::COMMA)) => Some(Shift { state: S35 }),
            (S23, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_35,
                    index: 2,
                },
            }),
            (S24, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S24, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S25, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S25, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S26, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S26, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S27, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S27, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S28, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S28, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 0,
                },
            }),
            (S28, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S28, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 0,
                },
            }),
            (S28, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S29, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S43 }),
            (S30, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S31, LookT(Token::COLON)) => Some(Shift { state: S47 }),
            (S32, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S32, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S32, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S32, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S32, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S33, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S33, LookT(Token::ID)) => Some(Shift { state: S49 }),
            (S33, LookT(Token::TOKEN_STR)) => Some(Shift { state: S54 }),
            (S33, LookT(Token::STRING)) => Some(Shift { state: S53 }),
            (S33, LookT(Token::INT)) => Some(Shift { state: S55 }),
            (S34, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S34, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S34, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S35, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_35,
                    index: 0,
                },
            }),
            (S35, LookT(Token::ID)) => Some(Shift { state: S22 }),
            (S35, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S35, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S35, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S36, LookT(Token::VERBATIM_KW)) => Some(Shift { state: S59 }),
            (S36, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_28,
                    index: 0,
                },
            }),
            (S37, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S60 }),
            (S38, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 2,
                },
            }),
            (S38, LookT(Token::COMMA)) => Some(Shift { state: S61 }),
            (S38, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 2,
                },
            }),
            (S39, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S39, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S39, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S40, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S40, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S40, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S41, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S41, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S41, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S42, LookT(Token::ID)) => Some(Shift { state: S63 }),
            (S43, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S43, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S43, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S43, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S43, LookT(Token::BAR)) => Some(Shift { state: S68 }),
            (S43, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S43, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S43, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S44, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S76 }),
            (S45, LookT(Token::COLON_COLON)) => Some(Shift { state: S77 }),
            (S45, LookT(Token::BRACE_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Rule,
                    index: 0,
                },
            }),
            (S46, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S46, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S47, LookT(Token::SHIFT_KW)) => Some(Shift { state: S79 }),
            (S47, LookT(Token::REDUCE_KW)) => Some(Shift { state: S78 }),
            (S48, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeArgs,
                    index: 0,
                },
            }),
            (S49, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S49, LookT(Token::PAR_LEFT)) => Some(Shift { state: S81 }),
            (S49, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S49, LookT(Token::EQUAL)) => Some(Shift { state: S82 }),
            (S50, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S83 }),
            (S51, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S51, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S52, LookT(Token::COMMA)) => Some(Shift { state: S84 }),
            (S52, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 2,
                },
            }),
            (S53, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S53, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S54, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S54, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S55, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S55, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S56, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S56, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S57, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_35,
                    index: 1,
                },
            }),
            (S58, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S85 }),
            (S59, LookT(Token::STRING)) => Some(Shift { state: S86 }),
            (S60, LookT(Token::ID)) => Some(Shift { state: S22 }),
            (S60, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S60, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S60, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S61, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S61, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 0,
                },
            }),
            (S61, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S61, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 0,
                },
            }),
            (S61, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S62, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S63, LookT(Token::COLON)) => Some(Shift { state: S89 }),
            (S64, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S90 }),
            (S65, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_29,
                    index: 0,
                },
            }),
            (S65, LookT(Token::BAR)) => Some(Shift { state: S68 }),
            (S66, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Block,
                    index: 0,
                },
            }),
            (S67, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S67, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S67, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S67, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S67, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S67, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S67, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S68, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S68, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S68, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S69, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S69, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S69, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S69, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S69, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S69, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S70, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S94 }),
            (S71, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S71, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S71, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S71, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S71, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S71, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S72, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Fields,
                    index: 0,
                },
            }),
            (S73, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S73, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S73, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S73, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S73, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S73, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S74, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Block,
                    index: 1,
                },
            }),
            (S75, LookT(Token::PAR_LEFT)) => Some(Shift { state: S96 }),
            (S75, LookT(Token::ID)) => Some(Shift { state: S98 }),
            (S75, LookT(Token::TOKEN_STR)) => Some(Shift { state: S101 }),
            (S75, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S76, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S76, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S76, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_37,
                    index: 0,
                },
            }),
            (S76, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S76, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S77, LookT(Token::ID)) => Some(Shift { state: S110 }),
            (S78, LookT(Token::PAR_LEFT)) => Some(Shift { state: S111 }),
            (S79, LookT(Token::PAR_LEFT)) => Some(Shift { state: S112 }),
            (S80, LookT(Token::OVER_KW)) => Some(Shift { state: S113 }),
            (S81, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S81, LookT(Token::ID)) => Some(Shift { state: S49 }),
            (S81, LookT(Token::TOKEN_STR)) => Some(Shift { state: S54 }),
            (S81, LookT(Token::STRING)) => Some(Shift { state: S53 }),
            (S81, LookT(Token::INT)) => Some(Shift { state: S55 }),
            (S82, LookT(Token::TOKEN_STR)) => Some(Shift { state: S54 }),
            (S82, LookT(Token::STRING)) => Some(Shift { state: S53 }),
            (S82, LookT(Token::INT)) => Some(Shift { state: S55 }),
            (S83, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S83, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S84, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S84, LookT(Token::ID)) => Some(Shift { state: S49 }),
            (S84, LookT(Token::TOKEN_STR)) => Some(Shift { state: S54 }),
            (S84, LookT(Token::STRING)) => Some(Shift { state: S53 }),
            (S84, LookT(Token::INT)) => Some(Shift { state: S55 }),
            (S85, Eof) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S85, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S86, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_28,
                    index: 1,
                },
            }),
            (S87, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S117 }),
            (S88, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 1,
                },
            }),
            (S88, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 1,
                },
            }),
            (S89, LookT(Token::REGEX_KW)) => Some(Shift { state: S119 }),
            (S89, LookT(Token::EXTERNAL_KW)) => Some(Shift { state: S118 }),
            (S89, LookT(Token::TOKEN_STR)) => Some(Shift { state: S120 }),
            (S90, Eof) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S90, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S91, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_29,
                    index: 1,
                },
            }),
            (S92, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 1,
                },
            }),
            (S93, LookT(Token::ID)) => Some(Shift { state: S122 }),
            (S94, LookT(Token::ID)) => Some(Shift { state: S22 }),
            (S94, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S94, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S94, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S95, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S95, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S95, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S95, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S96, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S96, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S96, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S96, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S97, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S97, LookT(Token::STAR)) => Some(Shift { state: S132 }),
            (S97, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S134 }),
            (S97, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S133 }),
            (S97, LookT(Token::PLUS)) => Some(Shift { state: S138 }),
            (S97, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S137 }),
            (S97, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S140 }),
            (S97, LookT(Token::QUESTION)) => Some(Shift { state: S139 }),
            (S97, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S97, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S97, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S97, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S97, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S97, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S98, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S98, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S98, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S99, LookT(Token::COLON)) => Some(Shift { state: S141 }),
            (S100, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S100, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S100, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S100, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S100, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S100, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S100, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S101, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S101, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S102, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 1,
                },
            }),
            (S103, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S103, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S103, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S103, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S104, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S142 }),
            (S105, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S105, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S105, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S105, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S106, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S144 }),
            (S107, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S107, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S107, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S107, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S108, LookT(Token::ID)) => Some(Shift { state: S146 }),
            (S108, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S109, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S109, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S109, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_37,
                    index: 0,
                },
            }),
            (S109, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S109, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S110, LookT(Token::BRACE_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Rule,
                    index: 1,
                },
            }),
            (S111, LookT(Token::ID)) => Some(Shift { state: S149 }),
            (S112, LookT(Token::UNDERSCORE)) => Some(Shift { state: S151 }),
            (S112, LookT(Token::ID)) => Some(Shift { state: S150 }),
            (S112, LookT(Token::TOKEN_STR)) => Some(Shift { state: S153 }),
            (S113, LookT(Token::COLON)) => Some(Shift { state: S154 }),
            (S114, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S155 }),
            (S115, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S115, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S116, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 1,
                },
            }),
            (S117, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S117, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S117, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S118, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 2,
                },
            }),
            (S118, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 2,
                },
            }),
            (S118, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 2,
                },
            }),
            (S119, LookT(Token::PAR_LEFT)) => Some(Shift { state: S156 }),
            (S120, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 0,
                },
            }),
            (S120, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 0,
                },
            }),
            (S120, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 0,
                },
            }),
            (S121, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Token,
                    index: 0,
                },
            }),
            (S121, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Token,
                    index: 0,
                },
            }),
            (S121, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Token,
                    index: 0,
                },
            }),
            (S122, LookT(Token::ARROW)) => Some(Shift { state: S157 }),
            (S123, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S158 }),
            (S124, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_32,
                    index: 1,
                },
            }),
            (S124, LookT(Token::BAR)) => Some(Shift { state: S160 }),
            (S125, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S125, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S126, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S161 }),
            (S127, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Alternative,
                    index: 0,
                },
            }),
            (S127, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Alternative,
                    index: 0,
                },
            }),
            (S128, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S128, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S128, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S128, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S129, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S129, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_34,
                    index: 0,
                },
            }),
            (S129, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_34,
                    index: 0,
                },
            }),
            (S129, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S129, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S130, LookT(Token::STAR)) => Some(Shift { state: S164 }),
            (S130, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S166 }),
            (S130, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S165 }),
            (S130, LookT(Token::PLUS)) => Some(Shift { state: S170 }),
            (S130, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S169 }),
            (S130, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S172 }),
            (S130, LookT(Token::QUESTION)) => Some(Shift { state: S171 }),
            (S130, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S130, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S130, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S130, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S130, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S131, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S131, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S132, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S132, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S132, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S132, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S132, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S132, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S132, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S133, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S133, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S133, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S133, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S133, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S133, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S133, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S134, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S134, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S134, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S134, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S134, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S134, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S134, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S135, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S135, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S135, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S135, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S135, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S135, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S135, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S136, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S136, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S136, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S136, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S136, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S136, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S136, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S137, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S137, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S137, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S137, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S137, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S137, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S137, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S138, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S138, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S138, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S138, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S138, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S138, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S138, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S139, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S139, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S139, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S139, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S139, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S139, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S139, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S140, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S140, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S140, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S140, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S140, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S140, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S140, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S141, LookT(Token::PAR_LEFT)) => Some(Shift { state: S96 }),
            (S141, LookT(Token::ID)) => Some(Shift { state: S173 }),
            (S141, LookT(Token::TOKEN_STR)) => Some(Shift { state: S101 }),
            (S142, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S142, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S143, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S143, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 1,
                },
            }),
            (S144, LookT(Token::ID)) => Some(Shift { state: S22 }),
            (S144, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S144, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S144, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S145, LookT(Token::COLON)) => Some(Shift { state: S176 }),
            (S146, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S147, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_37,
                    index: 1,
                },
            }),
            (S148, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S177 }),
            (S149, LookT(Token::COLON_COLON)) => Some(Shift { state: S178 }),
            (S149, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Rule,
                    index: 0,
                },
            }),
            (S150, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::IdOrTokenStr,
                    index: 0,
                },
            }),
            (S151, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S179 }),
            (S152, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S180 }),
            (S153, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::IdOrTokenStr,
                    index: 1,
                },
            }),
            (S154, LookT(Token::SHIFT_KW)) => Some(Shift { state: S182 }),
            (S154, LookT(Token::REDUCE_KW)) => Some(Shift { state: S181 }),
            (S155, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S155, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S156, LookT(Token::STRING)) => Some(Shift { state: S184 }),
            (S157, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S157, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S157, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S157, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S157, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S157, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S157, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S157, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S158, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S158, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S158, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S158, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S158, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S158, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S159, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 1,
                },
            }),
            (S160, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S160, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S160, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S161, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S161, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S162, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S190 }),
            (S163, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_34,
                    index: 1,
                },
            }),
            (S163, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_34,
                    index: 1,
                },
            }),
            (S164, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S164, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S164, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S164, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S164, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S165, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S165, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S165, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S165, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S165, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S166, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S166, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S166, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S166, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S166, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S167, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S167, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S167, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S167, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S167, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S168, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S168, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S168, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S168, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S168, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S169, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S169, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S169, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S169, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S169, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S170, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S170, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S170, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S170, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S170, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S171, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S171, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S171, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S171, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S171, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S172, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S172, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S172, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S172, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S172, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S173, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S173, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S174, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S174, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S174, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S174, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S174, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S174, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S174, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S175, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S191 }),
            (S176, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S194 }),
            (S176, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S176, LookT(Token::STRING)) => Some(Shift { state: S195 }),
            (S177, LookT(Token::OVER_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 2,
                },
            }),
            (S178, LookT(Token::ID)) => Some(Shift { state: S197 }),
            (S179, LookT(Token::OVER_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 0,
                },
            }),
            (S180, LookT(Token::OVER_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 1,
                },
            }),
            (S181, LookT(Token::PAR_LEFT)) => Some(Shift { state: S198 }),
            (S182, LookT(Token::PAR_LEFT)) => Some(Shift { state: S199 }),
            (S183, LookT(Token::LOOKAHEAD_KW)) => Some(Shift { state: S200 }),
            (S183, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_39,
                    index: 0,
                },
            }),
            (S184, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S202 }),
            (S185, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 5,
                rule_index: RuleIdx {
                    lhs: Node::Variant,
                    index: 0,
                },
            }),
            (S185, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 5,
                rule_index: RuleIdx {
                    lhs: Node::Variant,
                    index: 0,
                },
            }),
            (S186, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S186, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S186, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S186, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S186, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S186, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S186, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S186, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S187, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Fields,
                    index: 0,
                },
            }),
            (S187, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Fields,
                    index: 0,
                },
            }),
            (S188, LookT(Token::PAR_LEFT)) => Some(Shift { state: S204 }),
            (S188, LookT(Token::ID)) => Some(Shift { state: S205 }),
            (S188, LookT(Token::TOKEN_STR)) => Some(Shift { state: S208 }),
            (S188, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S189, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_32,
                    index: 1,
                },
            }),
            (S189, LookT(Token::BAR)) => Some(Shift { state: S160 }),
            (S190, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S190, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S191, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S191, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S191, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S191, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S192, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestField,
                    index: 0,
                },
            }),
            (S192, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestField,
                    index: 0,
                },
            }),
            (S192, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestField,
                    index: 0,
                },
            }),
            (S192, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestField,
                    index: 0,
                },
            }),
            (S192, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestField,
                    index: 0,
                },
            }),
            (S193, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S211 }),
            (S194, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S216 }),
            (S194, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_38,
                    index: 0,
                },
            }),
            (S194, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S194, LookT(Token::STRING)) => Some(Shift { state: S215 }),
            (S195, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S195, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S195, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S195, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S195, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S196, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S196, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S196, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S196, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S196, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S197, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Rule,
                    index: 1,
                },
            }),
            (S198, LookT(Token::ID)) => Some(Shift { state: S149 }),
            (S199, LookT(Token::UNDERSCORE)) => Some(Shift { state: S219 }),
            (S199, LookT(Token::ID)) => Some(Shift { state: S150 }),
            (S199, LookT(Token::TOKEN_STR)) => Some(Shift { state: S153 }),
            (S200, LookT(Token::COLON)) => Some(Shift { state: S221 }),
            (S201, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S222 }),
            (S202, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 1,
                },
            }),
            (S202, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 1,
                },
            }),
            (S202, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 1,
                },
            }),
            (S203, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 1,
                },
            }),
            (S203, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 1,
                },
            }),
            (S204, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S204, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S204, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S204, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S205, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S205, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S205, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S206, LookT(Token::COLON)) => Some(Shift { state: S224 }),
            (S207, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S207, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S208, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S208, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S209, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::STAR)) => Some(Shift { state: S225 }),
            (S209, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S227 }),
            (S209, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S226 }),
            (S209, LookT(Token::PLUS)) => Some(Shift { state: S231 }),
            (S209, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S230 }),
            (S209, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S233 }),
            (S209, LookT(Token::QUESTION)) => Some(Shift { state: S232 }),
            (S209, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S209, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S210, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_32,
                    index: 0,
                },
            }),
            (S211, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S211, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S211, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_37,
                    index: 0,
                },
            }),
            (S211, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S211, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S212, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S216 }),
            (S212, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_38,
                    index: 0,
                },
            }),
            (S212, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S212, LookT(Token::STRING)) => Some(Shift { state: S215 }),
            (S213, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S236 }),
            (S214, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S237 }),
            (S215, LookT(Token::BRACKET_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S215, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S215, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S215, LookT(Token::STRING)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S216, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S216 }),
            (S216, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_38,
                    index: 0,
                },
            }),
            (S216, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S216, LookT(Token::STRING)) => Some(Shift { state: S215 }),
            (S217, LookT(Token::BRACKET_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S217, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S217, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S217, LookT(Token::STRING)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S218, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S239 }),
            (S219, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S240 }),
            (S220, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S241 }),
            (S221, LookT(Token::ID)) => Some(Shift { state: S244 }),
            (S221, LookT(Token::TOKEN_STR)) => Some(Shift { state: S243 }),
            (S222, Eof) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::INCLUDE_KW)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::CONFLICT_KW)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S222, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S223, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S245 }),
            (S224, LookT(Token::PAR_LEFT)) => Some(Shift { state: S204 }),
            (S224, LookT(Token::ID)) => Some(Shift { state: S246 }),
            (S224, LookT(Token::TOKEN_STR)) => Some(Shift { state: S208 }),
            (S225, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S225, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S226, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S226, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S227, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S227, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S228, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S228, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S229, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S229, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S230, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S230, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S231, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S231, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S232, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S232, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S233, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S233, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S234, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S248 }),
            (S235, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_38,
                    index: 1,
                },
            }),
            (S236, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S236, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S236, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_37,
                    index: 0,
                },
            }),
            (S236, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S236, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S237, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S237, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S237, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S237, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S237, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S238, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S250 }),
            (S239, LookT(Token::LOOKAHEAD_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 2,
                },
            }),
            (S239, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 2,
                },
            }),
            (S240, LookT(Token::LOOKAHEAD_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 0,
                },
            }),
            (S240, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 0,
                },
            }),
            (S241, LookT(Token::LOOKAHEAD_KW)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 1,
                },
            }),
            (S241, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 1,
                },
            }),
            (S242, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_39,
                    index: 1,
                },
            }),
            (S243, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::IdOrTokenStr,
                    index: 1,
                },
            }),
            (S244, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::IdOrTokenStr,
                    index: 0,
                },
            }),
            (S245, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S245, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S246, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S246, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S247, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S247, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S248, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S248, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S248, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S248, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S248, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S249, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S251 }),
            (S250, LookT(Token::BRACKET_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S250, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S250, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S250, LookT(Token::STRING)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S251, LookT(Token::BRACKET_LEFT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S251, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S251, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S251, LookT(Token::STRING)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S252, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S5 }),
            (S252, LookT(Token::INCLUDE_KW)) => Some(Shift { state: S253 }),
            (S252, LookT(Token::TOKENS_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S252, LookT(Token::NODE_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S252, LookT(Token::TEST_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S252, LookT(Token::CONFLICT_KW)) => Some(Shift { state: S256 }),
            (S252, LookT(Token::SHARP)) => Some(Shift { state: S8 }),
            (S253, LookT(Token::STRING)) => Some(Shift { state: S258 }),
            (S254, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 4,
                },
            }),
            (S255, Eof) => Some(Accept(Node::TopLevelItem)),
            (S256, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S259 }),
            (S257, LookT(Token::TOKENS_KW)) => Some(Shift { state: S260 }),
            (S257, LookT(Token::NODE_KW)) => Some(Shift { state: S261 }),
            (S257, LookT(Token::TEST_KW)) => Some(Shift { state: S262 }),
            (S258, LookT(Token::SEMI_COLON)) => Some(Shift { state: S263 }),
            (S259, LookT(Token::PREFER_KW)) => Some(Shift { state: S264 }),
            (S260, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S265 }),
            (S261, LookT(Token::ID)) => Some(Shift { state: S266 }),
            (S262, LookT(Token::STRING)) => Some(Shift { state: S267 }),
            (S263, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 0,
                },
            }),
            (S264, LookT(Token::COLON)) => Some(Shift { state: S268 }),
            (S265, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S265, LookT(Token::VERBATIM_KW)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 0,
                },
            }),
            (S265, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S265, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_27,
                    index: 0,
                },
            }),
            (S265, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S266, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S270 }),
            (S267, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S268, LookT(Token::SHIFT_KW)) => Some(Shift { state: S79 }),
            (S268, LookT(Token::REDUCE_KW)) => Some(Shift { state: S78 }),
            (S269, LookT(Token::VERBATIM_KW)) => Some(Shift { state: S59 }),
            (S269, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_28,
                    index: 0,
                },
            }),
            (S270, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S270, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S270, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S270, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S270, LookT(Token::BAR)) => Some(Shift { state: S68 }),
            (S270, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S270, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S270, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S271, LookT(Token::BRACE_LEFT)) => Some(Shift { state: S276 }),
            (S272, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 3,
                },
            }),
            (S273, LookT(Token::OVER_KW)) => Some(Shift { state: S277 }),
            (S274, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S278 }),
            (S275, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S279 }),
            (S276, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S276, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S276, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_37,
                    index: 0,
                },
            }),
            (S276, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S276, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S277, LookT(Token::COLON)) => Some(Shift { state: S281 }),
            (S278, Eof) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 1,
                },
            }),
            (S279, Eof) => Some(Reduce {
                rhs_size: 6,
                rule_index: RuleIdx {
                    lhs: Node::TopLevelItem,
                    index: 2,
                },
            }),
            (S280, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S282 }),
            (S281, LookT(Token::SHIFT_KW)) => Some(Shift { state: S182 }),
            (S281, LookT(Token::REDUCE_KW)) => Some(Shift { state: S181 }),
            (S282, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestValue,
                    index: 0,
                },
            }),
            (S283, LookT(Token::LOOKAHEAD_KW)) => Some(Shift { state: S200 }),
            (S283, LookT(Token::BRACE_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_39,
                    index: 0,
                },
            }),
            (S284, LookT(Token::BRACE_RIGHT)) => Some(Shift { state: S285 }),
            (S285, Eof) => Some(Reduce {
                rhs_size: 10,
                rule_index: RuleIdx {
                    lhs: Node::Conflict,
                    index: 0,
                },
            }),
            (S286, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S286, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S286, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S287, Eof) => Some(Accept(Node::Token)),
            (S288, LookT(Token::ID)) => Some(Shift { state: S289 }),
            (S289, LookT(Token::COLON)) => Some(Shift { state: S290 }),
            (S290, LookT(Token::REGEX_KW)) => Some(Shift { state: S292 }),
            (S290, LookT(Token::EXTERNAL_KW)) => Some(Shift { state: S291 }),
            (S290, LookT(Token::TOKEN_STR)) => Some(Shift { state: S293 }),
            (S291, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 2,
                },
            }),
            (S292, LookT(Token::PAR_LEFT)) => Some(Shift { state: S295 }),
            (S293, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 0,
                },
            }),
            (S294, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Token,
                    index: 0,
                },
            }),
            (S295, LookT(Token::STRING)) => Some(Shift { state: S296 }),
            (S296, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S297 }),
            (S297, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TokenDef,
                    index: 1,
                },
            }),
            (S298, LookT(Token::REGEX_KW)) => Some(Shift { state: S292 }),
            (S298, LookT(Token::EXTERNAL_KW)) => Some(Shift { state: S291 }),
            (S298, LookT(Token::TOKEN_STR)) => Some(Shift { state: S293 }),
            (S299, Eof) => Some(Accept(Node::TokenDef)),
            (S300, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S300, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S300, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S300, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S300, LookT(Token::BAR)) => Some(Shift { state: S305 }),
            (S300, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S300, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S300, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S301, Eof) => Some(Accept(Node::Block)),
            (S302, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_29,
                    index: 0,
                },
            }),
            (S302, LookT(Token::BAR)) => Some(Shift { state: S305 }),
            (S303, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Block,
                    index: 0,
                },
            }),
            (S304, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S304, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S304, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S304, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S304, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S304, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S304, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S305, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S305, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S305, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S306, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Block,
                    index: 1,
                },
            }),
            (S307, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Fields,
                    index: 0,
                },
            }),
            (S308, LookT(Token::PAR_LEFT)) => Some(Shift { state: S312 }),
            (S308, LookT(Token::ID)) => Some(Shift { state: S314 }),
            (S308, LookT(Token::TOKEN_STR)) => Some(Shift { state: S316 }),
            (S308, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S309, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_29,
                    index: 1,
                },
            }),
            (S310, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 1,
                },
            }),
            (S311, LookT(Token::ID)) => Some(Shift { state: S318 }),
            (S312, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S312, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S312, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S312, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S313, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S313, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S313, LookT(Token::STAR)) => Some(Shift { state: S320 }),
            (S313, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S322 }),
            (S313, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S321 }),
            (S313, LookT(Token::PLUS)) => Some(Shift { state: S326 }),
            (S313, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S325 }),
            (S313, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S328 }),
            (S313, LookT(Token::QUESTION)) => Some(Shift { state: S327 }),
            (S313, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S313, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S313, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S313, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S313, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S314, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S314, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S314, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S315, LookT(Token::COLON)) => Some(Shift { state: S329 }),
            (S316, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S316, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S317, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S317, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S317, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S317, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S317, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S317, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S317, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S318, LookT(Token::ARROW)) => Some(Shift { state: S330 }),
            (S319, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S331 }),
            (S320, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S320, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S320, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S320, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S320, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S320, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S320, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S321, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S321, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S321, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S321, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S321, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S321, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S321, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S322, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S322, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S322, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S322, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S322, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S322, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S322, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S323, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S323, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S323, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S323, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S323, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S323, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S323, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S324, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S324, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S324, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S324, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S324, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S324, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S324, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S325, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S325, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S325, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S325, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S325, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S325, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S325, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S326, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S326, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S326, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S326, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S326, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S326, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S326, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S327, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S327, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S327, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S327, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S327, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S327, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S327, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S328, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S328, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S328, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S328, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S328, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S328, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S328, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S329, LookT(Token::PAR_LEFT)) => Some(Shift { state: S312 }),
            (S329, LookT(Token::ID)) => Some(Shift { state: S332 }),
            (S329, LookT(Token::TOKEN_STR)) => Some(Shift { state: S316 }),
            (S330, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S330, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S330, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S330, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S330, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S330, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S330, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S330, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S331, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S331, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S332, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S332, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S333, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S333, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S333, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S333, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S333, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S333, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S333, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S334, Eof) => Some(Reduce {
                rhs_size: 5,
                rule_index: RuleIdx {
                    lhs: Node::Variant,
                    index: 0,
                },
            }),
            (S334, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 5,
                rule_index: RuleIdx {
                    lhs: Node::Variant,
                    index: 0,
                },
            }),
            (S335, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S335, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S335, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S335, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S335, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S335, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S335, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S335, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S336, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Fields,
                    index: 0,
                },
            }),
            (S336, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Fields,
                    index: 0,
                },
            }),
            (S337, LookT(Token::PAR_LEFT)) => Some(Shift { state: S339 }),
            (S337, LookT(Token::ID)) => Some(Shift { state: S340 }),
            (S337, LookT(Token::TOKEN_STR)) => Some(Shift { state: S342 }),
            (S337, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S338, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 1,
                },
            }),
            (S338, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 1,
                },
            }),
            (S339, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S339, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S339, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S339, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S340, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S340, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S340, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S341, LookT(Token::COLON)) => Some(Shift { state: S346 }),
            (S342, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S342, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S343, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S343, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S344, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::STAR)) => Some(Shift { state: S347 }),
            (S344, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S349 }),
            (S344, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S348 }),
            (S344, LookT(Token::PLUS)) => Some(Shift { state: S353 }),
            (S344, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S352 }),
            (S344, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S355 }),
            (S344, LookT(Token::QUESTION)) => Some(Shift { state: S354 }),
            (S344, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S344, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S345, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S356 }),
            (S346, LookT(Token::PAR_LEFT)) => Some(Shift { state: S339 }),
            (S346, LookT(Token::ID)) => Some(Shift { state: S357 }),
            (S346, LookT(Token::TOKEN_STR)) => Some(Shift { state: S342 }),
            (S347, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S347, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S348, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S348, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S349, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S349, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S350, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S350, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S351, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S351, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S352, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S352, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S353, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S353, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S354, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S354, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S355, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S355, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S356, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S356, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S357, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S357, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S358, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::DOC_COMMENT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::SHARP)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::BAR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S358, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S359, LookT(Token::BAR)) => Some(Shift { state: S361 }),
            (S360, Eof) => Some(Accept(Node::Variant)),
            (S361, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S41 }),
            (S361, LookT(Token::SHARP)) => Some(Shift { state: S37 }),
            (S361, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S362, LookT(Token::ID)) => Some(Shift { state: S363 }),
            (S363, LookT(Token::ARROW)) => Some(Shift { state: S364 }),
            (S364, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S364, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S364, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S364, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S364, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S364, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S364, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S365, Eof) => Some(Reduce {
                rhs_size: 5,
                rule_index: RuleIdx {
                    lhs: Node::Variant,
                    index: 0,
                },
            }),
            (S366, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_30,
                    index: 0,
                },
            }),
            (S366, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S366, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S366, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S366, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S366, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S366, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S367, Eof) => Some(Accept(Node::Fields)),
            (S368, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S69 }),
            (S368, LookT(Token::SHARP)) => Some(Shift { state: S70 }),
            (S368, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S368, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S368, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S368, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S369, Eof) => Some(Accept(Node::Field)),
            (S370, LookT(Token::PAR_LEFT)) => Some(Shift { state: S371 }),
            (S370, LookT(Token::ID)) => Some(Shift { state: S373 }),
            (S370, LookT(Token::TOKEN_STR)) => Some(Shift { state: S375 }),
            (S370, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S371, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S371, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S371, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S371, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S372, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 0,
                },
            }),
            (S373, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::COLON)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S373, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S373, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S374, LookT(Token::COLON)) => Some(Shift { state: S378 }),
            (S375, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S375, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S376, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S376, LookT(Token::STAR)) => Some(Shift { state: S379 }),
            (S376, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S381 }),
            (S376, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S380 }),
            (S376, LookT(Token::PLUS)) => Some(Shift { state: S385 }),
            (S376, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S384 }),
            (S376, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S387 }),
            (S376, LookT(Token::QUESTION)) => Some(Shift { state: S386 }),
            (S377, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S388 }),
            (S378, LookT(Token::PAR_LEFT)) => Some(Shift { state: S371 }),
            (S378, LookT(Token::ID)) => Some(Shift { state: S389 }),
            (S378, LookT(Token::TOKEN_STR)) => Some(Shift { state: S375 }),
            (S379, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S380, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S381, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S382, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S383, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S384, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S385, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S386, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S387, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S388, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S388, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S389, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S389, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S390, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::Field,
                    index: 1,
                },
            }),
            (S391, LookT(Token::ID)) => Some(Shift { state: S393 }),
            (S391, LookT(Token::INT)) => Some(Shift { state: S394 }),
            (S392, Eof) => Some(Accept(Node::Label)),
            (S393, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 0,
                },
            }),
            (S394, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Label,
                    index: 1,
                },
            }),
            (S395, LookT(Token::PAR_LEFT)) => Some(Shift { state: S371 }),
            (S395, LookT(Token::ID)) => Some(Shift { state: S389 }),
            (S395, LookT(Token::TOKEN_STR)) => Some(Shift { state: S375 }),
            (S396, Eof) => Some(Accept(Node::Item)),
            (S397, LookT(Token::PAR_LEFT)) => Some(Shift { state: S398 }),
            (S397, LookT(Token::ID)) => Some(Shift { state: S399 }),
            (S397, LookT(Token::TOKEN_STR)) => Some(Shift { state: S400 }),
            (S398, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S398, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S398, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S398, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S399, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S400, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S401, Eof) => Some(Accept(Node::Primary)),
            (S402, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S403 }),
            (S403, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S404, LookT(Token::PAR_LEFT)) => Some(Shift { state: S405 }),
            (S404, LookT(Token::ID)) => Some(Shift { state: S407 }),
            (S404, LookT(Token::TOKEN_STR)) => Some(Shift { state: S409 }),
            (S405, LookT(Token::PAR_LEFT)) => Some(Shift { state: S128 }),
            (S405, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_33,
                    index: 0,
                },
            }),
            (S405, LookT(Token::ID)) => Some(Shift { state: S125 }),
            (S405, LookT(Token::TOKEN_STR)) => Some(Shift { state: S131 }),
            (S406, Eof) => Some(Accept(Node::Alternative)),
            (S407, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S407, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 0,
                },
            }),
            (S408, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Alternative,
                    index: 0,
                },
            }),
            (S409, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S409, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 1,
                },
            }),
            (S410, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_34,
                    index: 0,
                },
            }),
            (S410, LookT(Token::PAR_LEFT)) => Some(Shift { state: S405 }),
            (S410, LookT(Token::ID)) => Some(Shift { state: S407 }),
            (S410, LookT(Token::TOKEN_STR)) => Some(Shift { state: S409 }),
            (S411, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S411, LookT(Token::STAR)) => Some(Shift { state: S414 }),
            (S411, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S416 }),
            (S411, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S415 }),
            (S411, LookT(Token::PLUS)) => Some(Shift { state: S420 }),
            (S411, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S419 }),
            (S411, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S422 }),
            (S411, LookT(Token::QUESTION)) => Some(Shift { state: S421 }),
            (S411, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S411, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S411, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 0,
                },
            }),
            (S412, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S423 }),
            (S413, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::_34,
                    index: 1,
                },
            }),
            (S414, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S414, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S414, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S414, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 0,
                },
            }),
            (S415, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S415, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S415, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S415, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 2,
                },
            }),
            (S416, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S416, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S416, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S416, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 1,
                },
            }),
            (S417, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S417, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S417, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S417, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_31,
                    index: 1,
                },
            }),
            (S418, Eof) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S418, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S418, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S418, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 2,
                rule_index: RuleIdx {
                    lhs: Node::Item,
                    index: 0,
                },
            }),
            (S419, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S419, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S419, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S419, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 4,
                },
            }),
            (S420, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S420, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S420, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S420, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 3,
                },
            }),
            (S421, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S421, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S421, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S421, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 6,
                },
            }),
            (S422, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S422, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S422, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S422, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Modifier,
                    index: 5,
                },
            }),
            (S423, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::STAR_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::UNDERSCORE_STAR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::PLUS_UNDERSCORE)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::UNDERSCORE_PLUS)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::QUESTION)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::PAR_LEFT)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S423, LookT(Token::TOKEN_STR)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Primary,
                    index: 2,
                },
            }),
            (S424, LookT(Token::STAR)) => Some(Shift { state: S379 }),
            (S424, LookT(Token::STAR_UNDERSCORE)) => Some(Shift { state: S381 }),
            (S424, LookT(Token::UNDERSCORE_STAR)) => Some(Shift { state: S380 }),
            (S424, LookT(Token::PLUS)) => Some(Shift { state: S385 }),
            (S424, LookT(Token::PLUS_UNDERSCORE)) => Some(Shift { state: S384 }),
            (S424, LookT(Token::UNDERSCORE_PLUS)) => Some(Shift { state: S387 }),
            (S424, LookT(Token::QUESTION)) => Some(Shift { state: S386 }),
            (S425, Eof) => Some(Accept(Node::Modifier)),
            (S426, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S430 }),
            (S426, LookT(Token::SHARP)) => Some(Shift { state: S429 }),
            (S427, Eof) => Some(Accept(Node::Attribute)),
            (S428, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 1,
                },
            }),
            (S429, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S431 }),
            (S430, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Attribute,
                    index: 0,
                },
            }),
            (S431, LookT(Token::ID)) => Some(Shift { state: S22 }),
            (S431, LookT(Token::TOKEN_STR)) => Some(Shift { state: S25 }),
            (S431, LookT(Token::STRING)) => Some(Shift { state: S24 }),
            (S431, LookT(Token::INT)) => Some(Shift { state: S27 }),
            (S432, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S433 }),
            (S433, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::NormalAttribute,
                    index: 0,
                },
            }),
            (S434, LookT(Token::SHARP)) => Some(Shift { state: S429 }),
            (S435, Eof) => Some(Accept(Node::NormalAttribute)),
            (S436, LookT(Token::ID)) => Some(Shift { state: S437 }),
            (S436, LookT(Token::TOKEN_STR)) => Some(Shift { state: S440 }),
            (S436, LookT(Token::STRING)) => Some(Shift { state: S439 }),
            (S436, LookT(Token::INT)) => Some(Shift { state: S442 }),
            (S437, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S437, LookT(Token::PAR_LEFT)) => Some(Shift { state: S443 }),
            (S437, LookT(Token::EQUAL)) => Some(Shift { state: S444 }),
            (S438, Eof) => Some(Accept(Node::AttributeItem)),
            (S439, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S440, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S441, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S442, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S443, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S443, LookT(Token::ID)) => Some(Shift { state: S49 }),
            (S443, LookT(Token::TOKEN_STR)) => Some(Shift { state: S54 }),
            (S443, LookT(Token::STRING)) => Some(Shift { state: S53 }),
            (S443, LookT(Token::INT)) => Some(Shift { state: S55 }),
            (S444, LookT(Token::TOKEN_STR)) => Some(Shift { state: S440 }),
            (S444, LookT(Token::STRING)) => Some(Shift { state: S439 }),
            (S444, LookT(Token::INT)) => Some(Shift { state: S442 }),
            (S445, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S447 }),
            (S446, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S447, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S448, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S448, LookT(Token::ID)) => Some(Shift { state: S450 }),
            (S448, LookT(Token::TOKEN_STR)) => Some(Shift { state: S455 }),
            (S448, LookT(Token::STRING)) => Some(Shift { state: S454 }),
            (S448, LookT(Token::INT)) => Some(Shift { state: S456 }),
            (S449, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeArgs,
                    index: 0,
                },
            }),
            (S450, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S450, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 0,
                },
            }),
            (S450, LookT(Token::PAR_LEFT)) => Some(Shift { state: S457 }),
            (S450, LookT(Token::EQUAL)) => Some(Shift { state: S458 }),
            (S451, Eof) => Some(Accept(Node::AttributeArgs)),
            (S452, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S452, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 1,
                },
            }),
            (S453, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 2,
                },
            }),
            (S453, LookT(Token::COMMA)) => Some(Shift { state: S459 }),
            (S454, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S454, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 1,
                },
            }),
            (S455, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S455, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 2,
                },
            }),
            (S456, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S456, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Literal,
                    index: 0,
                },
            }),
            (S457, LookT(Token::PAR_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S457, LookT(Token::ID)) => Some(Shift { state: S49 }),
            (S457, LookT(Token::TOKEN_STR)) => Some(Shift { state: S54 }),
            (S457, LookT(Token::STRING)) => Some(Shift { state: S53 }),
            (S457, LookT(Token::INT)) => Some(Shift { state: S55 }),
            (S458, LookT(Token::TOKEN_STR)) => Some(Shift { state: S455 }),
            (S458, LookT(Token::STRING)) => Some(Shift { state: S454 }),
            (S458, LookT(Token::INT)) => Some(Shift { state: S456 }),
            (S459, Eof) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 0,
                },
            }),
            (S459, LookT(Token::ID)) => Some(Shift { state: S450 }),
            (S459, LookT(Token::TOKEN_STR)) => Some(Shift { state: S455 }),
            (S459, LookT(Token::STRING)) => Some(Shift { state: S454 }),
            (S459, LookT(Token::INT)) => Some(Shift { state: S456 }),
            (S460, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S463 }),
            (S461, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S461, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 3,
                },
            }),
            (S462, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::_36,
                    index: 1,
                },
            }),
            (S463, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S463, LookT(Token::COMMA)) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::AttributeItem,
                    index: 2,
                },
            }),
            (S464, LookT(Token::TOKEN_STR)) => Some(Shift { state: S440 }),
            (S464, LookT(Token::STRING)) => Some(Shift { state: S439 }),
            (S464, LookT(Token::INT)) => Some(Shift { state: S442 }),
            (S465, Eof) => Some(Accept(Node::Literal)),
            (S466, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S467, Eof) => Some(Accept(Node::TestValue)),
            (S468, LookT(Token::DOC_COMMENT)) => Some(Shift { state: S103 }),
            (S468, LookT(Token::SHARP)) => Some(Shift { state: S106 }),
            (S468, LookT(Token::ID)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S468, LookT(Token::INT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_26,
                    index: 0,
                },
            }),
            (S469, Eof) => Some(Accept(Node::TestField)),
            (S470, LookT(Token::ID)) => Some(Shift { state: S146 }),
            (S470, LookT(Token::INT)) => Some(Shift { state: S102 }),
            (S471, LookT(Token::COLON)) => Some(Shift { state: S472 }),
            (S472, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S474 }),
            (S472, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S472, LookT(Token::STRING)) => Some(Shift { state: S475 }),
            (S473, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::TestField,
                    index: 0,
                },
            }),
            (S474, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S216 }),
            (S474, LookT(Token::BRACKET_RIGHT)) => Some(Reduce {
                rhs_size: 0,
                rule_index: RuleIdx {
                    lhs: Node::_38,
                    index: 0,
                },
            }),
            (S474, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S474, LookT(Token::STRING)) => Some(Shift { state: S215 }),
            (S475, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 0,
                },
            }),
            (S476, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 1,
                },
            }),
            (S477, LookT(Token::BRACKET_RIGHT)) => Some(Shift { state: S478 }),
            (S478, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::TestItem,
                    index: 2,
                },
            }),
            (S479, LookT(Token::BRACKET_LEFT)) => Some(Shift { state: S474 }),
            (S479, LookT(Token::ID)) => Some(Shift { state: S45 }),
            (S479, LookT(Token::STRING)) => Some(Shift { state: S475 }),
            (S480, Eof) => Some(Accept(Node::TestItem)),
            (S481, LookT(Token::ID)) => Some(Shift { state: S483 }),
            (S482, Eof) => Some(Accept(Node::Rule)),
            (S483, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::Rule,
                    index: 0,
                },
            }),
            (S483, LookT(Token::COLON_COLON)) => Some(Shift { state: S484 }),
            (S484, LookT(Token::ID)) => Some(Shift { state: S485 }),
            (S485, Eof) => Some(Reduce {
                rhs_size: 3,
                rule_index: RuleIdx {
                    lhs: Node::Rule,
                    index: 1,
                },
            }),
            (S486, LookT(Token::CONFLICT_KW)) => Some(Shift { state: S256 }),
            (S487, Eof) => Some(Accept(Node::Conflict)),
            (S488, LookT(Token::SHIFT_KW)) => Some(Shift { state: S490 }),
            (S488, LookT(Token::REDUCE_KW)) => Some(Shift { state: S489 }),
            (S489, LookT(Token::PAR_LEFT)) => Some(Shift { state: S492 }),
            (S490, LookT(Token::PAR_LEFT)) => Some(Shift { state: S493 }),
            (S491, Eof) => Some(Accept(Node::ConflictAction)),
            (S492, LookT(Token::ID)) => Some(Shift { state: S149 }),
            (S493, LookT(Token::UNDERSCORE)) => Some(Shift { state: S495 }),
            (S493, LookT(Token::ID)) => Some(Shift { state: S150 }),
            (S493, LookT(Token::TOKEN_STR)) => Some(Shift { state: S153 }),
            (S494, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S497 }),
            (S495, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S498 }),
            (S496, LookT(Token::PAR_RIGHT)) => Some(Shift { state: S499 }),
            (S497, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 2,
                },
            }),
            (S498, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 0,
                },
            }),
            (S499, Eof) => Some(Reduce {
                rhs_size: 4,
                rule_index: RuleIdx {
                    lhs: Node::ConflictAction,
                    index: 1,
                },
            }),
            (S500, LookT(Token::ID)) => Some(Shift { state: S503 }),
            (S500, LookT(Token::TOKEN_STR)) => Some(Shift { state: S502 }),
            (S501, Eof) => Some(Accept(Node::IdOrTokenStr)),
            (S502, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::IdOrTokenStr,
                    index: 1,
                },
            }),
            (S503, Eof) => Some(Reduce {
                rhs_size: 1,
                rule_index: RuleIdx {
                    lhs: Node::IdOrTokenStr,
                    index: 0,
                },
            }),
            _ => None,
        }
    }
    fn goto(&self, state: Self::State, node: Node) -> Option<Self::State> {
        use ParserState::*;
        match (state, node) {
            (S68, Node::NormalAttribute) => Some(S40),
            (S479, Node::Rule) => Some(S271),
            (S73, Node::NormalAttribute) => Some(S71),
            (S300, Node::_29) => Some(S306),
            (S0, Node::TopLevelItem) => Some(S4),
            (S96, Node::_33) => Some(S126),
            (S157, Node::_30) => Some(S187),
            (S368, Node::Attribute) => Some(S73),
            (S154, Node::ConflictAction) => Some(S183),
            (S65, Node::_29) => Some(S91),
            (S216, Node::Rule) => Some(S213),
            (S330, Node::_30) => Some(S336),
            (S4, Node::Attribute) => Some(S6),
            (S39, Node::NormalAttribute) => Some(S40),
            (S129, Node::Primary) => Some(S130),
            (S111, Node::Rule) => Some(S148),
            (S160, Node::Alternative) => Some(S189),
            (S270, Node::Attribute) => Some(S73),
            (S335, Node::Attribute) => Some(S73),
            (S361, Node::_26) => Some(S362),
            (S43, Node::_26) => Some(S75),
            (S94, Node::AttributeItem) => Some(S23),
            (S270, Node::Fields) => Some(S66),
            (S176, Node::TestValue) => Some(S196),
            (S130, Node::_31) => Some(S168),
            (S304, Node::NormalAttribute) => Some(S71),
            (S364, Node::Fields) => Some(S365),
            (S312, Node::Item) => Some(S129),
            (S308, Node::Primary) => Some(S313),
            (S376, Node::Modifier) => Some(S382),
            (S404, Node::_34) => Some(S408),
            (S410, Node::Item) => Some(S410),
            (S176, Node::Rule) => Some(S193),
            (S457, Node::_36) => Some(S48),
            (S204, Node::Alternative) => Some(S124),
            (S459, Node::Literal) => Some(S452),
            (S81, Node::AttributeArgs) => Some(S114),
            (S411, Node::Modifier) => Some(S417),
            (S337, Node::Item) => Some(S343),
            (S474, Node::TestItem) => Some(S212),
            (S371, Node::Primary) => Some(S130),
            (S366, Node::Attribute) => Some(S73),
            (S128, Node::Alternative) => Some(S124),
            (S157, Node::Field) => Some(S186),
            (S339, Node::_33) => Some(S345),
            (S344, Node::Modifier) => Some(S350),
            (S398, Node::_34) => Some(S127),
            (S252, Node::Attribute) => Some(S6),
            (S186, Node::_26) => Some(S188),
            (S366, Node::Fields) => Some(S367),
            (S189, Node::_32) => Some(S210),
            (S211, Node::NormalAttribute) => Some(S107),
            (S486, Node::Conflict) => Some(S487),
            (S444, Node::Literal) => Some(S446),
            (S395, Node::Item) => Some(S396),
            (S188, Node::Label) => Some(S206),
            (S474, Node::_38) => Some(S477),
            (S212, Node::Rule) => Some(S213),
            (S305, Node::_26) => Some(S311),
            (S410, Node::_34) => Some(S413),
            (S4, Node::Conflict) => Some(S2),
            (S0, Node::Attribute) => Some(S6),
            (S97, Node::Modifier) => Some(S135),
            (S398, Node::Item) => Some(S129),
            (S339, Node::Primary) => Some(S130),
            (S68, Node::Attribute) => Some(S39),
            (S405, Node::_33) => Some(S412),
            (S330, Node::_26) => Some(S337),
            (S378, Node::Primary) => Some(S376),
            (S300, Node::_30) => Some(S307),
            (S368, Node::NormalAttribute) => Some(S71),
            (S265, Node::Token) => Some(S38),
            (S34, Node::Literal) => Some(S56),
            (S128, Node::_34) => Some(S127),
            (S300, Node::Field) => Some(S304),
            (S448, Node::AttributeArgs) => Some(S451),
            (S67, Node::Attribute) => Some(S73),
            (S96, Node::Primary) => Some(S130),
            (S312, Node::Primary) => Some(S130),
            (S124, Node::_32) => Some(S159),
            (S43, Node::Block) => Some(S64),
            (S94, Node::Literal) => Some(S26),
            (S468, Node::Attribute) => Some(S105),
            (S443, Node::_36) => Some(S48),
            (S30, Node::Rule) => Some(S44),
            (S89, Node::TokenDef) => Some(S121),
            (S346, Node::Primary) => Some(S344),
            (S65, Node::Variant) => Some(S65),
            (S109, Node::_26) => Some(S108),
            (S43, Node::Attribute) => Some(S73),
            (S404, Node::Item) => Some(S410),
            (S448, Node::AttributeItem) => Some(S453),
            (S61, Node::NormalAttribute) => Some(S40),
            (S76, Node::NormalAttribute) => Some(S107),
            (S94, Node::_35) => Some(S123),
            (S252, Node::TopLevelItem) => Some(S255),
            (S286, Node::_26) => Some(S288),
            (S371, Node::Item) => Some(S129),
            (S470, Node::Label) => Some(S471),
            (S84, Node::AttributeItem) => Some(S52),
            (S405, Node::Alternative) => Some(S124),
            (S0, Node::Conflict) => Some(S2),
            (S144, Node::Literal) => Some(S26),
            (S157, Node::Attribute) => Some(S73),
            (S75, Node::Label) => Some(S99),
            (S391, Node::Label) => Some(S392),
            (S330, Node::Attribute) => Some(S73),
            (S4, Node::_26) => Some(S10),
            (S376, Node::_31) => Some(S383),
            (S269, Node::_28) => Some(S274),
            (S364, Node::_30) => Some(S307),
            (S479, Node::TestItem) => Some(S480),
            (S330, Node::Fields) => Some(S334),
            (S252, Node::_26) => Some(S257),
            (S300, Node::_26) => Some(S308),
            (S0, Node::File) => Some(S3),
            (S60, Node::AttributeItem) => Some(S23),
            (S370, Node::Label) => Some(S374),
            (S339, Node::Item) => Some(S129),
            (S216, Node::TestItem) => Some(S212),
            (S265, Node::NormalAttribute) => Some(S40),
            (S209, Node::_31) => Some(S229),
            (S364, Node::Field) => Some(S304),
            (S397, Node::Primary) => Some(S401),
            (S270, Node::NormalAttribute) => Some(S71),
            (S160, Node::Primary) => Some(S130),
            (S443, Node::AttributeItem) => Some(S52),
            (S474, Node::Rule) => Some(S213),
            (S335, Node::Field) => Some(S335),
            (S128, Node::_33) => Some(S162),
            (S194, Node::TestValue) => Some(S217),
            (S270, Node::Field) => Some(S67),
            (S60, Node::Literal) => Some(S26),
            (S67, Node::NormalAttribute) => Some(S71),
            (S426, Node::Attribute) => Some(S427),
            (S313, Node::Modifier) => Some(S323),
            (S96, Node::Item) => Some(S129),
            (S47, Node::ConflictAction) => Some(S80),
            (S344, Node::_31) => Some(S351),
            (S410, Node::Primary) => Some(S411),
            (S443, Node::Literal) => Some(S51),
            (S67, Node::Field) => Some(S67),
            (S176, Node::TestItem) => Some(S192),
            (S204, Node::Primary) => Some(S130),
            (S481, Node::Rule) => Some(S482),
            (S35, Node::_35) => Some(S57),
            (S81, Node::Literal) => Some(S51),
            (S198, Node::Rule) => Some(S218),
            (S337, Node::Label) => Some(S341),
            (S361, Node::NormalAttribute) => Some(S40),
            (S60, Node::_35) => Some(S87),
            (S61, Node::Token) => Some(S38),
            (S130, Node::Modifier) => Some(S167),
            (S404, Node::Primary) => Some(S411),
            (S366, Node::NormalAttribute) => Some(S71),
            (S128, Node::Primary) => Some(S130),
            (S43, Node::NormalAttribute) => Some(S71),
            (S304, Node::_26) => Some(S308),
            (S436, Node::Literal) => Some(S441),
            (S457, Node::Literal) => Some(S51),
            (S76, Node::Attribute) => Some(S105),
            (S468, Node::NormalAttribute) => Some(S107),
            (S35, Node::Literal) => Some(S26),
            (S366, Node::Field) => Some(S304),
            (S457, Node::AttributeItem) => Some(S52),
            (S68, Node::_26) => Some(S93),
            (S335, Node::_30) => Some(S338),
            (S81, Node::_36) => Some(S48),
            (S33, Node::AttributeArgs) => Some(S50),
            (S212, Node::TestItem) => Some(S212),
            (S144, Node::_35) => Some(S175),
            (S236, Node::NormalAttribute) => Some(S107),
            (S4, Node::_25) => Some(S13),
            (S35, Node::AttributeItem) => Some(S23),
            (S39, Node::_26) => Some(S62),
            (S199, Node::IdOrTokenStr) => Some(S220),
            (S364, Node::_26) => Some(S308),
            (S6, Node::NormalAttribute) => Some(S7),
            (S283, Node::_39) => Some(S284),
            (S224, Node::Primary) => Some(S209),
            (S221, Node::IdOrTokenStr) => Some(S242),
            (S302, Node::_29) => Some(S309),
            (S286, Node::Token) => Some(S287),
            (S398, Node::Primary) => Some(S130),
            (S431, Node::_35) => Some(S432),
            (S436, Node::AttributeItem) => Some(S438),
            (S300, Node::Block) => Some(S301),
            (S216, Node::_38) => Some(S238),
            (S160, Node::Item) => Some(S129),
            (S448, Node::Literal) => Some(S452),
            (S493, Node::IdOrTokenStr) => Some(S496),
            (S500, Node::IdOrTokenStr) => Some(S501),
            (S108, Node::Label) => Some(S145),
            (S194, Node::Rule) => Some(S213),
            (S300, Node::Attribute) => Some(S73),
            (S270, Node::Block) => Some(S275),
            (S305, Node::NormalAttribute) => Some(S40),
            (S431, Node::Literal) => Some(S26),
            (S186, Node::Field) => Some(S186),
            (S312, Node::Alternative) => Some(S124),
            (S144, Node::AttributeItem) => Some(S23),
            (S43, Node::Variant) => Some(S65),
            (S466, Node::Rule) => Some(S271),
            (S270, Node::_29) => Some(S74),
            (S204, Node::Item) => Some(S129),
            (S186, Node::NormalAttribute) => Some(S71),
            (S368, Node::_26) => Some(S370),
            (S276, Node::_37) => Some(S280),
            (S304, Node::_30) => Some(S310),
            (S298, Node::TokenDef) => Some(S299),
            (S97, Node::_31) => Some(S136),
            (S128, Node::Item) => Some(S129),
            (S267, Node::TestValue) => Some(S272),
            (S335, Node::_26) => Some(S337),
            (S405, Node::Primary) => Some(S130),
            (S109, Node::Attribute) => Some(S105),
            (S157, Node::NormalAttribute) => Some(S71),
            (S75, Node::Item) => Some(S100),
            (S330, Node::NormalAttribute) => Some(S71),
            (S286, Node::Attribute) => Some(S39),
            (S329, Node::Primary) => Some(S313),
            (S61, Node::_26) => Some(S42),
            (S330, Node::Field) => Some(S335),
            (S236, Node::Attribute) => Some(S105),
            (S370, Node::Item) => Some(S372),
            (S212, Node::_38) => Some(S235),
            (S265, Node::Attribute) => Some(S39),
            (S81, Node::AttributeItem) => Some(S52),
            (S224, Node::Item) => Some(S247),
            (S276, Node::_26) => Some(S108),
            (S304, Node::Field) => Some(S304),
            (S141, Node::Item) => Some(S174),
            (S364, Node::Attribute) => Some(S73),
            (S15, Node::AttributeItem) => Some(S23),
            (S105, Node::_26) => Some(S143),
            (S276, Node::TestField) => Some(S109),
            (S472, Node::TestItem) => Some(S473),
            (S443, Node::AttributeArgs) => Some(S445),
            (S76, Node::TestField) => Some(S109),
            (S426, Node::NormalAttribute) => Some(S428),
            (S112, Node::IdOrTokenStr) => Some(S152),
            (S308, Node::Label) => Some(S315),
            (S424, Node::Modifier) => Some(S425),
            (S4, Node::TopLevelItem) => Some(S4),
            (S300, Node::NormalAttribute) => Some(S71),
            (S76, Node::_26) => Some(S108),
            (S211, Node::_37) => Some(S234),
            (S28, Node::NormalAttribute) => Some(S40),
            (S43, Node::Fields) => Some(S66),
            (S457, Node::AttributeArgs) => Some(S460),
            (S270, Node::_26) => Some(S75),
            (S361, Node::Attribute) => Some(S39),
            (S431, Node::AttributeItem) => Some(S23),
            (S404, Node::Alternative) => Some(S406),
            (S492, Node::Rule) => Some(S494),
            (S0, Node::_26) => Some(S10),
            (S105, Node::Attribute) => Some(S105),
            (S488, Node::ConflictAction) => Some(S491),
            (S73, Node::_26) => Some(S95),
            (S15, Node::_35) => Some(S21),
            (S204, Node::_34) => Some(S127),
            (S265, Node::_27) => Some(S269),
            (S252, Node::Conflict) => Some(S254),
            (S28, Node::_27) => Some(S36),
            (S188, Node::Primary) => Some(S209),
            (S75, Node::Primary) => Some(S97),
            (S33, Node::Literal) => Some(S51),
            (S129, Node::_34) => Some(S163),
            (S302, Node::Variant) => Some(S302),
            (S366, Node::_26) => Some(S308),
            (S398, Node::Alternative) => Some(S124),
            (S76, Node::_37) => Some(S104),
            (S211, Node::_26) => Some(S108),
            (S6, Node::Attribute) => Some(S6),
            (S368, Node::Field) => Some(S369),
            (S448, Node::_36) => Some(S449),
            (S73, Node::Attribute) => Some(S73),
            (S194, Node::_38) => Some(S214),
            (S312, Node::_33) => Some(S319),
            (S211, Node::TestField) => Some(S109),
            (S474, Node::TestValue) => Some(S217),
            (S300, Node::Variant) => Some(S302),
            (S129, Node::Item) => Some(S129),
            (S290, Node::TokenDef) => Some(S294),
            (S84, Node::Literal) => Some(S51),
            (S281, Node::ConflictAction) => Some(S283),
            (S305, Node::Attribute) => Some(S39),
            (S43, Node::_29) => Some(S74),
            (S194, Node::TestItem) => Some(S212),
            (S84, Node::_36) => Some(S116),
            (S270, Node::Variant) => Some(S65),
            (S364, Node::NormalAttribute) => Some(S71),
            (S371, Node::_34) => Some(S127),
            (S268, Node::ConflictAction) => Some(S273),
            (S28, Node::Token) => Some(S38),
            (S109, Node::TestField) => Some(S109),
            (S36, Node::_28) => Some(S58),
            (S236, Node::_37) => Some(S249),
            (S270, Node::_30) => Some(S72),
            (S186, Node::Attribute) => Some(S73),
            (S28, Node::Attribute) => Some(S39),
            (S337, Node::Primary) => Some(S344),
            (S30, Node::TestValue) => Some(S46),
            (S236, Node::TestField) => Some(S109),
            (S371, Node::Alternative) => Some(S124),
            (S0, Node::_25) => Some(S9),
            (S267, Node::Rule) => Some(S271),
            (S157, Node::Fields) => Some(S185),
            (S33, Node::_36) => Some(S48),
            (S398, Node::_33) => Some(S402),
            (S405, Node::Item) => Some(S129),
            (S236, Node::_26) => Some(S108),
            (S82, Node::Literal) => Some(S115),
            (S109, Node::NormalAttribute) => Some(S107),
            (S204, Node::_33) => Some(S223),
            (S6, Node::_26) => Some(S14),
            (S411, Node::_31) => Some(S418),
            (S15, Node::Literal) => Some(S26),
            (S276, Node::Attribute) => Some(S105),
            (S286, Node::NormalAttribute) => Some(S40),
            (S329, Node::Item) => Some(S333),
            (S188, Node::Item) => Some(S207),
            (S366, Node::_30) => Some(S307),
            (S434, Node::NormalAttribute) => Some(S435),
            (S61, Node::_27) => Some(S88),
            (S212, Node::TestValue) => Some(S217),
            (S458, Node::Literal) => Some(S461),
            (S370, Node::Primary) => Some(S376),
            (S96, Node::_34) => Some(S127),
            (S67, Node::_30) => Some(S92),
            (S405, Node::_34) => Some(S127),
            (S339, Node::Alternative) => Some(S124),
            (S33, Node::AttributeItem) => Some(S52),
            (S335, Node::NormalAttribute) => Some(S71),
            (S109, Node::_37) => Some(S147),
            (S378, Node::Item) => Some(S390),
            (S39, Node::Attribute) => Some(S39),
            (S4, Node::NormalAttribute) => Some(S7),
            (S312, Node::_34) => Some(S127),
            (S141, Node::Primary) => Some(S97),
            (S157, Node::_26) => Some(S188),
            (S183, Node::_39) => Some(S201),
            (S304, Node::Attribute) => Some(S73),
            (S300, Node::Fields) => Some(S303),
            (S472, Node::Rule) => Some(S271),
            (S96, Node::Alternative) => Some(S124),
            (S308, Node::Item) => Some(S317),
            (S43, Node::_30) => Some(S72),
            (S371, Node::_33) => Some(S377),
            (S466, Node::TestValue) => Some(S467),
            (S472, Node::TestValue) => Some(S476),
            (S359, Node::Variant) => Some(S360),
            (S43, Node::Field) => Some(S67),
            (S464, Node::Literal) => Some(S465),
            (S346, Node::Item) => Some(S358),
            (S105, Node::NormalAttribute) => Some(S107),
            (S339, Node::_34) => Some(S127),
            (S61, Node::Attribute) => Some(S39),
            (S459, Node::AttributeItem) => Some(S453),
            (S252, Node::NormalAttribute) => Some(S7),
            (S313, Node::_31) => Some(S324),
            (S211, Node::Attribute) => Some(S105),
            (S468, Node::_26) => Some(S470),
            (S265, Node::_26) => Some(S42),
            (S186, Node::_30) => Some(S203),
            (S28, Node::_26) => Some(S42),
            (S395, Node::Primary) => Some(S376),
            (S468, Node::TestField) => Some(S469),
            (S160, Node::_34) => Some(S127),
            (S276, Node::NormalAttribute) => Some(S107),
            (S209, Node::Modifier) => Some(S228),
            (S479, Node::TestValue) => Some(S476),
            (S0, Node::NormalAttribute) => Some(S7),
            (S67, Node::_26) => Some(S75),
            (S459, Node::_36) => Some(S462),
            (S216, Node::TestValue) => Some(S217),
            _ => None,
        }
    }
}

type ParserInner<'src> = ::lr_parser::LR1Parser<
    'static,
    Tokenizer<'src>,
    TransitionTable,
    fn(Node) -> bool,
    fn(Token) -> ::lr_parser::parser::RemappedToken<'static, Token>,
>;
#[doc = "Wrapper around a [`lr_parser::LR1Parser`](::lr_parser::LR1Parser)."]
pub struct Parser<'src> {
    inner: ParserInner<'src>,
}

impl<'src> Iterator for Parser<'src> {
    type Item = ::lr_parser::parser::ParseResult<TransitionTable>;
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}
impl<'src> Parser<'src> {
    #[doc = r" Create a new `Parser` from `source`."]
    #[doc = r""]
    #[doc = r" You must create a new parser for each source you want to parse: it is not possible to reuse the current parser (note that creating the parser is very cheap, so you should not worry)."]
    pub fn new(source: &'src str) -> Parser<'src> {
        Self {
            inner: ::lr_parser::LR1Parser::new(
                &TransitionTable,
                ParserState::S0,
                Tokenizer::new(source),
                Node::silent,
                Token::remap,
            ),
        }
    }
    #[doc = r" Consume `self` to create a `SyntaxTree`."]
    pub fn syntax_tree(
        mut self,
    ) -> (
        ::lr_parser::syntax_tree::SyntaxTree<Token, Node>,
        Vec<::lr_parser::syntax_tree::SyntaxError<Token, Node>>,
    ) {
        ::lr_parser::syntax_tree::SyntaxTree::from_parser(&mut self.inner, &Grammar)
    }
}

pub mod ast {
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Ast<T> {
        #[doc = "Reference in the [`SyntaxTree`](::lr_parser::syntax_tree::SyntaxTree)"]
        pub syntax_index: ::lr_parser::syntax_tree::SyntaxIdx,
        pub item: T,
    }
    impl<T> std::ops::Deref for Ast<T> {
        type Target = T;
        fn deref(&self) -> &Self::Target {
            &self.item
        }
    }

    #[doc = " Starting node.\n\n This represents the content of a single grammar file."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct File {
        pub top_level_items: Vec<Ast<TopLevelItem>>,
    }
    #[doc = " One of the items that can appear at the top-level of a `File`."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum TopLevelItem {
        #[doc = " An `include \"...\";` directive."]
        Include { path: Ast<String> },
        #[doc = " A `tokens { ... }` block."]
        Tokens {
            attributes: Vec<Ast<Attribute>>,
            tokens: Vec<Ast<Token>>,
            verbatim: Option<(Ast<()>, Ast<String>)>,
        },
        #[doc = " A `node { ... }` block."]
        Node {
            attributes: Vec<Ast<Attribute>>,
            name: Ast<String>,
            block: Ast<Block>,
        },
        #[doc = " A `test \"...\" ...` item."]
        Test {
            attributes: Vec<Ast<Attribute>>,
            source: Ast<String>,
            value: Ast<TestValue>,
        },
        #[doc = " A `conflict { ... }` block."]
        Conflict(Ast<Conflict>),
    }
    #[doc = " One of the tokens in a `tokens { ... }` block."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Token {
        pub attributes: Vec<Ast<Attribute>>,
        pub name: Ast<String>,
        pub definition: Ast<TokenDef>,
    }
    #[doc = " How is the token defined.\n\n This can either be a single quoted string, a `regex` or an `external` token.\n\n # Example\n ```text\n tokens {\n\t\tSINGLE_QUOTED: 'some_token',\n\t\tREGEX: regex(\"[a]+regex?\"),\n\t\tEXTERNAL: external\n }\n ```"]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum TokenDef {
        Raw(Ast<String>),
        Regex(Ast<String>),
        External,
    }
    #[doc = " Content of a `node` block.\n\n This is what is between the `{` and `}` tokens."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Block {
        Single(Ast<Fields>),
        Multiple(Vec<Ast<Variant>>),
    }
    #[doc = " One of the variants in a block with multiple variants."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Variant {
        pub attributes: Vec<Ast<Attribute>>,
        pub name: Ast<String>,
        pub fields: Ast<Fields>,
    }
    #[doc = " A repetition of `Field`s."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Fields(pub Vec<Ast<Field>>);
    #[doc = " A field in a `Block`.\n\n This can be labeled or not.\n\n # Example\n ```text\n labeled_field: '+'\n (Stmt ';')* // Anonymous field\n ```"]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Field {
        Anonymous {
            attributes: Vec<Ast<Attribute>>,
            item: Ast<Item>,
        },
        Labeled {
            attributes: Vec<Ast<Attribute>>,
            name: Ast<Label>,
            item: Ast<Item>,
        },
    }
    #[doc = " Label of a `Field`.\n\n This is either an identifier or an integer."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Label {
        Id(Ast<String>),
        Number(Ast<u32>),
    }
    #[doc = " Second part of a `Field` (after the optional `Label`)."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Item {
        pub primary: Ast<Primary>,
        pub modifier: Option<Ast<Modifier>>,
    }
    #[doc = " Part of an `Item` stripped of the eventual modifier."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Primary {
        Ident(Ast<String>),
        TokenStr(Ast<String>),
        Parent(Vec<Ast<Alternative>>),
    }
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Alternative(pub Vec<Ast<Item>>);
    #[doc = " Modifier than can be applied to an `Item`."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Modifier {
        Repeat,
        Trailing,
        Separate,
        Plus,
        PlusTrailing,
        PlusSeparate,
        Optional,
    }
    #[doc = " Either a doc comment, or a `NormalAttribute`."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Attribute {
        Doc(Ast<String>),
        Normal(Ast<NormalAttribute>),
    }
    #[doc = " An attribute of the form `#[...]`"]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct NormalAttribute {
        pub attribute_list: Vec<Ast<AttributeItem>>,
    }
    #[doc = " One of the part that can appear inside a `NormalAttribute`."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum AttributeItem {
        Id(Ast<String>),
        Literal(Ast<Literal>),
        Function {
            name: Ast<String>,
            args: Ast<AttributeArgs>,
        },
        Equal(Ast<String>, Ast<Literal>),
    }
    #[doc = " Arguments of a function-like attribute.\n\n # Example\n ```text\n #[function_like(hello, 1, 'world')]\n                 ^^^^^^^^^^^^^^^^^ these are the AttributeArgs\n ```"]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct AttributeArgs(pub Vec<Ast<AttributeItem>>);
    #[doc = " Either an integer, or a single-quoted/double-quoted string."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Literal {
        Int(Ast<u32>),
        String(Ast<String>),
        TokenStr(Ast<String>),
    }
    #[doc = " Value for a `test` block to test against."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct TestValue {
        pub rule_name: Ast<Rule>,
        pub fields: Vec<Ast<TestField>>,
    }
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct TestField {
        pub attributes: Vec<Ast<Attribute>>,
        pub name: Ast<Label>,
        pub item: Ast<TestItem>,
    }
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum TestItem {
        Token(Ast<String>),
        Node(Ast<TestValue>),
        Compound(Vec<Ast<TestItem>>),
    }
    #[doc = " References a node, or a node's variant."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum Rule {
        Node(Ast<String>),
        Variant {
            base: Ast<String>,
            variant: Ast<String>,
        },
    }
    #[doc = " A `conflict { ... }` block."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Conflict {
        pub prefer: Ast<ConflictAction>,
        pub over: Ast<ConflictAction>,
        pub lookahead: Option<(Ast<()>, Ast<()>, Ast<IdOrTokenStr>)>,
    }
    #[doc = " One of the actions that can appear in a `conflict { ... }` block."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum ConflictAction {
        ShiftAny,
        ShiftToken { token: Ast<IdOrTokenStr> },
        Reduce(Ast<Rule>),
    }
    #[doc = " Either an identifier, or a single-quoted string.\n\n This is used to refer to a token."]
    #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub enum IdOrTokenStr {
        Id(Ast<String>),
        TokenStr(Ast<String>),
    }
}
