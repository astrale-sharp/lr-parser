use super::{
    ast::{self, *},
    Node, Token,
};
use abstract_tree::ConversionError;
use lr_parser::{
    syntax_tree::{
        abstract_tree::{self, ContinuableError},
        SyntaxIdx, SyntaxTree,
    },
    Span,
};
use std::{convert::TryFrom, num::ParseIntError};

/// Error while building the AST.
#[derive(Clone, Debug)]
pub enum AstError {
    Conversion(ConversionError<Token, Node>),
    ParseNumber(Span, ParseIntError),
    InvalidEscape(Span, char),
    InvalidByteEscape(Span, Option<char>),
}

impl ContinuableError for AstError {
    fn continuable(&self) -> bool {
        match self {
            AstError::Conversion(err) => err.continuable(),
            _ => false,
        }
    }
}

impl From<ConversionError<Token, Node>> for AstError {
    fn from(err: ConversionError<Token, Node>) -> Self {
        Self::Conversion(err)
    }
}

/// The source string contains the opening and closing quotes, as well as the
/// eventual `r` and `#` symbols for raw strings.
///
/// # Panics
///
/// The string must be valid (aka `"..."` with no `\` character right before the
/// closing `"`. It can be simple quote or raw strings though).
fn escape_string(mut s: &str, mut span: Span) -> Result<String, AstError> {
    let mut chars = s.chars();
    let mut offset = 1;
    if chars.next() == Some('r') {
        offset += 1;
        while chars.next() == Some('#') {
            offset += 1;
        }
    }
    if offset > 1 {
        // raw string, so no string escaping
        let end_offset = offset - 1;
        span.start += offset;
        span.end -= end_offset;
        return Ok(s[offset..s.len() - end_offset].to_string());
    }
    let end_offset = if offset == 1 { 1 } else { offset - 1 };
    span.start += offset;
    span.end -= end_offset;
    s = &s[offset..s.len() - end_offset];
    let mut chars = s.chars();
    let mut result = String::new();
    while let Some(c) = chars.next() {
        if c == '\\' {
            match chars.next().unwrap() {
                'n' => result.push('\n'),
                '\\' => result.push('\\'),
                'r' => result.push('\r'),
                't' => result.push('\t'),
                '\'' => result.push('\''),
                '"' => result.push('"'),
                'x' => {
                    let mut get_byte = || match chars.next() {
                        Some(b) => match b {
                            '0'..='9' => Ok(b as u32 as u8 - b'0'),
                            'a'..='z' => Ok(b as u32 as u8 - b'a' + 10),
                            'A'..='Z' => Ok(b as u32 as u8 - b'A' + 10),
                            _ => Err(Some(b)),
                        },
                        None => Err(None),
                    };
                    let first_byte = match get_byte() {
                        Ok(ok) => ok,
                        Err(err) => return Err(AstError::InvalidByteEscape(span, err)),
                    };
                    let second_byte = match get_byte() {
                        Ok(ok) => ok,
                        Err(err) => return Err(AstError::InvalidByteEscape(span, err)),
                    };
                    result.push((first_byte << 4 | second_byte) as char)
                }
                c => return Err(AstError::InvalidEscape(span, c)),
            }
        } else {
            result.push(c)
        }
    }
    Ok(result)
}

/// Parse an identifier from source.
///
/// This can be a raw identifier (e.g. `"r#tokens"`)
fn id_to_string(token_source: &str, _: Span) -> Result<String, AstError> {
    Ok(token_source
        .strip_prefix("r#")
        .unwrap_or(token_source)
        .to_string())
}

/// Parse a doc comment from source
fn doc_comment_to_string(token_source: &str, _: Span) -> Result<String, AstError> {
    Ok(token_source[3..].to_string())
}

/// Parse a string from source (remove quotes)
fn token_str_to_string(token_source: &str, span: Span) -> Result<String, AstError> {
    escape_string(token_source, span)
}

/// Parse a string from source (remove quotes)
fn string_to_string(token_source: &str, span: Span) -> Result<String, AstError> {
    escape_string(token_source, span)
}

/// Parse an integer from source
fn parse_int(source: &str, span: Span) -> Result<u32, AstError> {
    match source.get(span.clone()) {
        Some(int) => match int.parse() {
            Ok(int) => Ok(int),
            Err(err) => Err(AstError::ParseNumber(span, err)),
        },
        None => unreachable!("invalid span: {:?}", span),
    }
}

fn consume_comma_list<Leaf>(
    tree: &SyntaxTree<Token, Node>,
    source: &str,
    children: &mut std::iter::Peekable<&mut dyn Iterator<Item = SyntaxIdx>>,
) -> Result<Vec<Leaf>, AstError>
where
    Leaf: for<'a> TryFrom<(&'a SyntaxTree<Token, Node>, SyntaxIdx, &'a str), Error = AstError>,
{
    let mut comma = true;
    tree.consume_repeat(children, move |children| {
        if !comma {
            return Ok(None);
        }
        let token = tree.consume_opt(children, source)?;
        if token.is_some() {
            comma = tree.consume_token_opt(children, Token::COMMA).is_some();
        }
        Ok(token)
    })
}

macro_rules! apply_into {
    ($tree:expr, $source:expr, $children:expr, Token :: $token:ident) => {{
        let into_function = apply_into!(@get_into_function $token);
        let (syntax_index, span) = $tree.consume_token($children, Token::$token)?;
        let token_source = &$source[span.clone()];
        Ok::<_, AstError>(Ast {
            syntax_index,
            item: into_function(token_source, span)?,
        })
    }};
    (@get_into_function DOC_COMMENT) => {
        doc_comment_to_string
    };
    (@get_into_function STRING) => {
        string_to_string
    };
    (@get_into_function TOKEN_STR) => {
        token_str_to_string
    };
    (@get_into_function ID) => {
        id_to_string
    };
    (@get_into_function INT) => {
        parse_int
    };
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<File> {
    type Error = AstError;

    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::File)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let top_level_items: Vec<Ast<TopLevelItem>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: File { top_level_items },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<TopLevelItem> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::TopLevelItem)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                tree.consume_token(children, Token::INCLUDE_KW)?;
                let path = apply_into!(tree, source, children, Token::STRING)?;
                tree.consume_token(children, Token::SEMI_COLON)?;
                TopLevelItem::Include { path }
            }
            1 => {
                let attributes: Vec<Ast<Attribute>> =
                    tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
                tree.consume_token(children, Token::TOKENS_KW)?;
                tree.consume_token(children, Token::BRACE_LEFT)?;
                let tokens: Vec<Ast<ast::Token>> = consume_comma_list(tree, source, children)?;
                let verbatim: Option<(Ast<()>, Ast<String>)> = if let Some((verbatim_index, _)) =
                    tree.consume_token_opt(children, Token::VERBATIM_KW)
                {
                    let verbatim = apply_into!(tree, source, children, Token::STRING)?;
                    Some((
                        Ast {
                            syntax_index: verbatim_index,
                            item: (),
                        },
                        verbatim,
                    ))
                } else {
                    None
                };
                tree.consume_token(children, Token::BRACE_RIGHT)?;
                TopLevelItem::Tokens {
                    attributes,
                    tokens,
                    verbatim,
                }
            }
            2 => {
                let attributes: Vec<Ast<Attribute>> =
                    tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
                tree.consume_token(children, Token::NODE_KW)?;
                let name = apply_into!(tree, source, children, Token::ID)?;
                tree.consume_token(children, Token::BRACE_LEFT)?;
                let block: Ast<Block> = tree.consume(children, source)?;
                tree.consume_token(children, Token::BRACE_RIGHT)?;
                TopLevelItem::Node {
                    attributes,
                    name,
                    block,
                }
            }
            3 => {
                let attributes: Vec<Ast<Attribute>> =
                    tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
                tree.consume_token(children, Token::TEST_KW)?;
                let test_source = apply_into!(tree, source, children, Token::STRING)?;
                let value: Ast<TestValue> = tree.consume(children, source)?;
                TopLevelItem::Test {
                    attributes,
                    source: test_source,
                    value,
                }
            }
            4 => TopLevelItem::Conflict(tree.consume(children, source)?),
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<ast::Token> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::Token)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let attributes: Vec<Ast<Attribute>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
        let name = apply_into!(tree, source, children, Token::ID)?;
        tree.consume_token(children, Token::COLON)?;
        let definition: Ast<TokenDef> = tree.consume(children, source)?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: ast::Token {
                attributes,
                name,
                definition,
            },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<TokenDef> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::TokenDef)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(tree, source, children, Token::TOKEN_STR)?;
                TokenDef::Raw(field_0)
            }
            1 => {
                tree.consume_token(children, Token::REGEX_KW)?;
                tree.consume_token(children, Token::PAR_LEFT)?;
                let field_0 = apply_into!(tree, source, children, Token::STRING)?;
                tree.consume_token(children, Token::PAR_RIGHT)?;
                TokenDef::Regex(field_0)
            }
            2 => {
                tree.consume_token(children, Token::EXTERNAL_KW)?;
                TokenDef::External
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Block> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Block)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0: Ast<Fields> = tree.consume(children, source)?;
                Block::Single(field_0)
            }
            1 => Block::Multiple(
                tree.consume_repeat(children, |children| tree.consume_opt(children, source))?,
            ),
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Variant> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::Variant)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        tree.consume_token(children, Token::BAR)?;
        let attributes: Vec<Ast<Attribute>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
        let name = apply_into!(tree, source, children, Token::ID)?;
        tree.consume_token(children, Token::ARROW)?;
        let fields: Ast<Fields> = tree.consume(children, source)?;
        tree.assert_empty(children)?;

        Ok(Ast {
            syntax_index,
            item: Variant {
                attributes,
                name,
                fields,
            },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Fields> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::Fields)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let field_0: Vec<Ast<Field>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;

        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: Fields(field_0),
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Field> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Field)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let attributes: Vec<Ast<Attribute>> =
                    tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
                let item: Ast<Item> = tree.consume(children, source)?;
                Field::Anonymous { attributes, item }
            }
            1 => {
                let attributes: Vec<Ast<Attribute>> =
                    tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
                let name: Ast<Label> = tree.consume(children, source)?;
                tree.consume_token(children, Token::COLON)?;
                let item: Ast<Item> = tree.consume(children, source)?;
                Field::Labeled {
                    attributes,
                    name,
                    item,
                }
            }
            _ => unreachable!(),
        };

        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Label> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Label)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => Label::Id(apply_into!(tree, source, children, Token::ID)?),
            1 => Label::Number(apply_into!(tree, source, children, Token::INT)?),
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Item> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::Item)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let primary: Ast<Primary> = tree.consume(children, source)?;
        let modifier: Option<Ast<Modifier>> = tree.consume_opt(children, source)?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: Item { primary, modifier },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Primary> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Primary)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => Primary::Ident(apply_into!(tree, source, children, Token::ID)?),
            1 => Primary::TokenStr(apply_into!(tree, source, children, Token::TOKEN_STR)?),
            2 => {
                tree.consume_token(children, Token::PAR_LEFT)?;
                let field_0: Vec<Ast<Alternative>> = {
                    let mut no_bar = false;
                    tree.consume_repeat(children, move |children| {
                        if no_bar {
                            return Ok(None);
                        }
                        let alternative = tree.consume_opt(children, source)?;
                        if tree.consume_token_opt(children, Token::BAR).is_none() {
                            no_bar = true;
                        }
                        Ok(alternative)
                    })?
                };
                tree.consume_token(children, Token::PAR_RIGHT)?;
                Primary::Parent(field_0)
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Alternative> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::Alternative)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let field_0: Vec<Ast<Item>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: Alternative(field_0),
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Modifier> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, _source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Modifier)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                tree.consume_token(children, Token::STAR)?;
                Modifier::Repeat
            }
            1 => {
                tree.consume_token(children, Token::PLUS)?;
                Modifier::Plus
            }
            2 => {
                tree.consume_token(children, Token::STAR_UNDERSCORE)?;
                Modifier::Trailing
            }
            3 => {
                tree.consume_token(children, Token::UNDERSCORE_STAR)?;
                Modifier::Separate
            }
            4 => {
                tree.consume_token(children, Token::PLUS_UNDERSCORE)?;
                Modifier::PlusTrailing
            }
            5 => {
                tree.consume_token(children, Token::UNDERSCORE_PLUS)?;
                Modifier::PlusSeparate
            }
            6 => {
                tree.consume_token(children, Token::QUESTION)?;
                Modifier::Optional
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Attribute> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Attribute)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => Attribute::Doc(apply_into!(tree, source, children, Token::DOC_COMMENT)?),
            1 => Attribute::Normal(tree.consume(children, source)?),
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<NormalAttribute> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::NormalAttribute)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        tree.consume_token(children, Token::SHARP)?;
        tree.consume_token(children, Token::BRACKET_LEFT)?;
        let attribute_list: Vec<Ast<AttributeItem>> = consume_comma_list(tree, source, children)?;
        if attribute_list.is_empty() {
            let span = tree.get(syntax_index).unwrap().span();
            panic!("repetition matched 0 items: {:?}", &source[span])
        }
        tree.consume_token(children, Token::BRACKET_RIGHT)?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: NormalAttribute { attribute_list },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<AttributeItem> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::AttributeItem)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(tree, source, children, Token::ID)?;
                AttributeItem::Id(field_0)
            }
            1 => {
                let field_0: Ast<Literal> = tree.consume(children, source)?;
                AttributeItem::Literal(field_0)
            }
            2 => {
                let name = apply_into!(tree, source, children, Token::ID)?;
                tree.consume_token(children, Token::PAR_LEFT)?;
                let args: Ast<AttributeArgs> = tree.consume(children, source)?;
                tree.consume_token(children, Token::PAR_RIGHT)?;
                AttributeItem::Function { name, args }
            }
            3 => {
                let field_0 = apply_into!(tree, source, children, Token::ID)?;
                tree.consume_token(children, Token::EQUAL)?;
                let field_1: Ast<Literal> = tree.consume(children, source)?;
                AttributeItem::Equal(field_0, field_1)
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<AttributeArgs> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::AttributeArgs)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let field_0 = consume_comma_list(tree, source, children)?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: AttributeArgs(field_0),
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Literal> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Literal)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(tree, source, children, Token::INT)?;
                Literal::Int(field_0)
            }
            1 => {
                let field_0 = apply_into!(tree, source, children, Token::STRING)?;
                Literal::String(field_0)
            }
            2 => {
                let field_0 = apply_into!(tree, source, children, Token::TOKEN_STR)?;
                Literal::TokenStr(field_0)
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<TestValue> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::TestValue)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let rule_name: Ast<Rule> = tree.consume(children, source)?;
        tree.consume_token(children, Token::BRACE_LEFT)?;
        let fields: Vec<Ast<TestField>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
        tree.consume_token(children, Token::BRACE_RIGHT)?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: TestValue { rule_name, fields },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<TestField> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::TestField)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let attributes: Vec<Ast<Attribute>> =
            tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
        let name: Ast<Label> = tree.consume(children, source)?;
        tree.consume_token(children, Token::COLON)?;
        let item: Ast<TestItem> = tree.consume(children, source)?;
        tree.assert_empty(children)?;
        Ok(Ast {
            syntax_index,
            item: TestField {
                attributes,
                name,
                item,
            },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<TestItem> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::TestItem)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(tree, source, children, Token::STRING)?;
                TestItem::Token(field_0)
            }
            1 => {
                let field_0: Ast<TestValue> = tree.consume(children, source)?;
                TestItem::Node(field_0)
            }
            2 => {
                tree.consume_token(children, Token::BRACKET_LEFT)?;
                let field_0: Vec<Ast<TestItem>> =
                    tree.consume_repeat(children, |children| tree.consume_opt(children, source))?;
                tree.consume_token(children, Token::BRACKET_RIGHT)?;
                TestItem::Compound(field_0)
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Rule> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::Rule)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(tree, source, children, Token::ID)?;
                Rule::Node(field_0)
            }
            1 => {
                let base = apply_into!(tree, source, children, Token::ID)?;
                tree.consume_token(children, Token::COLON_COLON)?;
                let variant = apply_into!(tree, source, children, Token::ID)?;
                Rule::Variant { base, variant }
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<Conflict> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (_, mut children) = tree.expect_node(syntax_index, Node::Conflict)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        tree.consume_token(children, Token::CONFLICT_KW)?;
        tree.consume_token(children, Token::BRACE_LEFT)?;
        tree.consume_token(children, Token::PREFER_KW)?;
        tree.consume_token(children, Token::COLON)?;
        let prefer: Ast<ConflictAction> = tree.consume(children, source)?;
        tree.consume_token(children, Token::OVER_KW)?;
        tree.consume_token(children, Token::COLON)?;
        let over: Ast<ConflictAction> = tree.consume(children, source)?;
        let lookahead: Option<(Ast<()>, Ast<()>, Ast<IdOrTokenStr>)> = {
            if let Some((index_0, _)) = tree.consume_token_opt(children, Token::LOOKAHEAD_KW) {
                let field_0 = Ast {
                    syntax_index: index_0,
                    item: (),
                };
                let field_1 = Ast {
                    syntax_index: tree.consume_token(children, Token::COLON)?.0,
                    item: (),
                };
                let field_2: Ast<IdOrTokenStr> = tree.consume(children, source)?;
                Some((field_0, field_1, field_2))
            } else {
                None
            }
        };
        tree.consume_token(children, Token::BRACE_RIGHT)?;
        Ok(Ast {
            syntax_index,
            item: Conflict {
                prefer,
                over,
                lookahead,
            },
        })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<ConflictAction> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::ConflictAction)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => {
                tree.consume_token(children, Token::SHIFT_KW)?;
                tree.consume_token(children, Token::PAR_LEFT)?;
                tree.consume_token(children, Token::UNDERSCORE)?;
                tree.consume_token(children, Token::PAR_RIGHT)?;
                ConflictAction::ShiftAny
            }
            1 => {
                tree.consume_token(children, Token::SHIFT_KW)?;
                tree.consume_token(children, Token::PAR_LEFT)?;
                let token: Ast<IdOrTokenStr> = tree.consume(children, source)?;
                tree.consume_token(children, Token::PAR_RIGHT)?;
                ConflictAction::ShiftToken { token }
            }
            2 => {
                tree.consume_token(children, Token::REDUCE_KW)?;
                tree.consume_token(children, Token::PAR_LEFT)?;
                let field_0: Ast<Rule> = tree.consume(children, source)?;
                tree.consume_token(children, Token::PAR_RIGHT)?;
                ConflictAction::Reduce(field_0)
            }
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}

impl TryFrom<(&SyntaxTree<Token, Node>, SyntaxIdx, &str)> for Ast<IdOrTokenStr> {
    type Error = AstError;
    fn try_from(
        (tree, syntax_index, source): (&SyntaxTree<Token, Node>, SyntaxIdx, &str),
    ) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = tree.expect_node(syntax_index, Node::IdOrTokenStr)?;
        let children = &mut (&mut children as &mut dyn Iterator<Item = SyntaxIdx>).peekable();
        let item = match rule_idx.index {
            0 => IdOrTokenStr::Id(apply_into!(tree, source, children, Token::ID)?),
            1 => IdOrTokenStr::TokenStr(apply_into!(tree, source, children, Token::TOKEN_STR)?),
            _ => unreachable!(),
        };
        tree.assert_empty(children)?;
        Ok(Ast { syntax_index, item })
    }
}
