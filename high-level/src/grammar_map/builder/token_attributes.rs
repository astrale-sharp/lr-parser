//! Parse attributes (`#[...]`) on tokens.

#![allow(clippy::blocks_in_if_conditions)]

use super::{
    attribute_parser::{self, *},
    FileId, MappingError, NamesMapping, RemappedTokenOwned, SyntaxTreeLocation, Warning,
};
use crate::parser::ast::{Ast, Attribute, AttributeArgs, AttributeItem, Literal};

/// Attributes attached to a token block
#[derive(Default, Debug)]
pub(super) struct TokenBlockAttributes {
    /// Documentation of the tokens.
    ///
    /// `None` if no documentation was supplied.
    pub documentation: Option<String>,
    /// `#[context(name = type, ...)]`
    pub context: Option<Vec<(String, String)>>,
}

/// Attributes attached to a token
#[derive(Default)]
pub(super) struct TokenAttributes {
    /// Documentation of the token.
    ///
    /// `None` if no documentation was supplied.
    pub documentation: Option<String>,
    /// `#[skip]`
    pub skip: bool,
    /// `#[callback(ID)]`
    pub callback: Option<String>,
    /// `#[into(ID)]`
    pub into: Option<(String, String)>,
    /// `#[priority(INT)]`
    pub priority: Option<u32>,
    /// `#[remap(...)]`
    pub remap: RemappedTokenOwned,
}

impl TokenBlockAttributes {
    /// Parse the `attributes` list.
    pub(super) fn parse(
        file: FileId,
        attributes: &[Ast<Attribute>],
        warnings: &mut Vec<Warning>,
    ) -> Result<Self, MappingError> {
        let mut result = Self::default();

        for attr in attributes {
            match &**attr {
                Attribute::Doc(doc) => {
                    attribute_parser::add_documentation_line(&mut result.documentation, doc);
                }
                Attribute::Normal(attr) => {
                    let parse_single_attribute = |attr: &Ast<AttributeItem>| {
                        parse_function(attr, |name, args| {
                            if name.as_str() == "context" {
                                let mut context = Vec::new();
                                parse_repeat_err("context", file, &args.0, |attr| {
                                    parse_equal(attr, |id, lit| {
                                        if let Literal::String(value) = &**lit {
                                            context.push((
                                                id.item.to_string(),
                                                value.item.to_string(),
                                            ));
                                            Ok(true)
                                        } else {
                                            Ok(false)
                                        }
                                    })
                                })?;
                                Ok(true)
                            } else {
                                Ok(false)
                            }
                        })
                    };
                    if parse_single(&*attr.attribute_list, parse_single_attribute)? {
                        continue;
                    }

                    warnings.push(Warning::UnknownAttribute(SyntaxTreeLocation {
                        file,
                        element: attr.element.clone(),
                    }));
                }
            }
        }

        Ok(result)
    }
}

impl TokenAttributes {
    /// Parse the `attributes` list.
    ///
    /// We need the `builder` to get tokens and nodes mappings.
    pub(super) fn parse(
        file: FileId,
        attributes: &[Ast<Attribute>],
        builder: &NamesMapping,
        warnings: &mut Vec<Warning>,
    ) -> Result<Self, MappingError> {
        let mut result = Self::default();
        for attr in attributes {
            match &**attr {
                Attribute::Doc(doc) => {
                    attribute_parser::add_documentation_line(&mut result.documentation, doc);
                }
                Attribute::Normal(attr) => {
                    let parse_single_attribute = |attr: &Ast<AttributeItem>| {
                        if parse_id(attr, |id| {
                            if id.as_str() == "skip" {
                                result.skip = true;
                                Ok(true)
                            } else {
                                Ok(false)
                            }
                        })? {
                            return Ok(true);
                        }

                        let function = |name: &Ast<String>, args: &Ast<AttributeArgs>| {
                            let args_location = SyntaxTreeLocation {
                                file,
                                element: args.element.clone(),
                            };
                            match name.as_str() {
                                "priority" => {
                                    parse_single_err("priority", file, args, |attr| {
                                        parse_int(attr, |n| {
                                            result.priority = Some(**n);
                                            Ok(true)
                                        })
                                    })?;
                                    Ok(true)
                                }
                                "callback" => {
                                    parse_single_err("callback", file, args, |attr| {
                                        parse_id(attr, |id| {
                                            result.callback = Some(id.item.clone());
                                            Ok(true)
                                        })
                                    })?;
                                    Ok(true)
                                }
                                "into" => {
                                    let args_location = SyntaxTreeLocation {
                                        file,
                                        element: args.element.clone(),
                                    };
                                    match args.0.as_slice() {
                                        [function, return_type] => {
                                            let mut stored_function = String::new();
                                            let mut stored_return_type = String::new();
                                            if !parse_id(function, |function| {
                                                stored_function = function.item.clone();
                                                Ok(true)
                                            })? {
                                                return Err(MappingError::IncorrectAttrArguments {
                                                    function_name: "into",
                                                    location: SyntaxTreeLocation {
                                                        file,
                                                        element: function.element.clone(),
                                                    },
                                                    additional_msg: "expected an identifier",
                                                });
                                            }
                                            if !parse_string(return_type, |side| {
                                                stored_return_type = side.item.clone();
                                                Ok(true)
                                            })? {
                                                return Err(MappingError::IncorrectAttrArguments {
                                                    function_name: "into",
                                                    location: SyntaxTreeLocation {
                                                        file,
                                                        element: return_type.element.clone(),
                                                    },
                                                    additional_msg: "expected a string",
                                                });
                                            }
                                            result.into =
                                                Some((stored_function, stored_return_type));
                                            Ok(true)
                                        }
                                        _ => Err(MappingError::IncorrectAttrArguments {
                                            function_name: "into",
                                            location: args_location,
                                            additional_msg: "expected two arguments",
                                        }),
                                    }
                                }
                                "remap" => {
                                    if !parse_single(&args.0, |attr| {
                                        if !parse_id(attr, |id| {
                                            if let Some((id, _)) =
                                                builder.get_token_by_name(id.item.as_str())
                                            {
                                                result.remap = RemappedTokenOwned::Transform(id);
                                                Ok(true)
                                            } else {
                                                Err(MappingError::UnknownToken(
                                                    SyntaxTreeLocation {
                                                        file,
                                                        element: id.element.clone(),
                                                    },
                                                ))
                                            }
                                        })? {
                                            Err(MappingError::IncorrectAttrArguments {
                                                function_name: "remap",
                                                location: SyntaxTreeLocation {
                                                    file,
                                                    element: attr.element.clone(),
                                                },
                                                additional_msg: "expected a token identifier",
                                            })
                                        } else {
                                            Ok(true)
                                        }
                                    })? {
                                        if args.0.is_empty() {
                                            return Err(MappingError::AttrRequireArgument(
                                                "remap",
                                                args_location,
                                            ));
                                        }
                                        let mut split = Vec::new();
                                        parse_repeat_err("remap", file, &args.0, |attr| {
                                            parse_token_str(attr, |token_str| {
                                                if let Some(id) =
                                                    builder.get_token_id_by_expr(token_str.as_str())
                                                {
                                                    split.push((id, token_str.len()));
                                                    Ok(true)
                                                } else {
                                                    Err(MappingError::UnknownToken(
                                                        SyntaxTreeLocation {
                                                            file,
                                                            element: token_str.element.clone(),
                                                        },
                                                    ))
                                                }
                                            })
                                        })?;
                                        result.remap = RemappedTokenOwned::Split(split.into());
                                        Ok(true)
                                    } else {
                                        Ok(true)
                                    }
                                }
                                _ => Ok(false),
                            }
                        };
                        if parse_function(attr, function)? {
                            return Ok(true);
                        }

                        Ok(false)
                    };
                    if parse_single(&*attr.attribute_list, parse_single_attribute)? {
                        continue;
                    }

                    warnings.push(Warning::UnknownAttribute(SyntaxTreeLocation {
                        file,
                        element: attr.element.clone(),
                    }));
                }
            }
        }
        Ok(result)
    }
}
