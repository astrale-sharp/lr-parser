//! Builder for [`super::GrammarMap`].

mod attribute_parser;
mod node_attributes;
mod operators;
mod token_attributes;

use self::{
    node_attributes::{FieldAttributes, NodeAttributes, VariantAttributes},
    operators::{OperatorKind, PrecedenceTable},
    token_attributes::{TokenAttributes, TokenBlockAttributes},
};
use super::{
    error::SyntaxTreeLocation,
    node_in_map::{Alternative, FieldItem, NodeContent, NodeOrigin, NodeVariant, Primary},
    Field, FileId, MappingError, NodeInMap, NodeType, RemappedTokenOwned, TokenInMap, TokenType,
    TokensInMap, Warning,
};
use crate::parser::ast::{self, Ast};
use lr_parser::grammar::{ConflictSolution, ConflictingAction, RuleIdx, RuntimeGrammar, Symbol};
use std::{collections::HashMap, convert::TryInto, hash::Hash};

#[derive(Debug)]
struct TokenInfo<'items> {
    source_file: FileId,
    id: TokenType,
    expression: Option<&'items str>,
    /// Reference into the AST
    ast: &'items Ast<ast::Token>,
}

#[derive(Debug)]
struct NodeInfo<'items> {
    location: SyntaxTreeLocation,
    attributes: &'items [Ast<ast::Attribute>],
    name: &'items Ast<String>,
    block: &'items Ast<ast::Block>,
}

/// Borrowed version of [`super::ExpressionKind`].
#[derive(Debug, Clone, Copy)]
enum ExpressionKind<'items> {
    Raw(&'items str),
    Regex(&'items str),
    External,
}

impl ExpressionKind<'_> {
    fn into_owned(self) -> super::ExpressionKind {
        match self {
            ExpressionKind::Raw(s) => super::ExpressionKind::Raw(s.to_owned()),
            ExpressionKind::Regex(s) => super::ExpressionKind::Regex(s.to_owned()),
            ExpressionKind::External => super::ExpressionKind::External,
        }
    }
}

#[derive(Debug)]
pub(super) struct Builder<'warnings, Step> {
    /// Accumulated warnings
    pub warnings: &'warnings mut Vec<Warning>,
    pub step: Step,
}

/// There is no overlapping between `tokens_names` and `nodes_names`
#[derive(Debug, Default)]
pub(super) struct NamesMapping<'items> {
    tokens_block_attributes: TokenBlockAttributes,
    tokens: Vec<TokenInfo<'items>>,
    token_verbatim: Option<&'items str>,
    nodes: Vec<NodeInfo<'items>>,
    /// Map names to tokens
    tokens_names: HashMap<&'items str, TokenType>,
    /// Map names to nodes
    nodes_names: HashMap<&'items str, NodeType>,
    /// Map token expressions to tokens
    tokens_exprs: HashMap<&'items str, TokenType>,
    /// List of `(prefer, over)` conflict solutions
    conflicts: Vec<(FileId, &'items Ast<ast::Conflict>)>,
}

/// Result of [`Builder::process_tokens`].
pub(super) struct TokensProcessed<'items> {
    pub(super) tokens_in_map: TokensInMap,
    nodes: Vec<NodeInfo<'items>>,
    /// Map names to nodes
    nodes_names: HashMap<&'items str, NodeType>,
    /// List of `(prefer, over)` conflict solutions
    conflicts: Vec<(FileId, &'items Ast<ast::Conflict>)>,
}

/// Result of [`Builder::process_nodes`].
pub(super) struct NodesProcessed<'items> {
    tokens_in_map: TokensInMap,
    nodes: Vec<NodeInMap>,
    /// Map names to nodes
    nodes_names: HashMap<String, NodeType>,
    location_to_node: HashMap<SyntaxTreeLocation, NodeType>,
    grammar: RuntimeGrammar<TokenType, NodeType>,
    start_node_id: NodeType,
    /// List of `(prefer, over)` conflict solutions
    conflicts: Vec<(FileId, &'items Ast<ast::Conflict>)>,
}

/// Result of [`Builder::process_conflicts`].
///
/// Final result of the various `process` functions.
pub(super) struct AllProcessed {
    pub(super) tokens_in_map: TokensInMap,
    pub(super) nodes: Vec<NodeInMap>,
    /// Map names to nodes
    pub(super) nodes_names: HashMap<String, NodeType>,
    pub(super) location_to_node: HashMap<SyntaxTreeLocation, NodeType>,
    pub(super) grammar: RuntimeGrammar<TokenType, NodeType>,
    pub(super) start_node_id: NodeType,
}

impl NamesMapping<'_> {
    /// Retrieve some token by its name
    fn get_token_by_name(&self, name: &str) -> Option<(TokenType, &TokenInfo)> {
        self.tokens_names
            .get(name)
            .and_then(|id| self.tokens.get(id.as_index()).map(|info| (*id, info)))
    }

    fn get_token_id_by_expr(&self, expr: &str) -> Option<TokenType> {
        self.tokens_exprs.get(expr).copied()
    }

    /// Check that the name is free to use.
    ///
    /// `location` will be used for error reporting if needed.
    fn check_name_use(&self, name: &str, location: SyntaxTreeLocation) -> Result<(), MappingError> {
        if let Some(TokenInfo {
            source_file, ast, ..
        }) = self
            .tokens_names
            .get(name)
            .and_then(|id| self.tokens.get(id.as_index()))
        {
            return Err(MappingError::AlreadyDefinedToken {
                token_definition: SyntaxTreeLocation {
                    file: *source_file,
                    element: ast.element.clone(),
                },
                new_definition: location,
            });
        }
        if let Some(NodeInfo {
            location: source_location,
            name,
            ..
        }) = self
            .nodes_names
            .get(name)
            .and_then(|id| self.nodes.get(*id as usize))
        {
            return Err(MappingError::AlreadyDefinedNode {
                node_definition: SyntaxTreeLocation {
                    file: source_location.file,
                    element: name.element.clone(),
                },
                new_definition: location,
            });
        }
        Ok(())
    }

    /// Check that the expression is free to use.
    ///
    /// `location` will be used for error reporting if needed.
    fn check_token_str_use(
        &self,
        expression: &str,
        location: SyntaxTreeLocation,
    ) -> Result<(), MappingError> {
        match self
            .tokens_exprs
            .get(expression)
            .and_then(|id| self.tokens.get(id.as_index()))
        {
            Some(TokenInfo {
                source_file, ast, ..
            }) => Err(MappingError::AlreadyDefinedTokenStr {
                old_definition: SyntaxTreeLocation {
                    file: *source_file,
                    element: ast.element.clone(),
                },
                new_definition: location,
            }),
            None => Ok(()),
        }
    }
}

impl<'items, 'warnings> Builder<'warnings, NamesMapping<'items>> {
    /// Assign an ID to every node/token encountered. Also register raw token
    /// expressions.
    ///
    /// # Errors
    /// Return an error if the same name is used for multiple objects (also detect
    /// raw token expressions conflicts).
    pub fn new(
        items: &'items [(Ast<ast::TopLevelItem>, FileId)],
        warnings: &'warnings mut Vec<Warning>,
    ) -> Result<Self, MappingError> {
        let mut result = NamesMapping::default();
        for (item, file_id) in items {
            match &**item {
                ast::TopLevelItem::Include { .. } => { /* already done */ }
                ast::TopLevelItem::Tokens {
                    attributes,
                    tokens,
                    verbatim,
                } => {
                    result.tokens_block_attributes =
                        TokenBlockAttributes::parse(*file_id, attributes, warnings)?;
                    result.token_verbatim = verbatim.as_ref().map(|s| s.1.as_str());
                    for token in tokens {
                        let token_id = (result.tokens.len() + 1)
                            .try_into()
                            .map_err(|_| MappingError::TokenOverflow)?;
                        let token_name = token.name.as_str();
                        result.check_name_use(
                            token_name,
                            SyntaxTreeLocation {
                                file: *file_id,
                                element: token.element.clone(),
                            },
                        )?;
                        match &*token.definition {
                            ast::TokenDef::Raw(raw) => {
                                result.check_token_str_use(
                                    raw.as_str(),
                                    SyntaxTreeLocation {
                                        file: *file_id,
                                        element: token.element.clone(),
                                    },
                                )?;
                                result.tokens.push(TokenInfo {
                                    source_file: *file_id,
                                    id: token_id,
                                    expression: Some(raw.as_str()),
                                    ast: token,
                                });
                                result.tokens_names.insert(token_name, token_id);
                                result.tokens_exprs.insert(raw.as_str(), token_id);
                            }
                            ast::TokenDef::Regex(_) | ast::TokenDef::External => {
                                result.tokens.push(TokenInfo {
                                    source_file: *file_id,
                                    id: token_id,
                                    expression: None,
                                    ast: token,
                                });
                                result.tokens_names.insert(token_name, token_id);
                            }
                        }
                    }
                }
                ast::TopLevelItem::Node {
                    attributes,
                    name,
                    block,
                } => {
                    let node_id = result
                        .nodes
                        .len()
                        .try_into()
                        .map_err(|_| MappingError::NodeOverflow)?;
                    result.check_name_use(
                        name,
                        SyntaxTreeLocation {
                            file: *file_id,
                            element: name.element.clone(),
                        },
                    )?;
                    result.nodes.push(NodeInfo {
                        location: SyntaxTreeLocation {
                            file: *file_id,
                            element: item.element.clone(),
                        },
                        attributes,
                        name,
                        block,
                    });
                    result.nodes_names.insert(name, node_id);
                }
                ast::TopLevelItem::Test { .. } => {
                    // TODO
                    eprintln!("tests are not supported right now")
                }
                ast::TopLevelItem::Conflict(conflict) => {
                    result.conflicts.push((*file_id, conflict))
                }
            }
        }

        Ok(Self {
            warnings,
            step: result,
        })
    }

    /// Process `tokens { ... }` blocks.
    pub fn process_tokens(
        mut self,
    ) -> Result<Builder<'warnings, TokensProcessed<'items>>, MappingError> {
        let mut tokens_in_map = TokensInMap {
            verbatim: self.step.token_verbatim.map(|s| s.to_string()),
            documentation: None,
            context: Vec::new(),
            tokens: Vec::new(),
            location_to_token: HashMap::new(),
            token_str_to_token: HashMap::new(),
            name_to_token: HashMap::new(),
        };

        for TokenInfo {
            source_file,
            id: token_id,
            ast,
            ..
        } in &self.step.tokens
        {
            let token_id = *token_id;
            let file = *source_file;
            let TokenAttributes {
                documentation,
                skip,
                callback,
                into,
                priority,
                remap,
            } = TokenAttributes::parse(file, &ast.attributes, &self.step, &mut self.warnings)?;

            let element = &ast.element;
            let name = ast.name.as_str();
            let expression = match &*ast.definition {
                ast::TokenDef::Raw(raw) => ExpressionKind::Raw(raw),
                ast::TokenDef::Regex(regex) => ExpressionKind::Regex(regex),
                ast::TokenDef::External => ExpressionKind::External,
            };
            tokens_in_map.location_to_token.insert(
                SyntaxTreeLocation {
                    file,
                    element: element.clone(),
                },
                token_id,
            );
            tokens_in_map
                .name_to_token
                .insert(name.to_string(), token_id);
            if let ExpressionKind::Raw(raw) = expression {
                match tokens_in_map.token_str_to_token.get(raw) {
                    Some(index) => {
                        return Err(MappingError::AlreadyDefinedTokenStr {
                            old_definition: tokens_in_map.tokens[index.as_index()].location.clone(),
                            new_definition: SyntaxTreeLocation {
                                file,
                                element: element.clone(),
                            },
                        })
                    }
                    None => {
                        tokens_in_map
                            .token_str_to_token
                            .insert(raw.to_string(), token_id);
                    }
                }
            }
            tokens_in_map.tokens.push(TokenInMap {
                id: token_id,
                location: SyntaxTreeLocation {
                    file,
                    element: element.clone(),
                },
                name: name.to_string(),
                expression: expression.into_owned(),
                skip,
                documentation,
                callback,
                into,
                priority,
                remap,
            });
        }
        tokens_in_map.documentation = self.step.tokens_block_attributes.documentation;
        tokens_in_map.context = self
            .step
            .tokens_block_attributes
            .context
            .unwrap_or_default();

        Ok(Builder {
            warnings: self.warnings,
            step: TokensProcessed {
                tokens_in_map,
                nodes: self.step.nodes,
                nodes_names: self.step.nodes_names,
                conflicts: self.step.conflicts,
            },
        })
    }
}

impl<'items, 'warnings> Builder<'warnings, TokensProcessed<'items>> {
    /// Process `node { ... }` blocks.
    pub(super) fn process_nodes(
        self,
    ) -> Result<Builder<'warnings, NodesProcessed<'items>>, MappingError> {
        let mut grammar_builder = GrammarBuilder {
            nodes_names: &self.step.nodes_names,
            tokens_in_map: &self.step.tokens_in_map,
            grammar: RuntimeGrammar::new(),
            compiled_rules: HashMap::new(),
            user_defined_node_names: self
                .step
                .nodes
                .iter()
                .map(|node| node.name.item.clone())
                .collect::<Vec<_>>(),
            nodes: Vec::new(),
            node_id: self.step.nodes.len() as _,
        };

        let mut start_node_span = None;
        let mut start_node_id = None;
        let mut nodes: Vec<NodeInMap> = Vec::new();
        let mut location_to_node = HashMap::new();

        for (
            id,
            NodeInfo {
                location: node_location,
                attributes,
                name,
                block,
            },
        ) in self.step.nodes.into_iter().enumerate()
        {
            let file = node_location.file;
            let lhs = id as NodeType;
            let node_attributes = NodeAttributes::parse(
                node_location.clone(),
                file,
                attributes,
                &mut start_node_span,
                self.warnings,
            )?;
            if node_attributes.start.is_some() {
                start_node_id = Some(lhs);
            }
            let silent = node_attributes.silent.is_some();
            location_to_node.insert(node_location.clone(), lhs);
            match &block.item {
                ast::Block::Single(fields) => {
                    let mut processed_fields = Vec::new();
                    let (rule_idx, field_items) =
                        grammar_builder.grammar_add_rule(lhs, &fields.0, file, &node_location)?;
                    for (field, field_item) in fields.0.iter().zip(field_items.into_iter()) {
                        let FieldAttributes {
                            documentation,
                            prefix,
                            infix,
                            suffix,
                        } = match &**field {
                            ast::Field::Anonymous { attributes, .. }
                            | ast::Field::Named { attributes, .. } => {
                                FieldAttributes::parse(file, attributes, self.warnings)?
                            }
                        };
                        if let Some((location, _)) = prefix {
                            self.warnings.push(Warning::IgnoredAttribute(
                                location,
                                "'prefix' can only be used on a node with multiple variants",
                            ))
                        }
                        if let Some((location, _, _)) = infix {
                            self.warnings.push(Warning::IgnoredAttribute(
                                location,
                                "'infix' can only be used on a node with multiple variants",
                            ))
                        }
                        if let Some((location, _)) = suffix {
                            self.warnings.push(Warning::IgnoredAttribute(
                                location,
                                "'suffix' can only be used on a node with multiple variants",
                            ))
                        }
                        processed_fields.push(Field {
                            location: SyntaxTreeLocation {
                                file,
                                element: field.element.clone(),
                            },
                            documentation,
                            label: match &**field {
                                ast::Field::Anonymous { .. } => None,
                                ast::Field::Named { name, .. } => Some(name.clone()),
                            },
                            item: field_item,
                        })
                    }
                    nodes.push(NodeInMap {
                        id: lhs,
                        location: node_location,
                        origin: NodeOrigin::UserDefined {
                            name: name.clone(),
                            documentation: node_attributes.documentation,
                            silent,
                            content: NodeContent::Single {
                                rule_idx,
                                fields: processed_fields,
                            },
                        },
                    });
                }
                ast::Block::Multiple(block_variants) => {
                    let mut variants = Vec::new();
                    let mut precedence_table = PrecedenceTable::new();

                    for variant in block_variants {
                        let attributes =
                            VariantAttributes::parse(file, &variant.attributes, self.warnings)?;
                        let (rule_idx, field_items) = grammar_builder.grammar_add_rule(
                            lhs,
                            &variant.fields.0,
                            file,
                            &node_location,
                        )?;

                        let mut fields = Vec::new();

                        for (field, field_item) in
                            variant.fields.0.iter().zip(field_items.into_iter())
                        {
                            let FieldAttributes {
                                documentation,
                                prefix,
                                infix,
                                suffix,
                            } = match &**field {
                                ast::Field::Anonymous { attributes, .. }
                                | ast::Field::Named { attributes, .. } => {
                                    FieldAttributes::parse(file, attributes, self.warnings)?
                                }
                            };

                            let token = match &**field {
                                ast::Field::Anonymous { item, .. }
                                | ast::Field::Named { item, .. } => {
                                    if item.modifier.is_some() {
                                        None
                                    } else {
                                        match &*item.primary {
                                            ast::Primary::Ident(name) => self
                                                .step
                                                .tokens_in_map
                                                .name_to_token
                                                .get(name.as_str())
                                                .copied(),
                                            ast::Primary::TokenStr(token_str) => self
                                                .step
                                                .tokens_in_map
                                                .token_str_to_token
                                                .get(token_str.as_str())
                                                .copied(),
                                            ast::Primary::Parent(_) => None,
                                        }
                                    }
                                }
                            };

                            let token: Result<TokenType, MappingError> = match token {
                                Some(token) => Ok(token),
                                None => Err(MappingError::FieldIsNotToken(SyntaxTreeLocation {
                                    file,
                                    element: match &**field {
                                        ast::Field::Anonymous { item, .. }
                                        | ast::Field::Named { item, .. } => item.element.clone(),
                                    },
                                })),
                            };

                            if let Some((_, precedence)) = prefix {
                                match token {
                                    Ok(token) => {
                                        precedence_table.add_operator(
                                            token,
                                            rule_idx,
                                            OperatorKind::Prefix(precedence),
                                        );
                                    }
                                    Err(err) => return Err(err),
                                }
                            }
                            if let Some((_, precedence, side)) = infix {
                                match token {
                                    Ok(token) => {
                                        precedence_table.add_operator(
                                            token,
                                            rule_idx,
                                            OperatorKind::Infix(precedence, side),
                                        );
                                    }
                                    Err(err) => return Err(err),
                                }
                            }
                            if let Some((_, precedence)) = suffix {
                                let token = token?;
                                precedence_table.add_operator(
                                    token,
                                    rule_idx,
                                    OperatorKind::Suffix(precedence),
                                );
                            }
                            fields.push(Field {
                                location: SyntaxTreeLocation {
                                    file,
                                    element: field.element.clone(),
                                },
                                documentation,
                                label: match &**field {
                                    ast::Field::Anonymous { .. } => None,
                                    ast::Field::Named { name, .. } => Some(name.clone()),
                                },
                                item: field_item,
                            })
                        }

                        variants.push(NodeVariant {
                            documentation: attributes.documentation,
                            rule_idx,
                            name: variant.name.clone(),
                            fields,
                        });
                    }

                    for solution in precedence_table.conflict_solutions() {
                        grammar_builder.grammar.add_conflict_resolution(solution)
                    }
                    nodes.push(NodeInMap {
                        id: lhs,
                        location: node_location.clone(),
                        origin: NodeOrigin::UserDefined {
                            name: name.clone(),
                            documentation: node_attributes.documentation,
                            silent,
                            content: NodeContent::Multiple { variants },
                        },
                    });
                }
            }
        }
        let start_node_id = match start_node_id {
            Some(id) => id,
            None => return Err(MappingError::NoStart),
        };

        for internal_node in grammar_builder.nodes {
            nodes.push(NodeInMap {
                id: internal_node.id,
                location: internal_node.location,
                origin: NodeOrigin::Internal {
                    expression: internal_node.expression,
                },
            })
        }
        let grammar = grammar_builder.grammar;

        Ok(Builder {
            warnings: self.warnings,
            step: NodesProcessed {
                tokens_in_map: self.step.tokens_in_map,
                nodes,
                nodes_names: self
                    .step
                    .nodes_names
                    .into_iter()
                    .map(|(name, value)| (name.to_string(), value))
                    .collect(),
                location_to_node,
                grammar,
                start_node_id,
                conflicts: self.step.conflicts,
            },
        })
    }
}

/// Node held by a [`GrammarBuilder`].
struct InternalNode {
    /// This node's ID.
    id: NodeType,
    /// Location of the node in source code.
    location: SyntaxTreeLocation,
    /// Textual representation of the node.
    expression: String,
}

impl FieldItem {
    /// Transform `ast::Item` into `CompiledItem`.
    fn compile_item(
        file: FileId,
        item: &Ast<ast::Item>,
        name_to_symbol: &impl Fn(&str) -> Option<Symbol<TokenType, NodeType>>,
        token_str_to_token: &impl Fn(&str) -> Option<TokenType>,
    ) -> Result<Ast<Self>, MappingError> {
        let compiled_primary = match &*item.primary {
            ast::Primary::Ident(id) => {
                if let Some(symbol) = name_to_symbol(id.as_str()) {
                    match symbol {
                        Symbol::Token(token) => Primary::Token(token),
                        Symbol::Node(node) => Primary::Node(node),
                    }
                } else {
                    return Err(MappingError::UnknownIdent(SyntaxTreeLocation {
                        file,
                        element: id.element.clone(),
                    }));
                }
            }
            ast::Primary::TokenStr(token_str) => {
                if let Some(token) = token_str_to_token(token_str.as_str()) {
                    Primary::Token(token)
                } else {
                    return Err(MappingError::UnknownTokenStr(SyntaxTreeLocation {
                        file,
                        element: token_str.element.clone(),
                    }));
                }
            }
            ast::Primary::Parent(parent) => Primary::Parent(
                parent
                    .iter()
                    .map(|alternative| {
                        Ok(Ast {
                            item: Alternative(
                                alternative
                                    .0
                                    .iter()
                                    .map(|item| {
                                        Self::compile_item(
                                            file,
                                            item,
                                            name_to_symbol,
                                            token_str_to_token,
                                        )
                                    })
                                    .collect::<Result<Vec<_>, _>>()?,
                            ),
                            element: alternative.element.clone(),
                        })
                    })
                    .collect::<Result<Vec<_>, _>>()?,
            ),
        };
        Ok(Ast {
            item: Self {
                primary: Ast {
                    item: compiled_primary,
                    element: item.primary.element.clone(),
                },
                modifier: item.modifier.clone(),
            },
            element: item.element.clone(),
        })
    }
}

/// Wrapper around a `FieldItem`, that can be hashed and compared independently of
/// the additional field of `Ast`.
#[derive(Clone, Debug)]
struct HashableFieldItem(Ast<FieldItem>);

impl HashableFieldItem {
    fn eq_for_modifier(
        mod1: &Option<Ast<ast::Modifier>>,
        mod2: &Option<Ast<ast::Modifier>>,
    ) -> bool {
        if let Some(mod1) = mod1 {
            if let Some(mod2) = mod2 {
                mod1.item == mod2.item
            } else {
                false
            }
        } else {
            true
        }
    }

    fn eq_for_field_item(item1: &FieldItem, item2: &FieldItem) -> bool {
        Self::eq_for_primary(&item1.primary, &item2.primary)
            && Self::eq_for_modifier(&item1.modifier, &item2.modifier)
    }

    fn eq_for_primary(primary1: &Primary, primary2: &Primary) -> bool {
        match primary1 {
            Primary::Token(t1) => {
                if let Primary::Token(t2) = primary2 {
                    t1 == t2
                } else {
                    false
                }
            }
            Primary::Node(n1) => {
                if let Primary::Node(n2) = primary2 {
                    n1 == n2
                } else {
                    false
                }
            }
            Primary::Parent(alternatives1) => {
                if let Primary::Parent(alternatives2) = primary2 {
                    if alternatives1.len() != alternatives2.len() {
                        return false;
                    }
                    for (alt1, alt2) in alternatives1.iter().zip(alternatives2.iter()) {
                        if !Self::eq_for_alternative(alt1, alt2) {
                            return false;
                        }
                    }
                    true
                } else {
                    false
                }
            }
        }
    }

    fn eq_for_alternative(alt1: &Alternative, alt2: &Alternative) -> bool {
        if alt1.0.len() != alt2.0.len() {
            return false;
        }
        for (item1, item2) in alt1.0.iter().zip(alt2.0.iter()) {
            if !Self::eq_for_field_item(item1, item2) {
                return false;
            }
        }
        true
    }

    fn hash_field_item<H: std::hash::Hasher>(item: &FieldItem, state: &mut H) {
        Self::hash_primary(&item.primary, state);
        Self::hash_modifier(&item.modifier, state);
    }

    fn hash_modifier<H: std::hash::Hasher>(modifier: &Option<Ast<ast::Modifier>>, state: &mut H) {
        let modifier = modifier.as_ref().map(|modifier| modifier.item);
        modifier.hash(state)
    }

    fn hash_primary<H: std::hash::Hasher>(primary: &Primary, state: &mut H) {
        match primary {
            Primary::Token(token) => token.hash(state),
            Primary::Node(node) => node.hash(state),
            Primary::Parent(alternatives) => {
                for alt in alternatives {
                    for item in alt.0.iter() {
                        Self::hash_field_item(item, state)
                    }
                }
            }
        }
    }
}

impl PartialEq for HashableFieldItem {
    fn eq(&self, other: &Self) -> bool {
        Self::eq_for_field_item(&self.0, &other.0)
    }
}

impl Eq for HashableFieldItem {}

impl Hash for HashableFieldItem {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        Self::hash_field_item(&self.0, state)
    }
}

/// Holds the information to gradually build the grammar rules.
struct GrammarBuilder<'builder, 'items> {
    /// Result of the building process.
    grammar: RuntimeGrammar<TokenType, NodeType>,
    /// Reference to the tokens.
    tokens_in_map: &'builder TokensInMap,
    /// Maps node names to their ID.
    nodes_names: &'builder HashMap<&'items str, NodeType>,
    /// Maps a rule (in a form where it can be compared) to it's produced node's
    /// ID.
    compiled_rules: HashMap<HashableFieldItem, NodeType>,
    /// Names of user-defined nodes.
    user_defined_node_names: Vec<String>,
    /// Constructed nodes so far.
    nodes: Vec<InternalNode>,
    /// Next available node ID.
    ///
    /// This should be the same as `nodes.len()`, but correctly typed.
    node_id: NodeType,
}

impl GrammarBuilder<'_, '_> {
    /// Add a new node to the grammar builder, without any rules associated to it.
    ///
    /// Returns the ID of the new node.
    fn add_node(
        &mut self,
        location: SyntaxTreeLocation,
        expression: String,
    ) -> Result<u16, MappingError> {
        if self.node_id == NodeType::MAX {
            Err(MappingError::NodeOverflow)
        } else {
            let new_node_id = self.node_id;
            self.nodes.push(InternalNode {
                id: new_node_id,
                location,
                expression,
            });
            self.node_id += 1;
            Ok(new_node_id)
        }
    }

    /// Get the symbol's ID (token or node) corresponding to `name`.
    fn name_to_symbol(&self, name: &str) -> Option<Symbol<TokenType, NodeType>> {
        if let Some(node_id) = self.nodes_names.get(name) {
            Some(Symbol::Node(*node_id))
        } else {
            self.tokens_in_map
                .name_to_token
                .get(name)
                .map(|token_id| Symbol::Token(*token_id))
        }
    }

    /// Add a rule to the grammar, and returns its index.
    fn grammar_add_rule(
        &mut self,
        lhs: NodeType,
        fields: &[Ast<ast::Field>],
        file: FileId,
        node_location: &SyntaxTreeLocation,
    ) -> Result<(RuleIdx<NodeType>, Vec<Ast<FieldItem>>), MappingError> {
        let mut rhs = Vec::new();
        let mut field_items = Vec::new();
        for field in fields {
            match &field.item {
                ast::Field::Anonymous { item, .. } => {
                    let (node, field_item) = self.expand_item(item, file)?;
                    field_items.push(field_item.0);
                    if let Some(node) = node {
                        rhs.push(Symbol::Node(node));
                    } else {
                        self.add_primary_to_rhs(&mut rhs, &item.primary, file)?;
                    }
                }
                ast::Field::Named { name: _, item, .. } => {
                    let (node, field_item) = self.expand_item(item, file)?;
                    field_items.push(field_item.0);
                    if let Some(node) = node {
                        rhs.push(Symbol::Node(node));
                    } else {
                        self.add_primary_to_rhs(&mut rhs, &item.primary, file)?;
                    }
                }
            }
        }
        Ok((self.add_rule(lhs, rhs, node_location)?, field_items))
    }

    /// Add a rule to the underlying [`grammar`](Self::grammar), checking for errors.
    fn add_rule(
        &mut self,
        lhs: NodeType,
        rhs: Vec<Symbol<TokenType, NodeType>>,
        location: &SyntaxTreeLocation,
    ) -> Result<RuleIdx<NodeType>, MappingError> {
        match self.grammar.add_rule(lhs, rhs) {
            Some(rule_idx) => Ok(rule_idx),
            None => Err(MappingError::VariantOverflow {
                node: location.clone(),
            }),
        }
    }

    /// Expand the given item, creating new nodes if needed, then returns the id
    /// of the created node.
    ///
    /// If the modifier of `item` is `None`, returns `Ok(None)`.
    ///
    /// Else, returns the identifier for the newly created rule.
    ///
    /// In any cases, returns the representation of the created item as a
    /// [`HashableFieldItem`].
    fn expand_item(
        &mut self,
        item: &Ast<ast::Item>,
        file: FileId,
    ) -> Result<(Option<NodeType>, HashableFieldItem), MappingError> {
        let compiled_rule = HashableFieldItem(FieldItem::compile_item(
            file,
            &*item,
            &|name| self.name_to_symbol(name),
            &|token_str| {
                self.tokens_in_map
                    .token_str_to_token
                    .get(token_str)
                    .copied()
            },
        )?);
        let modifier = match &item.modifier {
            Some(modifier) => modifier,
            None => return Ok((None, compiled_rule)),
        };

        let mut rhs = Vec::new();
        self.add_primary_to_rhs(&mut rhs, &item.primary, file)?;
        if rhs.is_empty() {
            return Err(MappingError::EmptyModifier(SyntaxTreeLocation {
                file,
                element: item.element.clone(),
            }));
        }
        let expression = {
            let mut result = if rhs.len() > 1 {
                String::from("(")
            } else {
                String::new()
            };
            let mut rhs_iter = rhs.iter().copied();
            if let Some(symbol) = rhs_iter.next() {
                result.push_str(&self.display_symbol(symbol))
            }
            for symbol in rhs_iter {
                result.push(' ');
                result.push_str(&self.display_symbol(symbol))
            }
            if rhs.len() > 1 {
                result.push(')')
            }
            result.push_str(match &**modifier {
                ast::Modifier::Repeat => "*",
                ast::Modifier::Trailing => "*_",
                ast::Modifier::Separate => "_*",
                ast::Modifier::Plus => "+",
                ast::Modifier::PlusTrailing => "+_",
                ast::Modifier::PlusSeparate => "_+",
                ast::Modifier::Optional => "?",
            });
            result
        };

        if let Some(node_id) = self.compiled_rules.get(&compiled_rule) {
            return Ok((Some(*node_id), compiled_rule));
        }

        // ID of the node we are about to create.
        let location = SyntaxTreeLocation {
            file,
            element: item.element.clone(),
        };
        let lhs = self.add_node(location.clone(), expression)?;
        match &**modifier {
            // { (xx)* } -> { } | { xx (xx)* }
            ast::Modifier::Repeat => {
                self.add_rule(lhs, Vec::new(), &location)?;
                rhs.push(Symbol::Node(lhs));
                self.add_rule(lhs, rhs, &location)?;
            }
            // { (xx y)*_ } -> { } | { xx } | { xx y (xx y)*_ }
            ast::Modifier::Trailing => {
                if rhs.len() < 2 {
                    return Err(MappingError::SingleItemModifier {
                        location: SyntaxTreeLocation {
                            file,
                            element: item.primary.element.clone(),
                        },
                        modifier: **modifier,
                    });
                }
                self.add_rule(lhs, Vec::new(), &location)?;
                rhs.push(Symbol::Node(lhs));
                self.add_rule(lhs, rhs.clone(), &location)?;
                rhs.pop();
                rhs.pop();
                self.add_rule(lhs, rhs, &location)?;
            }
            // { (xx y)_* } -> { } | { xx (y xx)* }
            // TODO: add a test ! (this is complicated :( )
            ast::Modifier::Separate => {
                if rhs.len() < 2 {
                    return Err(MappingError::SingleItemModifier {
                        location: SyntaxTreeLocation {
                            file,
                            element: item.primary.element.clone(),
                        },
                        modifier: **modifier,
                    });
                }
                let last_elem = rhs.pop().unwrap();
                let last_elem_representation = self.display_symbol(last_elem);
                let repeat_expression = {
                    let mut res = format!("({}", last_elem_representation);
                    for symbol in &rhs {
                        res.push(' ');
                        res.push_str(&self.display_symbol(*symbol))
                    }
                    res.push_str(")*");
                    res
                };

                let mut rhs_repeat = rhs.clone();
                let lhs_repeat = self.add_node(
                    SyntaxTreeLocation {
                        file,
                        element: item.element.clone(),
                    },
                    repeat_expression,
                )?;
                rhs_repeat.insert(0, last_elem);
                rhs_repeat.push(Symbol::Node(lhs_repeat));
                self.add_rule(lhs_repeat, rhs_repeat, &location)?;
                self.add_rule(lhs_repeat, Vec::new(), &location)?;

                rhs.push(Symbol::Node(lhs_repeat));
                self.add_rule(lhs, Vec::new(), &location)?;
                self.add_rule(lhs, rhs, &location)?;
            }
            // { (xx)+ } -> { xx } | { xx (xx)+ }
            ast::Modifier::Plus => {
                self.add_rule(lhs, rhs.clone(), &location)?;
                rhs.push(Symbol::Node(lhs));
                self.add_rule(lhs, rhs, &location)?;
            }
            // { (xx y)+_ } -> { xx } | { xx y } | { xx y (xx y)+_ }
            ast::Modifier::PlusTrailing => {
                if rhs.len() < 2 {
                    return Err(MappingError::SingleItemModifier {
                        location: SyntaxTreeLocation {
                            file,
                            element: item.primary.element.clone(),
                        },
                        modifier: **modifier,
                    });
                }
                // { xx y }
                self.add_rule(lhs, rhs.clone(), &location)?;
                rhs.push(Symbol::Node(lhs));
                // { xx y (xx y)+_ }
                self.add_rule(lhs, rhs.clone(), &location)?;
                rhs.pop();
                rhs.pop();
                // { xx }
                self.add_rule(lhs, rhs, &location)?;
            }
            // { (xx y)_+ } -> { xx } | { xx y (xx y)_+ }
            // TODO: add a test ! (this is complicated :( )
            ast::Modifier::PlusSeparate => {
                if rhs.len() < 2 {
                    return Err(MappingError::SingleItemModifier {
                        location: SyntaxTreeLocation {
                            file,
                            element: item.primary.element.clone(),
                        },
                        modifier: **modifier,
                    });
                }
                rhs.push(Symbol::Node(lhs));
                // { xx y (xx y)+_ }
                self.add_rule(lhs, rhs.clone(), &location)?;
                rhs.pop();
                rhs.pop();
                // { xx }
                self.add_rule(lhs, rhs, &location)?;
            }
            ast::Modifier::Optional => {
                self.add_rule(lhs, Vec::new(), &location)?;
                self.add_rule(lhs, rhs, &location)?;
            }
        };
        self.compiled_rules.insert(compiled_rule.clone(), lhs);
        Ok((Some(lhs), compiled_rule))
    }

    /// Get the text for the given symbol.
    ///
    /// If the symbol is a not-yet-computed node (or an unknown token), returns
    /// `"UNKNOWN"`.
    fn display_symbol(&self, symbol: Symbol<TokenType, NodeType>) -> String {
        match symbol {
            Symbol::Token(token) => self
                .tokens_in_map
                .tokens
                .get(token.as_index())
                .map(|token| match token.expression() {
                    crate::ExpressionKind::Raw(raw) => {
                        format!("'{}'", raw)
                    }
                    _ => token.name().to_string(),
                })
                .unwrap_or_else(|| String::from("UNKNOWN_TOKEN")),
            Symbol::Node(node) => {
                if let Some(name) = self.user_defined_node_names.get(node as usize) {
                    name.clone()
                } else {
                    let node = node as usize - self.user_defined_node_names.len();
                    self.nodes
                        .get(node)
                        .map(|node| node.expression.clone())
                        .unwrap_or_else(|| format!("UNKNOWN_NODE_{}", node))
                }
            }
        }
    }

    /// Try to add the symbol(s) in `primary` to `rhs`.
    ///
    /// The symbols are expanded as needed using [`Self::expand_item`].
    ///
    /// # Example
    /// - If `primary` is `'+'`, it will push `Token::Plus` in `rhs`.
    /// - If `primary` is `('+' N '-'?)`, it will push `Token::Plus, Node::N, Node::MinusOpt` in `rhs`.
    /// - If `primary` is `('+' | '-')`, it will push `Node::PlusOrMinus` in `rhs`.
    fn add_primary_to_rhs(
        &mut self,
        rhs: &mut Vec<Symbol<TokenType, NodeType>>,
        primary: &Ast<ast::Primary>,
        file: FileId,
    ) -> Result<(), MappingError> {
        match &primary.item {
            ast::Primary::Ident(id) => match self.name_to_symbol(id.item.as_str()) {
                Some(symbol) => rhs.push(symbol),
                None => {
                    return Err(MappingError::UnknownIdent(SyntaxTreeLocation {
                        file,
                        element: id.element.clone(),
                    }));
                }
            },
            ast::Primary::TokenStr(token_str) => {
                match self
                    .tokens_in_map
                    .token_str_to_token
                    .get(token_str.item.as_str())
                {
                    Some(token_id) => {
                        rhs.push(Symbol::Token(*token_id));
                    }
                    None => {
                        return Err(MappingError::UnknownTokenStr(SyntaxTreeLocation {
                            file,
                            element: token_str.element.clone(),
                        }))
                    }
                }
            }
            ast::Primary::Parent(new_items) => match new_items.as_slice() {
                [one] => {
                    for item in &one.0 {
                        // FIXME: is this normal to ignore the second return here ?
                        let (node, _) = self.expand_item(item, file)?;
                        if let Some(node) = node {
                            rhs.push(Symbol::Node(node));
                        } else {
                            self.add_primary_to_rhs(rhs, &item.primary, file)?;
                        }
                    }
                }
                multiple => {
                    let mut variants = Vec::new();
                    let mut expression = String::from("(");
                    for alternative in multiple {
                        let mut alternative_rhs = Vec::new();
                        for item in &alternative.0 {
                            // FIXME: is this normal to ignore the second return here ?
                            let (node, _) = self.expand_item(item, file)?;
                            if let Some(node) = node {
                                alternative_rhs.push(Symbol::Node(node));
                            } else {
                                self.add_primary_to_rhs(&mut alternative_rhs, &item.primary, file)?;
                            }
                        }
                        if !expression.is_empty() {
                            expression.push_str(" | ");
                        }
                        for symbol in &alternative_rhs {
                            expression.push_str(&self.display_symbol(*symbol));
                        }
                        variants.push((alternative_rhs, &alternative.element))
                    }
                    expression.push(')');
                    let new_id = self.add_node(
                        SyntaxTreeLocation {
                            file,
                            element: primary.element.clone(),
                        },
                        expression,
                    )?;
                    for (variant, element) in variants {
                        self.add_rule(
                            new_id,
                            variant,
                            &SyntaxTreeLocation {
                                file,
                                element: element.clone(),
                            },
                        )?;
                    }
                    rhs.push(Symbol::Node(new_id))
                }
            },
        }
        Ok(())
    }
}

impl<'items> Builder<'_, NodesProcessed<'items>> {
    /// Process the `conflict { ... }` blocks.
    pub(super) fn process_conflicts(self) -> Result<AllProcessed, MappingError> {
        let NodesProcessed {
            tokens_in_map,
            nodes,
            nodes_names,
            location_to_node,
            mut grammar,
            start_node_id,
            conflicts,
        } = self.step;

        let get_id_or_token_str =
            |file: FileId, id_or_token_str: &ast::IdOrTokenStr| match id_or_token_str {
                ast::IdOrTokenStr::Id(name) => tokens_in_map
                    .name_to_token
                    .get(name.as_str())
                    .copied()
                    .ok_or_else(|| {
                        MappingError::UnknownToken(SyntaxTreeLocation {
                            file,
                            element: name.element.clone(),
                        })
                    }),
                ast::IdOrTokenStr::TokenStr(expr) => tokens_in_map
                    .token_str_to_token
                    .get(expr.as_str())
                    .copied()
                    .ok_or_else(|| {
                        MappingError::UnknownTokenStr(SyntaxTreeLocation {
                            file,
                            element: expr.element.clone(),
                        })
                    }),
            };

        // Look through our nodes/tokens, raise an error if a name is not found.
        let resolve_conflict_action =
            |file: FileId,
             action: &ast::ConflictAction|
             -> Result<ConflictingAction<TokenType, NodeType>, MappingError> {
                match action {
                    ast::ConflictAction::ShiftAny => Ok(ConflictingAction::ShiftAny),
                    ast::ConflictAction::ShiftToken { token } => {
                        let token = get_id_or_token_str(file, &**token)?;
                        Ok(ConflictingAction::Shift(token))
                    }
                    ast::ConflictAction::Reduce(rule) => match &rule.item {
                        ast::Rule::Node(name) => {
                            let node_id = *nodes_names.get(name.as_str()).ok_or_else(|| {
                                MappingError::UnknownTokenStr(SyntaxTreeLocation {
                                    file,
                                    element: name.element.clone(),
                                })
                            })?;
                            let node = &nodes[node_id as usize];
                            match &node.origin {
                                NodeOrigin::UserDefined { content, .. } => match content {
                                    NodeContent::Single { rule_idx, .. } => {
                                        Ok(ConflictingAction::Reduce(*rule_idx))
                                    }
                                    NodeContent::Multiple { .. } => {
                                        Ok(ConflictingAction::ReduceNode(node_id))
                                    }
                                },
                                NodeOrigin::Internal { .. } => unreachable!(),
                            }
                        }
                        ast::Rule::Variant { base, variant } => {
                            let variant_element = variant.element.clone();
                            let node = *nodes_names.get(base.as_str()).ok_or_else(|| {
                                MappingError::UnknownTokenStr(SyntaxTreeLocation {
                                    file,
                                    element: base.element.clone(),
                                })
                            })?;
                            let node = &nodes[node as usize];
                            match &node.origin {
                                NodeOrigin::UserDefined { content, .. } => match content {
                                    NodeContent::Single { .. } => {
                                        Err(MappingError::UnknownNodeVariant {
                                            unknown: SyntaxTreeLocation {
                                                file,
                                                element: variant_element,
                                            },
                                            node: node.location.clone(),
                                        })
                                    }
                                    NodeContent::Multiple { variants } => {
                                        for v in variants {
                                            if v.name.item == variant.item {
                                                return Ok(ConflictingAction::Reduce(v.rule_idx));
                                            }
                                        }
                                        Err(MappingError::UnknownNodeVariant {
                                            unknown: SyntaxTreeLocation {
                                                file,
                                                element: variant_element,
                                            },
                                            node: node.location.clone(),
                                        })
                                    }
                                },
                                NodeOrigin::Internal { .. } => unreachable!(),
                            }
                        }
                    },
                }
            };

        for (file, conflict) in conflicts {
            let prefer = resolve_conflict_action(file, &conflict.prefer.item)?;
            let over = resolve_conflict_action(file, &conflict.over.item)?;
            let lookahead = if let Some((_, _, id_or_token_str)) = &conflict.lookahead {
                Some(get_id_or_token_str(file, id_or_token_str)?)
            } else {
                None
            };
            grammar.add_conflict_resolution(ConflictSolution {
                prefer,
                over,
                lookahead,
            });
        }
        Ok(AllProcessed {
            tokens_in_map,
            nodes,
            nodes_names,
            location_to_node,
            grammar,
            start_node_id,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use lr_parser::{syntax_tree::SyntaxElement, text_ref::TextRc};
    use std::{num::NonZeroU16, rc::Rc, sync::Arc};

    fn make_ast<T>(item: T) -> Ast<T> {
        Ast {
            element: Arc::new(SyntaxElement {
                text_ref: TextRc::new_full(Rc::from("")),
                kind: lr_parser::syntax_tree::NodeOrToken::ErrorToken(None),
            }),
            item,
        }
    }

    #[test]
    fn alternatives() {
        const PLUS_TOKEN: TokenType = TokenType(unsafe { NonZeroU16::new_unchecked(1) });
        const MINUS_TOKEN: TokenType = TokenType(unsafe { NonZeroU16::new_unchecked(2) });
        // ('+' | '-')
        let item = make_ast(ast::Item {
            primary: make_ast(ast::Primary::Parent(vec![
                make_ast(ast::Alternative(vec![make_ast(ast::Item {
                    primary: make_ast(ast::Primary::TokenStr(make_ast(String::from("+")))),
                    modifier: None,
                })])),
                make_ast(ast::Alternative(vec![make_ast(ast::Item {
                    primary: make_ast(ast::Primary::TokenStr(make_ast(String::from("-")))),
                    modifier: None,
                })])),
            ])),
            modifier: None,
        });
        let compiled =
            FieldItem::compile_item(0, &item, &|_| unreachable!(), &|token| match token {
                "+" => Some(PLUS_TOKEN),
                "-" => Some(MINUS_TOKEN),
                _ => unreachable!(),
            })
            .unwrap();
        assert!(compiled.modifier.is_none());
        match &*compiled.primary {
            Primary::Parent(alternatives) => {
                assert_eq!(alternatives.len(), 2);
                let alt0 = &alternatives[0];
                let alt1 = &alternatives[1];
                assert_eq!(alt0.0.len(), 1);
                assert_eq!(alt1.0.len(), 1);
                let alt0 = &alt0.0[0];
                let alt1 = &alt1.0[0];
                assert!(alt0.modifier.is_none());
                assert!(alt1.modifier.is_none());
                match &*alt0.primary {
                    Primary::Token(PLUS_TOKEN) => {}
                    _ => panic!("wrong primary: {:?}", compiled.primary),
                }
                match &*alt1.primary {
                    Primary::Token(MINUS_TOKEN) => {}
                    _ => panic!("wrong primary: {:?}", compiled.primary),
                }
            }
            _ => panic!("wrong primary: {:?}", compiled.primary),
        }
    }
}
