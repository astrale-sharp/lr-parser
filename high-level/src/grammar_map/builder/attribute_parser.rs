use crate::{
    parser::ast::{Ast, AttributeArgs, AttributeItem, Literal},
    FileId, MappingError, SyntaxTreeLocation,
};

/// Add a new documentation line to `doc`.
///
/// This adds a `\n` before `line` if `doc` contained something.
pub(super) fn add_documentation_line(doc: &mut Option<String>, line: &str) {
    let doc = match doc.as_mut() {
        Some(doc) => {
            doc.push('\n');
            doc
        }
        None => doc.get_or_insert(String::new()),
    };
    doc.push_str(line)
}

/// If `attr` is `AttributeItem::Id`, call `callback`.
///
/// Else, returns `false`.
pub(super) fn parse_id(
    attr: &Ast<AttributeItem>,
    mut callback: impl FnMut(&Ast<String>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    if let AttributeItem::Id(id) = &**attr {
        callback(id)
    } else {
        Ok(false)
    }
}

/// If `attr` is `AttributeItem::Literal(Literal::TokenStr)`, call `callback`.
///
/// Else, returns `false`.
pub(super) fn parse_token_str(
    attr: &Ast<AttributeItem>,
    mut callback: impl FnMut(&Ast<String>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    if let AttributeItem::Literal(lit) = &**attr {
        if let Literal::TokenStr(s) = &**lit {
            callback(s)
        } else {
            Ok(false)
        }
    } else {
        Ok(false)
    }
}

/// If `attr` is `AttributeItem::Literal(Literal::String)`, call `callback`.
///
/// Else, returns `false`.
pub(super) fn parse_string(
    attr: &Ast<AttributeItem>,
    mut callback: impl FnMut(&Ast<String>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    if let AttributeItem::Literal(lit) = &**attr {
        if let Literal::String(s) = &**lit {
            callback(s)
        } else {
            Ok(false)
        }
    } else {
        Ok(false)
    }
}

/// If `attr` is `AttributeItem::Literal(Literal::Int)`, call `callback`.
///
/// Else, returns `false`.
pub(super) fn parse_int(
    attr: &Ast<AttributeItem>,
    mut callback: impl FnMut(&Ast<u32>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    if let AttributeItem::Literal(lit) = &**attr {
        if let Literal::Int(i) = &**lit {
            callback(i)
        } else {
            Ok(false)
        }
    } else {
        Ok(false)
    }
}

/// If `attr` is `AttributeItem::Equal`, call `callback`.
///
/// Else, returns `false`.
pub(super) fn parse_equal(
    attr: &Ast<AttributeItem>,
    mut callback: impl FnMut(&Ast<String>, &Ast<Literal>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    if let AttributeItem::Equal(id, lit) = &**attr {
        callback(id, lit)
    } else {
        Ok(false)
    }
}

/// If `attr` is `AttributeItem::Parent`, call `callback`.
///
/// Else, returns `false`.
pub(super) fn parse_function(
    attr: &Ast<AttributeItem>,
    callback: impl FnOnce(&Ast<String>, &Ast<AttributeArgs>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    if let AttributeItem::Function { name, args } = &**attr {
        callback(name, args)
    } else {
        Ok(false)
    }
}

/// If `attrs` has a single element, call `callback` on it.
///
/// Else, returns `false`.
pub(super) fn parse_single(
    attrs: &[Ast<AttributeItem>],
    callback: impl FnOnce(&Ast<AttributeItem>) -> Result<bool, MappingError>,
) -> Result<bool, MappingError> {
    match attrs {
        [attr] => callback(attr),
        _ => Ok(false),
    }
}

/// If `attrs` has a single element, call `callback` on it.
///
/// Else, returns an error.
pub(super) fn parse_single_err(
    function_name: &'static str,
    file: FileId,
    attrs: &Ast<AttributeArgs>,
    mut callback: impl FnMut(&Ast<AttributeItem>) -> Result<bool, MappingError>,
) -> Result<(), MappingError> {
    let location = SyntaxTreeLocation {
        file,
        element: attrs.element.clone(),
    };
    match attrs.0.as_slice() {
        [attr] => {
            if !callback(attr)? {
                Err(MappingError::IncorrectAttrArguments {
                    function_name,
                    location,
                    additional_msg: "",
                })
            } else {
                Ok(())
            }
        }
        [] => Err(MappingError::AttrRequireArgument(function_name, location)),
        _ => Err(MappingError::IncorrectAttrArguments {
            function_name,
            location,
            additional_msg: "too many arguments",
        }),
    }
}

/// Call `callback` on all elements of `attrs`.
///
/// Emit an error if `callback` returns false.
pub(super) fn parse_repeat_err(
    function_name: &'static str,
    file: FileId,
    attrs: &[Ast<AttributeItem>],
    mut callback: impl FnMut(&Ast<AttributeItem>) -> Result<bool, MappingError>,
) -> Result<(), MappingError> {
    for attr in attrs {
        if !callback(attr)? {
            return Err(MappingError::IncorrectAttrArguments {
                function_name,
                location: SyntaxTreeLocation {
                    file,
                    element: attr.element.clone(),
                },
                additional_msg: "",
            });
        }
    }
    Ok(())
}
