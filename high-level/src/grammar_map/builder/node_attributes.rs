//! Parse attributes (`#[...]`) on nodes.

#![allow(clippy::blocks_in_if_conditions)]

use super::{
    attribute_parser::{self, parse_function, parse_id, parse_int, parse_single, parse_single_err},
    FileId, MappingError, SyntaxTreeLocation, Warning,
};
use crate::parser::ast::{Ast, Attribute, AttributeArgs, AttributeItem};
use std::ops::Deref;

/// Attributes attached to a node.
#[derive(Default)]
pub(super) struct NodeAttributes {
    /// Documentation of the node.
    ///
    /// `None` if no documentation was supplied.
    pub documentation: Option<String>,
    /// `#[start]`
    pub start: Option<SyntaxTreeLocation>,
    /// `#[silent]`
    pub silent: Option<SyntaxTreeLocation>,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u8)]
pub(super) enum PrecedenceSide {
    /// No associativity
    None,
    /// Left-associative
    Left,
    /// Right-associative
    Right,
}

/// Attributes attached to a node's variant
#[derive(Clone, Debug, Default)]
pub(super) struct VariantAttributes {
    /// Documentation of the node variant.
    ///
    /// `None` if no documentation was supplied.
    pub documentation: Option<String>,
}

/// Attributes attached to a field (like `'as'`, `tokens: Token*`, `rule: Rule`...)
#[derive(Clone, Debug, Default)]
pub(super) struct FieldAttributes {
    /// Documentation of the node variant.
    ///
    /// `None` if no documentation was supplied.
    pub documentation: Option<String>,
    /// `#[prefix(INT)]`
    pub prefix: Option<(SyntaxTreeLocation, u32)>,
    /// `#[infix(INT, side)]`
    ///
    /// `side` must be one of `left`, `right` or `none`.
    pub infix: Option<(SyntaxTreeLocation, u32, PrecedenceSide)>,
    /// `#[suffix(INT)]`
    pub suffix: Option<(SyntaxTreeLocation, u32)>,
}

impl NodeAttributes {
    /// Parse the `attributes` list.
    ///
    /// # Parameters
    /// - `node_location`: used to fill `start_node` if `attributes` contains
    /// `#[start]` + emit an error if it was already filled.
    /// - `start_node`: `Some` if a node was already marked with `#[start]`.
    pub fn parse(
        node_location: SyntaxTreeLocation,
        file: FileId,
        attributes: &[Ast<Attribute>],
        start_node: &mut Option<SyntaxTreeLocation>,
        warnings: &mut Vec<Warning>,
    ) -> Result<Self, MappingError> {
        let mut result = Self::default();
        for attr in attributes {
            match &**attr {
                Attribute::Doc(doc) => {
                    match &mut result.documentation {
                        Some(documentation) => {
                            if !documentation.is_empty() {
                                documentation.push('\n');
                            }
                            documentation.push_str(doc);
                        }
                        None => result.documentation = Some(doc.deref().clone()),
                    }
                    continue;
                }
                Attribute::Normal(attr) => {
                    let parse_single_attribute = |attr: &Ast<AttributeItem>| {
                        if parse_id(attr, |id| match id.as_str() {
                            "start" => match start_node {
                                Some(old_location) => Err(MappingError::MultipleStart {
                                    old_node: old_location.clone(),
                                    new_node: node_location.clone(),
                                }),
                                None => {
                                    *start_node = Some(node_location.clone());
                                    result.start = Some(SyntaxTreeLocation {
                                        file,
                                        element: attr.element.clone(),
                                    });
                                    Ok(true)
                                }
                            },
                            "silent" => {
                                result.silent = Some(SyntaxTreeLocation {
                                    file,
                                    element: attr.element.clone(),
                                });
                                Ok(true)
                            }
                            _ => Ok(false),
                        })? {
                            return Ok(true);
                        }
                        Ok(false)
                    };
                    if parse_single(&*attr.attribute_list, parse_single_attribute)? {
                        continue;
                    }

                    warnings.push(Warning::UnknownAttribute(SyntaxTreeLocation {
                        file,
                        element: attr.element.clone(),
                    }));
                }
            }
        }
        Ok(result)
    }
}

impl VariantAttributes {
    /// Parse the `attributes` list.
    pub fn parse(
        file: FileId,
        attributes: &[Ast<Attribute>],
        warnings: &mut Vec<Warning>,
    ) -> Result<Self, MappingError> {
        let mut result = Self::default();
        for attr in attributes {
            match &**attr {
                Attribute::Doc(doc) => {
                    attribute_parser::add_documentation_line(&mut result.documentation, doc);
                }
                Attribute::Normal(attr) => {
                    warnings.push(Warning::UnknownAttribute(SyntaxTreeLocation {
                        file,
                        element: attr.element.clone(),
                    }));
                }
            }
        }
        Ok(result)
    }
}

impl FieldAttributes {
    /// Parse the `attributes` list.
    pub fn parse(
        file: FileId,
        attributes: &[Ast<Attribute>],
        warnings: &mut Vec<Warning>,
    ) -> Result<Self, MappingError> {
        let mut result = Self::default();
        for attr in attributes {
            match &**attr {
                Attribute::Doc(doc) => {
                    attribute_parser::add_documentation_line(&mut result.documentation, doc);
                }
                Attribute::Normal(attr) => {
                    let attr_location = SyntaxTreeLocation {
                        file,
                        element: attr.element.clone(),
                    };
                    let parse_single_attribute = |item: &Ast<AttributeItem>| {
                        let function = |name: &Ast<String>, args: &Ast<AttributeArgs>| match name
                            .as_str()
                        {
                            "infix" => {
                                let args_location = SyntaxTreeLocation {
                                    file,
                                    element: args.element.clone(),
                                };
                                match args.0.as_slice() {
                                    [value, side] => {
                                        let mut stored_value = 0;
                                        let mut stored_side = String::new();
                                        if !parse_int(value, |value| {
                                            stored_value = value.item;
                                            Ok(true)
                                        })? {
                                            return Err(MappingError::IncorrectAttrArguments {
                                                function_name: "infix",
                                                location: SyntaxTreeLocation {
                                                    file,
                                                    element: value.element.clone(),
                                                },
                                                additional_msg: "expected a number",
                                            });
                                        }
                                        if !parse_id(side, |side| {
                                            stored_side = side.item.clone();
                                            Ok(true)
                                        })? {
                                            return Err(MappingError::IncorrectAttrArguments {
                                                function_name: "infix",
                                                location: SyntaxTreeLocation {
                                                    file,
                                                    element: side.element.clone(),
                                                },
                                                additional_msg: "expected an identifier",
                                            });
                                        }
                                        let side = match stored_side.as_str() {
                                            "left" => PrecedenceSide::Left,
                                            "right" => PrecedenceSide::Right,
                                            "none" => PrecedenceSide::None,
                                            _ => {
                                                return Err(MappingError::UnknownPrecedenceSide(
                                                    SyntaxTreeLocation {
                                                        file,
                                                        element: side.element.clone(),
                                                    },
                                                ))
                                            }
                                        };
                                        result.infix = Some((attr_location, stored_value, side));
                                        Ok(true)
                                    }
                                    _ => Err(MappingError::IncorrectAttrArguments {
                                        function_name: "infix",
                                        location: args_location,
                                        additional_msg: "expected two arguments",
                                    }),
                                }
                            }
                            "prefix" => {
                                parse_single_err("prefix", file, args, |attr| {
                                    parse_int(attr, |i| {
                                        result.prefix = Some((attr_location.clone(), i.item));
                                        Ok(true)
                                    })
                                })?;
                                Ok(true)
                            }
                            "suffix" => {
                                parse_single_err("suffix", file, args, |attr| {
                                    parse_int(attr, |i| {
                                        result.suffix = Some((attr_location.clone(), i.item));
                                        Ok(true)
                                    })
                                })?;
                                Ok(true)
                            }
                            _ => Ok(false),
                        };
                        if parse_function(item, function)? {
                            return Ok(true);
                        }

                        Ok(false)
                    };
                    if parse_single(&*attr.attribute_list, parse_single_attribute)? {
                        continue;
                    }

                    warnings.push(Warning::UnknownAttribute(SyntaxTreeLocation {
                        file,
                        element: attr.element.clone(),
                    }));
                }
            }
        }
        Ok(result)
    }
}
