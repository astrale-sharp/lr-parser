use super::{
    ast::{self, *},
    Node, Token,
};
use abstract_tree::ConversionError;
use lr_parser::{
    syntax_tree::abstract_tree,
    text_ref::{TextRc, TextReference as _},
};
use std::{convert::TryFrom, num::ParseIntError, sync::Arc};

type SyntaxElement = lr_parser::syntax_tree::SyntaxElement<Token, Node, TextRc>;

/// Error while building the AST.
#[derive(Clone, Debug)]
pub enum AstError {
    Conversion(ConversionError<Token, Node, TextRc>),
    InvalidRange(TextRc),
    ParseNumber(TextRc, ParseIntError),
    InvalidEscape(TextRc, char),
    InvalidByteEscape(TextRc, Option<char>),
}

impl abstract_tree::ContinuableError for AstError {
    fn continuable(&self) -> bool {
        match self {
            AstError::Conversion(conversion_error) => {
                abstract_tree::ContinuableError::continuable(conversion_error)
            }
            _ => false,
        }
    }
}

impl From<ConversionError<Token, Node, TextRc>> for AstError {
    fn from(err: ConversionError<Token, Node, TextRc>) -> Self {
        Self::Conversion(err)
    }
}

/// The source string contains the opening and closing quotes, as well as the
/// eventual `r` and `#` symbols for raw strings.
///
/// # Panics
///
/// The string must be valid (aka `"..."` with no `\` character right before the
/// closing `"`. It can be simple quote or raw strings though).
fn escape_string(text_ref: TextRc) -> Result<String, AstError> {
    let mut text: &str = text_ref.as_ref();
    let mut chars = text.chars();
    let mut span = text_ref.get_span();
    let mut offset = 1;
    if chars.next() == Some('r') {
        offset += 1;
        while chars.next() == Some('#') {
            offset += 1;
        }
    }
    if offset > 1 {
        // raw string, so no string escaping
        let end_offset = offset - 1;
        span.start += offset;
        span.end -= end_offset;
        return Ok(text[offset..text.len() - end_offset].to_string());
    }
    let end_offset = if offset == 1 { 1 } else { offset - 1 };
    span.start += offset;
    span.end -= end_offset;
    text = &text[offset..text.len() - end_offset];
    let mut chars = text.chars();
    let mut result = String::new();
    while let Some(c) = chars.next() {
        if c == '\\' {
            match chars.next().unwrap() {
                'n' => result.push('\n'),
                '\\' => result.push('\\'),
                'r' => result.push('\r'),
                't' => result.push('\t'),
                '\'' => result.push('\''),
                '"' => result.push('"'),
                'x' => {
                    let mut get_byte = || match chars.next() {
                        Some(b) => match b {
                            '0'..='9' => Ok(b as u32 as u8 - b'0'),
                            'a'..='z' => Ok(b as u32 as u8 - b'a' + 10),
                            'A'..='Z' => Ok(b as u32 as u8 - b'A' + 10),
                            _ => Err(AstError::InvalidByteEscape(
                                text_ref.new_subspan(span.clone()),
                                Some(b),
                            )),
                        },
                        None => Err(AstError::InvalidByteEscape(
                            text_ref.new_subspan(span.clone()),
                            None,
                        )),
                    };
                    let first_byte = get_byte()?;
                    let second_byte = get_byte()?;
                    result.push((first_byte << 4 | second_byte) as char)
                }
                c => return Err(AstError::InvalidEscape(text_ref.new_subspan(span), c)),
            }
        } else {
            result.push(c)
        }
    }
    Ok(result)
}

/// Parse an identifier from source.
///
/// This can be a raw identifier (e.g. `"r#tokens"`)
fn id_to_string(text_ref: TextRc) -> Result<String, AstError> {
    Ok(match text_ref.as_ref().strip_prefix("r#") {
        Some(stripped) => stripped,
        None => text_ref.as_ref(),
    }
    .to_string())
}

/// Parse an doc comment from source
fn doc_comment_to_string(text_ref: TextRc) -> Result<String, AstError> {
    Ok(text_ref.as_ref()[3..].to_string())
}

/// Parse a string from source (remove quotes)
fn token_str_to_string(text_ref: TextRc) -> Result<String, AstError> {
    escape_string(text_ref)
}

/// Parse a string from source (remove quotes)
fn string_to_string(text_ref: TextRc) -> Result<String, AstError> {
    escape_string(text_ref)
}

/// Parse an integer from source
fn parse_int(text_ref: TextRc) -> Result<u32, AstError> {
    match text_ref.as_ref().parse() {
        Ok(int) => Ok(int),
        Err(err) => Err(AstError::ParseNumber(text_ref, err)),
    }
}

fn consume_comma_list<Leaf>(
    children: &mut std::iter::Peekable<&mut dyn Iterator<Item = &Arc<SyntaxElement>>>,
) -> Result<Vec<Leaf>, AstError>
where
    Leaf: for<'a> TryFrom<&'a Arc<SyntaxElement>, Error = AstError>,
{
    let mut comma = true;
    SyntaxElement::consume_repeat(children, move |children| {
        if !comma {
            return Ok(None);
        }
        let token = SyntaxElement::consume_opt(children)?;
        if token.is_some() {
            comma = SyntaxElement::consume_token_opt(children, Token::Comma).is_some();
        }
        Ok(token)
    })
}

macro_rules! apply_into {
    ($children:expr, Token :: $token:ident) => {{
        let into_function = apply_into!(@get_into_function $token);
        let element = SyntaxElement::consume_token($children, Token::$token)?;
        Ok::<_, AstError>(Ast {
            element: element.clone(),
            item: into_function(element.text_ref.clone())?,
        })
    }};
    (@get_into_function DocComment) => {
        doc_comment_to_string
    };
    (@get_into_function String) => {
        string_to_string
    };
    (@get_into_function TokenStr) => {
        token_str_to_string
    };
    (@get_into_function Id) => {
        id_to_string
    };
    (@get_into_function Int) => {
        parse_int
    };
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<File> {
    type Error = AstError;

    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::File)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let top_level_items: Vec<Ast<TopLevelItem>> =
            SyntaxElement::consume_repeat(children, |children| {
                SyntaxElement::consume_opt(children)
            })?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: File { top_level_items },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<TopLevelItem> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::TopLevelItem)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                SyntaxElement::consume_token(children, Token::IncludeKw)?;
                let path = apply_into!(children, Token::String)?;
                SyntaxElement::consume_token(children, Token::SemiColon)?;
                TopLevelItem::Include { path }
            }
            1 => {
                let attributes: Vec<Ast<Attribute>> =
                    SyntaxElement::consume_repeat(children, |children| {
                        SyntaxElement::consume_opt(children)
                    })?;
                SyntaxElement::consume_token(children, Token::TokensKw)?;
                SyntaxElement::consume_token(children, Token::LBrace)?;
                let tokens: Vec<Ast<ast::Token>> = consume_comma_list(children)?;
                let verbatim: Option<(Ast<()>, Ast<String>)> = if let Some(verbatim_kw) =
                    SyntaxElement::consume_token_opt(children, Token::VerbatimKw)
                {
                    let verbatim = apply_into!(children, Token::String)?;
                    Some((
                        Ast {
                            element: verbatim_kw.clone(),
                            item: (),
                        },
                        verbatim,
                    ))
                } else {
                    None
                };
                SyntaxElement::consume_token(children, Token::RBrace)?;
                TopLevelItem::Tokens {
                    attributes,
                    tokens,
                    verbatim,
                }
            }
            2 => {
                let attributes: Vec<Ast<Attribute>> =
                    SyntaxElement::consume_repeat(children, |children| {
                        SyntaxElement::consume_opt(children)
                    })?;
                SyntaxElement::consume_token(children, Token::NodeKw)?;
                let name = apply_into!(children, Token::Id)?;
                SyntaxElement::consume_token(children, Token::LBrace)?;
                let block: Ast<Block> = SyntaxElement::consume(children)?;
                SyntaxElement::consume_token(children, Token::RBrace)?;
                TopLevelItem::Node {
                    attributes,
                    name,
                    block,
                }
            }
            3 => {
                let attributes: Vec<Ast<Attribute>> =
                    SyntaxElement::consume_repeat(children, |children| {
                        SyntaxElement::consume_opt(children)
                    })?;
                SyntaxElement::consume_token(children, Token::TestKw)?;
                let test_source = apply_into!(children, Token::String)?;
                let value: Ast<TestValue> = SyntaxElement::consume(children)?;
                TopLevelItem::Test {
                    attributes,
                    source: test_source,
                    value,
                }
            }
            4 => TopLevelItem::Conflict(SyntaxElement::consume(children)?),
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<ast::Token> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::Token)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let attributes: Vec<Ast<Attribute>> =
            SyntaxElement::consume_repeat(children, |children| {
                SyntaxElement::consume_opt(children)
            })?;
        let name = apply_into!(children, Token::Id)?;
        SyntaxElement::consume_token(children, Token::Colon)?;
        let definition: Ast<TokenDef> = SyntaxElement::consume(children)?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: ast::Token {
                attributes,
                name,
                definition,
            },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<TokenDef> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::TokenDef)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(children, Token::TokenStr)?;
                TokenDef::Raw(field_0)
            }
            1 => {
                SyntaxElement::consume_token(children, Token::RegexKw)?;
                SyntaxElement::consume_token(children, Token::LPar)?;
                let field_0 = apply_into!(children, Token::String)?;
                SyntaxElement::consume_token(children, Token::RPar)?;
                TokenDef::Regex(field_0)
            }
            2 => {
                SyntaxElement::consume_token(children, Token::ExternalKw)?;
                TokenDef::External
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Block> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Block)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0: Ast<Fields> = SyntaxElement::consume(children)?;
                Block::Single(field_0)
            }
            1 => Block::Multiple(SyntaxElement::consume_repeat(children, |children| {
                SyntaxElement::consume_opt(children)
            })?),
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Variant> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::Variant)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        SyntaxElement::consume_token(children, Token::Bar)?;
        let attributes: Vec<Ast<Attribute>> =
            SyntaxElement::consume_repeat(children, |children| {
                SyntaxElement::consume_opt(children)
            })?;
        let name = apply_into!(children, Token::Id)?;
        SyntaxElement::consume_token(children, Token::Arrow)?;
        let fields: Ast<Fields> = SyntaxElement::consume(children)?;
        SyntaxElement::assert_empty(children)?;

        Ok(Ast {
            element: element.clone(),
            item: Variant {
                attributes,
                name,
                fields,
            },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Fields> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::Fields)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let field_0: Vec<Ast<Field>> = SyntaxElement::consume_repeat(children, |children| {
            SyntaxElement::consume_opt(children)
        })?;

        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: Fields(field_0),
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Field> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Field)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let attributes: Vec<Ast<Attribute>> =
                    SyntaxElement::consume_repeat(children, |children| {
                        SyntaxElement::consume_opt(children)
                    })?;
                let item: Ast<Item> = SyntaxElement::consume(children)?;
                Field::Anonymous { attributes, item }
            }
            1 => {
                let attributes: Vec<Ast<Attribute>> =
                    SyntaxElement::consume_repeat(children, |children| {
                        SyntaxElement::consume_opt(children)
                    })?;
                let name: Ast<Label> = SyntaxElement::consume(children)?;
                SyntaxElement::consume_token(children, Token::Colon)?;
                let item: Ast<Item> = SyntaxElement::consume(children)?;
                Field::Named {
                    attributes,
                    name,
                    item,
                }
            }
            _ => unreachable!(),
        };

        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Label> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Label)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => Label::Id(apply_into!(children, Token::Id)?),
            1 => Label::Number(apply_into!(children, Token::Int)?),
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Item> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::Item)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let primary: Ast<Primary> = SyntaxElement::consume(children)?;
        let modifier: Option<Ast<Modifier>> = SyntaxElement::consume_opt(children)?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: Item { primary, modifier },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Primary> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Primary)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => Primary::Ident(apply_into!(children, Token::Id)?),
            1 => Primary::TokenStr(apply_into!(children, Token::TokenStr)?),
            2 => {
                SyntaxElement::consume_token(children, Token::LPar)?;
                let field_0: Vec<Ast<Alternative>> = {
                    let mut no_bar = false;
                    SyntaxElement::consume_repeat(children, move |children| {
                        if no_bar {
                            return Ok(None);
                        }
                        let alternative = SyntaxElement::consume_opt(children)?;
                        if SyntaxElement::consume_token_opt(children, Token::Bar).is_none() {
                            no_bar = true;
                        }
                        Ok(alternative)
                    })?
                };
                SyntaxElement::consume_token(children, Token::RPar)?;
                Primary::Parent(field_0)
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Alternative> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::Alternative)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let field_0: Vec<Ast<Item>> = SyntaxElement::consume_repeat(children, |children| {
            SyntaxElement::consume_opt(children)
        })?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: Alternative(field_0),
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Modifier> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Modifier)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                SyntaxElement::consume_token(children, Token::Star)?;
                Modifier::Repeat
            }
            1 => {
                SyntaxElement::consume_token(children, Token::Plus)?;
                Modifier::Plus
            }
            2 => {
                SyntaxElement::consume_token(children, Token::StarUnderscore)?;
                Modifier::Trailing
            }
            3 => {
                SyntaxElement::consume_token(children, Token::UnderscoreStar)?;
                Modifier::Separate
            }
            4 => {
                SyntaxElement::consume_token(children, Token::PlusUnderscore)?;
                Modifier::PlusTrailing
            }
            5 => {
                SyntaxElement::consume_token(children, Token::UnderscorePlus)?;
                Modifier::PlusSeparate
            }
            6 => {
                SyntaxElement::consume_token(children, Token::Question)?;
                Modifier::Optional
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Attribute> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Attribute)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => Attribute::Doc(apply_into!(children, Token::DocComment)?),
            1 => Attribute::Normal(SyntaxElement::consume(children)?),
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<NormalAttribute> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::NormalAttribute)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        SyntaxElement::consume_token(children, Token::Sharp)?;
        SyntaxElement::consume_token(children, Token::LBracket)?;
        let attribute_list: Vec<Ast<AttributeItem>> = consume_comma_list(children)?;
        if attribute_list.is_empty() {
            panic!(
                "repetition matched 0 items: {:?}",
                element.text_ref.as_ref()
            )
        }
        SyntaxElement::consume_token(children, Token::RBracket)?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: NormalAttribute { attribute_list },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<AttributeItem> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::AttributeItem)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(children, Token::Id)?;
                AttributeItem::Id(field_0)
            }
            1 => {
                let field_0: Ast<Literal> = SyntaxElement::consume(children)?;
                AttributeItem::Literal(field_0)
            }
            2 => {
                let name = apply_into!(children, Token::Id)?;
                SyntaxElement::consume_token(children, Token::LPar)?;
                let args: Ast<AttributeArgs> = SyntaxElement::consume(children)?;
                SyntaxElement::consume_token(children, Token::RPar)?;
                AttributeItem::Function { name, args }
            }
            3 => {
                let field_0 = apply_into!(children, Token::Id)?;
                SyntaxElement::consume_token(children, Token::Equal)?;
                let field_1: Ast<Literal> = SyntaxElement::consume(children)?;
                AttributeItem::Equal(field_0, field_1)
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<AttributeArgs> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::AttributeArgs)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let field_0 = consume_comma_list(children)?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: AttributeArgs(field_0),
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Literal> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Literal)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(children, Token::Int)?;
                Literal::Int(field_0)
            }
            1 => {
                let field_0 = apply_into!(children, Token::String)?;
                Literal::String(field_0)
            }
            2 => {
                let field_0 = apply_into!(children, Token::TokenStr)?;
                Literal::TokenStr(field_0)
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<TestValue> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::TestValue)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let rule_name: Ast<Rule> = SyntaxElement::consume(children)?;
        SyntaxElement::consume_token(children, Token::LBrace)?;
        let fields: Vec<Ast<TestField>> = SyntaxElement::consume_repeat(children, |children| {
            SyntaxElement::consume_opt(children)
        })?;
        SyntaxElement::consume_token(children, Token::RBrace)?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: TestValue { rule_name, fields },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<TestField> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::TestField)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let attributes: Vec<Ast<Attribute>> =
            SyntaxElement::consume_repeat(children, |children| {
                SyntaxElement::consume_opt(children)
            })?;
        let name: Ast<Label> = SyntaxElement::consume(children)?;
        SyntaxElement::consume_token(children, Token::Colon)?;
        let item: Ast<TestItem> = SyntaxElement::consume(children)?;
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item: TestField {
                attributes,
                name,
                item,
            },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<TestItem> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::TestItem)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(children, Token::String)?;
                TestItem::Token(field_0)
            }
            1 => {
                let field_0: Ast<TestValue> = SyntaxElement::consume(children)?;
                TestItem::Node(field_0)
            }
            2 => {
                SyntaxElement::consume_token(children, Token::LBracket)?;
                let field_0: Vec<Ast<TestItem>> =
                    SyntaxElement::consume_repeat(children, |children| {
                        SyntaxElement::consume_opt(children)
                    })?;
                SyntaxElement::consume_token(children, Token::RBracket)?;
                TestItem::Compound(field_0)
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Rule> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::Rule)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                let field_0 = apply_into!(children, Token::Id)?;
                Rule::Node(field_0)
            }
            1 => {
                let base = apply_into!(children, Token::Id)?;
                SyntaxElement::consume_token(children, Token::ColonColon)?;
                let variant = apply_into!(children, Token::Id)?;
                Rule::Variant { base, variant }
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<Conflict> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (_, mut children) = element.expect_node(Node::Conflict)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        SyntaxElement::consume_token(children, Token::ConflictKw)?;
        SyntaxElement::consume_token(children, Token::LBrace)?;
        SyntaxElement::consume_token(children, Token::PreferKw)?;
        SyntaxElement::consume_token(children, Token::Colon)?;
        let prefer: Ast<ConflictAction> = SyntaxElement::consume(children)?;
        SyntaxElement::consume_token(children, Token::OverKw)?;
        SyntaxElement::consume_token(children, Token::Colon)?;
        let over: Ast<ConflictAction> = SyntaxElement::consume(children)?;
        let lookahead = {
            if let Some(lookahead_kw) =
                SyntaxElement::consume_token_opt(children, Token::LookaheadKw)
            {
                let field_0 = Ast {
                    element: lookahead_kw.clone(),
                    item: (),
                };
                let field_1 = Ast {
                    element: SyntaxElement::consume_token(children, Token::Colon)?.clone(),
                    item: (),
                };
                let field_2: Ast<IdOrTokenStr> = SyntaxElement::consume(children)?;
                Some((field_0, field_1, field_2))
            } else {
                None
            }
        };
        SyntaxElement::consume_token(children, Token::RBrace)?;
        Ok(Ast {
            element: element.clone(),
            item: Conflict {
                prefer,
                over,
                lookahead,
            },
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<ConflictAction> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::ConflictAction)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => {
                SyntaxElement::consume_token(children, Token::ShiftKw)?;
                SyntaxElement::consume_token(children, Token::LPar)?;
                SyntaxElement::consume_token(children, Token::Underscore)?;
                SyntaxElement::consume_token(children, Token::RPar)?;
                ConflictAction::ShiftAny
            }
            1 => {
                SyntaxElement::consume_token(children, Token::ShiftKw)?;
                SyntaxElement::consume_token(children, Token::LPar)?;
                let token: Ast<IdOrTokenStr> = SyntaxElement::consume(children)?;
                SyntaxElement::consume_token(children, Token::RPar)?;
                ConflictAction::ShiftToken { token }
            }
            2 => {
                SyntaxElement::consume_token(children, Token::ReduceKw)?;
                SyntaxElement::consume_token(children, Token::LPar)?;
                let field_0: Ast<Rule> = SyntaxElement::consume(children)?;
                SyntaxElement::consume_token(children, Token::RPar)?;
                ConflictAction::Reduce(field_0)
            }
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}

impl TryFrom<&Arc<SyntaxElement>> for Ast<IdOrTokenStr> {
    type Error = AstError;
    fn try_from(element: &Arc<SyntaxElement>) -> Result<Self, Self::Error> {
        let (rule_idx, mut children) = element.expect_node(Node::IdOrTokenStr)?;
        let children =
            &mut (&mut children as &mut dyn Iterator<Item = &Arc<SyntaxElement>>).peekable();
        let item = match rule_idx.index {
            0 => IdOrTokenStr::Id(apply_into!(children, Token::Id)?),
            1 => IdOrTokenStr::TokenStr(apply_into!(children, Token::TokenStr)?),
            _ => unreachable!(),
        };
        SyntaxElement::assert_empty(children)?;
        Ok(Ast {
            element: element.clone(),
            item,
        })
    }
}
